-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.30-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for fuel
CREATE DATABASE IF NOT EXISTS `fuel` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `fuel`;

-- Dumping structure for table fuel.data_analisa
CREATE TABLE IF NOT EXISTS `data_analisa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `received_date` date DEFAULT NULL,
  `start_analisa` datetime DEFAULT NULL,
  `end_analisa` datetime DEFAULT NULL,
  `report_date` datetime DEFAULT NULL,
  `sampling_date` datetime DEFAULT NULL,
  `lab_number` varchar(200) DEFAULT NULL,
  `eval_code` varchar(45) DEFAULT NULL,
  `eval_code_oil_quality` varchar(45) DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `attention_id` int(11) DEFAULT NULL,
  `total_hours` float DEFAULT NULL,
  `appearance` varchar(45) DEFAULT NULL,
  `appearance_code` varchar(45) DEFAULT NULL,
  `density` varchar(45) DEFAULT NULL,
  `density_code` varchar(45) DEFAULT NULL,
  `total_acid_number` varchar(45) DEFAULT NULL,
  `total_acid_number_code` varchar(45) DEFAULT NULL,
  `flash_point` varchar(45) DEFAULT NULL,
  `flash_point_code` varchar(45) DEFAULT NULL,
  `kinematic_viscosity` varchar(45) DEFAULT NULL,
  `kinematic_viscosity_code` varchar(45) DEFAULT NULL,
  `water_content` varchar(45) DEFAULT NULL,
  `water_content_code` varchar(45) DEFAULT NULL,
  `ash_content` varchar(45) DEFAULT NULL,
  `ash_content_code` varchar(45) DEFAULT NULL,
  `pour_point` varchar(45) DEFAULT NULL,
  `pour_point_code` varchar(45) DEFAULT NULL,
  `cetane_index` varchar(45) DEFAULT NULL,
  `cetane_index_code` varchar(45) DEFAULT NULL,
  `conradson_carbon` varchar(45) DEFAULT NULL,
  `conradson_carbon_code` varchar(45) DEFAULT NULL,
  `distillation_recov` varchar(45) DEFAULT NULL,
  `distillation_ibp` varchar(45) DEFAULT NULL,
  `distillation_ibp_code` varchar(45) DEFAULT NULL,
  `distillation_5` varchar(45) DEFAULT NULL,
  `distillation_5_code` varchar(45) DEFAULT NULL,
  `distillation_10` varchar(45) DEFAULT NULL,
  `distillation_10_code` varchar(45) DEFAULT NULL,
  `distillation_20` varchar(45) DEFAULT NULL,
  `distillation_20_code` varchar(45) DEFAULT NULL,
  `distillation_30` varchar(45) DEFAULT NULL,
  `distillation_30_code` varchar(45) DEFAULT NULL,
  `distillation_40` varchar(45) DEFAULT NULL,
  `distillation_40_code` varchar(45) DEFAULT NULL,
  `distillation_50` varchar(45) DEFAULT NULL,
  `distillation_50_code` varchar(45) DEFAULT NULL,
  `distillation_60` varchar(45) DEFAULT NULL,
  `distillation_60_code` varchar(45) DEFAULT NULL,
  `distillation_70` varchar(45) DEFAULT NULL,
  `distillation_70_code` varchar(45) DEFAULT NULL,
  `distillation_80` varchar(45) DEFAULT NULL,
  `distillation_80_code` varchar(45) DEFAULT NULL,
  `distillation_90` varchar(45) DEFAULT NULL,
  `distillation_90_code` varchar(45) DEFAULT NULL,
  `distillation_95` varchar(45) DEFAULT NULL,
  `distillation_95_code` varchar(45) DEFAULT NULL,
  `distillation_ep` varchar(45) DEFAULT NULL,
  `distillation_ep_code` varchar(45) DEFAULT NULL,
  `distillation_recovery` varchar(45) DEFAULT NULL,
  `distillation_recovery_code` varchar(45) DEFAULT NULL,
  `distillation_residue` varchar(45) DEFAULT NULL,
  `destillation_residue_code` varchar(45) DEFAULT NULL,
  `destillation_loss` varchar(45) DEFAULT NULL,
  `destillation_loss_code` varchar(45) DEFAULT NULL,
  `destillation_recovery_300` varchar(45) DEFAULT NULL,
  `destillation_recovery_300_code` varchar(45) DEFAULT NULL,
  `sulphur_content` varchar(45) DEFAULT NULL,
  `sulphur_content_code` varchar(45) DEFAULT NULL,
  `sediment` varchar(45) DEFAULT NULL,
  `sediment_code` varchar(45) DEFAULT NULL,
  `colour` varchar(45) DEFAULT NULL,
  `colour_code` varchar(45) DEFAULT NULL,
  `copper_corrosion` varchar(45) DEFAULT NULL,
  `copper_corrosion_code` varchar(45) DEFAULT NULL,
  `sludge_content` varchar(45) DEFAULT NULL,
  `particles_4` varchar(45) DEFAULT NULL,
  `particles_4_code` varchar(45) DEFAULT NULL,
  `particles_6` varchar(45) DEFAULT NULL,
  `particles_6_code` varchar(45) DEFAULT NULL,
  `particles_14` varchar(45) DEFAULT NULL,
  `particles_14_code` varchar(45) DEFAULT NULL,
  `iso_4406` varchar(45) DEFAULT NULL,
  `iso_4406_code` varchar(45) DEFAULT NULL,
  `specific_gravity` varchar(45) DEFAULT NULL,
  `specific_gravity_code` varchar(45) DEFAULT NULL,
  `api_gravity` varchar(45) DEFAULT NULL,
  `api_gravity_code` varchar(45) DEFAULT NULL,
  `cloudn_point` varchar(45) DEFAULT NULL,
  `cloudn_point_code` varchar(45) DEFAULT NULL,
  `rancimat` varchar(45) DEFAULT NULL,
  `rancimat_point` varchar(45) DEFAULT NULL,
  `lubricity_of_diesel` varchar(45) DEFAULT NULL,
  `lubricity_of_diesel_code` varchar(45) DEFAULT NULL,
  `fame_content` varchar(45) DEFAULT NULL,
  `fame_content_code` varchar(45) DEFAULT NULL,
  `calorific_value` varchar(45) DEFAULT NULL,
  `calorific_value_code` varchar(45) DEFAULT NULL,
  `strong_acid` varchar(45) DEFAULT NULL,
  `strong_acid_code` varchar(45) DEFAULT NULL,
  `cerosine_content` varchar(45) DEFAULT NULL,
  `cerosine_content_code` varchar(45) DEFAULT NULL,
  `recommended1` text,
  `recommended2` text,
  `recommended3` text,
  `equipment_condition` varchar(45) DEFAULT NULL,
  `equipment_condition_code` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lab_number_UNIQUE` (`lab_number`),
  KEY `fk_data_analisa_customer1_idx` (`customer_id`),
  KEY `fk_data_analisa_unit1_idx` (`unit_id`),
  KEY `fk_data_analisa_attention1_idx` (`attention_id`),
  CONSTRAINT `fk_data_analisa_attention1` FOREIGN KEY (`attention_id`) REFERENCES `attention` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_data_analisa_customer1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_data_analisa_unit1` FOREIGN KEY (`unit_id`) REFERENCES `unit` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- Dumping data for table fuel.data_analisa: ~2 rows (approximately)
/*!40000 ALTER TABLE `data_analisa` DISABLE KEYS */;
REPLACE INTO `data_analisa` (`id`, `received_date`, `start_analisa`, `end_analisa`, `report_date`, `sampling_date`, `lab_number`, `eval_code`, `eval_code_oil_quality`, `customer_id`, `unit_id`, `attention_id`, `total_hours`, `appearance`, `appearance_code`, `density`, `density_code`, `total_acid_number`, `total_acid_number_code`, `flash_point`, `flash_point_code`, `kinematic_viscosity`, `kinematic_viscosity_code`, `water_content`, `water_content_code`, `ash_content`, `ash_content_code`, `pour_point`, `pour_point_code`, `cetane_index`, `cetane_index_code`, `conradson_carbon`, `conradson_carbon_code`, `distillation_recov`, `distillation_ibp`, `distillation_ibp_code`, `distillation_5`, `distillation_5_code`, `distillation_10`, `distillation_10_code`, `distillation_20`, `distillation_20_code`, `distillation_30`, `distillation_30_code`, `distillation_40`, `distillation_40_code`, `distillation_50`, `distillation_50_code`, `distillation_60`, `distillation_60_code`, `distillation_70`, `distillation_70_code`, `distillation_80`, `distillation_80_code`, `distillation_90`, `distillation_90_code`, `distillation_95`, `distillation_95_code`, `distillation_ep`, `distillation_ep_code`, `distillation_recovery`, `distillation_recovery_code`, `distillation_residue`, `destillation_residue_code`, `destillation_loss`, `destillation_loss_code`, `destillation_recovery_300`, `destillation_recovery_300_code`, `sulphur_content`, `sulphur_content_code`, `sediment`, `sediment_code`, `colour`, `colour_code`, `copper_corrosion`, `copper_corrosion_code`, `sludge_content`, `particles_4`, `particles_4_code`, `particles_6`, `particles_6_code`, `particles_14`, `particles_14_code`, `iso_4406`, `iso_4406_code`, `specific_gravity`, `specific_gravity_code`, `api_gravity`, `api_gravity_code`, `cloudn_point`, `cloudn_point_code`, `rancimat`, `rancimat_point`, `lubricity_of_diesel`, `lubricity_of_diesel_code`, `fame_content`, `fame_content_code`, `calorific_value`, `calorific_value_code`, `strong_acid`, `strong_acid_code`, `cerosine_content`, `cerosine_content_code`, `recommended1`, `recommended2`, `recommended3`, `equipment_condition`, `equipment_condition_code`) VALUES
	(16, '2017-11-02', NULL, NULL, '2017-11-24 11:49:37', '2017-11-02 11:49:37', '2809/T/17', 'B', NULL, 2, 1, 1, NULL, 'CLEAR', 'A', '847.8', NULL, '0,01', 'A', '74', 'A', '3.660', 'A', '0.05', 'A', '0.00207', 'A', '-1', 'A', '51.9', 'A', '0.025', 'A', '', '161', 'A', '189', 'A', '215', 'A', '245', 'A', '263', 'A', '276', 'A', '290', 'A', '305', 'A', '320', 'A', '340', 'A', '365', 'A', '379', 'A', '387', 'A', '98', 'A', '1', 'A', '1', 'A', '57', 'A', '0.15', 'A', '0.001', 'A', '1.5', 'A', '1A', 'A', '', '7924', 'A', '2209', 'A', '247', 'A', '20/18/15', 'B', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '< 5', 'A', '<1', 'A', '<1', 'A', '<1', 'A', 'Berdasarkan hasil analisa parameter yang din uji, nilai claenliness tidak sesuai nilai typical', NULL, NULL, NULL, NULL),
	(17, '2017-11-02', NULL, NULL, '2017-11-24 11:49:37', '2017-11-02 11:49:37', '2810/T/17', 'B', NULL, 2, 1, 1, NULL, '', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', NULL, NULL, '', '10770', 'A', '1464', 'A', '74', 'A', '21/18/13', 'B', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<5', 'A', '346', 'B', '8', 'A', '19', 'A', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `data_analisa` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
