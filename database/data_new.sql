-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.30-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for fuel
CREATE DATABASE IF NOT EXISTS `fuel` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `fuel`;

-- Dumping structure for table fuel.data_analisa
CREATE TABLE IF NOT EXISTS `data_analisa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `received_date` date DEFAULT NULL,
  `start_analisa` datetime DEFAULT NULL,
  `end_analisa` datetime DEFAULT NULL,
  `report_date` datetime DEFAULT NULL,
  `sampling_date` datetime DEFAULT NULL,
  `lab_number` varchar(200) DEFAULT NULL,
  `eval_code` varchar(45) DEFAULT NULL,
  `eval_code_oil_quality` varchar(45) DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `attention_id` int(11) DEFAULT NULL,
  `total_hours` float DEFAULT NULL,
  `appearance` varchar(45) DEFAULT NULL,
  `appearance_code` varchar(45) DEFAULT NULL,
  `density` varchar(45) DEFAULT NULL,
  `density_code` varchar(45) DEFAULT NULL,
  `total_acid_number` varchar(45) DEFAULT NULL,
  `total_acid_number_code` varchar(45) DEFAULT NULL,
  `flash_point` varchar(45) DEFAULT NULL,
  `flash_point_code` varchar(45) DEFAULT NULL,
  `kinematic_viscosity` varchar(45) DEFAULT NULL,
  `kinematic_viscosity_code` varchar(45) DEFAULT NULL,
  `water_content` varchar(45) DEFAULT NULL,
  `water_content_code` varchar(45) DEFAULT NULL,
  `ash_content` varchar(45) DEFAULT NULL,
  `ash_content_code` varchar(45) DEFAULT NULL,
  `pour_point` varchar(45) DEFAULT NULL,
  `pour_point_code` varchar(45) DEFAULT NULL,
  `cetane_index` varchar(45) DEFAULT NULL,
  `cetane_index_code` varchar(45) DEFAULT NULL,
  `conradson_carbon` varchar(45) DEFAULT NULL,
  `conradson_carbon_code` varchar(45) DEFAULT NULL,
  `distillation_recov` varchar(45) DEFAULT NULL,
  `distillation_ibp` varchar(45) DEFAULT NULL,
  `distillation_ibp_code` varchar(45) DEFAULT NULL,
  `distillation_5` varchar(45) DEFAULT NULL,
  `distillation_5_code` varchar(45) DEFAULT NULL,
  `distillation_10` varchar(45) DEFAULT NULL,
  `distillation_10_code` varchar(45) DEFAULT NULL,
  `distillation_20` varchar(45) DEFAULT NULL,
  `distillation_20_code` varchar(45) DEFAULT NULL,
  `distillation_30` varchar(45) DEFAULT NULL,
  `distillation_30_code` varchar(45) DEFAULT NULL,
  `distillation_40` varchar(45) DEFAULT NULL,
  `distillation_40_code` varchar(45) DEFAULT NULL,
  `distillation_50` varchar(45) DEFAULT NULL,
  `distillation_50_code` varchar(45) DEFAULT NULL,
  `distillation_60` varchar(45) DEFAULT NULL,
  `distillation_60_code` varchar(45) DEFAULT NULL,
  `distillation_70` varchar(45) DEFAULT NULL,
  `distillation_70_code` varchar(45) DEFAULT NULL,
  `distillation_80` varchar(45) DEFAULT NULL,
  `distillation_80_code` varchar(45) DEFAULT NULL,
  `distillation_90` varchar(45) DEFAULT NULL,
  `distillation_90_code` varchar(45) DEFAULT NULL,
  `distillation_95` varchar(45) DEFAULT NULL,
  `distillation_95_code` varchar(45) DEFAULT NULL,
  `distillation_ep` varchar(45) DEFAULT NULL,
  `distillation_ep_code` varchar(45) DEFAULT NULL,
  `distillation_recovery` varchar(45) DEFAULT NULL,
  `distillation_recovery_code` varchar(45) DEFAULT NULL,
  `distillation_residue` varchar(45) DEFAULT NULL,
  `destillation_residue_code` varchar(45) DEFAULT NULL,
  `destillation_loss` varchar(45) DEFAULT NULL,
  `destillation_loss_code` varchar(45) DEFAULT NULL,
  `destillation_recovery_300` varchar(45) DEFAULT NULL,
  `destillation_recovery_300_code` varchar(45) DEFAULT NULL,
  `sulphur_content` varchar(45) DEFAULT NULL,
  `sulphur_content_code` varchar(45) DEFAULT NULL,
  `sediment` varchar(45) DEFAULT NULL,
  `sediment_code` varchar(45) DEFAULT NULL,
  `colour` varchar(45) DEFAULT NULL,
  `colour_code` varchar(45) DEFAULT NULL,
  `copper_corrosion` varchar(45) DEFAULT NULL,
  `copper_corrosion_code` varchar(45) DEFAULT NULL,
  `sludge_content` varchar(45) DEFAULT NULL,
  `particles_4` varchar(45) DEFAULT NULL,
  `particles_4_code` varchar(45) DEFAULT NULL,
  `particles_6` varchar(45) DEFAULT NULL,
  `particles_6_code` varchar(45) DEFAULT NULL,
  `particles_14` varchar(45) DEFAULT NULL,
  `particles_14_code` varchar(45) DEFAULT NULL,
  `iso_4406` varchar(45) DEFAULT NULL,
  `iso_4406_code` varchar(45) DEFAULT NULL,
  `specific_gravity` varchar(45) DEFAULT NULL,
  `specific_gravity_code` varchar(45) DEFAULT NULL,
  `api_gravity` varchar(45) DEFAULT NULL,
  `api_gravity_code` varchar(45) DEFAULT NULL,
  `cloudn_point` varchar(45) DEFAULT NULL,
  `cloudn_point_code` varchar(45) DEFAULT NULL,
  `rancimat` varchar(45) DEFAULT NULL,
  `rancimat_point` varchar(45) DEFAULT NULL,
  `lubricity_of_diesel` varchar(45) DEFAULT NULL,
  `lubricity_of_diesel_code` varchar(45) DEFAULT NULL,
  `fame_content` varchar(45) DEFAULT NULL,
  `fame_content_code` varchar(45) DEFAULT NULL,
  `calorific_value` varchar(45) DEFAULT NULL,
  `calorific_value_code` varchar(45) DEFAULT NULL,
  `strong_acid` varchar(45) DEFAULT NULL,
  `strong_acid_code` varchar(45) DEFAULT NULL,
  `cerosine_content` varchar(45) DEFAULT NULL,
  `cerosine_content_code` varchar(45) DEFAULT NULL,
  `recommended1` text,
  `recommended2` text,
  `recommended3` text,
  `equipment_condition` varchar(45) DEFAULT NULL,
  `equipment_condition_code` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lab_number_UNIQUE` (`lab_number`),
  KEY `fk_data_analisa_customer1_idx` (`customer_id`),
  KEY `fk_data_analisa_unit1_idx` (`unit_id`),
  KEY `fk_data_analisa_attention1_idx` (`attention_id`),
  CONSTRAINT `fk_data_analisa_attention1` FOREIGN KEY (`attention_id`) REFERENCES `attention` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_data_analisa_customer1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_data_analisa_unit1` FOREIGN KEY (`unit_id`) REFERENCES `unit` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- Dumping data for table fuel.data_analisa: ~2 rows (approximately)
/*!40000 ALTER TABLE `data_analisa` DISABLE KEYS */;
REPLACE INTO `data_analisa` (`id`, `received_date`, `start_analisa`, `end_analisa`, `report_date`, `sampling_date`, `lab_number`, `eval_code`, `eval_code_oil_quality`, `customer_id`, `unit_id`, `attention_id`, `total_hours`, `appearance`, `appearance_code`, `density`, `density_code`, `total_acid_number`, `total_acid_number_code`, `flash_point`, `flash_point_code`, `kinematic_viscosity`, `kinematic_viscosity_code`, `water_content`, `water_content_code`, `ash_content`, `ash_content_code`, `pour_point`, `pour_point_code`, `cetane_index`, `cetane_index_code`, `conradson_carbon`, `conradson_carbon_code`, `distillation_recov`, `distillation_ibp`, `distillation_ibp_code`, `distillation_5`, `distillation_5_code`, `distillation_10`, `distillation_10_code`, `distillation_20`, `distillation_20_code`, `distillation_30`, `distillation_30_code`, `distillation_40`, `distillation_40_code`, `distillation_50`, `distillation_50_code`, `distillation_60`, `distillation_60_code`, `distillation_70`, `distillation_70_code`, `distillation_80`, `distillation_80_code`, `distillation_90`, `distillation_90_code`, `distillation_95`, `distillation_95_code`, `distillation_ep`, `distillation_ep_code`, `distillation_recovery`, `distillation_recovery_code`, `distillation_residue`, `destillation_residue_code`, `destillation_loss`, `destillation_loss_code`, `destillation_recovery_300`, `destillation_recovery_300_code`, `sulphur_content`, `sulphur_content_code`, `sediment`, `sediment_code`, `colour`, `colour_code`, `copper_corrosion`, `copper_corrosion_code`, `sludge_content`, `particles_4`, `particles_4_code`, `particles_6`, `particles_6_code`, `particles_14`, `particles_14_code`, `iso_4406`, `iso_4406_code`, `specific_gravity`, `specific_gravity_code`, `api_gravity`, `api_gravity_code`, `cloudn_point`, `cloudn_point_code`, `rancimat`, `rancimat_point`, `lubricity_of_diesel`, `lubricity_of_diesel_code`, `fame_content`, `fame_content_code`, `calorific_value`, `calorific_value_code`, `strong_acid`, `strong_acid_code`, `cerosine_content`, `cerosine_content_code`, `recommended1`, `recommended2`, `recommended3`, `equipment_condition`, `equipment_condition_code`) VALUES
	(16, '2017-11-02', NULL, NULL, '2017-11-24 11:49:37', '2017-11-02 11:49:37', '2809/T/17', 'B', NULL, 2, 1, 1, NULL, 'CLEAR', 'A', '847.8', NULL, '0,01', 'A', '74', 'A', '3.660', 'A', '0.05', 'A', '0.00207', 'A', '-1', 'A', '51.9', 'A', '0.025', 'A', '', '161', 'A', '189', 'A', '215', 'A', '245', 'A', '263', 'A', '276', 'A', '290', 'A', '305', 'A', '320', 'A', '340', 'A', '365', 'A', '379', 'A', '387', 'A', '98', 'A', '1', 'A', '1', 'A', '57', 'A', '0.15', 'A', '0.001', 'A', '1.5', 'A', '1A', 'A', '', '7924', 'A', '2209', 'A', '247', 'A', '20/18/15', 'B', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '< 5', 'A', '<1', 'A', '<1', 'A', '<1', 'A', 'Berdasarkan hasil analisa parameter yang din uji, nilai claenliness tidak sesuai nilai typical', NULL, NULL, NULL, NULL),
	(17, '2017-11-02', NULL, NULL, '2017-11-24 11:49:37', '2017-11-02 11:49:37', '2810/T/17', 'B', NULL, 2, 1, 1, NULL, '', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', NULL, NULL, '', '10770', 'A', '1464', 'A', '74', 'A', '21/18/13', 'B', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<5', 'A', '346', 'B', '8', 'A', '19', 'A', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `data_analisa` ENABLE KEYS */;

-- Dumping structure for table fuel.parameter
CREATE TABLE IF NOT EXISTS `parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) DEFAULT NULL,
  `satuan` varchar(45) DEFAULT NULL COMMENT 'unit atau satuan',
  `method` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `column_analisis_name` varchar(100) DEFAULT NULL,
  `note` text,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

-- Dumping data for table fuel.parameter: ~46 rows (approximately)
/*!40000 ALTER TABLE `parameter` DISABLE KEYS */;
REPLACE INTO `parameter` (`id`, `nama`, `satuan`, `method`, `code`, `column_analisis_name`, `note`, `order`) VALUES
	(1, 'Appearance', '-', 'Visual', NULL, 'appearance', NULL, NULL),
	(2, 'Density at 15 °C', NULL, 'ASTM D-1298', NULL, 'density', NULL, NULL),
	(3, 'Total Acid Number (TAN)', 'mg KOH/g', 'ASTM D 974-12', NULL, 'total_acid_number', NULL, NULL),
	(4, 'Flash Point PMCC', '°C', 'ASTM D 93-13e1', NULL, 'flash_point', NULL, NULL),
	(5, 'Viscosity Kin @ 40 °C', 'cSt', 'ASTM D 445-12', NULL, 'kinematic_viscosity', NULL, NULL),
	(6, 'Water Content Distillation', '% v/v', 'ASTM D 95-13e1', NULL, 'water_content', NULL, NULL),
	(7, 'Ash Content', '% wt', 'ASTM D 482-13', NULL, 'ash_content', NULL, NULL),
	(8, 'Pour Point', '°C', 'ASTM D 97-12', NULL, 'pour_point', NULL, NULL),
	(9, 'Cetane Index', '', 'ASTM D 976-11', NULL, 'cetane_index', NULL, NULL),
	(10, 'Conradson Carbon Residue On 10% Distillate Residue', '% wt', 'ASTM D 189-10', NULL, 'conradson_carbon', NULL, NULL),
	(11, 'Distillation Recovery Basis :', '°C', 'ASTM 86-12', NULL, 'distillation_recov', NULL, NULL),
	(12, 'IBP', NULL, NULL, NULL, 'distillation_ibp', NULL, NULL),
	(13, '5% vol.', NULL, NULL, NULL, 'distillation_5', NULL, NULL),
	(14, '10% vol', NULL, NULL, NULL, 'distillation_10', NULL, NULL),
	(15, '20% vol', NULL, NULL, NULL, 'distillation_20', NULL, NULL),
	(16, '30% vol', NULL, NULL, NULL, 'distillation_30', NULL, NULL),
	(17, '40% vol', NULL, NULL, NULL, 'distillation_40', NULL, NULL),
	(18, '50% vol', '', '', NULL, 'distillation_50', NULL, NULL),
	(19, '60% vol', '-', '', NULL, 'distillation_60', NULL, NULL),
	(20, '70% vol', NULL, NULL, NULL, 'distillation_70', NULL, NULL),
	(21, '80% vol', NULL, NULL, NULL, 'distillation_80', NULL, NULL),
	(22, '90% vol', NULL, '', NULL, 'distillation_90', NULL, NULL),
	(23, '95% vol', '', NULL, NULL, 'distillation_95', NULL, NULL),
	(24, 'EP', '', NULL, NULL, 'distillation_bp', NULL, NULL),
	(25, 'Recovery % vol', '', NULL, NULL, 'distillation_recovery', NULL, NULL),
	(26, 'Residue % vol', '', '', NULL, 'distillation_residue', NULL, NULL),
	(27, 'Loss % vol', '', '', NULL, 'distillation_loss', NULL, NULL),
	(28, 'Recovery at 300 C', '% vol', '', NULL, 'distillation_recovery_300', NULL, NULL),
	(29, 'Sulphur Content', '% wt', 'ASTM D 5185-12', NULL, 'sulphur_content', NULL, NULL),
	(30, 'Sediment', '% wt', 'ASTM D 473-12', NULL, 'sediment', NULL, NULL),
	(31, 'Colour ASTM', 'No. ASTM', 'ASTM D 1500-12', NULL, 'colour', NULL, NULL),
	(32, 'Copper Strip Corrosion at 50 C/3Hrs', 'Class', 'ASTM 130-12', NULL, 'copper_corrosion', NULL, NULL),
	(33, 'Solid Particles (Particle Size)', '', '', NULL, 'sludge_content', NULL, NULL),
	(34, ' > 4 µm', 'Counts/ml', '', NULL, 'particles_4', NULL, NULL),
	(35, '> 6 µm', 'Counts/ml', '', NULL, 'particles_6', NULL, NULL),
	(36, '> 14 µm', 'Counts/ml', '', NULL, 'particles_14', NULL, NULL),
	(37, 'ISO CODE 4406', NULL, '', NULL, 'iso_4406', NULL, NULL),
	(38, 'Specific Grafity', NULL, '', NULL, 'specific_gravity', NULL, NULL),
	(39, 'Api Grafity', '', '', NULL, 'api_gravity', NULL, NULL),
	(40, 'Cloudn Point', '', '', NULL, 'cloudn_point', NULL, NULL),
	(41, 'RANCIMAT', '', '', NULL, 'rancimat', NULL, NULL),
	(42, 'Lubricity Diesel Fuel ( HFRR TEST )', '', '', NULL, 'lubricity_of_diesel', NULL, NULL),
	(43, 'Fame Content', '', '', NULL, 'fame_content', NULL, NULL),
	(44, 'Calorific Value', '', '', NULL, 'calorific_value', NULL, NULL),
	(45, 'Strong Acid Number', NULL, NULL, NULL, 'strong_acid', NULL, NULL),
	(46, 'Cerosine Content', '', '', NULL, 'cerosine_content', NULL, NULL);
/*!40000 ALTER TABLE `parameter` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
