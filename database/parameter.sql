-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.30-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for fuel
DROP DATABASE IF EXISTS `fuel`;
CREATE DATABASE IF NOT EXISTS `fuel` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `fuel`;

-- Dumping structure for table fuel.parameter
DROP TABLE IF EXISTS `parameter`;
CREATE TABLE IF NOT EXISTS `parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) DEFAULT NULL,
  `satuan` varchar(45) DEFAULT NULL COMMENT 'unit atau satuan',
  `method` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `column_analisis_name` varchar(100) DEFAULT NULL,
  `note` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

-- Dumping data for table fuel.parameter: ~46 rows (approximately)
/*!40000 ALTER TABLE `parameter` DISABLE KEYS */;
REPLACE INTO `parameter` (`id`, `nama`, `satuan`, `method`, `code`, `column_analisis_name`, `note`) VALUES
	(1, 'Appearance', '-', 'Visual', NULL, 'appearance', NULL),
	(2, 'Density at 15 °C', NULL, 'ASTM D-1298', NULL, 'density', NULL),
	(3, 'Total Acid Number (TAN)', 'mg KOH/g', 'ASTM D 974-12', NULL, 'total_acid_number', NULL),
	(4, 'Flash Point PMCC', '°C', 'ASTM D 93-13e1', NULL, 'flash_point', NULL),
	(5, 'Viscosity Kin @ 40 °C', 'cSt', 'ASTM D 445-12', NULL, 'kinematic_viscosity', NULL),
	(6, 'Water Content Distillation', '% v/v', 'ASTM D 95-13e1', NULL, 'water_content', NULL),
	(7, 'Ash Content', '% wt', 'ASTM D 482-13', NULL, 'ash_content', NULL),
	(8, 'Pour Point', '°C', 'ASTM D 97-12', NULL, 'pour_point', NULL),
	(9, 'Cetane Index', '', 'ASTM D 976-11', NULL, 'cetane_index', NULL),
	(10, 'Conradson Carbon Residue On 10% Distillate Residue', '% wt', 'ASTM D 189-10', NULL, 'conradson_carbon', NULL),
	(11, 'Distillation Recovery Basis :', '°C', 'ASTM 86-12', NULL, 'distillation_recov', NULL),
	(12, 'IBP', NULL, NULL, NULL, 'distillation_ibp', NULL),
	(13, '5% vol.', NULL, NULL, NULL, 'distillation_5', NULL),
	(14, '10% vol', NULL, NULL, NULL, 'distillation_10', NULL),
	(15, '20% vol', NULL, NULL, NULL, 'distillation_20', NULL),
	(16, '30% vol', NULL, NULL, NULL, 'distillation_30', NULL),
	(17, '40% vol', NULL, NULL, NULL, 'distillation_40', NULL),
	(18, '50% vol', '', '', NULL, 'distillation_50', NULL),
	(19, '60% vol', '-', '', NULL, 'distillation_60', NULL),
	(20, '70% vol', NULL, NULL, NULL, 'distillation_70', NULL),
	(21, '80% vol', NULL, NULL, NULL, 'distillation_80', NULL),
	(22, '90% vol', NULL, '', NULL, 'distillation_90', NULL),
	(23, '95% vol', '', NULL, NULL, 'distillation_95', NULL),
	(24, 'EP', '', NULL, NULL, 'distillation_bp', NULL),
	(25, 'Recovery % vol', '', NULL, NULL, 'distillation_recovery', NULL),
	(26, 'Residue % vol', '', '', NULL, 'distillation_residue', NULL),
	(27, 'Loss % vol', '', '', NULL, 'distillation_loss', NULL),
	(28, 'Recovery at 300 C', '% vol', '', NULL, 'distillation_recovery_300', NULL),
	(29, 'Sulphur Content', '% wt', 'ASTM D 5185-12', NULL, 'sulphur_content', NULL),
	(30, 'Sediment', '% wt', 'ASTM D 473-12', NULL, 'sediment', NULL),
	(31, 'Colour ASTM', 'No. ASTM', 'ASTM D 1500-12', NULL, 'colour', NULL),
	(32, 'Copper Strip Corrosion at 50 C/3Hrs', 'Class', 'ASTM 130-12', NULL, 'copper_corrosion', NULL),
	(33, 'Solid Particles (Particle Size)', '', '', NULL, 'sludge_content', NULL),
	(34, ' > 4 µm', 'Counts/ml', '', NULL, 'particles_4', NULL),
	(35, '> 6 µm', 'Counts/ml', '', NULL, 'particles_6', NULL),
	(36, '> 14 µm', 'Counts/ml', '', NULL, 'particles_14', NULL),
	(37, 'ISO CODE 4406', NULL, '', NULL, 'iso_4406', NULL),
	(38, 'Specific Grafity', NULL, '', NULL, 'specific_gravity', NULL),
	(39, 'Api Grafity', '', '', NULL, 'api_gravity', NULL),
	(40, 'Cloudn Point', '', '', NULL, 'cloudn_point', NULL),
	(41, 'RANCIMAT', '', '', NULL, 'rancimat', NULL),
	(42, 'Lubricity Diesel Fuel ( HFRR TEST )', '', '', NULL, 'lubricity_of_diesel', NULL),
	(43, 'Fame Content', '', '', NULL, 'fame_content', NULL),
	(44, 'Calorific Value', '', '', NULL, 'calorific_value', NULL),
	(45, 'Strong Acid Number', NULL, NULL, NULL, 'strong_acid', NULL),
	(46, 'Cerosine Content', '', '', NULL, 'cerosine_content', NULL);
/*!40000 ALTER TABLE `parameter` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
