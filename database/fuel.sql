-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.30-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for fuel
CREATE DATABASE IF NOT EXISTS `fuel` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `fuel`;

-- Dumping structure for table fuel.access_rule
CREATE TABLE IF NOT EXISTS `access_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  `role_description` varchar(250) DEFAULT NULL,
  `rule_settings` text,
  `time_create` datetime NOT NULL,
  `time_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table fuel.attention
CREATE TABLE IF NOT EXISTS `attention` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attention_customer1_idx` (`customer_id`),
  CONSTRAINT `fk_attention_customer1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table fuel.category
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` text,
  `recom` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table fuel.customer
CREATE TABLE IF NOT EXISTS `customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `address` text,
  `location` text,
  `type` varchar(4) DEFAULT NULL COMMENT 'jenis perusahaan contoh PT, CV',
  `time_create` datetime DEFAULT NULL,
  `time_update` datetime DEFAULT NULL,
  `users_index` int(11) DEFAULT NULL,
  PRIMARY KEY (`customer_id`),
  KEY `fk_customer_users1_idx` (`users_index`),
  CONSTRAINT `fk_customer_users1` FOREIGN KEY (`users_index`) REFERENCES `users` (`index`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table fuel.data_analisa
CREATE TABLE IF NOT EXISTS `data_analisa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `received_date` date DEFAULT NULL,
  `start_analisa` datetime DEFAULT NULL,
  `end_analisa` datetime DEFAULT NULL,
  `report_date` date DEFAULT NULL,
  `sampling_date` date DEFAULT NULL,
  `lab_number` varchar(200) DEFAULT NULL,
  `eval_code` varchar(45) DEFAULT NULL,
  `eval_code_oil_quality` varchar(45) DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `attention_id` int(11) DEFAULT NULL,
  `total_hours` float DEFAULT NULL,
  `appearance` varchar(45) DEFAULT NULL,
  `appearance_code` varchar(45) DEFAULT NULL,
  `density` varchar(45) DEFAULT NULL,
  `density_code` varchar(45) DEFAULT NULL,
  `total_acid_number` varchar(45) DEFAULT NULL,
  `total_acid_number_code` varchar(45) DEFAULT NULL,
  `flash_point` varchar(45) DEFAULT NULL,
  `flash_point_code` varchar(45) DEFAULT NULL,
  `kinematic_viscosity` varchar(45) DEFAULT NULL,
  `kinematic_viscosity_code` varchar(45) DEFAULT NULL,
  `water_content` varchar(45) DEFAULT NULL,
  `water_content_code` varchar(45) DEFAULT NULL,
  `ash_content` varchar(45) DEFAULT NULL,
  `ash_content_code` varchar(45) DEFAULT NULL,
  `pour_point` varchar(45) DEFAULT NULL,
  `pour_point_code` varchar(45) DEFAULT NULL,
  `cetane_index` varchar(45) DEFAULT NULL,
  `cetane_index_code` varchar(45) DEFAULT NULL,
  `conradson_carbon` varchar(45) DEFAULT NULL,
  `conradson_carbon_code` varchar(45) DEFAULT NULL,
  `distillation_recov` varchar(45) DEFAULT NULL,
  `distillation_ibp` varchar(45) DEFAULT NULL,
  `distillation_ibp_code` varchar(45) DEFAULT NULL,
  `distillation_5` varchar(45) DEFAULT NULL,
  `distillation_5_code` varchar(45) DEFAULT NULL,
  `distillation_10` varchar(45) DEFAULT NULL,
  `distillation_10_code` varchar(45) DEFAULT NULL,
  `distillation_20` varchar(45) DEFAULT NULL,
  `distillation_20_code` varchar(45) DEFAULT NULL,
  `distillation_30` varchar(45) DEFAULT NULL,
  `distillation_30_code` varchar(45) DEFAULT NULL,
  `distillation_40` varchar(45) DEFAULT NULL,
  `distillation_40_code` varchar(45) DEFAULT NULL,
  `distillation_50` varchar(45) DEFAULT NULL,
  `distillation_50_code` varchar(45) DEFAULT NULL,
  `distillation_60` varchar(45) DEFAULT NULL,
  `distillation_60_code` varchar(45) DEFAULT NULL,
  `distillation_70` varchar(45) DEFAULT NULL,
  `distillation_70_code` varchar(45) DEFAULT NULL,
  `distillation_80` varchar(45) DEFAULT NULL,
  `distillation_80_code` varchar(45) DEFAULT NULL,
  `distillation_90` varchar(45) DEFAULT NULL,
  `distillation_90_code` varchar(45) DEFAULT NULL,
  `distillation_95` varchar(45) DEFAULT NULL,
  `distillation_95_code` varchar(45) DEFAULT NULL,
  `distillation_ep` varchar(45) DEFAULT NULL,
  `distillation_ep_code` varchar(45) DEFAULT NULL,
  `distillation_recovery` varchar(45) DEFAULT NULL,
  `distillation_recovery_code` varchar(45) DEFAULT NULL,
  `distillation_residue` varchar(45) DEFAULT NULL,
  `distillation_residue_code` varchar(45) DEFAULT NULL,
  `destillation_loss` varchar(45) DEFAULT NULL,
  `destillation_loss_code` varchar(45) DEFAULT NULL,
  `destillation_recovery_300` varchar(45) DEFAULT NULL,
  `destillation_recovery_300_code` varchar(45) DEFAULT NULL,
  `sulphur_content` varchar(45) DEFAULT NULL,
  `sulphur_content_code` varchar(45) DEFAULT NULL,
  `sediment` varchar(45) DEFAULT NULL,
  `sediment_code` varchar(45) DEFAULT NULL,
  `colour` varchar(45) DEFAULT NULL,
  `colour_code` varchar(45) DEFAULT NULL,
  `copper_corrosion` varchar(45) DEFAULT NULL,
  `copper_corrosion_code` varchar(45) DEFAULT NULL,
  `sludge_content` varchar(45) DEFAULT NULL,
  `particles_4` varchar(45) DEFAULT NULL,
  `particles_4_code` varchar(45) DEFAULT NULL,
  `particles_6` varchar(45) DEFAULT NULL,
  `particles_6_code` varchar(45) DEFAULT NULL,
  `particles_14` varchar(45) DEFAULT NULL,
  `particles_14_code` varchar(45) DEFAULT NULL,
  `iso_4406` varchar(45) DEFAULT NULL,
  `iso_4406_code` varchar(45) DEFAULT NULL,
  `specific_gravity` varchar(45) DEFAULT NULL,
  `specific_gravity_code` varchar(45) DEFAULT NULL,
  `api_gravity` varchar(45) DEFAULT NULL,
  `api_gravity_code` varchar(45) DEFAULT NULL,
  `cloudn_point` varchar(45) DEFAULT NULL,
  `cloudn_point_code` varchar(45) DEFAULT NULL,
  `rancimat` varchar(45) DEFAULT NULL,
  `rancimat_code` varchar(45) DEFAULT NULL,
  `lubricity_of_diesel` varchar(45) DEFAULT NULL,
  `lubricity_of_diesel_code` varchar(45) DEFAULT NULL,
  `fame_content` varchar(45) DEFAULT NULL,
  `fame_content_code` varchar(45) DEFAULT NULL,
  `calorific_value` varchar(45) DEFAULT NULL,
  `calorific_value_code` varchar(45) DEFAULT NULL,
  `strong_acid` varchar(45) DEFAULT NULL,
  `strong_acid_code` varchar(45) DEFAULT NULL,
  `cerosine_content` varchar(45) DEFAULT NULL,
  `cerosine_content_code` varchar(45) DEFAULT NULL,
  `recommended1` text,
  `recommended2` text,
  `recommended3` text,
  `equipment_condition` varchar(45) DEFAULT NULL,
  `equipment_condition_code` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lab_number_UNIQUE` (`lab_number`),
  KEY `fk_data_analisa_customer1_idx` (`customer_id`),
  KEY `fk_data_analisa_unit1_idx` (`unit_id`),
  KEY `fk_data_analisa_attention1_idx` (`attention_id`),
  CONSTRAINT `fk_data_analisa_attention1` FOREIGN KEY (`attention_id`) REFERENCES `attention` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_data_analisa_customer1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_data_analisa_unit1` FOREIGN KEY (`unit_id`) REFERENCES `unit` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table fuel.matrix
CREATE TABLE IF NOT EXISTS `matrix` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parameter_id` int(11) NOT NULL,
  `condition1` varchar(45) DEFAULT NULL,
  `condition2` varchar(45) DEFAULT NULL,
  `condition3` varchar(45) DEFAULT NULL,
  `condition4` varchar(45) DEFAULT NULL,
  `condition5` varchar(45) DEFAULT NULL,
  `recom1` text,
  `rekom2` text,
  `rekom3` text,
  `rekom4` text,
  PRIMARY KEY (`id`),
  KEY `fk_matrix_parameter1_idx` (`parameter_id`),
  CONSTRAINT `fk_matrix_parameter1` FOREIGN KEY (`parameter_id`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table fuel.parameter
CREATE TABLE IF NOT EXISTS `parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) DEFAULT NULL,
  `satuan` varchar(45) DEFAULT NULL COMMENT 'unit atau satuan',
  `method` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `column_analisis_name` varchar(100) DEFAULT NULL,
  `note` text,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table fuel.parameter_has_data_analisa
CREATE TABLE IF NOT EXISTS `parameter_has_data_analisa` (
  `parameter_id` int(11) NOT NULL,
  `data_analisa_id` int(11) NOT NULL,
  `value` varchar(45) DEFAULT NULL,
  `eval` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`parameter_id`,`data_analisa_id`),
  KEY `fk_parameter_has_data_analisa_data_analisa1_idx` (`data_analisa_id`),
  KEY `fk_parameter_has_data_analisa_parameter1_idx` (`parameter_id`),
  CONSTRAINT `fk_parameter_has_data_analisa_data_analisa1` FOREIGN KEY (`data_analisa_id`) REFERENCES `data_analisa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parameter_has_data_analisa_parameter1` FOREIGN KEY (`parameter_id`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table fuel.penentuan_category
CREATE TABLE IF NOT EXISTS `penentuan_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voltage` varchar(45) DEFAULT NULL,
  `type_of_equipment` varchar(200) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_penentuan_category_category1_idx` (`category_id`),
  CONSTRAINT `fk_penentuan_category_category1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table fuel.report_type
CREATE TABLE IF NOT EXISTS `report_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `tarmplate` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table fuel.report_type_has_parameter
CREATE TABLE IF NOT EXISTS `report_type_has_parameter` (
  `report_type_id` int(11) NOT NULL,
  `parameter_id` int(11) NOT NULL,
  `p_code` varchar(100) DEFAULT 'a',
  `p_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`report_type_id`,`parameter_id`),
  KEY `fk_report_type_has_parameter_parameter1_idx` (`parameter_id`),
  KEY `fk_report_type_has_parameter_report_type1_idx` (`report_type_id`),
  CONSTRAINT `fk_report_type_has_parameter_parameter1` FOREIGN KEY (`parameter_id`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_report_type_has_parameter_report_type1` FOREIGN KEY (`report_type_id`) REFERENCES `report_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table fuel.unit
CREATE TABLE IF NOT EXISTS `unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `location` varchar(200) DEFAULT NULL,
  `serial_number` varchar(45) DEFAULT NULL,
  `model_unit` varchar(45) DEFAULT NULL,
  `type_unit` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_unit_customer1_idx` (`customer_id`),
  CONSTRAINT `fk_unit_customer1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table fuel.users
CREATE TABLE IF NOT EXISTS `users` (
  `index` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password_hash` varchar(200) DEFAULT NULL,
  `auth_key` varchar(200) NOT NULL,
  `access_token` varchar(200) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `password_reset_token` varchar(200) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `time_create` datetime NOT NULL,
  `access_rule_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`index`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `fk_users_access_rule1_idx` (`access_rule_id`),
  CONSTRAINT `fk_users_access_rule1` FOREIGN KEY (`access_rule_id`) REFERENCES `access_rule` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table fuel.user_log
CREATE TABLE IF NOT EXISTS `user_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_login` datetime NOT NULL,
  `date_logout` datetime DEFAULT NULL,
  `ip_login` varchar(45) DEFAULT NULL,
  `log` text,
  `users_index` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_log_users_idx` (`users_index`),
  CONSTRAINT `fk_user_log_users` FOREIGN KEY (`users_index`) REFERENCES `users` (`index`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
