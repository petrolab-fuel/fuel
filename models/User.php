<?php

namespace app\models;

use app\modelsDB\Users;
use Yii;
use yii\web\IdentityInterface;

/**
 * Class User
 * @package app\models
 *
 * @property mixed $authKey
 * @property null|string $passwordResetToken
 * @property int $ruleAccess
 * @property string $fullName
 * @property string $ruleName
 * @property string $password
 * @property \app\modelsDB\Customer|null $customer
 * @property null $customerId
 * @property mixed $id
 */
class User extends Users implements IdentityInterface
{

    /**
     * @return array
     */
    public static function admin()
    {
        return [
            'index' => '0',
            'username' => 'admin',
            // 'password_hash' => '31e0b6d892f396b9a23ff3c03481a0af',
            'password_hash' => '21232f297a57a5a743894a0e4a801fc3',
            'auth_key' => '',
            'access_token' => '',
            'status' => '',
            'password_reset_token' => '',
            'email' => '',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        if ($id === 'admin')
            return new static(self::admin());
        return self::findOne(['username' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        if ($username == 'admin')
            return new static(self::admin());
        return static::findOne(['username' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->username;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password_hash === md5($password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = md5($password);
    }

    /**
     * @return int
     */
    public function getRuleAccess()
    {
        if ($this->username === 'admin')
            return 1;
        return $this->access_rule_id;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        if ($this->username === 'admin')
            return 'Administrator';
        return $this->username;
    }

    /**
     * @return string
     */
    public function getRuleName()
    {
        if ($this->username === 'admin')
            return 'Administrator';
        return $this->accessRule !== null ? $this->accessRule->role_name : 'Petrolab';
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'access_token' => $token,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = 24 * 60 * 60;
        return $timestamp + $expire >= time();
    }

    /**
     * Generates new password reset token
     *
     * @throws \yii\base\Exception
     */
    public function generatePasswordResetToken()
    {
        $this->access_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generates aunt key
     *
     * @throws \yii\base\Exception
     */
    public function generateAuntKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->access_token = null;
    }

    /**
     * @return null|string
     */
    public function getPasswordResetToken()
    {
        return $this->access_token;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->time_create = date('Y-m-d H:i:s');
            }

            return true;
        }
        return false;
    }

    /**
     * @return \app\modelsDB\Customer|null
     */
    public function getCustomer()
    {
        return sizeof($this->customers) > 0 ? $this->customers[0] : null;
    }

    public function getCustomerId()
    {
        return $this->customer == null ? null : $this->customer->customer_id;
    }
}
