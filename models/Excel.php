<?php

/**
 * Created by PhpStorm.
 * User: Wisard17
 * Date: 9/8/2017
 * Time: 10:57 AM
 */

namespace app\models;

use PHPExcel;
use PHPExcel_IOFactory;
use yii\base\Model;

class Excel extends Model
{
    /**
     * @param array $array
     * @param string $startCell
     * @param string $titleSheet
     * @return PHPExcel
     */
    public function genExcelByArray($array, $startCell = 'A1', $titleSheet = 'sheet', $setHeader = false)
    {

        if (PHP_SAPI == 'cli')
            die('This example should only be run from a Web Browser');

        $objPHPExcel = new PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Petrolab Services")
            ->setLastModifiedBy("Petrolab Services")
            ->setTitle("Petrolab Services ")
            ->setSubject("Petrolab Services")
            ->setDescription("Petrolab Services.")
            ->setKeywords("Petrolab Services")
            ->setCategory("Petrolab Services ");

        if ($setHeader) {
            //            preg_match('!\d+!', $startCell, $nm);
            //            preg_match("/[a-zA-Z]+/", $startCell, $st);
            //
            //            $nm = isset($nm[0]) ? intval($nm[0]) : 1;
            //            $st = isset($st[0]) ? $st[0] : 'A';
            $header = [];
            if (isset($array[0]))
                foreach ($array[0] as $idx => $value) {
                    $header[0][] = $idx;
                }

            //            $objPHPExcel->setActiveSheetIndex(0)->fromArray($header, null, $startCell);
            $array = array_merge($header, $array);
            //            $startCell = $st . ($nm + 1);

        }

        $objPHPExcel->setActiveSheetIndex(0)->fromArray($array, null, $startCell);

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle($titleSheet);

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        return $objPHPExcel;
    }

    /**
     * @param string $fileName
     * @return PHPExcel
     */
    public function renderData($fileName)
    {
        return PHPExcel_IOFactory::load($fileName);
    }

    /**
     * void function to download data
     *
     * @param PHPExcel $objPHPExcel
     * @param string $fileName
     * @param bool $office2007
     */
    public function download($objPHPExcel, $fileName, $office2007 = false)
    {
        $v = 'Excel5';
        $ext = '.xls';
        $h1 = 'Content-Type: application/vnd.ms-excel';
        $h2 = 'Expires: Mon, 26 Jul 1997 05:00:00 GMT';

        if ($office2007) {
            $v = 'Excel2007';
            $ext = '.xlsx';
            $h1 = 'Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
            $h2 = 'Expires: Mon, 26 Jul 1997 05:00:00 GMT';
        }

        header($h1);
        header('Content-Disposition: attachment;filename="' . $fileName . $ext . '"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header($h2); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $v);
        $objWriter->save('php://output');
        exit;
    }
    public function newExcel()
    {
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("Petrolab Services")
            ->setLastModifiedBy("Petrolab Services")
            ->setTitle("Petrolab Services ")
            ->setSubject("Petrolab Services")
            ->setDescription("Petrolab Services.")
            ->setKeywords("Petrolab Services")
            ->setCategory("Petrolab Services ");
        return $objPHPExcel;
    }
}
