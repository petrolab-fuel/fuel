<?php
/**
 * Created by
 * User: Wisard17
 * Date: 07/02/2018
 * Time: 11.54 AM
 */

namespace app\models;

use Yii;

/**
 * Class DataAnalisa
 * @package app\models
 *
 * @property string $serialNumber
 * @property string $reportDate
 * @property string $sampleDate
 * @property string $receiveDate
 * @property string $location
 * @property string $attentionName
 * @property string $modelUnit
 * @property string $typeUnit
 * @property-read string $rowId
 * @property string $customerName
 */
class DataAnalisa extends \app\modelsDB\DataAnalisa
{
    public $code;
    public $_customer;

    public function getCustomer()
    {
        if ($this->_customer == null)
            $this->_customer = parent::getCustomer();
        return $this->_customer;
    }

    public $_unit;

    public function getUnit()
    {
        if ($this->_unit == null)
            $this->_unit = parent::getUnit();
        return $this->_unit;
    }

    /**
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customer == null ? '' : $this->customer->name;
    }

    /**
     * @return string
     */
    public function getAttentionName()
    {
        return $this->attention == null ? '' : $this->attention->name;
    }

    /**
 * @return string
 */

    public function getModelUnit()
    {
        return $this->unit == null ? '' : $this->unit->model_unit;
    }

    /**
     * @return string
     */

    public function getTypeUnit()
    {
        return $this->unit == null?'':$this->unit->type_unit;
    }
    /**
     * @return string
     */
    public function getSerialNumber()
    {
        return $this->unit == null ? '' : $this->unit->serial_number;
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->unit == null ? '' : $this->unit->location;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getSampleDate()
    {
        return Yii::$app->formatter->asDate($this->sampling_date);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getReceiveDate()
    {
        return Yii::$app->formatter->asDate($this->received_date);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getReportDate()
    {
        return Yii::$app->formatter->asDate($this->report_date);
    }

    public static function allStatus()
    {
        return [
            'all' => [
                'status' => [
                    'A' => 'Normal',
                    'B' => 'Attention',
                    'C' => 'Urgent',
                    'D' => 'Severe',
                ],
                'label' => [
                    'A' => 'success',
                    'B' => 'warning',
                    'C' => 'danger',
                    'D' => 'danger',
                ],
            ],
            'fuel' => [
                'status' => [
                    'A' => 'Normal',
                    'B' => 'Attention',
                    'C' => 'Urgent',
                    'D' => 'Severe',
                ],
                'label' => [
                    'A' => 'success',
                    'B' => 'warning',
                    'C' => 'danger',
                    'D' => 'danger',
                ],
            ],
            'furan' => [
                'status' => [
                    'A' => 'Normal',
                    'B' => 'Accelerated',
                    'C' => 'Excessive',
                    'D' => 'High Risk',
                    'E' => 'End of Expected',
                ],
                'label' => [
                    'A' => 'success',
                    'B' => 'warning',
                    'C' => 'danger',
                    'D' => 'danger',
                    'E' => 'danger',
                ],
            ],
            'oil_quality' => [
                'status' => [
                    'A' => 'Good',
                    'B' => 'Fair',
                    'C' => 'Poor',
                ],
                'label' => [
                    'A' => 'success',
                    'B' => 'warning',
                    'C' => 'danger',
                ],
            ],
            'global_color' => [
//                'A' => '#00B500',
                'B' => '#FFED00',
                'C' => '#DF6240',
//                'D' => '#ff2828',
                'E' => 'black',
            ],
        ];

    }

    public static function decodeStatus()
    {
        return [
            'all' => [
                    'Normal' => 'A',
                    'Attention' => 'B',
                    'Urgent' => 'C',
                    'Severe' => 'D',
            ],
            'dga' => [
                    'Condition 1' => 'A',
                    'Condition 2' => 'B',
                    'Condition 3' => 'C',
                    'Condition 4' => 'D',
            ],
            'furan' => [
                    'Normal' => 'A',
                    'Accelerated' => 'B',
                    'Excessive' => 'C',
                    'High Risk' => 'D',
                    'End of Expected' => 'E',
            ],
            'oil_quality' => [
                    'Good' => 'A',
                    'Fair' => 'B',
                    'Poor' => 'C',
            ],
        ];

    }

    /**
     * @return string
     */
    public function getRowId()
    {
        return str_replace([' '], '', $this->lab_number);
    }

}