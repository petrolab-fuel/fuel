<?php

/**
 * Created by
 * User: Wisard17
 * Date: 10/13/2017
 * Time: 8:55 AM
 */

namespace app\models;

use app\smartadmin\SideBar;
use Yii;
use yii\base\Model;
use yii\helpers\Url;

/**
 * Class Menu
 * @package app\models
 *
 * @property array $listMenu
 */
class Menu extends Model
{

    public static function listMenu()
    {
        $self = new self();
        return $self->listMenu;
    }

    /**
     * @return array
     */
    public static function getListMenu()
    {
        return [
            'options' => ['class' => 'sidebar-menu'],
            'items' => [

                ['label' => 'Login', 'icon' => 'fa fa-user', 'url' => ['/site/login'], 'visible' => Yii::$app->user->isGuest],
                ['label' => 'Dashboard', 'icon' => 'fa fa-dashboard', 'url' => ['/site/index'], 'visible' => !Yii::$app->user->isGuest],
                [
                    'label' => 'Entry Data', 'icon' => 'fa fa-flask', 'url' => ['/master/default/new'],
                    'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->access_rule_id <= 1,
                ],
                [
                    'label' => 'Package', 'icon' => 'fa fa-book', 'url' => ['/master/paket/index'],
                    'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->access_rule_id <= 1,
                ],
                [
                    'label' => 'Data Analisa', 'icon' => 'fa fa-flask', 'url' => ['/master/default/index'],
                    'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->access_rule_id <= 1,
                ],
                [
                    'label' => 'Spesifikasi', 'icon' => 'fa fa-flask', 'url' => ['/master/spesifikasi'],
                    'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->access_rule_id <= 1,
                ],
                [
                    'label' => 'Parameter', 'icon' => 'fa fa-flask', 'url' => ['/master/parameters'],
                    'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->access_rule_id <= 1,
                ],
                [
                    'label' => 'Matrix', 'icon' => 'fa fa-flask', 'url' => ['/master/matrix'],
                    'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->access_rule_id <= 1,
                ],

                [
                    'label' => 'Import Data Analisa', 'icon' => 'fa fa-upload', 'url' => ['/master/default/upload-data'],
                    'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->access_rule_id <= 1,
                ],
               


                //                ['label' => 'Monitoring', 'icon' => 'fa fa-desktop', 'visible' => !Yii::$app->user->isGuest,
                //                    'items' => [
                //                        ['label' => 'Transformer', 'url' => ['/monitoring/transformer'],],
                //                        ['label' => 'Transformer Health', 'url' => ['/monitoring/transformer-health'],]
                //                    ],
                //                ],

                //                ['label' => 'Master', 'icon' => 'fa fa-cube', 'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->access_rule_id < 4,
                //                    'items' => [
                //                        ['label' => 'Customer', 'url' => ['/master/customers/default'], ],
                //                        ['label' => 'Unit', 'url' => ['/master/units/default'], ],
                //                    ],
                //                ],
                //
                //                ['label' => 'Report', 'icon' => 'fa fa-book', 'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->access_rule_id < 4,
                //                    'items' => [
                //                        ['label' => 'List Fuel', 'url' => ['/reports/fuel'], ],
                //                        ['label' => 'Entry Analisa Fuel', 'url' => ['/reports/add'], ],
                //                        ['label' => 'Import Data Analisa Fuel', 'url' => ['/monitoring/default'], ],
                //
                //                    ],
                //                ],
                [
                    'label' => 'Users', 'icon' => 'fa fa-users', 'url' => ['/users'],
                    'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->access_rule_id <= 1,
                    //                    'items' => [
                    //                        ['label' => 'List Report', 'url' => ['#'], ],
                    //                        ['label' => 'Entry Data', 'url' => ['#'], ],
                    //                    ],
                ],
            ],
        ];
    }
}
