<?php
/**
 * Created by
 * User     : Wisard17
 * Date     : 15/08/2019
 * Time     : 12.02 PM
 * File Name: DataTable.php
 **/

namespace app\component;


use app\smartadmin\assets\plugins\DataTableAsset;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/**
 * Class DataTable
 * @package app\modules\pcb\models
 */
class DataTable extends Widget
{
    /**
     * @var array the HTML attributes for the container tag of the list view.
     * The "tag" element specifies the tag name of the container element and defaults to "div".
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];

    public $columns;

    /** @var  Model */
    public $model;

    public $request;

    /**
     * Initializes the view.
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if ($this->columns === null) {
            throw new InvalidConfigException('The "columns" property must be set.');
        }

        if ($this->model === null) {
            throw new InvalidConfigException('The "model" property must be set.');
        }

        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
    }

    /**
     * Runs the widget.
     */
    public function run()
    {
        $assetTable = DataTableAsset::register($this->view);
        echo $this->loadTable();
        $this->runJs();
    }

    /**
     * @return string
     */
    public function loadTable()
    {
        $content = '';

        return '<div id="loading_datatable_' . $this->options['id'] . '" class="loading">
                        <div class="lds-ripple"><div></div><div></div></div>
                    </div>' . Html::tag('table', '',
                ['class' => 'table table-responsive', 'width' => '100%', 'id' => $this->options['id']]);
    }


    /**
     * @return array
     */
    protected function loadColumnHeader()
    {
        $out = [];
        foreach ($this->columns as $column) {
            if (is_array($column)) {
                if (isset($column['data']) && !isset($column['title'])) {
                    $column['title'] = $this->model->getAttributeLabel($column['data']);
                }
                $column['name'] = $column['title'];
                $out[] = $column;
            } else {
                $out[] = [
                    'title' => $this->model->getAttributeLabel($column),
                    'name' => $this->model->getAttributeLabel($column),
                    'data' => (string)$column,
                ];
            }
        }

        return $out;
    }

    /**
     * @param $name
     * @param $string
     * @param $addition
     * @return array|string|string[]
     */
    protected static function putNameIn($name, $string, $addition)
    {
        if (stripos($string, $name) !== false) {
            return str_replace($name, "$name $addition", $string);
        }
        return $string;
    }

    /**
     * @return string
     */
    protected function loadColumnFilter()
    {
        $out = '<tr>';
        foreach ($this->columns as $column) {
            if (isset($column['searchable']) && !$column['searchable']) {
                $out .= '<th></th>';
                continue;
            }
            if (is_array($column)) {
                $col = isset($column['data']) ? $column['data'] : '';
                if (isset($column['input'])) {
                    $intp = self::putNameIn('<select', $column['input'], "name='filter_$col'");
                    $intp = self::putNameIn('<input', $intp, "name='filter_$col'");
                    $out .= "<th class='hasinput' >$intp</th>";
                } elseif ($this->model->hasAttribute($col)) {
                    $out .= "<th class='hasinput' ><input type='text' class='' placeholder=' Filter " .
                        $this->model->getAttributeLabel($col) . "' name='filter_$col' /></th>";
                    if ($col == 'name') {
                        print_r($column);
                        die();
                    }
                } else {
                    $out .= '<th></th>';
                }
            } elseif ($this->model->hasAttribute($column)) {
                $out .= "<th class='hasinput' ><input type='text' class='' placeholder=' Filter " .
                    $this->model->getAttributeLabel($column) . "' name='filter_$column'/></th>";
            } else {
                $out .= '<th></th>';
            }
        }

        return $out . '</tr>';
    }

    public $ajax, $jsAfterLoadTable = '', $jsOutTable = '';

    /**
     * All JS
     */
    protected function runJs()
    {
        $loadingHtml = '<div align="center"><i class="fa fa-spinner fa-pulse fa-4x fa-fw margin-bottom"></i></div>';
        $idTable = $this->options['id'];
        $columns = Json::encode($this->loadColumnHeader());
        $columnsFilter = $this->loadColumnFilter();

        $order = "[0, 'desc']";
        if (isset($this->options['order'])) {
            $order = $this->options['order'];
            unset($this->options['order']);
        }

        $ajax = "''";
        if ($this->ajax != '') {
            $ajax = $this->ajax;
        }

        if (isset($this->options['filterColumns'])) {

            unset($this->options['filterColumns']);
        }

        $gf = 'f';
        if (isset($this->options['globalSearch'])) {
            $gf = $this->options['globalSearch'] ? 'f' : '';
            unset($this->options['globalSearch']);
        }

        $rC = "''";
        if (isset($this->options['rowCallback'])) {
            $rC = $this->options['rowCallback'];
            unset($this->options['rowCallback']);
        }

        $addOption = Json::encode($this->options);

        $jsScript = <<< JS
var delay2 = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();
var renderdata = (function () {
    // var dataSet = ;
    let domElement = $('#$idTable');

    let domOuttable = domElement.parent();

    let newHeader = $("$columnsFilter");

    let option = {
        sDom: "<'dt-toolbar'<'col-xs-12 col-sm-6'$gf><'bar-export col-sm-3 col-xs-12 hidden-xs'><'col-sm-3 pull-right col-xs-12 hidden-xs'l>r>" +
            "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        scrollX: true,
        // rowId: "",
        processing: true,
        serverSide: true,
        stateSave: true       
    };
    option.ajax = $ajax;
    option.columns = $columns;
    option.order = [$order];
    option.createdRow = function (row, data, index) {
            if (data.classRow !== '')
                $(row).addClass(data.classRow);
    };
    
    if ($rC != '') {
        option.rowCallback = $rC;
    }
    
    option.initComplete = function () {
            delay2(function () {
               
            }, 1000);
            var state = table.state.loaded();
            if (state) {
                table.columns().eq(0).each(function (colIdx) {
                    var colname = table.column(colIdx).dataSrc();
                    var colSearch = state.columns[colIdx].search;
                    if (colSearch.search) {
                        newHeader.find('[name=filter_' + colname + ']').val(colSearch.search);
                    }
                });
                
            }
            domOuttable.find('thead').eq(0).append(newHeader);
            $this->jsAfterLoadTable
        }; 

    $.each($addOption, function (key, val) {
        if (key != 'id') {
            option[key] = val;    
        }
    });
    
    var table = domElement.DataTable(option);
    
    $this->jsOutTable
    
    let loading = domOuttable.find('#loading_datatable_$idTable');
    
    var startTime;
 
    table
        .on( 'preDraw', function () {
            startTime = new Date().getTime();
            loading.fadeIn(1000);
        } )
        .on( 'draw.dt', function () {
            console.log( 'Redraw took at: '+(new Date().getTime()-startTime)+'mS' );
            loading.fadeOut(1000);
        } );
    
    domOuttable.find('thead').eq(0).delegate('th input[type=text]', 'keyup ', function () {
        var self = this;
        delay2(function () {
            table
                .column($(self).parent().index() + ':visible')
                .search(self.value)
                .draw();
        }, 1000);

    });

    domOuttable.find('thead').eq(0).delegate('th select', 'change', function () {
        var self = this;
        delay2(function () {
            table
                .column($(self).parent().index() + ':visible')
                .search(self.value)
                .draw();
        }, 1000);

    });

    domOuttable.delegate('[data-action=update_status]', 'click', function (a) {
        a.preventDefault();
        a.stopPropagation();
        var elm = $(this);
        var td = elm.closest('td');
        var cll = table.cell(td);

        $.ajax({
            url: elm.attr('href'),
            method: 'post',
            data: {},
            error: function (response) {
                alert_notif({status: "error", message: 'tidak bisa save'});
            },
            success: function (response) {
                cll.data(response.button);
                alert_notif(response);

            },

        });

    });
    
    
    
    function alert_notif(res) {
        $.sound_on = false;
        var color = res.status === 'success' ? '#659265' : '#C46A69';
        var icon = res.status === 'success' ? 'fa-check' : 'fa-times';
        $.smallBox({
            title: res.status,
            content: res.message,
            color: color,
            iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
            timeout: 4000
        });
    }
    
    return {
        "test": function () {
            alert('daa');
        },
        table: table
    };
})();        


window.tbl_0004 = renderdata.table;
window.tbl_000$idTable = renderdata.table;

JS;
        $this->view->registerJs($jsScript, 3, 'runfor_' . $this->options['id']);
    }
}