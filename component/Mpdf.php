<?php
/**
 * Created by
 * User     : Wisard17
 * Date     : 26/09/2019
 * Time     : 03.09 PM
 * File Name: Mpdf.php
 **/

namespace app\component;


use Codeception\Exception\ConfigurationException;
use Yii;

/**
 * Class Mpdf
 * @package app\components
 */
class Mpdf extends \Mpdf\Mpdf
{

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    /**
     * @param $fileName
     * @param string $dir
     * @throws \Mpdf\MpdfException
     * @throws \yii\base\InvalidConfigException
     */
    public function registerCssFile($fileName, $dir = '')
    {
        if ($dir != '') {
            $fileName = $dir . '/' . $fileName;
        }
        $place = $this->checkDir($fileName);
        $stylesheet = file_get_contents($place);
        $this->WriteHTML($stylesheet,1);
    }

    /**
     * @param $string
     * @throws \Mpdf\MpdfException
     */
    public function registerCss($string)
    {
        $this->WriteHTML($string,1);
    }

    public function setDraft()
    {
        $this->SetWatermarkText('DRAFT');
        $this->watermarkTextAlpha = 0.1;
        $this->watermarkAngle = 45;
        $this->showWatermarkText = true;
    }

    /**
     * @param $dir
     * @return false|string
     * @throws \yii\base\InvalidConfigException
     * @throws ConfigurationException
     */
    protected function checkDir($dir)
    {
//        Yii::$app->assetManager->publish($dir);
        $directory = Yii::getAlias($dir);

        if (!file_exists($directory)) {
            throw new ConfigurationException('check dir or file');
        }
        return $directory;
//        return Yii::$app->assetManager->getPublishedUrl($dir);
    }
}