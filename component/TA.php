<?php

namespace app\component;

use yii\base\Model;

class TA extends Model
{

    public static function logo()
    {
        $data = "width: 300px;";
        $data .= "margin-top:1px;";
        return "style='$data'";
    }
    public static function logo2()
    {
        $data = "width: 300px;";
        return "style='$data'";
    }
    public static function space($val)
    {
        $data = "&nbsp;";
        for ($i = 0; $i < $val; $i++) {
            $data = $data . "&nbsp;";
        }
        return $data;
    }
    public static function br($val)
    {
        $data = "<br>";
        for ($i = 0; $i < $val; $i++) {
            $data = $data . "<br>";
        }
        return $data;
    }
    public static function overLine()
    {
        $data = "width: 100%;";
        $data .= "height: 1.5px;";
        $data .= "background-color: black;";
        return "style='$data'";
    }
    public static function overLine2($aa)
    {
        $data = "width: 100%;";
        $data .= "height: $aa" . "px;";
        $data .= "background-color: black;";
        return "style='$data'";
    }
    public static function overLine3($aa, $color)
    {
        $data = "width: 100%;";
        $data .= "height: $aa" . "px;";
        $data .= "background-color: $color;";
        return "style='$data'";
    }
    public static function tHead()
    {
        $data = "font-size: 10px;";
        return "style='$data'";
    }
    public static function tHeadC($color)
    {
        $data = "font-size: 10px;";
        $data .= "border-right: 1px solid $color;";
        $data .= "border-bottom: 1px solid $color;";
        $data .= "border-spacing: 0;";
        $data .= "height: 25px;";
        return "style='$data'";
    }
    public static function tHeadC1($color)
    {
        $data = "font-size: 12px;";
        $data .= "border-left: 1px solid $color;";
        $data .= "border-top: 1px solid $color;";
        $data .= "border-right: 1px solid $color;";
        $data .= "border-bottom: 1px solid $color;";
        $data .= "border-spacing: 0;";
        $data .= "height: 25px;";
        return "style='$data'";
    }

    public static function tbC($color)
    {
        $data = "font-size: 10px;";
        $data .= "border-right: 1px solid $color;";
        //$data.="border-bottom: 1px solid $color;";
        $data .= "border-spacing: 0;";
        //$data.="height: 15px;";
        return "style='$data'";
    }
    public static function tbC2($color, $par = "")
    {
        $data = "font-size: 10px;";
        $data .= "border-right: 1px solid $color;";
        $data .= "font-weight:bold;";
        //$data.="border-bottom: 1px solid $color;";
        $data .= "border-spacing: 0;";
        $data .= "$par;";
        return "style='$data'";
    }
    public static function tbEnd($color)
    {
        $data = "font-size: 10px;";
        return "style='$data'";
    }
    public static function tHeadEnd($color)
    {
        $data = "font-size: 10px;";
        $data .= "border-bottom: 1px solid $color;";
        return "style='$data'";
    }
    public static function tHead2()
    {
        $data = "font-size: 10px;";
        return "style='$data'";
    }
    public static function tR()
    {
        $data = "padding:0px;";
        return "style='$data'";
    }
    public static function txt($font)
    {
        $data = "font-size: $font" . ";";
        return "style='$data'";
    }
    public static function txt2($font, $h)
    {
        $data = "font-size: $font" . ";";
        $data = "line-height: $h" . ";";
        return "style='$data'";
    }
    public static function tb_border($font)
    {
        $data = "";
        $data .= "font-size: $font" . ";";
        $data .= "border-right: 0.5px solid black;";
        //$data.="border-collapse: collapse;";
        return "style='$data'";
    }
    public static function tb_border2($font, $data)
    {
        $data .= "font-size: $font" . ";";
        return "style='$data'";
    }
    public static function tb_borderTB($font)
    {
        $data = "";
        $data .= "font-size: $font" . ";";
        $data .= "border: 0 solid black;";
        $data .= "border-collapse: collapse;";
        return "style='$data'";
    }
    public static function headContent($font)
    {
        $data = "";
        $data .= "font-size: $font" . ";";
        $data .= "border:0.5px solid black;";
        return "style='$data'";
    }
    public static function headContent2($font, $data = "")
    {
        $data .= "font-size: $font" . ";";
        return "style='$data'";
    }
    public static function logo_code1()
    {
        $data = "";
        //$data.="position: absolute;";
        //$data.="margin-top: 100px;";
        //$data.="margin-left: 100px;";
        return "style='$data'";
    }

    public static function codeImg($code)
    {
        if (strtoupper($code) == 'A') {
            return "reports/e_code/a.png";
        } elseif (strtoupper($code) == 'B') {
            return "reports/e_code/b.png";
        } elseif (strtoupper($code) == 'C') {
            return "reports/e_code/c.png";
        } elseif (strtoupper($code) == 'D') {
            return "reports/e_code/d.png";
        } elseif (strtoupper($code) == 'E') {
            return "reports/e_code/e.png";
        } else {
            return "reports/e_code/a.png";
        }
    }
    public static function codeStatus($code)
    {
        if (strtoupper($code) == 'A') {
            return "Normal";
        } elseif (strtoupper($code) == 'B') {
            return "Attention";
        } elseif (strtoupper($code) == 'C') {
            return "Urgent";
        } elseif (strtoupper($code) == 'D') {
            return "Urgent";
        } elseif (strtoupper($code) == 'E') {
            return "Urgent";
        } else {
            return "Normal";
        }
    }
    public static function codeStatusFuran($code)
    {
        if (strtoupper($code) == 'A') {
            return "Normal";
        } elseif (strtoupper($code) == 'B') {
            return "Accelerated";
        } elseif (strtoupper($code) == 'C') {
            return "Excessive";
        } elseif (strtoupper($code) == 'D') {
            return "High Risk";
        } elseif (strtoupper($code) == 'E') {
            return "End of Expected";
        } else {
            return "Normal";
        }
    }
    public static function codeStatusDga($code)
    {
        if (strtoupper($code) == 'A') {
            return "Cond. 1";
        } elseif (strtoupper($code) == 'B') {
            return "Cond. 2";
        } elseif (strtoupper($code) == 'C') {
            return "Cond. 3";
        } elseif (strtoupper($code) == 'D') {
            return "Cond. 4";
        } else {
            return "Cond. 1";
        }
    }
    public static function colorCode($code)
    {

        if (strtoupper($code) == 'A') {
        } elseif (strtoupper($code) == 'B') {
            return "font color:#ffcc00;";
        } elseif (strtoupper($code) == 'C') {
            return "font color:#ff0000;";
        } elseif (strtoupper($code) == 'D') {
            return "font color:#ff0000;";
        } elseif (strtoupper($code) == 'E') {
            return "background:#ff0000;";
        }
    }
    public static function matric($code)
    {

        if ($code) {
            return $code;
        } else {
            return '-';
        }
    }
    public static function cS($var)
    {

        if ($var) {
            return str_replace(" ", "&nbsp;", $var);
        } else {
            return '-';
        }
    }
}
