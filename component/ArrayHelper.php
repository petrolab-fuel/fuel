<?php
/**
 * Created by
 * User     : Wisard17
 * Date     : 24/06/2019
 * Time     : 10.43 AM
 * File Name: ArrayHelper.php
 **/


namespace app\component;

/**
 * Class ArrayHelper
 * @package app\components
 */
class ArrayHelper extends \yii\helpers\ArrayHelper
{
    /**
     * @param array $array
     * @return string
     */
    public static function toString($array)
    {
        $out = '(';
        if (is_array($array)) {
            foreach ($array as $idx => $value) {
                if (is_array($value))
                    $out .= " $idx => " . self::toString($value);
                else
                    $out .= " $idx => " . $value;
            }
        } else
            $out .= $array;
        return "$out)";
    }
}