<?php

namespace app\helpers;
use Yii;
use yii\helpers\Url;


class HelpMe
{
	public function json(){
		return \Yii::$app->response->format=\Yii\web\Response::FORMAT_JSON;
	}
	public function urlImg($img){
		return \Yii::$app->urlManager->createUrl('/img'.'/'.$img);
	}
	public function fDate($format,$date){
		return date($format,strtotime($date));
	}
	public function urlChart($img){
		return "/../../../web/img/reports/chart/run/$img";
	}
	public function urlChartDefault(){
		return "/../../../web/img/reports/chart/default/";
	}
	public function urlChartRun(){
		return "/../../../web/img/reports/chart/run/";
	}
}