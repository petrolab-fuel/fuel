<?php

/* @var $this yii\web\View */
use app\smartadmin\assets\plugins\ChartAsset;

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;

ChartAsset::register($this);
?>
<head>
    <title>Pie chart</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <?php $chart->printScripts(); ?>
        <?php $chart0->printScripts(); ?>
        <?php $chart1->printScripts(); ?>
        <?php $chart2->printScripts(); ?>
    </head>
<!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
                <article class="col-sm-12">

                    <!-- new widget -->
                    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header>
                            <!-- <span class="widget-icon"> <i class="glyphicon glyphicon-stats txt-color-darken"></i> </span> -->
                            <h2>Transformer Overall Status</h2>

                        </header>

                        <!-- widget div-->
                        <div>
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <label>Title:</label>
                                <input type="text" />
                            </div>
                            <!-- end widget edit box -->

                            <div class="widget-body no-padding smart-form">
                                <!-- content -->
                                
                                 <div id="container"></div>
                                    <script type="text/javascript">
                                      
                                      data = [{
                                          name : 'Normal',
                                          y: <?php echo json_encode($pie['normal']);?>,
                                          color: '#00B500'
                                        }, {
                                          name : 'Attention',
                                          y: <?php echo json_encode($pie['attention']);?>,
                                          color: '#FFED00'
                                        }, {
                                          name : 'Urgent',
                                          y: <?php echo json_encode($pie['urgent']);?>,
                                          color: '#DF6240'
                                        },{ 
                                          name : 'Severe',
                                          y: <?php echo json_encode($pie['severe']);?>,
                                          color: '#FF0000'
                                        }];

                                      function setChart(name, data, color) {
                                        // chart.xAxis[0].setCategories(categories);
                                        chart.series[0].remove();
                                        chart.addSeries({
                                          name: name,
                                          data: data,
                                          color: color || 'white'
                                        });
                                      }
                                    <?php echo $chart->render("chart"); ?>
                                    </script>
                               
                                
                                <!-- end content -->
                            </div>

                        </div>
                        <!-- end widget div -->
                    </div>
                    <!-- end widget -->

                </article>
            </div>

            <!-- end row -->  

            <!-- row -->
            <div class="row">
                <article class="col-sm-12">

                    <!-- new widget -->
                    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-1" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header>
                            <!-- <span class="widget-icon"> <i class="glyphicon glyphicon-stats txt-color-darken"></i> </span> -->
                            <h2>DGA </h2>

                        </header>

                        <!-- widget div-->
                        <div>
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <label>Title:</label>
                                <input type="text" />
                            </div>
                            <!-- end widget edit box -->

                            <div class="widget-body no-padding smart-form">
                                <!-- content -->
                                
                                 <div id="container0"></div>
                                    <script type="text/javascript">
                                      
                                      data = [{
                                          name : 'Condition 1',
                                          y: <?php echo json_encode($pie0['con1']);?>,
                                          color: '#00B500'
                                        }, {
                                          name : 'Condition 2',
                                          y: <?php echo json_encode($pie0['con2']);?>,
                                          color: '#FFED00'
                                        }, {
                                          name : 'Condition 3',
                                          y: <?php echo json_encode($pie0['con3']);?>,
                                          color: '#DF6240'
                                        },{ 
                                          name : 'Condition 4',
                                          y: <?php echo json_encode($pie0['con4']);?>,
                                          color: '#FF0000'
                                        }];

                                      function setChart(name, data, color) {
                                        // chart.xAxis[0].setCategories(categories);
                                        chart.series[0].remove();
                                        chart.addSeries({
                                          name: name,
                                          data: data,
                                          color: color || 'white'
                                        });
                                      }
                                    <?php echo $chart0->render("chart"); ?>
                                    </script>
                               
                                
                                <!-- end content -->
                            </div>

                        </div>
                        <!-- end widget div -->
                    </div>
                    <!-- end widget -->

                </article>
            </div>

            <!-- end row -->

            <!-- row -->
            <div class="row">
                <article class="col-sm-12 col-md-12 col-lg-6">


                    <!-- new widget -->
                    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-2" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header>
                            <!-- <span class="widget-icon"> <i class="glyphicon glyphicon-stats txt-color-darken"></i> </span> -->
                            <h2>FURAN </h2>

                        </header>

                        <!-- widget div-->
                        <div>
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <label>Title:</label>
                                <input type="text" />
                            </div>
                            <!-- end widget edit box -->

                            <div class="widget-body no-padding smart-form">
                                <!-- content -->
                                
                                 <div id="container1"></div>
                                    <script type="text/javascript">
                                      
                                      data = [{
                                          name : 'Normal',
                                          y: <?php echo json_encode($pie1['normal']);?>,
                                          color: '#00B500'
                                        }, {
                                          name : 'Accelerated',
                                          y: <?php echo json_encode($pie1['accelerated']);?>,
                                          color: '#FFED00'
                                        }, {
                                          name : 'Excessive',
                                          y: <?php echo json_encode($pie1['excessive']);?>,
                                          color: '#DF6240'
                                        },{
                                          name : 'High Risk',
                                          y: <?php echo json_encode($pie1['high_risk']);?>,
                                          color: '#ff2828'
                                        },{
                                          name : 'End of Expected',
                                          y: <?php echo json_encode($pie1['end_of_expected']);?>,
                                          color: 'black'
                                        }];

                                      function setChart(name, data, color) {
                                        // chart.xAxis[0].setCategories(categories);
                                        chart.series[0].remove();
                                        chart.addSeries({
                                          name: name,
                                          data: data,
                                          color: color || 'white'
                                        });
                                      }
                                    <?php echo $chart1->render("chart"); ?>
                                    </script>
                               
                                
                                <!-- end content -->
                            </div>

                        </div>
                        <!-- end widget div -->
                    </div>
                    <!-- end widget -->

                </article>

                <article class="col-sm-12 col-md-12 col-lg-6">


                    <!-- new widget -->
                    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-3" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header>
                            <!-- <span class="widget-icon"> <i class="glyphicon glyphicon-stats txt-color-darken"></i> </span> -->
                            <h2>OIL QUALITY </h2>

                        </header>

                        <!-- widget div-->
                        <div>
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <label>Title:</label>
                                <input type="text" />
                            </div>
                            <!-- end widget edit box -->

                            <div class="widget-body no-padding smart-form">
                                <!-- content -->
                                
                                 <div id="container2"></div>
                                    <script type="text/javascript">
                                      
                                      data = [{
                                          name : 'Good',
                                          y: <?php echo json_encode($pie2['good']);?>,
                                          color: '#00B500'
                                        }, {
                                          name : 'Fair',
                                          y: <?php echo json_encode($pie2['fair']);?>,
                                          color: '#FFED00'
                                          // drilldown: {
                                          //   name: 'FAIR',
                                          //   categories: ['Urgent Follow Up', 'Urgent'],
                                          //   data: [{
                                          //       // name : 'Urgent Follow Up',
                                          //       y: 10,
                                          //       color: '#0047AB'
                                          //   },{
                                          //       // name : 'Urgent',
                                          //       y: 20,
                                          //       color: '#FF0000'
                                          //   }],
                                          // }
                                        }, {
                                          name : 'Poor',
                                          y: <?php echo json_encode($pie2['poor']);?>,
                                          color: '#FF0000'
                                        }];

                                      function setChart(name, data, color, type) {
                                        // chart.xAxis[0].setCategories(categories);
                                        chart.series[0].remove();
                                        chart.addSeries({
                                          name: name,
                                          data: data,
                                          color: color || 'white',
                                        });
                                      }
                                    <?php echo $chart2->render("chart"); ?>
                                    </script>
                               
                                
                                <!-- end content -->
                            </div>

                        </div>
                        <!-- end widget div -->
                    </div>
                    <!-- end widget -->

                </article>
            </div>

            <!-- end row -->

        </section>
        <!-- end widget grid -->
