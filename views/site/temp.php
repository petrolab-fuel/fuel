<?php
/**
* Created by
* User: Wisard17
* Date: 9/29/2017
* Time: 4:11 PM
*/


use yii\helpers\Url;
// use app\modelsDB\Attention;
$this->title = Yii::t('app', 'Dashboard');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dashboard'), 'url' => Url::toRoute(['/reports/fuel'])];
$date=date('Y-m-d');
?>
<div class="main" role="main">
    <div id="content">
        <section id="widget-grid" class="">
            <div class="row">
                <?php foreach ($model as $rec){
                    $status=$rec->report_date==$date?'hot':'sale';
                    ?>
                <div class="col-sm-6 col-md-6 col-lg-4">
                    <!-- product -->
                    <div class="product-content product-wrap clearfix">
                        <div class="row">
                            <div class="col-md-5 col-sm-12 col-xs-12">
                                <div class="product-image">
                                    <span class="tag2 <?= $status?>">
                                        Hot
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-12 col-xs-12">
                                <div class="product-deatil">
                                    <h5 class="name">
                                        <a href="#">
                                            <?= $rec->attentionName ?> <span>Lab Number</span>
                                        </a>
                                    </h5>
                                    <p class="product-deatil">
                                        <span><?= $rec->lab_number ?></span>
                                    </p>
                                    <span class="tag1"></span>
                                </div>
                                <div class="description">
                                    <p><?= $rec->report_date ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end product -->
                </div>
                <?php } ?>
            </div>
        </section>
    </div>
</div>
