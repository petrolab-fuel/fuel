<?php
/**
 * Created by
 * User: Wisard17
 * Date: 15/07/2018
 * Time: 06.04 PM
 */

namespace app\smartadmin\assets\plugins;


use yii\web\AssetBundle;

/**
 * Class HighChart
 * @package app\assets\highchart
 */
class DatePickerAssets extends AssetBundle
{
    public $sourcePath = '@app/smartadmin/resources/assets/js/bootstrap-datepicker';

    public $css = [
        'css/bootstrap-datepicker3.min.css'
    ];

    public $js = [
        'js/bootstrap-datepicker.min.js',
//        'js/plugin/modules/data.js',
//        'js/plugin/modules/drilldown.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\jui\JuiAsset',
    ];
}