<?php


namespace app\smartadmin\assets\plugins;

use yii\web\AssetBundle;

class AngularAssets extends AssetBundle
{
    public $sourcePath = '@app/smartadmin/resources/assets/angular';

    public $css = [

    ];

    public $js = [
        'angular.min.js',
        'angular-route.min.js',
    ];


}