<?php
/**
 * Created by PhpStorm.
 * User: Wisard17
 * Date: 8/1/2017
 * Time: 4:21 PM
 */

namespace app\smartadmin\assets\plugins;

use yii\web\AssetBundle;

class DTAsset extends AssetBundle
{
    public $sourcePath = '@app/smartadmin/resources/assets/plugins/datatables';

    public $css = [
         'datatables.min.css',
    ];

    public $js = [
 'datatables.min.js',
    ];

    public $depends = [
        'app\smartadmin\assets\LayoutAsset',
    ];
}