<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Url;
use yii\helpers\Html;

use \app\smartadmin\Alert;
use \app\smartadmin\assets\LayoutAsset;



$asset = LayoutAsset::register($this);
// app\smartadmin\assets\plugins\PluginsAssets::register($this);
// app\modules\chat\assets\ChatAssets::register($this);

$this->registerJs('pageSetUp();');

// $this->registerJs('var sound_path="' . $asset->baseUrl . '/sound/";', \yii\web\View::POS_HEAD);

$this->title = $this->title;
//Meta
$this->registerMetaTag(['charset' => Yii::$app->charset]);
//$this->registerMetaTag(['http-equiv' => 'X-UA-Compatible', 'content' => 'IE=edge,chrome=1']);
$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no']);
$this->registerMetaTag(['name' => 'description', 'content' => 'TintSoft']);
$this->registerMetaTag(['name' => 'author', 'content' => 'TintSoft Team']);

//FAVICONS
$this->registerLinkTag(['rel' => 'shortcut icon', 'href' => $asset->baseUrl . '/img/favicon/favicon.png', 'type' => 'image/x-icon']);
$this->registerLinkTag(['rel' => 'icon', 'href' => $asset->baseUrl . '/img/favicon/favicon.png', 'type' => 'image/x-icon']);

//指定Web剪辑参考网页图标: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html
$this->registerLinkTag(['rel' => 'apple-touch-icon', 'href' => $asset->baseUrl . '/img/splash/sptouch-icon-iphone.png']);
$this->registerLinkTag(['rel' => 'apple-touch-icon', 'sizes' => '76x76', 'href' => $asset->baseUrl . '/img/splash/touch-icon-ipad.png']);
$this->registerLinkTag(['rel' => 'apple-touch-icon', 'sizes' => '120x120', 'href' => $asset->baseUrl . '/img/splash/touch-icon-iphone-retina.png']);
$this->registerLinkTag(['rel' => 'apple-touch-icon', 'sizes' => '152x152', 'href' => $asset->baseUrl . '/img/splash/touch-icon-ipad-retina.png']);
//iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance
$this->registerMetaTag(['name' => 'apple-mobile-web-app-capable', 'content' => 'yes']);
$this->registerMetaTag(['name' => 'apple-mobile-web-app-status-bar-style', 'content' => 'black']);

//Startup image for web apps
$this->registerLinkTag(['rel' => 'apple-touch-startup-image', 'href' => $asset->baseUrl . '/img/splash/ipad-landscape.png', 'media' => 'screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)']);
$this->registerLinkTag(['rel' => 'apple-touch-startup-image', 'href' => $asset->baseUrl . '/img/splash/ipad-portrait.png', 'media' => 'screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)']);
$this->registerMetaTag(['rel' => 'apple-touch-startup-image', 'href' => $asset->baseUrl . '/img/splash/iphone.png', 'media' => 'screen and (max-device-width: 320px)']);
?>
<?php $this->beginPage() ?><!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <?= Html::tag('title', Html::encode($this->title)); ?>
        <?php $this->head() ?>

    </head>

    <!--

    TABLE OF CONTENTS.

    Use search to find needed section.

    ===================================================================

    |  01. #CSS Links                |  all CSS links and file paths  |
    |  02. #FAVICONS                 |  Favicon links and file paths  |
    |  03. #GOOGLE FONT              |  Google font link              |
    |  04. #APP SCREEN / ICONS       |  app icons, screen backdrops   |
    |  05. #BODY                     |  body tag                      |
    |  06. #HEADER                   |  header tag                    |
    |  07. #PROJECTS                 |  project lists                 |
    |  08. #TOGGLE LAYOUT BUTTONS    |  layout buttons and actions    |
    |  09. #MOBILE                   |  mobile view dropdown          |
    |  10. #SEARCH                   |  search field                  |
    |  11. #NAVIGATION               |  left panel & navigation       |
    |  12. #RIGHT PANEL              |  right panel userlist          |
    |  13. #MAIN PANEL               |  main panel                    |
    |  14. #MAIN CONTENT             |  content holder                |
    |  15. #PAGE FOOTER              |  page footer                   |
    |  16. #SHORTCUT AREA            |  dropdown shortcuts area       |
    |  17. #PLUGINS                  |  all scripts and plugins       |

    ===================================================================

    -->

    <!-- #BODY -->
    <!-- Possible Classes

        * 'smart-style-{SKIN#}'
        * 'smart-rtl'         - Switch theme mode to RTL
        * 'menu-on-top'       - Switch to top navigation (no DOM change required)
        * 'no-menu'			  - Hides the menu completely
        * 'hidden-menu'       - Hides the main menu but still accessable by hovering over left edge
        * 'fixed-header'      - Fixes the header
        * 'fixed-navigation'  - Fixes the main menu
        * 'fixed-ribbon'      - Fixes breadcrumb
        * 'fixed-page-footer' - Fixes footer
        * 'container'         - boxed layout mode (non-responsive: will not work with fixed-navigation & fixed-ribbon)
    -->
    <body id="extr-page" class="animated fadeInDown">
    <?php $this->beginBody() ?>


    <header id="header">
    <!--<span id="logo"></span>-->

    <div id="logo-group">
        <span id="logo"> <img style="width: 100%" src="<?= $asset->baseUrl; ?>/img/logo_petrolab.png" alt="SmartAdmin"> </span>

        <!-- END AJAX-DROPDOWN -->
    </div>

    <!-- <span id="extr-page-header-space"> <span class="hidden-mobile hiddex-xs">Need an account?</span> 
    <a href="#" class="btn btn-danger">Create account</a> </span> -->

    </header>

    <!-- MAIN PANEL -->
    <div id="main" role="main">

        <!-- MAIN CONTENT -->
        <div id="content" class="container">
            
         
            <?= $content ?>
            
        </div>
        <!-- END MAIN CONTENT -->

    </div>
    <!-- END MAIN PANEL -->

    <?php $this->endBody() ?>

    </body>
    </html>
<?php $this->endPage() ?>