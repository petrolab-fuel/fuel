<?php
/**
 * Created by PhpStorm.
 * User: Feri
 * Date: 6/8/2019
 * Time: 20:15
 */
    namespace app\assets;
    use yii\web\AssetBundle;
    class Select2Asset extends AssetBundle
    {
        public $sourcePath='@app/assets/plugins/select2';
        public $css=[
            'css/select2.min.css',
        ];
        public $js=[
            'select2.min.js',
        ];
        public $depends=[
            'yii\web\YiiAsset',
            'yii\bootstrap\BootstrapAsset',
        ];
    }
