<?php
/**
 * Created by PhpStorm.
 * User: Feri
 * Date: 9/19/2019
 * Time: 20:12
 */

namespace app\assets;

use yii\web\AssetBundle;
class Message extends AssetBundle
{
    public $sourcePath = '@app/assets/plugins/message/';

    public $css = [
        'sweetalert2.min.css',
    ];

    public $js = [
        'sweetalert2.all.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}