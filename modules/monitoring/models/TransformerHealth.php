<?php
/**
 * Created by Sublime Text.
 * User: doni46
 * Date: 1/18/2018
 * Time: 2:53 PM
 */

namespace app\modules\monitoring\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\DataAnalisa;
use app\modules\reports\helpers\Parameter as Par;

/**
 * Class TransformerHealth
 * @property Unit[] transaksi
 * @package app\modules\monitoring\models
 */
class TransformerHealth extends \app\models\Unit
{

    private $listReport1;
    private $listReport2;
    private $listReport3;
    private $listReport4;
    private $listReport5;

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'report1' => Yii::t('app', 'Report 1'),
            'report2' => Yii::t('app', 'Report 2'),
            'report3' => Yii::t('app', 'Report 3'),
            'report4' => Yii::t('app', 'Report 4'),
            'report5' => Yii::t('app', 'Report 5'),
        ]);
    }

    public function setReport()
    {
        $query= DataAnalisa::find()
        ->select('lab_number, unit_id,customer_id,received_date,
                    recommended1_dga,recommended2_dga,eval_code_dga,
                    recommended1_furan,recommended2_furan,eval_code_furan,
                    recommended1_oil_quality,recommended2_oil_quality,eval_code_oil_quality, 
                    MAX(eval_code) as code')
        // ->joinWith(['evalcode'])
        ->andWhere(['unit_id'=>$this->id])
        ->groupBy('unit_id,customer_id,received_date')
        // ->having(['<', 'DATEDIFF( NOW( ) , RECV_DT1 )', '360'])
        ->orderBy('unit_id,customer_id, received_date DESC')
        ->limit(5)
        ->all();
        // print_r($query);die();
        $i = 0;
        foreach ($query as $analisis) {
            $recomDga = $recomFuran = $recomOil = '';
            $recomDga .= isset($analisis->recommended1_dga)? ' 1.'.$analisis->recommended1_dga.'<br>' : '';
            $recomDga .= isset($analisis->recommended2_dga)? ' 2.'.$analisis->recommended2_dga.'<br>' : '<br>';
            $recomFuran .= isset($analisis->recommended1_furan)? ' 1.'.$analisis->recommended1_furan.'<br>' : '';
            $recomFuran .= isset($analisis->recommended2_furan)? ' 2.'.$analisis->recommended2_furan.'<br>' : '<br>';
            $recomOil .= isset($analisis->recommended1_oil_quality)? ' 1.'.$analisis->recommended1_oil_quality.'<br>' : '';
            $recomOil .= isset($analisis->recommended2_oil_quality)? ' 2.'.$analisis->recommended2_oil_quality.'<br>' : '<br>';
            $arr=[];
            $arr = [
                    'lebNo' => $analisis->lab_number,
                    'unitId' => $analisis->unit_id,
                    'customerId' => $analisis->customer_id,
                    'recv_dt1' => $analisis->received_date,
                    'evalCode' => isset($analisis->code)? $analisis->code:'',
                    'evalCodeDga' => isset($analisis->eval_code_dga)? $analisis->eval_code_dga:'',
                    'recomDga' => $recomDga,
                    'evalCodeFuran' => isset($analisis->eval_code_furan)? $analisis->eval_code_furan:'',
                    'recomFuran' => $recomFuran,
                    'evalCodeOilQuality' => isset($analisis->eval_code_oil_quality)? $analisis->eval_code_oil_quality:'',
                    'recomOilQuality' => $recomOil,
                ];

            if($i == 0 && $this->listReport5 == null){
                $this->listReport5 = $arr;
            }elseif($i == 1 && $this->listReport4 == null){
                $this->listReport4 = $arr;
            }elseif($i == 2 && $this->listReport3 == null){
                $this->listReport3 = $arr;
            }elseif($i == 3 && $this->listReport2 == null){
                $this->listReport2 = $arr;
            }elseif($i == 4 && $this->listReport1 == null){
                $this->listReport1 = $arr;
            } 
            $i++;
        }
        return true;
    }

    public function getReport1()
    {
        if($this->listReport1==null)
            $this->setReport();
        return isset($this->listReport1) ? TransformerHealth::button($this->listReport1) : '';
    }

    public function getReport2()
    {
        return isset($this->listReport2) ? TransformerHealth::button($this->listReport2) : '';
    }

    public function getReport3()
    {
        return isset($this->listReport3) ? TransformerHealth::button($this->listReport3) : '';
    }

    public function getReport4()
    {
        return isset($this->listReport4) ? TransformerHealth::button($this->listReport4) : '';
    }

    public function getReport5()
    {   
        return isset($this->listReport5) ? TransformerHealth::button($this->listReport5) : '';
    }

    public static function button($params)
    {
        $model = DataAnalisa::allStatus();
        
        $title = '';
        $title .= 'DGA: '.$model['dga']['status'][$params['evalCodeDga']]."<br>";
        $title .= ($params['evalCodeDga'] >= 'A')? 'Recom DGA: <br>'.$params['recomDga']:'';
        $title .= 'Furan: '.$model['furan']['status'][$params['evalCodeFuran']]."<br>";
        $title .= ($params['evalCodeFuran'] >= 'A')? 'Recom Furan: <br>'.$params['recomFuran']:'';
        $title .= 'Oil Quality: '.$model['oil_quality']['status'][$params['evalCodeOilQuality']]."<br>";
        $title .= ($params['evalCodeOilQuality'] >= 'A')? 'Recom Oil Quality: <br>'.$params['recomOilQuality']:'';
        $evalcode = $params['evalCode'];
        $evalcode = strtoupper($evalcode);
        $status = $model['all']['status'][$evalcode];
        $classLib = isset($evalcode)? $model['all']['label'][$evalcode] : 'default';
        $date = Yii::$app->formatter->asDate($params['recv_dt1']);
        $imgUrl = Yii::$app->HM->urlImg(Par::codeImg($evalcode));
        $link = Html::tag('a', 'Report detail', ['class' => "btn btn-primary",'title'=>'click for report detail','href'=>Url::to(["/reports/default/pdf","lab_number"=>$params['lebNo']]),'target'=>'_blank']);
        $button ='';
         $button = Html::tag('a', $date, ['class' => "btn btn-".$classLib." btn-sm",'data-placement'=>"bottom",'data-toggle'=>"popover",'data-container'=>"body",'data-action'=>$params['unitId']]);
        $button .= '
            <div id="popover-content-'.$params['unitId'].'" class="hide">
                <div class="row">
                 <div class="col-xs-4">
                   <img width="80%" height="80%" src="'.$imgUrl.'"/>
                 </div>
                 <div class="col-xs-8">
                   <h4>
                   '.$status.' <br/>
                   <small>
                     '.$title.'
                   </small>
                   </h4>
                   '.$link.'   
                 </div>
            </div>
        </div>';
        return $button;
        // return 
    }

    public function getActions()
    {
        $view = Html::tag('a', '<i class="fa fa-search"></i>', ['class' => 'btn btn-primary btn-xs', 'title' => 'View Detail']);
        $pdf = Html::tag('i', 'PDF', ['class' => 'fa fa-file-pdf-o', 'label' => 'Export PDF']);
        $viewpdf = Html::tag('a', '<i class="fa fa-file-pdf-o"></i>', ['class' => 'btn btn-danger btn-xs', 'title' => 'Export PDF']);

        return " $viewpdf  $view";
    }
}