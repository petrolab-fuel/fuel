<?php
/**
 * Created by sublime text.
 * User: doni
 * Date: 2/12/2017
 * Time: 11:44 PM
 */

namespace app\modules\monitoring\models;

use app\smartadmin\assets\plugins\DataTableAsset;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\View;

/**
 * Class DataTableView
 * @property string jsonData
 * @property string jsonDataTable
 * @package app\modules\monitoring\models
 */
class DataTrafoHealth extends Widget
{
    /**
     * @var array the HTML attributes for the container tag of the list view.
     * The "tag" element specifies the tag name of the container element and defaults to "div".
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];


    public $columns;

    /** @var  Transaksi */
    public $model;

    public $request;

    public $ajaxUrl;


    /**
     * Initializes the view.
     */
    public function init()
    {
        parent::init();

        if ($this->columns === null) {
            throw new InvalidConfigException('The "columns" property must be set.');
        }

        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }

        if ($this->model === null) {
            throw new InvalidConfigException('The "model" property must be set.');
        }

    }

    /**
     * Runs the widget.
     */
    public function run()
    {
        $assetTable = DataTableAsset::register($this->view);
        echo $this->loadTable();
        $this->runJs();
    }

    public function loadTable()
    {
        $content = '';

        return Html::tag('table', "",
            ['class' => "table table-responsive", 'width' => "100%", 'id' => $this->options['id']]);
    }


    /**
     * @return array
     */
    protected function loadColumnHeader()
    {
        $out = [];
        foreach ($this->columns as $column) {
            $showD = $column == '' ? 'details-control' : '';
            $out[] = [
                'title' => $this->model->getAttributeLabel($column),
                'data' => "$column",
                'className' => $showD,
            ];
        }

        return $out;
    }

    protected function loadColumnFilter()
    {
        $out = '<tr>';
        foreach ($this->columns as $column) {
            if (!in_array($column, [
                'actions',
                'report1','report2','report3','report4','report5',
                ])) {
                    $col = $this->model->getAttributeLabel($column);
                if($col === 'Status'){
                    $out .= "<th class='hasinput' rowspan='1' colspan='1'><select name='status'> <option value=''>Pilih Semua</option> <option value='N'> Normal </option> <option value='B'> Attention</option> <option value='C'> Urgent </option> <option value='D'> Severe</option></select></th>";
                }else{
                    $out .= "<th class='hasinput' rowspan='1' colspan='1'><input type='text' class='' placeholder=' Filter " . $this->model->getAttributeLabel($column) . "' /></th>";
                }
                
            } else {
                $out .= "<th class='hasinput' rowspan='1' colspan='1'></th>";
            }
        }

        return $out . '</tr>';
    }

    protected function runJs()
    {
        $csrf = Yii::$app->request->getCsrfToken();

        $loadingHtml = '<div align="center"><i class="fa fa-spinner fa-pulse fa-4x fa-fw margin-bottom"></i></div>';

        $ajaxUrl = Url::to(['index',
            'status' => @$_GET['status'],
            'type' => @$_GET['type'],
            '_csrf' => $csrf,
        ]);

        $idTable = $this->options['id'];
        $begin = @$_GET['recv_date_begin'];
        $end = @$_GET['recv_date_end'];
        $columns = Json::encode($this->loadColumnHeader());

        $colFilter = $this->loadColumnFilter();

        $detailUrl = Url::to(['detail']);
        $url = Url::to(['action/getlabnumber']);
        $urlto = Url::to(['report/pdf']);
        $jsScript = <<< JS
        
var delay2 = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

// Date Range Picker
$("#recv_date_end").datepicker({
    defaultDate: "+1w",
    changeMonth: true,
    numberOfMonths: 3,
    dateFormat: 'yy-mm-dd',
    prevText: '<i class="fa fa-chevron-left"></i>',
    nextText: '<i class="fa fa-chevron-right"></i>',
    onClose: function (selectedDate) {
        $("#recv_date_begin").datepicker("option", "maxDate", selectedDate);
    }

});
$("#recv_date_begin").datepicker({
    defaultDate: "+1w",
    changeMonth: true,
    numberOfMonths: 3,
    dateFormat: 'yy-mm-dd',
    prevText: '<i class="fa fa-chevron-left"></i>',
    nextText: '<i class="fa fa-chevron-right"></i>',
    onClose: function (selectedDate) {
        $("#recv_date_end").datepicker("option", "minDate", selectedDate);
    }
});

var renderdata = (function () {
    // var dataSet = ;
    var domElement = $('#$idTable');
    var detailurl = "$detailUrl";

    var upelemen = domElement.parent().parent();

    table = domElement.DataTable({
        "scrollX": true,
        "sDom": "<'dt-toolbar'<'col-sm-6 col-xs-12 hidden-xs'l>r>" +
        "t <'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        rowId: "Lab_No",
        "processing": true,
        "serverSide": true,
        "ajax": "$ajaxUrl",
        columns: $columns,
        order: [[0, "desc"]],
        "createdRow": function (row, data, index) {
            if (data.classRow !== '')
                $(row).addClass(data.classRow);
        },
        initComplete: function () {
            upelemen.find('thead').eq(0).append("$colFilter");

        }

    });

    upelemen.find('thead').eq(0).delegate('th input[type=text]', 'keyup ', function () {
        var self = this;
        delay2(function () {
            table
                .column($(self).parent().index() + ':visible')
                .search(self.value)
                .draw();

        }, 1000);

    });

    upelemen.find('thead').eq(0).delegate('th select[name=status]', 'keyup change', function () {
        var self = this;
        delay2(function () {
            table
                .column($(self).parent().index() + ':visible')
                .search(self.value)
                .draw();

        }, 1000);

    });

    domElement.on('click', function(e){
            $(e.target).popover({
                html: true,
                placement: 'left',
                content: function() {
                    var action = $(e.target).attr('data-action');
                    return $('#popover-content-'+action).html()
                }
            });
           
            });

    domElement.on('click', 'tbody .details-control', function () {

        var tr = $(this).closest('tr');
        tr.toggleClass('selected');

        var row = table.row(tr);

        var orderNo = row.data().Order_No;


        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {

            if (row.child() === undefined || row.child().find('div').length < 2) {
                row.child('$loadingHtml');
                $.get(detailurl, {
                    "orderno": orderNo,
                    "_csrf": '$csrf'

                }, function (dataresponse, status, xhr) {
                    if (status === "success")
                        row.child(dataresponse);
                    if (status === "error")
                        alert("Error: " + xhr.status + ": " + xhr.statusText);
                });
            }
            row.child.show();

            tr.addClass('shown');
        }

    });

    $.ajaxSetup({
        beforeSend:function(){
        HoldOn.open({
            theme:'sk-circle',
            message:"<h4>"+"Please Wait..."+"</h4>"
        });
        },
        complete:function(){
            HoldOn.close();
        }
        });
    
    $('[data-action=update]').click(function (e) {
       
        var date = $('#date').val();
            var form_data={
                _csrf:$('meta[name="csrf-token"]').attr("content")
                }       
            $.ajax({
                url: base_url+"/monitoring/machine_health/default/update",
                type: 'POST',
                data:form_data
            })
            .done(function(res) {
                if(res.status=='success'){
                    alert_success(res);
                }else{
                    alert_error({"status":"Error","message":"data filed updated"});
                }
                
                table.ajax.reload();
            })
            .fail(function() {
                alert_arror();
            })
            .always(function() {
                console.log("complete");
            });
    });

    function alert_success(res){
            $.sound_on = false;
            /*$.sound_path = '/sound';*/
            $.smallBox({
                title : res.status,
                content : res.message,
                color : "#659265",
                iconSmall : "fa fa-check fa-2x fadeInRight animated",
                timeout : 4000
            });
        }
        //mesage box success
        function alert_error(){
            $.sound_on = false;
            /*$.sound_path ='sound/';*/
            $.smallBox({
                title : "Error",
                content : "found error in proccess",
                color : "#C46A69",
                iconSmall : "fa fa-times fa-2x fadeInRight animated",
                timeout : 4000
            });
        }
        //mesage box error
    
    return {
        "test": function () {
            alert('daa');
        },
        table: table
    };
})();        

JS;


        $this->view->registerJs($jsScript, View::POS_READY, 'runfor_' . $this->options['id']);
    }


}