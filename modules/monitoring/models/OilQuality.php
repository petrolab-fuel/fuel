<?php
/**
 * Created by
 * User: Wisard17
 * Date: 07/02/2018
 * Time: 12.06 PM
 */

namespace app\modules\monitoring\models;


use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class OilQuality
 * @package app\modules\monitoring\models
 *
 * @property string $action
 */
class OilQuality extends DataAnalisa
{
    /**
     * @return string
     */
    public function getAction()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => "btn btn-default dropdown-toggle", 'data-toggle' => "dropdown", 'aria-expanded' => "false"
            ]) . Html::tag('ul',
                Html::tag('li', Html::tag('a', '<i class="fa fa-file-pdf-o"></i> view PDF', ['href' => Url::toRoute(['/orders/purchase/view', 'id' => $this->id]), 'target' => '_blank'])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-caret-square-o-right"></i> view Report', ['href' => Url::toRoute(['/orders/add', 'po_id' => $this->id])]))
//                Html::tag('li', Html::tag('a', '<i class="fa fa-print"></i> Print', ['href' => Url::toRoute(['/orders/purchase/print', 'id' => $this->id]), 'target' => '_blank'])) .
//                Html::tag('li', Html::tag('a', '<i class="fa fa-edit"></i> Edit', ['href' => Url::toRoute(['/orders/purchase/edit', 'id' => $this->id])])) .
//                Html::tag('li', Html::tag('a', '<i class="fa fa-window-close"></i> Delete',
//                    ['href' => Url::toRoute(['/orders/purchase/del', 'id' => $this->id]),
//                        'data-method' => 'post',
//                        'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
//                    ]))

                , ['class' => "dropdown-menu"]), ['class' => "btn-group"]);
    }
}