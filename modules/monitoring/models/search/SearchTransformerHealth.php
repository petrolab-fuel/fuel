<?php

namespace app\modules\monitoring\models\search;

use Yii;
// use app\components\Filter;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\monitoring\models\TransformerHealth;
use app\models\DataAnalisa;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * SearchTransformerHealth represents the model behind the search form of `app\modules\monitoring\models\TransformerHealth`.
 */
class SearchTransformerHealth extends TransformerHealth
{

     /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SearchTransformerHealth::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'=>$this->id,
            'category_id'=>$this->category_id,
            'customer_id'=>$this->customer_id,
            'gps' => $this->gps,
            'location'=>$this->location,
            'transformer_id'=>$this->transformer_id,
            'transformer_manufacture'=>$this->transformer_manufacture,
            'serial_number'=>$this->serial_number,
            'operating_voltage'=>$this->operating_voltage,
            'type_of_equipment'=>$this->type_of_equipment,
            'rating'=>$this->rating,
            'operating_temperature_of_oil'=>$this->operating_temperature_of_oil,
        ]);

        $query->andFilterWhere(['like', 'UnitID', $this->UnitID])
            ->andFilterWhere(['like', 'customer_id', $this->customer_id])
            ->andFilterWhere(['like', 'grouploc', $this->grouploc])
            ->andFilterWhere(['like', 'branch', $this->branch])
            ->andFilterWhere(['like', 'tgl1', $this->tgl1])
            ->andFilterWhere(['like', 'tgl2', $this->tgl2])
            ->andFilterWhere(['like', 'tgl3', $this->tgl3])
            ->andFilterWhere(['like', 'tgl4', $this->tgl4])
            ->andFilterWhere(['like', 'tgl5', $this->tgl5])
            ->andFilterWhere(['like', 'ec1', $this->ec1])
            ->andFilterWhere(['like', 'ec2', $this->ec2])
            ->andFilterWhere(['like', 'ec3', $this->ec3])
            ->andFilterWhere(['like', 'ec4', $this->ec4])
            ->andFilterWhere(['like', 'ec5', $this->ec5]);

        return $dataProvider;
    }

    public $allField = [
        'unitID'=>'transformer_id',
        'manufacture'=>'transformer_manufacture',
        'category'=>'name',
        'serialNo'=>'serial_number',
        'brand'=>'Brand',
        'location'=>'location',
        'report1'=>'transformer_id',
        'report2'=>'transformer_id',
        'report3'=>'transformer_id',
        'report4'=>'transformer_id',
        'report5'=>'transformer_id',

    ];

    public function ordering($params)
    {
        $ncol = isset($params['order'][0]['column']) ? $params['order'][0]['column'] : 0;
        $col = (isset($params['columns'][$ncol]) && array_key_exists($params['columns'][$ncol]['data'], $this->allField)) ?
            $this->allField[$params['columns'][$ncol]['data']] : '';
        $argg = isset($params['order'][0]['dir']) ? $params['order'][0]['dir'] : 'asc';
        $table = 'unit';
        // $table = in_array($col,['UnitNo','Model','SerialNo','Brand','UnitLocation'])? 'tbl_unit' : $table;
        $table = in_array($col,['name'])? 'category' : $table;

        return $col !== '' ? "$table.$col $argg " : '';
    }

    public function defaultFilterByUser($query)
    {
//        $dataId = Yii::$app->user->identity->data_id;


        // return Filter::security($query);
        return $query;
    }

    public $allData;

    public $currentData;

    /**
     * @param Query $query
     * @return int
     */
    public function calcAllData($query)
    {
        return $this->allData == null ? $query->count() : $this->allData;
    }

    public function searchData($params)
    {
        // $data = Transaksi::find()->select('customer_id');
        // $data = $this->defaultFilterByUser($data);
        // $data->groupBy('customer_id')->asArray()->all();

        $odr = $this->ordering($params);
        $query = TransformerHealth::find()->joinWith(['category','customer']);
        if(isset($params['status'])){
            $model = DataAnalisa::decodeStatus(); 
            $status = $params['status'];
            $type = $params['type'];
            $_POST['col'] = 'd.eval_code';
            if($type != 'all'){
                $_POST['col'] .= '_'.$type;
            }
            $_POST['status'] = $model[$type][$status];
            $query = TransformerHealth::find()->joinWith(['category','customer',
                'dataAnalisas d'=> function($q){$q->where([$_POST['col']=>$_POST['status']]);}]);
        }
        $data = $this->defaultFilterByUser($query);
        // $query->andWhere(['IN','CustomerID',$data]);

        $query->orderBy($odr);

        $start = isset($params['start']) ? $params['start'] : 0;
        $lang = isset($params['length']) ? $params['length'] : 10;

        $this->allData = $this->calcAllData($query);

        // if (isset($params['search']) && $params['search']['value'] != '') {
        //     $fltr = '';
        //     $s = 0;
        //     foreach ($params['columns'] as $col) {
        //         $lst[] = $col['data'];
        //         if (array_key_exists($col['data'], $this->allField) &&
        //             !array_key_exists($col['data'], $lst) && $this->allField[$col['data']] != '') {
        //             $fltr .= $s != 0 ? ' or ' : '';
        //             $fltr .= ' ' . $this->allField[$col['data']] . " like '%" . $params['search']['value'] . "%' ";
        //             $s++;
        //         }

        //     }

        //     $query->andWhere($fltr);
        // }

        $fltr = '';
        $fltr2 = '';
        $s = 0;
        if (isset($params['columns']))
            foreach ($params['columns'] as $col) {
                if (isset($params['search']) && $params['search']['value'] != '') {
                    $lst[] = $col['data'];
                    if (array_key_exists($col['data'], $this->allField) &&
                        !array_key_exists($col['data'], $lst) && $this->allField[$col['data']] != '') {
                        $fltr .= $s != 0 ? ' or ' : '';
                        $table = '`unit`.';
                
                        $fltr .= $table . $this->allField[$col['data']] . " like '%" . $params['search']['value'] . "%' ";
                        $s++;
                    }
                }
                if ($col['searchable'] == 'true' && $col['search']['value'] != '' &&
                    array_key_exists($col['data'], $this->allField) && $this->allField[$col['data']] != '') {
                    $fltr2 .= $fltr2 == '' ? '' : ' and ';
                        $table = '`unit`.';
                        // $table = in_array($this->allField[$col['data']],['UnitID','UnitNo','Model','UnitLocation','Brand']) ? '`tbl_unit`.': $table;
                        $table = in_array($this->allField[$col['data']],['name']) ? '`category`.' : $table;
                    $fltr2 .= ' ' . $this->allField[$col['data']] . " like '%" . $col['search']['value'] . "%' ";
                    $query->andFilterWhere(['like', $table . $this->allField[$col['data']], $col['search']['value']]);
                }
            }

        $query->andWhere($fltr);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $this->currentData = $query->count();

            $query->limit($lang)->offset($start);
            return $query;
        }

        $this->currentData = $query->count();

        $query->limit($lang)->offset($start);
        return $query;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function searchDataTable($params)
    {
        $data = $this->searchData($params);
        return Json::encode([
            "draw" => isset ($params['draw']) ? intval($params['draw']) : 0,
            "recordsTotal" => intval($this->allData),
            "recordsFiltered" => intval($this->currentData),
            "data" => $this->renderData($data),
        ]);
    }


    /**
     * @param $query Query
     * @return array
     */
    public function renderData($query)
    {
        $out = [];

        /** @var Transaksi $model */
        foreach ($query->all() as $model) {

            $out[] = array_merge(ArrayHelper::toArray($model), [
                // 'actions' => $model->actions,
                'unitID'=>$model->transformer_id,
                'category'=>$model->category->name,
                'manufacture'=>$model->transformer_manufacture,
                'serialNo'=>$model->serial_number,
                'location'=>$model->location,
                'report1'=>$model->report1,
                'report2'=>$model->report2,
                'report3'=>$model->report3,
                'report4'=>$model->report4,
                'report5'=>$model->report5,
            ]);

            

        }
        return $out;
    }

}
