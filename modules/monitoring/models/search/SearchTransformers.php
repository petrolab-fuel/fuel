<?php
/**
 * Created by
 * User: Wisard17
 * Date: 07/02/2018
 * Time: 12.20 PM
 */

namespace app\modules\monitoring\models\search;


use app\modules\report\models\Fuel;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use Yii;

/**
 * Class SearchOilQuality
 * @package app\modules\monitoring\models\search
 */
class SearchTransformers extends Transformer
{
    public $allField = [
        'received_date' => 'received_date',
        'start_analisa' => 'start_analisa',
        'end_analisa' => 'end_analisa',
        'report_date' => 'report_date',
        'sampling_date' => 'sampling_date',
        'lab_number' => 'lab_number',
        'eval_code' => 'eval_code',
        'eval_code_dga' => 'eval_code_dga',
        'eval_code_furan' => 'eval_code_furan',
        'eval_code_oil_quality' => 'eval_code_oil_quality',
        'customer_id' => 'customer_id',
        'unit_id' => 'unit_id',
        'attention_id' => 'attention_id',
        'report_type_id' => 'report_type_id',
        'appearance' => 'appearance',
        'appearance_code' => 'appearance_code',
        'colour' => 'colour',
        'colour_code' => 'colour_code',
        'breakdown_voltage' => 'breakdown_voltage',
        'breakdown_voltage_code' => 'breakdown_voltage_code',
        'water_content' => 'water_content',
        'water_content_code' => 'water_content_code',
        'acidity' => 'acidity',
        'acidity_code' => 'acidity_code',
        'dielectric_dissipation_factor' => 'dielectric_dissipation_factor',
        'dielectric_dissipation_factor_code' => 'dielectric_dissipation_factor_code',
        'resistivity' => 'resistivity',
        'resistivity_code' => 'resistivity_code',
        'inhibitor_content' => 'inhibitor_content',
        'inhibitor_content_code' => 'inhibitor_content_code',
        'passivator_content' => 'passivator_content',
        'passivator_content_code' => 'passivator_content_code',
        'sediment' => 'sediment',
        'sediment_code' => 'sediment_code',
        'interfacial_tension' => 'interfacial_tension',
        'interfacial_tension_code' => 'interfacial_tension_code',
        'corrosive_sulfur' => 'corrosive_sulfur',
        'corrosive_sulfur_code' => 'corrosive_sulfur_code',
        'sludge_content' => 'sludge_content',
        'sludge_content_code' => 'sludge_content_code',
        'kinematic_viscosity' => 'kinematic_viscosity',
        'kinematic_viscosity_code' => 'kinematic_viscosity_code',
        'particles_4' => 'particles_4',
        'particles_6' => 'particles_6',
        'particles_14' => 'particles_14',
        'particles_4_code' => 'particles_4_code',
        'particles_6_code' => 'particles_6_code',
        'particles_14_code' => 'particles_14_code',
        'flash_point' => 'flash_point',
        'flash_point_code' => 'flash_point_code',
        'pcb' => 'pcb',
        'pcb_code' => 'pcb_code',
        'oil_quality_index' => 'oil_quality_index',
        'oil_quality_index_code' => 'oil_quality_index_code',
        'density' => 'density',
        'density_code' => 'density_code',
        'specific_gravity' => 'specific_gravity',
        'specific_gravity_code' => 'specific_gravity_code',
        'metal_fe' => 'metal_fe',
        'metal_cu' => 'metal_cu',
        'metal_al' => 'metal_al',
        'metal_fe_code' => 'metal_fe_code',
        'metal_cu_code' => 'metal_cu_code',
        'metal_al_code' => 'metal_al_code',
        'note_oil_quality' => 'note_oil_quality',
        'recommended1' => 'recommended1',
        'recommended2' => 'recommended2',
        'recommended3' => 'recommended3',
        'hydrogen' => 'hydrogen',
        'hydrogen_code' => 'hydrogen_code',
        'methane' => 'methane',
        'methane_code' => 'methane_code',
        'ethane' => 'ethane',
        'ethane_code' => 'ethane_code',
        'ethylene' => 'ethylene',
        'ethylene_code' => 'ethylene_code',
        'acetylene' => 'acetylene',
        'acetylene_code' => 'acetylene_code',
        'carbon_monoxide' => 'carbon_monoxide',
        'carbon_monoxide_code' => 'carbon_monoxide_code',
        'carbon_dioxide' => 'carbon_dioxide',
        'carbon_dioxide_code' => 'carbon_dioxide_code',
        'oxygen' => 'oxygen',
        'oxygen_code' => 'oxygen_code',
        'nitrogen' => 'nitrogen',
        'nitrogen_code' => 'nitrogen_code',
        'persen_gas_by_volume' => 'persen_gas_by_volume',
        'persen_gas_by_volume_code' => 'persen_gas_by_volume_code',
        'tdcg' => 'tdcg',
        'tdcg_code' => 'tdcg_code',
        'co2_co' => 'co2_co',
        'co2_co_code' => 'co2_co_code',
        'equipment_condition' => 'equipment_condition',
        'equipment_condition_code' => 'equipment_condition_code',
        'furan_5h2f' => 'furan_5h2f',
        'furan_5h2f_code' => 'furan_5h2f_code',
        'furan_2fol' => 'furan_2fol',
        'furan_2fol_code' => 'furan_2fol_code',
        'furan_2fal' => 'furan_2fal',
        'furan_2fal_code' => 'furan_2fal_code',
        'furan_2acf' => 'furan_2acf',
        'furan_2acf_code' => 'furan_2acf_code',
        'furan_5m2f' => 'furan_5m2f',
        'furan_5m2f_code' => 'furan_5m2f_code',
        'furan_total' => 'furan_total',


        'action' => '',
        'serialNumber' => '',
        'reportDate' => 'report_date',
        'sampleDate' => 'sampling_date',
        'receiveDate' => 'received_date',
        'location' => '',
        'attentionName' => '',
        'transformerId' => '',
        'customerName' => '',
        'dgaStatus' => 'eval_code_dga',
        'oilQualityStatus' => 'eval_code_oil_quality',
        'furanStatus' => 'eval_code_furan',
    ];

    public function ordering($params)
    {
        $ncol = isset($params['order'][0]['column']) ? $params['order'][0]['column'] : 0;
        $col = (isset($params['columns'][$ncol]) && array_key_exists($params['columns'][$ncol]['data'], $this->allField)) ?
            $this->allField[$params['columns'][$ncol]['data']] : '';
        $argg = isset($params['order'][0]['dir']) ? $params['order'][0]['dir'] : 'asc';
        $table = self::tableName();
        if (isset($params['columns'][$ncol]['data']) && $params['columns'][$ncol]['data'] == 'serialNumber') {
            $col = 'serial_number';
            $table = 'unit';
        }
        if (isset($params['columns'][$ncol]['data']) && $params['columns'][$ncol]['data'] == 'location') {
            $col = 'location';
            $table = 'unit';
        }
        if (isset($params['columns'][$ncol]['data']) && $params['columns'][$ncol]['data'] == 'attentionName') {
            $col = 'name';
            $table = 'attention';
        }
        if (isset($params['columns'][$ncol]['data']) && $params['columns'][$ncol]['data'] == 'transformerId') {
            $col = 'transformer_id';
            $table = 'unit';
        }
        if (isset($params['columns'][$ncol]['data']) && $params['columns'][$ncol]['data'] == 'customerName') {
            $col = 'name';
            $table = 'customer';
        }

        return $col !== '' ? "$table.$col $argg " : '';
    }


    public $allData;

    public $currentData;

    /**
     * @param Query $query
     * @return int
     */
    public function calcAllData($query)
    {
        return $this->allData == null ? $query->count() : $this->allData;
    }

    /**
     * @param Query $query
     * @param $params
     * @return Query
     */
    public function defaultFilterByUser($query, $params)
    {
        $user = Yii::$app->user->identity;

        if ($user->access_rule_id == 4)
            $query->andWhere([self::tableName() . '.customer_id' => $user->customerId]);
        return $query;
    }

    /**
     * @param $params
     * @param bool $export
     * @param bool $join
     * @return $this|\yii\db\ActiveQuery|Query
     */
    public function searchData($params, $export = false, $join = true)
    {

        $odr = $this->ordering($params);

        $query = self::find()->joinWith(['customer', 'unit', 'attention']);//->where('Package_ID > 123');

        if (!$join)
            $query = self::find();

//        $query->andWhere(['report_type_id' => 1]);

        $query = $this->defaultFilterByUser($query, $params);

        $query->orderBy($odr);

        $start = isset($params['start']) ? $params['start'] : 0;
        $lang = isset($params['length']) ? $params['length'] : 10;

        $this->allData = $this->calcAllData($query);

        $table = self::tableName();

        $fltr = '';

        if (isset($params['columns']))
            foreach ($params['columns'] as $col) {
                if (isset($params['search']) && $params['search']['value'] != '') {
                    $lst[] = $col['data'];
                    if (array_key_exists($col['data'], $this->allField) &&
                        !array_key_exists($col['data'], $lst) && $this->allField[$col['data']] != '') {
                        $fltr .= $fltr != '' ? ' or ' : '';
                        $fltr .= ' `' . $table . '`.' . $this->allField[$col['data']] . " like '%" . $params['search']['value'] . "%' ";
                    }

                    if ($col['data'] == 'serialNumber') {
                        $fltr .= $fltr != '' ? ' or ' : '';
                        $fltr .= ' `unit`.serial_number ' . " like '%" . $params['search']['value'] . "%' ";
                    }
                    if ($col['data'] == 'location') {
                        $fltr .= $fltr != '' ? ' or ' : '';
                        $fltr .= ' `unit`.location ' . " like '%" . $params['search']['value'] . "%' ";
                    }
                    if ($col['data'] == 'transformerId') {
                        $fltr .= $fltr != '' ? ' or ' : '';
                        $fltr .= ' `unit`.transformer_id' . " like '%" . $params['search']['value'] . "%' ";
                    }
                    if ($col['data'] == 'attentionName') {
                        $fltr .= $fltr != '' ? ' or ' : '';
                        $fltr .= ' `attention`.name ' . " like '%" . $params['search']['value'] . "%' ";
                    }
                    if ($col['data'] == 'customerName') {
                        $fltr .= $fltr != '' ? ' or ' : '';
                        $fltr .= ' `customer`.name ' . " like '%" . $params['search']['value'] . "%' ";
                    }
                }
                if ($col['searchable'] == 'true' && $col['search']['value'] != '' &&
                    array_key_exists($col['data'], $this->allField)) {
                    if (in_array($col['data'], ['serialNumber', 'location', 'transformerId', 'attentionName', 'customerName'])) {
                        if ($col['data'] == 'serialNumber') {
                            $query->andFilterWhere(['like', '`unit`.serial_number', $col['search']['value']]);
                        }
                        if ($col['data'] == 'location') {
                            $query->andFilterWhere(['like', '`unit`.location', $col['search']['value']]);
                        }
                        if ($col['data'] == 'transformerId') {
                            $query->andFilterWhere(['like', '`unit`.transformer_id', $col['search']['value']]);
                        }
                        if ($col['data'] == 'attentionName') {
                            $query->andFilterWhere(['like', '`attention`.name', $col['search']['value']]);
                        }
                        if ($col['data'] == 'customerName') {
                            $query->andFilterWhere(['like', '`customer`.name', $col['search']['value']]);
                        }
                    } elseif ($this->allField[$col['data']] != '')
                        $query->andFilterWhere(['like', '`' . $table . '`.' . $this->allField[$col['data']], $col['search']['value']]);
                }

            }

        $query->andWhere($fltr);


        $this->load($params);

        if ($export) {
            return $query;
        }

        $this->currentData = $query->count();

        $query->limit($lang)->offset($start);
        return $query;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function searchDataTable($params)
    {
        $data = $this->searchData($params);
        return Json::encode([
            "draw" => isset ($params['draw']) ? intval($params['draw']) : 0,
            "recordsTotal" => intval($this->allData),
            "recordsFiltered" => intval($this->currentData),
            "data" => $this->renderData($data, $params),
        ]);
    }

    /**
     * @param $query Query
     * @param $params
     * @return array
     */
    public function renderData($query, $params)
    {
        $out = [];

        /** @var self $model */
        foreach ($query->all() as $model) {

            $out[] = array_merge(ArrayHelper::toArray($model), [
                'action' => $model->action,
                'serialNumber' => $model->serialNumber,
                'reportDate' => $model->reportDate,
                'sampleDate' => $model->sampleDate,
                'receiveDate' => $model->receiveDate,
                'location' => $model->location,
                'attentionName' => $model->attentionName,
                'transformerId' => $model->transformerId,
                'customerName' => $model->customerName,
                'dgaStatus' => $model->dgaStatus,
                'oilQualityStatus' => $model->oilQualityStatus,
                'furanStatus' => $model->furanStatus,
            ]);

        }
        return $out;
    }
}