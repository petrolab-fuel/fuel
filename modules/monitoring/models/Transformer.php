<?php
/**
 * Created by
 * User: Wisard17
 * Date: 08/02/2018
 * Time: 08.38 AM
 */

namespace app\modules\monitoring\models;


use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class Transformer
 * @package app\modules\monitoring\models
 *
 * @property string $dgaStatus
 * @property string $action
 * @property string $oilQualityStatus
 * @property string $furanStatus
 */
class Transformer extends DataAnalisa
{

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'attentionName' => Yii::t('app', 'Attention'),
            'customerName' => Yii::t('app', 'Customer'),
            'action' => Yii::t('app', ''),
            'dgaStatus' => Yii::t('app', 'DGA'),
            'oilQualityStatus' => Yii::t('app', 'Oil Quality'),
            'furanStatus' => Yii::t('app', 'Furan'),
        ]);
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return Html::tag('a', '<i class="fa fa-file-pdf-o" style="color: #ff0300;"></i>', [
            'href' => Url::toRoute(['/reports/default/pdf', 'lab_number' => $this->lab_number]),
            'target' => '_blank',
            'data-method' => 'post',
            'class' => 'btn btn-default',
        ]);

//        return Html::tag('div',
//            Html::tag('a', 'Action <span class="caret"></span>', [
//                'class' => "btn btn-default dropdown-toggle", 'data-toggle' => "dropdown", 'aria-expanded' => "false"
//            ]) . Html::tag('ul',
//                Html::tag('li', Html::tag('a', '<i class="fa fa-file-pdf-o"></i> view PDF', [
//                    'href' => Url::toRoute(['/reports/default/pdf', 'lab_number' => $this->lab_number]),
//                    'target' => '_blank',
//                    'data-method' => 'post',
//                ])) .
//                Html::tag('li', Html::tag('a', '<i class="fa fa-caret-square-o-right"></i> view Report', ['href' => Url::toRoute(['/orders/add', 'po_id' => $this->id])])) .
//                Html::tag('li', Html::tag('a', '<i class="fa fa-print"></i> Print', ['href' => Url::toRoute(['/orders/purchase/print', 'id' => $this->id]), 'target' => '_blank'])) .
//                Html::tag('li', Html::tag('a', '<i class="fa fa-edit"></i> Edit', ['href' => Url::toRoute(['/orders/purchase/edit', 'id' => $this->id])])) .
//                Html::tag('li', Html::tag('a', '<i class="fa fa-window-close"></i> Delete',
//                    ['href' => Url::toRoute(['/orders/purchase/del', 'id' => $this->id]),
//                        'data-method' => 'post',
//                        'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
//                    ]))
//
//                , ['class' => "dropdown-menu"]), ['class' => "btn-group"]);
    }

    /**
     * @return string
     */
    public function getDgaStatus()
    {
        $code = $this->eval_code_dga == null ? 'A' : strtoupper($this->eval_code_dga);
        $status = self::allStatus()['dga']['status'];
        $lable = self::allStatus()['dga']['label'];
        $txt = isset($status[$code]) ? $status[$code] : 'Not Run Evalcode';
        $cLabel = isset($lable[$code]) ? $lable[$code] : 'info';
        $listsColor = self::allStatus()['global_color'];
        $color = isset($listsColor[$code]) ? $listsColor[$code] : '#5bc0de';

        return '<span class="label label-' . $cLabel . '" style="background-color:' . $color . '">' . $txt . '</span>';
    }

    /**
     * @return string
     */
    public function getFuranStatus()
    {
        $code = $this->eval_code_furan == null ? 'A' : strtoupper($this->eval_code_furan);
        $status = self::allStatus()['furan']['status'];
        $lable = self::allStatus()['furan']['label'];
        $txt = isset($status[$code]) ? $status[$code] : 'Not Run Evalcode';
        $cLabel = isset($lable[$code]) ? $lable[$code] : 'info';
        $listsColor = self::allStatus()['global_color'];
        $color = isset($listsColor[$code]) ? $listsColor[$code] : '#5bc0de';

        return '<span class="label label-' . $cLabel . '" style="background-color:' . $color . '">' . $txt . '</span>';
    }

    /**
     * @return string
     */
    public function getOilQualityStatus()
    {
        $code = $this->eval_code_oil_quality == null ? 'A' : strtoupper($this->eval_code_oil_quality);
        $status = self::allStatus()['oil_quality']['status'];
        $lable = self::allStatus()['oil_quality']['label'];
        $txt = isset($status[$code]) ? $status[$code] : 'Not Run Evalcode';
        $cLabel = isset($lable[$code]) ? $lable[$code] : 'info';
        $listsColor = self::allStatus()['global_color'];
        $color = isset($listsColor[$code]) ? $listsColor[$code] : '#5bc0de';

        return '<span class="label label-' . $cLabel . '" style="background-color:' . $color . '">' . $txt . '</span>';
    }
}