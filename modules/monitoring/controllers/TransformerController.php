<?php
/**
 * Created by
 * User: Wisard17
 * Date: 08/02/2018
 * Time: 08.37 AM
 */

namespace app\modules\monitoring\controllers;


use app\modules\monitoring\models\search\SearchTransformers;
use yii\web\Controller;
use Yii;

/**
 * Class Transformer
 * @package app\modules\monitoring\controllers
 */
class TransformerController extends Controller
{

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SearchTransformers();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }


        return $this->render('index');
    }
}