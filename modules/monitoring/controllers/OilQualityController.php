<?php
/**
 * Created by
 * User: Wisard17
 * Date: 07/02/2018
 * Time: 11.34 AM
 */

namespace app\modules\monitoring\controllers;


use app\modules\monitoring\models\search\SearchOilQuality;
use Yii;
use yii\web\Controller;

/**
 * Class OilQualityController
 * @package app\modules\monitoring\controllers
 */
class OilQualityController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SearchOilQuality();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }


        return $this->render('index');
    }
}