<?php
/**
 * Created by
 * User: Wisard17
 * Date: 07/02/2018
 * Time: 11.36 AM
 */

namespace app\modules\monitoring\controllers;


use app\modules\monitoring\models\search\SearchDgaFuran;
use Yii;
use yii\web\Controller;

/**
 * Class DgaFuran
 * @package app\modules\monitoring\controllers
 */
class DgaFuranController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SearchDgaFuran();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }


        return $this->render('index');
    }
}