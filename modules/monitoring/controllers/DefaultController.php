<?php

namespace app\modules\monitoring\controllers;

use app\modelsDB\PenentuanCategory;
use Yii;
use yii\web\Controller;
use app\modules\reports\models\Fuel;
use app\modelsDB\DataAnalisa;
use app\modelsDB\Matrix;
/**
 * Default controller for the `monitoring` module
 */
class DefaultController extends Controller
{

	    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
 {
//       $arr=[];

//      for($i=0;$i>10;$i++){
//        $arr[].=[$i];
//      }

    // die;
       // $b=5;
       // $c='>3';
       // $d='$c=$b.$c?"BTL":"SLH";';
       // eval($d);
       // echo $c;
       // die;
          // $model=Matrix::findOne(['parameter_id'=>$id]);
       //    $model=Matrix::find()->all();

       //          // ->joinWith('parameter',true)
       //           // ->where(['parameter.column_analisis_name'=>'distillation_90'])
       //          // ->select(['matrix.*'])
       //          // ->orderBy(['matrix.parameter_id']);
       //          foreach($model as $model){
       //            $cond2=strtoupper($model->condition2);

       //            if (sizeOf(explode('CLEAR',$cond2))>1
                    
       //           }
       // // echo $model['condition2'];
       //  die;
        return $this->render('upload');
    }


    public function actionImport()
    {

        if(!empty($_FILES["excel_file"])) {

            $inputFile = $_FILES['excel_file']['tmp_name'];
            $extension = strtoupper(explode(".", $_FILES['excel_file']['name'])[1]);
            if ($extension == 'XLSX' || $extension == 'XLS' || $extension == 'ODS') {

                //Read spreadsheeet workbook
                try {

                    // PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die($e->getMessage());
                }
                $rowData = array();
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                for ($row = 1; $row < $highestRow; $row++) {

                    //  Read a row of data into an array
                    $rowData = $sheet->toArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                    $stat=FALSE;
                    if (($model = Fuel::findOne(['lab_number' => $rowData[$row][0]])) !== null) {
//                          $app= $rowData[$row][1];
//                          if (sizeOf(explode(':',$app))<2){
//                              $model->appearance =$app;
//                          }
//                          echo $app;
                        $stat=TRUE;
                         }
                        if($stat!==TRUE){
                          $model=new Fuel;
                          if (sizeOf(explode(':', $rowData[$row][0]))<2){
                             $model->lab_number = $rowData[$row][0];
                             }  
                        }
                        
                        if (sizeOf(explode(':', $rowData[$row][1]))<2){
                          $model->appearance = $rowData[$row][1];
                        }
                        if(sizeOf(explode(':', $rowData[$row][2]))<2){
                          $model->density = $rowData[$row][2];
                        }
                        if (sizeOf(explode(':', $rowData[$row][3]))<2){
                          $model->total_acid_number = $rowData[$row][3];
                        }
                        if (sizeOf(explode(':', $rowData[$row][4]))<2){
                          $model->strong_acid = $rowData[$row][4];
                        }
                        if (sizeOf(explode(':', $rowData[$row][5]))<2){
                          $model->calorific_value = $rowData[$row][5];
                        }
                        if (sizeOf(explode(':', $rowData[$row][6]))<2){
                          $model->api_gravity = $rowData[$row][6];
                        }
                        if (sizeOf(explode(':', $rowData[$row][7]))<2){
                          $model->specific_gravity = $rowData[$row][7];
                        }
                        if (sizeOf(explode(':', $rowData[$row][8]))<2){
                          $model->flash_point = $rowData[$row][8];
                        }

                        if (sizeOf(explode(':', $rowData[$row][9]))<2){
                          $model->kinematic_viscosity = $rowData[$row][9];
                        }
                        if (sizeOf(explode(':', $rowData[$row][10]))<2){
                          $model->water_content = $rowData[$row][10];
                        }
                        if (sizeOf(explode(':', $rowData[$row][11]))<2){
                          $model->ash_content = $rowData[$row][11];
                        }
                         if (sizeOf(explode(':', $rowData[$row][12]))<2){
                          $model->cloudn_point = $rowData[$row][12];
                        }
                        if (sizeOf(explode(':', $rowData[$row][13]))<2){
                          $model->pour_point = $rowData[$row][13];
                        }
                        if (sizeOf(explode(':', $rowData[$row][14]))<2){
                          $model->cetane_index = $rowData[$row][14];
                        }
                        if(sizeOf(explode(':', $rowData[$row][15]))<2){
                          $model->conradson_carbon = $rowData[$row][15];
                        }
                        if(sizeOf(explode(':', $rowData[$row][16]))<2){
                          $model->distillation_ibp = $rowData[$row][16];
                        }
                        if(sizeOf(explode(':', $rowData[$row][17]))<2){
                          $model->distillation_5 = $rowData[$row][17];
                        }
                        if(sizeOf(explode(':', $rowData[$row][18]))<2){
                          $model->distillation_10 = $rowData[$row][18];
                        }
                        if (sizeOf(explode(':', $rowData[$row][19]))<2){
                          $model->distillation_20 = $rowData[$row][19];
                        }
                        if (sizeOf(explode(':', $rowData[$row][20]))<2){
                          $model->distillation_30 = $rowData[$row][20];
                        }
                        if (sizeOf(explode(':', $rowData[$row][21]))<2){
                         $model->distillation_40 = $rowData[$row][21];
                        }
                        if (sizeOf(explode(':', $rowData[$row][22]))<2){
                         $model->distillation_50 = $rowData[$row][22];
                        }
                        if (sizeOf(explode(':', $rowData[$row][23]))<2){
                          $model->distillation_60 = $rowData[$row][23];
                        }
                        if (sizeOf(explode(':', $rowData[$row][24]))<2){
                           $model->distillation_70 = $rowData[$row][24];
                        }
                        if (sizeOf(explode(':', $rowData[$row][25]))<2){
                          $model->distillation_80 = $rowData[$row][25];
                        }
                        if (sizeOf(explode(':', $rowData[$row][26]))<2){
                          $model->distillation_90 = $rowData[$row][26];
                        }
                        if (sizeOf(explode(':', $rowData[$row][27]))<2){
                          $model->distillation_95 = $rowData[$row][27];
                        }
                        if (sizeOf(explode(':', $rowData[$row][28]))<2){
                          $model->distillation_ep = $rowData[$row][28];
                        }
                        if (sizeOf(explode(':', $rowData[$row][29]))<2){
                          $model->distillation_recovery = $rowData[$row][29];
                        }
                        if (sizeOf(explode(':', $rowData[$row][30]))<2){
                          $model->distillation_residue = $rowData[$row][30];
                        }
                        if (sizeOf(explode(':', $rowData[$row][31]))<2){
                          $model->destillation_loss = $rowData[$row][31];
                        }
                        if (sizeOf(explode(':', $rowData[$row][32]))<2){
                          $model->destillation_recovery_300 = $rowData[$row][32];
                        }
                        if (sizeOf(explode(':', $rowData[$row][33]))<2){
                          $model->sulphur_content = $rowData[$row][33];

                        }
                        if (sizeOf(explode(':', $rowData[$row][34]))<2){
                          $model->rancimat = $rowData[$row][34];

                        }
                        if (sizeOf(explode(':', $rowData[$row][35]))<2){
                          $model->lubricity_of_diesel = $rowData[$row][35];

                        }
                         if (sizeOf(explode(':', $rowData[$row][36]))<2){
                          $model->fame_content = $rowData[$row][36];
                        }
                        if (sizeOf(explode(':', $rowData[$row][37]))<2){
                          $model->cerosine_content = $rowData[$row][37];
                        }
                        
                        
                        if (sizeOf(explode(':', $rowData[$row][38]))<2){
                          $model->sediment = $rowData[$row][38];
                        }
                        if (sizeOf(explode(':', $rowData[$row][39]))<2){
                          $model->colour = $rowData[$row][39];
                        }
                        if (sizeOf(explode(':', $rowData[$row][40]))<2){
                          $model->copper_corrosion = $rowData[$row][40];
                        }
                        if (sizeOf(explode(':', $rowData[$row][41]))<2){
                          $model->particles_4 = $rowData[$row][41];
                        }
                        if (sizeOf(explode(':', $rowData[$row][42]))<2){
                          $model->particles_6 = $rowData[$row][42];
                        }
                       
                       
                        if (sizeOf(explode(':', $rowData[$row][43]))<2){
                          $model->particles_14 = $rowData[$row][43];
                        }
                        if (sizeOf(explode(':', $rowData[$row][44]))<2){
                          $model->iso_4406 = $rowData[$row][44];
                        }
                        if (sizeOf(explode(':', $rowData[$row][45]))<2){
                          $model->recommended1 = $rowData[$row][45];
                        }
                        $model->save(false);
                 
                    //Insert into database


                }
                echo 'success';

            }
        }
    }
    public function actionUnduh() 
    { 
        $path = Yii::getAlias('@arsipdir').'/import.xlsx';
        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path);
        }
    }

                    
}
