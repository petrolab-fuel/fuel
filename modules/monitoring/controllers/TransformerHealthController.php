<?php
/**
 * Created by
 * User: doni46
 * Date: 02/08/2018
 * Time: 11.33 AM
 */

namespace app\modules\monitoring\controllers;


use app\modules\monitoring\models\search\SearchTransformerHealth;
use Yii;
use yii\web\Controller;

/**
 * Class OilQualityController
 * @package app\modules\monitoring\controllers
 */
class TransformerHealthController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SearchTransformerHealth();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }


        return $this->render('index');
    }
}