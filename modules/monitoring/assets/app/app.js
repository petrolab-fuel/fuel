  
 $(document).ready(function(){
    function alert_notif(res) {
        $.sound_on = false;
        var color = res.status === 'success' ? '#659265' : '#C46A69';
        var icon = res.status === 'success' ? 'fa-check' : 'fa-times';
        $.smallBox({
            title: res.status,
            content: res.message,
            color: color,
            iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
            timeout: 4000
        });
    }

                  
          $('#import_excel').on('submit', function(event){  
                event.preventDefault();  
           $.ajax({  
                url:base_url+'/monitoring/default/import',  
                method:'POST',  
                data:new FormData(this),  
                contentType:false,  
                cache:false,
                processData:false,  
                beforeSend:function(){
                    // $('#submit').attr('disabled','disabled'),
                    $('#submit').val('Importing...');
                  },
                success:function(data){

                    alert_notif({status: "success", message: 'Berhasil import data'});
                     $('#submit').attr('disabled',false);
                }  
           });      
          });

    $('#test').select2({

    });
       
    });