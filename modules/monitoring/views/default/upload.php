<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use app\smartadmin\Alert;

$this->title = 'Import Data';
$this->params['breadcrumbs'][] = 'Monitoring';
$this->params['breadcrumbs'][] = $this->title;
  app\smartadmin\assets\plugins\PluginsAssets::register($this);
  app\modules\monitoring\assets\ImportAsset::register($this);

?>


<?= Alert::widget() ?>

<div class="row">
	<!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-3" data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
        
                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"
        
                        -->
                        <header>
                            <span class="widget-icon"> <i class="glyphicon glyphicon-import"></i> </span>
                            <h2>Import Data Analisis </h2>
        
                        </header>
        
                        <!-- widget div-->
                        <div>
        
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
        
                            </div>
                            <!-- end widget edit box -->
        
                            <!-- widget content -->
                            <div class="widget-body no-padding">


                            	<form action ="" class="smart-form" method="post" id="import_excel">
                                    <fieldset>
                                        
                                        <section class="col col-6">
											<label class="label">File input</label>
											<div class="input input-file">
												<input type="file" id="excel_file" name="excel_file" enctype="multipart/form-data"/>
											</div>
										</section>
                                        <section class="col col-6">
                                            <label class="label">&nbsp;</label>
                                            <?= Html::a('<i class="fa fa-download"></i> Template', ['default/unduh'], ['class' => 'btn btn-default btn-sm', 'type' => "button"])?>
                                        </section>
                                    </fieldset>
                                    <footer>
                                        <?= Html::button('Import', ['id'=>'submit','name'=>'submit','class' => 'btn btn-primary', 'type' => "submit"])?>
                                  </footer>
                                </form>

                        	</div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

        </article>
        <!-- WIDGET END -->
</div>
