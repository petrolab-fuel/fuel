<?php

namespace app\modules\ajax\controllers;

class NotifyController extends \yii\web\Controller
{

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionMsgs()
    {
        return $this->renderPartial('msgs');
    }

    public function actionNotification()
    {
        return $this->renderPartial('notification');
    }

    public function actionTask()
    {
        return $this->renderPartial('task');
    }

}
