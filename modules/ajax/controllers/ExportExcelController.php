<?php
/**
 * Created by PhpStorm.
 * User: Wisard17
 * Date: 9/8/2017
 * Time: 11:00 AM
 */

namespace app\modules\ajax\controllers;


use Yii;
use app\models\Excel;
use app\modules\reports\models\search\SearchFuel;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Class ExportExcelController
 *
 * @package app\modules\ajax\controllers
 */
class ExportExcelController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],

                    // ...
                ],
            ],
        ];
    }

    public function actionIndex()
    {

        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', 300);

        $searchModel = new SearchFuel();
        $request = Yii::$app->request->get();

        $query = $searchModel->searchData($request, true, false);

        if ($request == null)
            $query->limit(1000);

       $data = $query->select(['data_analisa.lab_number'=>'data_analisa.lab_number',
//           'data_analisa.lab_number',
           'data_analisa.spb'=>'data_analisa.spb',
           'data_analisa.po'=>'data_analisa.po',
           'data_analisa.Customer_id'=>'data_analisa.Customer_id',
           'customer.name'=>'customer.name',
           'attention.name as attention'=>'attention.name as attention',
           'customer.address'=>'customer.address',
          'data_analisa.eng_builder'=>'data_analisa.eng_builder',
           'data_analisa.eng_type'=> 'data_analisa.eng_type',
           'data_analisa.eng_sn'=>'data_analisa.eng_sn',
           'data_analisa.eng_location'=>'data_analisa.eng_location',
           'data_analisa.compartment as sampling_location'=>'data_analisa.compartment as sampling_location',
           'data_analisa.sludge_content as type'=>'data_analisa.sludge_content as type',
           'date_format(data_analisa.sampling_date,"%d-%m-%Y")as smpl_date'=>'date_format(data_analisa.sampling_date,"%d-%m-%Y")as smpl_date',
           'date_format(data_analisa.received_date,"%d-%m-%Y")as recv_date'=>'date_format(data_analisa.received_date,"%d-%m-%Y")as recv_date',
           'date_format(data_analisa.report_date,"%d-%m-%Y")as rpt_date'=>'date_format(data_analisa.report_date,"%d-%m-%Y")as rpt_date',
           'data_analisa.harga'=>'data_analisa.harga',
           'data_analisa.appearance'=>'data_analisa.appearance',
           'data_analisa.density'=>'data_analisa.density',
           'data_analisa.total_acid_number as TAN'=>'data_analisa.total_acid_number as TAN',
           'data_analisa.flash_point'=>'data_analisa.flash_point',
           'data_analisa.kinematic_viscosity'=>'data_analisa.kinematic_viscosity',
           'data_analisa.water_content'=>'data_analisa.water_content',
           'data_analisa.ash_content'=>'data_analisa.ash_content',
           'data_analisa.pour_point'=>'data_analisa.pour_point',
           'data_analisa.cetane_index'=> 'data_analisa.cetane_index',
           'data_analisa.conradson_carbon'=>'data_analisa.conradson_carbon',
           'data_analisa.distillation_recov'=>'data_analisa.distillation_recov',
            'data_analisa.distillation_ibp'=>'data_analisa.distillation_ibp',
            'data_analisa.distillation_5'=>'data_analisa.distillation_5',
            'data_analisa.distillation_10'=>'data_analisa.distillation_10',
            'data_analisa.distillation_20'=>'data_analisa.distillation_20',
            'data_analisa.distillation_30'=>'data_analisa.distillation_30',
            'data_analisa.distillation_40'=>'data_analisa.distillation_40',
            'data_analisa.distillation_50'=>'data_analisa.distillation_50',
            'data_analisa.distillation_60'=>'data_analisa.distillation_60',
            'data_analisa.distillation_70'=>'data_analisa.distillation_70',
            'data_analisa.distillation_80'=>'data_analisa.distillation_80',
            'data_analisa.distillation_90'=>'data_analisa.distillation_90',
            'data_analisa.distillation_95'=>'data_analisa.distillation_95',
            'data_analisa.distillation_ep'=>'data_analisa.distillation_ep',
            'data_analisa.distillation_recovery'=>'data_analisa.distillation_recovery',
            'data_analisa.distillation_residue'=>'data_analisa.distillation_residue',
            'data_analisa.distillation_loss'=>'data_analisa.distillation_loss',
            'data_analisa.distillation_recovery_300'=>'data_analisa.distillation_recovery_300',
            'data_analisa.sulphur_content'=>'data_analisa.sulphur_content',
           'data_analisa.sediment'=>'data_analisa.sediment',
            'data_analisa.colour'=>'data_analisa.colour',
            'data_analisa.copper_corrosion'=>'data_analisa.copper_corrosion',
            'data_analisa.particles_4'=>'data_analisa.particles_4',
            'data_analisa.particles_6'=>'data_analisa.particles_6',
            'data_analisa.particles_14'=>'data_analisa.particles_14',
            'data_analisa.iso_4406'=>'data_analisa.iso_4406',
            'data_analisa.specific_gravity'=>'data_analisa.specific_gravity',
            'data_analisa.api_gravity'=>'data_analisa.api_gravity',
            'data_analisa.cloudn_point'=>'data_analisa.cloudn_point',
            'data_analisa.rancimat'=>'data_analisa.rancimat',
            'data_analisa.lubricity_of_diesel'=>'data_analisa.lubricity_of_diesel',
            'data_analisa.fame_content'=>'data_analisa.fame_content',
            'data_analisa.calorific_value'=>'data_analisa.calorific_value',
            'data_analisa.strong_acid as SAN'=>'data_analisa.strong_acid as SAN',
            'data_analisa.cerosine_content'=>'data_analisa.cerosine_content',
            'data_analisa.recommended1 as recommended'=>'data_analisa.recommended1 as recommended'
            // 'tbl_transaction.Lab_No',
            // 'tbl_transaction.unit_id',
            // 'tbl_transaction.UNIT_NO',
            // 'tbl_transaction.branch',
            // 'tbl_transaction.name',
            // 'tbl_transaction.address',
            // 'tbl_transaction.ComponentID',
            // 'tbl_transaction.COMPONENT',
            // 'tbl_transaction.MODEL',
            // 'tbl_transaction.ORIG_VISC',
            // 'tbl_transaction.OIL_TYPE',
            // 'tbl_transaction.SAMPL_DT1',
            // 'tbl_transaction.RECV_DT1',
            // 'tbl_transaction.RPT_DT1',
            // 'tbl_transaction.OIL_MATRIX',
            // 'tbl_transaction.HRS_KM_OH',
            // 'tbl_transaction.HRS_KM_OC',
            // 'tbl_transaction.HRS_KM_TOT',
            // 'tbl_transaction.VISC_CST',
            // 'tbl_transaction.CST_CODE',
            // 'tbl_transaction.visc_40',
            // 'tbl_transaction.visc_40_code',
            // 'tbl_transaction.VISC_SAE',
            // 'tbl_transaction.SAE_CODE',
            // 'tbl_transaction.T_A_N',
            // 'tbl_transaction.TAN_CODE',
            // 'tbl_transaction.T_B_N',
            // 'tbl_transaction.TBN_CODE',
            // 'tbl_transaction.SILVER',
            // 'tbl_transaction.AG_CODE',
            // 'tbl_transaction.SILICON',
            // 'tbl_transaction.SI_CODE',
            // 'tbl_transaction.MAGNESIUM',
            // 'tbl_transaction.MG_CODE',
            // 'tbl_transaction.SODIUM',
            // 'tbl_transaction.NA_CODE',
            // 'tbl_transaction.CALCIUM',
            // 'tbl_transaction.CA_CODE',
            // 'tbl_transaction.ZINC',
            // 'tbl_transaction.ZN_CODE',
            // 'tbl_transaction.NICKEL',
            // 'tbl_transaction.NI_CODE',
            // 'tbl_transaction.IRON',
            // 'tbl_transaction.FE_CODE',
            // 'tbl_transaction.COPPER',
            // 'tbl_transaction.CU_CODE',
            // 'tbl_transaction.ALUMINIUM',
            // 'tbl_transaction.AL_CODE',
            // 'tbl_transaction.CHROMIUM',
            // 'tbl_transaction.CR_CODE',
            // 'tbl_transaction.TIN',
            // 'tbl_transaction.SN_CODE',
            // 'tbl_transaction.LEAD',
            // 'tbl_transaction.PB_CODE',
            // 'tbl_transaction.DILUTION',
            // 'tbl_transaction.DILUT_CODE',
            // 'tbl_transaction.DIR_TRANS',
            // 'tbl_transaction.TRANS_CODE',
            // 'tbl_transaction.OXIDATION',
            // 'tbl_transaction.OXID_CODE',
            // 'tbl_transaction.NITRATION',
            // 'tbl_transaction.NITR_CODE',
            // 'tbl_transaction.WATER',
            // 'tbl_transaction.WTR_CODE',
            // 'tbl_transaction.GLYCOL',
            // 'tbl_transaction.GLY_CODE',
            // 'tbl_transaction.SOX',
            // 'tbl_transaction.SOX_CODE',
            // 'tbl_transaction.EVAL_CODE',
            // 'tbl_transaction.MATRIX',
            // 'tbl_transaction.Molybdenum',
            // 'tbl_transaction.Molybdenum_CODE',
            // 'tbl_transaction.Boron',
            // 'tbl_transaction.Boron_CODE',
            // 'tbl_transaction.Potassium',
            // 'tbl_transaction.Potassium_CODE',
            // 'tbl_transaction.Barium',
            // 'tbl_transaction.Barium_CODE',
            // 'tbl_transaction.ISO4406',
            // 'tbl_transaction.ISO4406_CODE',
            // 'tbl_transaction.PQIndex',
            // 'tbl_transaction.PQIndex_CODE',
            // 'tbl_transaction.colourcode',
            // 'tbl_transaction.phosphor',
            // 'tbl_transaction.sulphur',
            // 'tbl_transaction.RECOMM1',
            // 'tbl_transaction.RECOMM2',
            // 'tbl_transaction.oil_change',
            // 'tbl_transaction.4um',
            // 'tbl_transaction.4um_code',
            // 'tbl_transaction.6um',
            // 'tbl_transaction.6um_code',
            // 'tbl_transaction.15um',
            // 'tbl_transaction.15um_code',
            // 'tbl_transaction.seq_I',
            // 'tbl_transaction.seq_I_code',
            // 'tbl_transaction.seq_II',
            // 'tbl_transaction.seq_II_code',
            // 'tbl_transaction.seq_III',
            // 'tbl_transaction.seq_III_code',
            // 'tbl_transaction.KarlFischer',
            // 'tbl_transaction.customer_id',
       ])->leftjoin('customer','data_analisa.customer_id=customer.customer_id')->leftjoin('attention','data_analisa.attention_id=attention.id')->distinct()->asArray()->all();
        $e = new Excel();

        $p = $e->genExcelByArray($data, 'A1', 'sheet', true);

        $fileName = 'fuel_export_excel_' . date('d-m-Y_H-i-s');
        $e->download($p, $fileName);
    }
    public function actionPdf(){
        $searchModel = new SearchFuel();
        $request = Yii::$app->request->get();

        $query = $searchModel->searchData($request, true, false);
         $data = $query->select(['data_analisa.lab_number'])->distinct()->asArray()->all();

        echo $data;
        
    }
}