<?php
namespace app\modules\reports\helpers;
use Yii;
/**
* 
*/
class Parameter
{
	public static function codeImg($code){
		if(strtoupper($code)=='A'){
			return "reports/e_code/a.png";
		}elseif(strtoupper($code)=='B'){
			return "reports/e_code/b.png";	
		}
		elseif(strtoupper($code)=='C'){
			return "reports/e_code/c.png";	
		}
		elseif(strtoupper($code)=='D'){
			return "reports/e_code/d.png";	
		}
		elseif(strtoupper($code)=='E'){
			return "reports/e_code/e.png";	
		}else{
			return "reports/e_code/a.png";	
		}
	}
	public static function codeStatus($code){
		if(strtoupper($code)=='A'){
			return "Normal";
		}elseif(strtoupper($code)=='B'){
			return "Attention";	
		}
		elseif(strtoupper($code)=='C'){
			return "Urgent";	
		}
		elseif(strtoupper($code)=='D'){
			return "Urgent";	
		}
		elseif(strtoupper($code)=='E'){
			return "Urgent";		
		}else{
			return "Normal";
		}
	}
	public static function codeStatusFuran($code){
		if(strtoupper($code)=='A'){
			return "Normal";
		}elseif(strtoupper($code)=='B'){
			return "Accelerated";	
		}
		elseif(strtoupper($code)=='C'){
			return "Excessive";	
		}
		elseif(strtoupper($code)=='D'){
			return "High Risk";	
		}
		elseif(strtoupper($code)=='E'){
			return "End of Expected";		
		}else{
			return "Normal";
		}
	}
	public static function codeStatusDga($code){
		if(strtoupper($code)=='A'){
			return "Cond. 1";
		}elseif(strtoupper($code)=='B'){
			return "Cond. 2";	
		}
		elseif(strtoupper($code)=='C'){
			return "Cond. 3";	
		}
		elseif(strtoupper($code)=='D'){
			return "Cond. 4";	
		}else{
			return "Cond. 1";
		}
	}
	public static function colorCode($code){
		
		if(strtoupper($code)=='A'){
			
		}elseif(strtoupper($code)=='B'){
			return "font color:#ffcc00;";
		}elseif(strtoupper($code)=='C'){
			return "font color:#ff0000;";
		}elseif(strtoupper($code)=='D'){
			return "font color:#ff0000;";
		}elseif(strtoupper($code)=='E'){
			return "background:#ff0000;";
		}
	}
	public static function matric($code){
		
		if($code){
			return $code;
		}else{
			return '-';
		}
	}
	public static function cS($var){
		
		if($var){
			return str_replace(" ", "&nbsp;", $var);
		}else{
			return '-';
		}
	}
}