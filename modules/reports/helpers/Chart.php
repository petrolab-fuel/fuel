<?php
namespace app\modules\reports\helpers;
use Yii;
use app\modules\reports\helpers\Filter;
/**
* 
*/
class Chart
{

/**
* chart oil analysis
*/
public function makeChartOa($c,$datas){

	$labels=[];
	$dataset=[];
	foreach ($datas as $key => $value) {
		array_push($labels, date('d-M-y',strtotime($value['report_date'])));
		array_push($dataset, Filter::clearNumber($value['breakdown_voltage']));
	}
	$title="Breakdown Voltage";
	$c->setPlotArea(25, 25, 325, 100, 0xffffff, -1, -1, 0xc0c0c0, -1);
	$c->addTitle2(Top, $title, "arialbd.ttf", 10, 0xffffff, 0x31319c);
	$c->xAxis->setLabels($labels);
	$c->xAxis->setLabelStyle("Arial Bold", 8, "0xcc6600");
	$c->yAxis->setLabelStyle("Arial Bold", 8, "0xcc6600");
	$layer = $c->addLineLayer();
	$dataSetObj = $layer->addDataSet($dataset, 0x0033FF, "onde");
	$dataSetObj->setDataSymbol('4', 9);
	$c->makeChart(__DIR__.Yii::$app->HM->urlChart("oa/chart.png"));
}
/**
* chart oil analysis
*/
public function makeChartWc($c,$datas){

	$labels=[];
	$dataset=[];
	foreach ($datas as $key => $value) {
		array_push($labels, date('d-M-y',strtotime($value['report_date'])));
		array_push($dataset, Filter::clearNumber($value['water_content']));
	}
	$title="Water Content";
	$c->setPlotArea(25, 25, 325, 100, 0xffffff, -1, -1, 0xc0c0c0, -1);
	$c->addTitle2(Top, $title, "arialbd.ttf", 10, 0xffffff, 0x31319c);
	$c->xAxis->setLabels($labels);
	$c->xAxis->setLabelStyle("Arial Bold", 8, "0xcc6600");
	$c->yAxis->setLabelStyle("Arial Bold", 8, "0xcc6600");
	$layer = $c->addLineLayer();
	$dataSetObj = $layer->addDataSet($dataset, 0x0033FF, "onde");
	$dataSetObj->setDataSymbol('2', 9);
	$c->makeChart(__DIR__.Yii::$app->HM->urlChart("wc/chart.png"));
}
	/**
* chart dga
*/
public function makeChartDga($c,$datas){
$smbl=8;
	$labels=[];
	$hidrogen=[];
	$methane=[];
	$ethane=[];
	$ethylene=[];
	$acetylene=[];
	$carbon_monoxide=[];
	foreach ($datas as $key => $value) {
		array_push($labels, date('d-M-y',strtotime($value['report_date'])));
		array_push($hidrogen, Filter::clearNumber($value['hydrogen']));
		array_push($methane, Filter::clearNumber($value['methane']));
		array_push($ethane, Filter::clearNumber($value['ethane']));
		array_push($ethylene, Filter::clearNumber($value['ethylene']));
		array_push($acetylene, Filter::clearNumber($value['acetylene']));
		array_push($carbon_monoxide, Filter::clearNumber($value['carbon_monoxide']));
	}
	$title="Dissolved Gas";
	$c->setPlotArea(25, 40, 670, 125, 0xffffff, -1, -1, 0xc0c0c0, -1);
	$c->addTitle2(Top, $title, "arialbd.ttf", 10, 0xffffff, 0x31319c);
	$c->xAxis->setLabels($labels);
	$c->xAxis->setLabelStyle("Arial Bold", 8, "0xcc6600");
	$c->yAxis->setLabelStyle("Arial Bold", 8, "0xcc6600");
	$layer = $c->addLineLayer();

	$legendObj = $c->addLegend(30, 14, false, '4', $smbl);
	$legendObj->setBackground(Transparent);

	$dataSetObj = $layer->addDataSet($hidrogen, 0xff0000, "H2");
	$dataSetObj->setDataSymbol('1', $smbl);

	$dataSetObj = $layer->addDataSet($methane, 0xffff00, "CH4");
	$dataSetObj->setDataSymbol('2', $smbl);

	$dataSetObj = $layer->addDataSet($ethane, 0x009900, "C2H6");
	$dataSetObj->setDataSymbol('3', $smbl);

	$dataSetObj = $layer->addDataSet($ethylene, 0x3333cc, "C2H2");
	$dataSetObj->setDataSymbol('4', $smbl);

	$dataSetObj = $layer->addDataSet($carbon_monoxide, 0x990033, "CO");
	$dataSetObj->setDataSymbol('5', $smbl);

	$c->makeChart(__DIR__.Yii::$app->HM->urlChart("dga/chart.png"));
}
	/**
* chart furan
*/
public function makeChartFuran($c,$datas){
$smbl=8;
	$labels=[];
	$furan_5h2f=[];
	$furan_2fol=[];
	$furan_2fal=[];
	$furan_2acf=[];
	$furan_5m2f=[];
	$furan_total=[];
	foreach ($datas as $key => $value) {
		array_push($labels, date('d-M-y',strtotime($value['report_date'])));
		array_push($furan_5h2f, Filter::clearNumber($value['furan_5h2f'])*1000);
		array_push($furan_2fol, Filter::clearNumber($value['furan_2fol'])*1000);
		array_push($furan_2fal, Filter::clearNumber($value['furan_2fal'])*1000);
		array_push($furan_2acf, Filter::clearNumber($value['furan_2acf'])*1000);
		array_push($furan_5m2f, Filter::clearNumber($value['furan_5m2f'])*1000);
		array_push($furan_total, Filter::clearNumber($value['furan_total'])*1000);
	}
	$title="Furan Analysis";
	$c->setPlotArea(25, 40, 670, 150, 0xffffff, -1, -1, 0xc0c0c0, -1);
	$c->addTitle2(Top, $title, "arialbd.ttf", 10, 0xffffff, 0x31319c);
	$c->xAxis->setLabels($labels);
	$c->xAxis->setLabelStyle("Arial Bold", 8, "0xcc6600");
	$c->yAxis->setLabelStyle("Arial Bold", 8, "0xcc6600");
	$layer = $c->addLineLayer();

	$legendObj = $c->addLegend(30, 14, false, '4', $smbl);
	$legendObj->setBackground(Transparent);

	$dataSetObj = $layer->addDataSet($furan_5h2f, 0xff0000, "5H2F");
	$dataSetObj->setDataSymbol('1', $smbl);

	$dataSetObj = $layer->addDataSet($furan_2fol, 0xffff00, "2Fol");
	$dataSetObj->setDataSymbol('2', $smbl);

	$dataSetObj = $layer->addDataSet($furan_2fal, 0x009900, "2Fal");
	$dataSetObj->setDataSymbol('3', $smbl);

	$dataSetObj = $layer->addDataSet($furan_2acf, 0x3333cc, "2Acf");
	$dataSetObj->setDataSymbol('4', $smbl);

	$dataSetObj = $layer->addDataSet($furan_5m2f, 0x990033, "5M2F");
	$dataSetObj->setDataSymbol('5', $smbl);

	$dataSetObj = $layer->addDataSet($furan_total, 0x990033, "Furan Total");
	$dataSetObj->setDataSymbol('6', $smbl);

	$c->makeChart(__DIR__.Yii::$app->HM->urlChart("furan/chart.png"));
}

/**
* delete all image
*/
public function deleteAllChart($dir){
	$file=glob(__DIR__.Yii::$app->HM->urlChartRun().$dir."/*");
	if($file){
		foreach ($file as $key => $value) {
			unlink($value);
		}
	}
}

}