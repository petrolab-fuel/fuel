<?php
namespace app\modules\reports\helpers;
use Yii;
/**
* 
*/
class Filter
{
	public static function clearNumber($number){
		if($number){
			$data=str_replace("<", "", $number);
			$data=str_replace(">", "", $data);
			$data=str_replace(" ", "", $data);
			$data=str_replace(",", ".", $data);
			return $data;
		}else{
			return 0;
		}
	}
	public static function resDate($date,$res){
		if($date && $res){
			$data=strtotime('d-m-Y',strtotime("($res*30) days", strtotime($date)));
			return $data;
		}else{
			return '';
		}
	}
}