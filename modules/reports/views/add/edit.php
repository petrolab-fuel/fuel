<?php
use yii\helpers\Url;

$this->title = Yii::t('app', 'Edit Data Analisa ');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Fuel'), 'url' => Url::toRoute(['/reports/fuel'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
    <div class="row">
    <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
                       <div class="jarviswidget jarviswidget-color-darken" id="wid-id-3" data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
        
                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"
        
                        -->
                        
                        <header>
                              <span class="widget-icon"> <i class="glyphicon glyphicon-book"></i> </span>
                            <h2>Edit Data Analisa </h2>
                        </header>
                
                    <!-- widget div-->
                    <div>
        
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
        
                            <!-- widget content -->
                            <div class="widget-body no-padding">
                                 <?= $this->render('_form',['model'=>$model,]) ?>
                                  
                            </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

        </article>
        <!-- WIDGET END -->
    </div>
</section>