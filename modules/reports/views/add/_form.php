
<?php
use app\smartadmin\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
// app\smartadmin\assets\plugins\PluginsAssets::register($this);
// app\modules\reports\assets\Analisa::register($this);

$form = ActiveForm::begin([

        'fieldConfig' => [
             // 'template' => "{label}<label class='input'>{input}</label>{error}",
             // 'inputOptions' => ['class' => 'form-control'],
         ],
         'options' => [
               'class' => 'smart-form',
               'id'=>'form-input',
               'enableAjaxValidation'=>true,
         ],

    ]);
    $a=Yii::$app->request->post('customer_id');
    $b=Yii::$app->request->post('attention_id');
$jsScript = <<< JS
    $(function(){

        // getlab();
        function getlab(){
            $.ajax({
                url: base_url+"/reports/add/lab",
                // data: {value: value, obj: obj},
                type: "POST",
                    success: function(data) {
                        // alert(data);
                    $("#lub_number").val(data);
                }
            });
        }

        function alert_notif(res) {
        $.sound_on = false;
        var color = res.status === 'success' ? '#659265' : '#C46A69';
        var icon = res.status === 'success' ? 'fa-check' : 'fa-times';
        $.smallBox({
            title: res.status,
            content: res.message,
            color: color,
            iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
            timeout: 4000
        });
    }
    $("#customer_id").on("change", function() {
    // alert('ee');

    var value = $(this).val(),
          obj = $(this).attr("id");
        // next = $(this).attr("data-next");
    // alert(next);
    $.ajax({
         url: base_url+"/reports/add/get",
         data: {value: value, obj: obj},
         type: "POST",
         success: function(data) {
             $("#att_id").html(data);
         }
    });
});

// $("#att_id").on("change",function () {
//     $("#alamat").val("");
//     var value = $("#customer_id").val(),
//         obj= $(this).attr("id");
//     $.ajax({
//         url: base_url + "/reports/add/get",
//         data: {value: value, obj: obj},
//         dataType:"JSON",
//         type: "POST",
//         success: function (data) {
//             $("#alamat").val(data.address);
//         }
//     });
// });

     
  // $("#btn").on("click",function(e){
  //    e.preventDefault; 
  //    var form=$("#form-input");
  //    $.ajax({
  //               type: form.attr("method"),
  //                url: form.attr("action"),
  //                data: form.serialize(),
  //                success: function (data) {
  //                   // $('#dialog_simple').dialog('open');
  //                 // alert_notif({status: "success", message: 'Save Success'});
  //                   alert('SUccess Add Data');
                        
  //                 },
  //                error:function(xhr){
  //                   alert("failure"+xhr.readyState+this.url);
  //                } 
  //        }) 
  //        return false;
  // });
    
    // $("#form-input").on("beforeSubmit", function () {

    //      var form=$(this);
    //         $.ajax({
    //             type: form.attr("method"),
    //              url: form.attr("action"),
    //              data: form.serialize(),
    //              success: function (data) {
    //              alert('succeess save');
    //              getlab();
    //               },
    //              error:function(xhr){
    //                 alert("failure"+xhr.readyState+this.url);
    //              } 
    //         }); 

    //     }).on('submit', function(e){
    //             e.preventDefault();
    //     });


  $("#sampling_date").datepicker({
                dateFormat: 'dd-mm-yy',
                changeMonth: true,
                changeYear: true,
                
                numberOfMonths: 1,
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
               
        
            });

$("#received_date").datepicker({
   dateFormat: 'dd-mm-yy',
                changeMonth: true,
                changeYear: true,
                
                numberOfMonths: 1,
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
               
        
            });

$("#report_date").datepicker({
   dateFormat: 'dd-mm-yy',
                changeMonth: true,
                changeYear: true,
                
                numberOfMonths: 1,
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
               
        
            });

     $('#dialog_simple').dialog({
            autoOpen : false,
            width : 600,
            resizable : false,
            modal : true,
            title : "<div class='widget-header'><h4><i class='fa fa-warning'></i> Empty the recycle bin?</h4></div>",
            buttons : [{
                html : "<i class='fa fa-trash-o'></i>&nbsp; Delete all items",
                "class" : "btn btn-danger",
                click : function() {
                    $(this).dialog("close");
                }
            }, {
                html : "<i class='fa fa-times'></i>&nbsp; Cancel",
                "class" : "btn btn-default",
                click : function() {
                    $(this).dialog("close");
                }
            }]
        });       
  });
JS;
$this->registerJs($jsScript);      
?>
    <ul class="nav nav-tabs tabs-pull-left">          
            <li class="active">
                <a href="#a1" data-toggle="tab">Informasi</a>
            </li>
           
            <li>
                <a href="#a2" data-toggle="tab">Parameter</a>
            </li>
    </ul>
        <fieldset>
            <div class="tab-content no-padding">
                <div class="tab-pane fade in active" id="a1">
                     <section >
                        <label>Lab No</label>
                        <?= $form->field($model, 'lab_number')->textInput(['id'=>'lub_number','readonly'=>true])->label(false)?>
                        </section>
                    <section>
                        <label>Customer</label>
                        <?= $form->field($model,'customer_id')->dropDownList($model->custArray,[
                         'class'=>'select2','style'=>'width:100%',
                        'prompt'=>'select..',
                        'id'=>'customer_id',
                          // 'name'=>'customer_id',
                         // 'value'=>$model->isNewRecord?$a:$model->customer_id,
                        // 'options'=>$model->customer_id,
                        // 'selected'=>true,
                        // 'autofocus' => true,
                         // 'class' => 'dependent-input form-control',
                        'data-next'=>'att_id'
                        ])->label(false)?>
                    </section>
                    <section >
                        <label>Attentions</label>
                        <?= $form->field($model,'attention_id')->dropDownList($model->attArray,
                        ['prompt'=>'select..',
                            'id'=>'att_id',
                            'class'=>'select2','style'=>'width:100%',
                            // 'name'=>'attention_id',
                            // 'value'=>$model->isNewRecord?$b:$model->attention_id,
                            'data-next'=>'alamat',
                            'style'=>'width:100%'])->label(false)?>
                    </section>
                    <div class="row">
                         <section class="col col-3">
                            <label>Sampling Point / Sample Name</label>
                             <?= $form->field($model, 'sludge_content')->dropDownlist(['FUEL'=>'FUEL','FAME'=>'FAME'])->label(false)?>
                        </section>
                        <section class="col col-3">
                         <label>Sampling Date</label>
                            <div class="input-group">
                            <?= $form->field($model, 'sampling_date')->textInput(['class'=>'form-control','id'=>'sampling_date','name'=>'sampling_date'])->label(false)?>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </section>
                         <section class="col col-3">
                            <label>Received Date</label>
                            <div class="input-group">
                                  <?= $form->field($model, 'received_date')->textInput(['class'=>'form-control','id'=>'received_date','name'=>'received_date'])->label(false)?>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </section>
                        <section class="col col-3">
                            <label>Analysis Date</label>
                                <div class="input-group">
                                    <?= $form->field($model, 'report_date')->textInput(['class'=>'form-control','id'=>'report_date','name'=>'report_date'])->label(false)?>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </section>          
                    </div>
                     <div class="row">
                        <section class="col col-3">
                            <label>Serial Number</label>
                            <?= $form->field($model, 'eng_sn')->label(false)?>
                        </section>
                        <section class="col col-3">
                            <label>Compartment/(Loaksi Sampling)</label>
                            <?= $form->field($model, 'compartment')->label(false)?>
                        </section>
                        <section class="col col-3">
                            <label>Eng Builder (Barge/Ponnton/Visel)</label>
                            <?= $form->field($model, 'eng_builder')->label(false)?>
                        </section>
                        <section class="col col-3">
                            <label>Type/etc(Type/Keterangan)</label>
                            <?= $form->field($model, 'eng_type')->label(false)?>
                        </section>
                    </div>
                    <section>
                        <label>Location (Lokasi)</label>
                            <?= $form->field($model, 'eng_location')->label(false)?>
                    </section>
                  </div>  
                     <div class="tab-pane fade in" id="a2">
                      <div class="row">
                            <section class="col col-2">
                                <label>Appearance</label>
                                <?= $form->field($model, 'appearance')->label(false)?>
                            </section>
                            <section class="col col-2">
                                    <label>Density 15 °C</label>
                                    <?= $form->field($model, 'density')->label(false)?>
                            </section>
                            <section class="col col-2">
                                <label>API Gravity</label>
                                <?=$form->field($model,'api_gravity')->label(false) ?>
                            </section>
                            <section class="col col-2">
                                    <label>TAN</label>
                                    <?= $form->field($model,'total_acid_number')->label(false)?>
                            </section>
                            <section class="col col-2">
                                <label>Strong Acid Number</label>
                                 <?=$form->field($model,'strong_acid')->label(false) ?>
                             </section>                       
                             <section class="col col-2">
                                <label>Calorific Value</label>
                                 <?=$form->field($model,'calorific_value')->label(false) ?>
                            </section>
                    </div> 
                    <div class="row">
                           
                            <section class="col col-2">
                                    <label>Flash Point (PMCC)</label>
                                    <?=$form->field($model,'flash_point')->label(false) ?>
                            </section>
                           
                            <section class="col col-2">
                                <label>Viscosity Kin @ 40 °C</label>
                                    <?=$form->field($model,'kinematic_viscosity')->label(false) ?>
                             </section>
                            <section class="col col-2">
                                <label>Water Content</label>
                                <?=$form->field($model,'water_content')->label(false) ?>
                            </section>
                            <section class="col col-2">
                            <label>Ash Content</label>
                                <?=$form->field($model,'ash_content')->label(false) ?>
                            </section>
                             <section class="col col-2">
                                 <label>Cloudn Point</label>
                                <?=$form->field($model,'cloudn_point')->label(false) ?>
                            </section>
                            <section class="col col-2">
                                <label>Pour Point</label>
                                <?=$form->field($model,'pour_point')->label(false) ?>
                            </section>
                    </div>  
                   <div class="row">
                            
                           
                            <section class="col col-2">
                                <label>Cetane Index</label>
                                <?=$form->field($model,'cetane_index')->label(false) ?>
                            </section>
                            <section class="col col-2">
                                <label>Conradson Carbon </label>
                                <?=$form->field($model,'conradson_carbon')->label(false) ?>
                            </section>
                            <section class="col col-2">
                               <label>Dist IBP</label> 
                                <?=$form->field($model,'distillation_ibp')->label(false) ?>
                            </section>
                            <section class="col col-2">
                                <label>Dist 5 % Vol</label>
                                <?=$form->field($model,'distillation_5')->label(false) ?>

                            </section>
                            <section class="col col-2">
                                <label>Dist 10 % Vol</label>
                                <?=$form->field($model,'distillation_10')->label(false) ?>
                            </section>
                            <section class="col col-2">
                                <label>Dist 20 % Vol</label>
                                <?=$form->field($model,'distillation_20')->label(false) ?>
                            </section>
                        </div>  
                        <div class="row">
                            
                            <section class="col col-2">
                                <label>Dist 30 % Vol</label>
                                <?=$form->field($model,'distillation_30')->label(false) ?>
                                
                            </section>
                             <section class="col col-2">
                                <label>Dist 40 % Vol</label>
                                <?=$form->field($model,'distillation_40')->label(false) ?>
                                
                            </section>
                             <section class="col col-2">
                                <label>Dist 50 % Vol</label>
                                <?=$form->field($model,'distillation_50')->label(false) ?>
                                
                            </section>
                             <section class="col col-2">
                                <label>Dist 60 % Vol</label>
                                <?=$form->field($model,'distillation_60')->label(false) ?>
                            </section>    
                                <section class="col col-2">
                                <label>Dist 70 % Vol</label>
                                <?=$form->field($model,'distillation_70')->label(false) ?>
                                
                            </section>
                              <section class="col col-2">
                                <label>Dist 80 % Vol</label>
                                <?=$form->field($model,'distillation_80')->label(false) ?>
                            </section>
                        </div>
                    <div class="row">
                           
                            <section class="col col-2">
                                <label>Dist 90 % Vol</label>
                                <?=$form->field($model,'distillation_90')->label(false) ?>
                            </section>
                            <section class="col col-2">
                                <label>Dist 95 % Vol</label>
                                <?=$form->field($model,'distillation_95')->label(false) ?>
                            </section>
                            <section class="col col-2">
                                <label>Dist EP</label>
                                <?=$form->field($model,'distillation_ep')->label(false) ?>
                            </section>
                            <section class="col col-2">
                                <label>Dist Recovery % Vol</label>
                                <?=$form->field($model,'distillation_recovery')->label(false) ?>
                            </section>
                            <section class="col col-2">
                                <label>Dist Residue % vol</label>
                                <?=$form->field($model,'distillation_residue')->label(false) ?>
                            </section>
                             <section class="col col-2">
                                <label>Dist Loss % Vol</label>
                                <?=$form->field($model,'destillation_loss')->label(false) ?>
                            </section>
                    </div>    
                    
                     <div class="row"> 
                            <section class="col col-2">
                                <label>Dist Recovery 300°C </label>
                                <?=$form->field($model,'destillation_recovery_300')->label(false) ?>
                            </section>
                            <section class="col col-2">
                                <label>Sulphur Content </label>
                                <?=$form->field($model,'sulphur_content')->label(false) ?>
                            </section>

                            <section class="col col-2">
                                <label>RANCIMAT</label>
                                <?=$form->field($model,'rancimat')->label(false) ?>
                            </section>
                            <section class="col col-2">
                                <label>Lubricity OF Diesel</label>
                                <?=$form->field($model,'lubricity_of_diesel')->label(false) ?>
                            </section>
                            <section class="col col-2">
                                <label>FAME Content</label>
                                <?=$form->field($model,'fame_content')->label(false) ?>
                             </section>
                             
                              <section class="col col-2">
                                        <label>Cerosine Content</label>
                                    <?=$form->field($model,'cerosine_content')->label(false) ?>
                            </section>   
                    </div>
                    <div class="row">
                            
                            <section class="col col-2">
                                <label>Sediment </label>
                                <?=$form->field($model,'sediment')->label(false) ?>
                            </section>
                            <section class="col col-2">
                                <label>Colour ASTM</label>
                                <?=$form->field($model,'colour')->label(false) ?>
                            </section>
                            <section class="col col-2">
                                <label>Spesific Gravity</label>
                                <?=$form->field($model,'specific_gravity')->label(false) ?>
                            </section>
                            <section class="col col-2">
                                <label>Copper Strip Corrosion </label>
                                <?=$form->field($model,'copper_corrosion')->label(false) ?>
                            </section>
                            <section class="col col-2">
                                <label>Particles > 4 µm</label>
                                <?=$form->field($model,'particles_4')->label(false) ?>
                            </section>
                            <section class="col col-2">
                            <label>Particles > 6 µm</label>
                            <?=$form->field($model,'particles_6')->label(false) ?>
                        </section>
                   </div>
                   <div class="row"> 
                         <section class="col col-2">
                            <label>Particles > 14 µm</label>
                            <?=$form->field($model,'particles_14')->label(false) ?>
                        </section>
                        <section class="col col-2">
                            <label>ISO 4406</label>
                            <?=$form->field($model,'iso_4406')->label(false) ?>
                        </section>
                        <section class="col col-8">
                            <label>Recommended1</label>
                            <?=$form->field($model,'recommended1')->textarea(['rows'=>'6','name'=>'recomm1'])->label(false) ?>
                        </section>
                    </div>
                </div>
            </div>    
        </fieldset>
    <footer>    
      <?= Html::submitButton(Yii::t('app', $model->isNewRecord ? 'Submit ' : 'Save '),['class' => 'btn btn-primary','id'=>'btn']) ?>
        <?= \app\smartadmin\helpers\Html::a('<i class="fa fa-cancel"></i>
                    Cancel', Url::toRoute(['/reports/fuel']), ['class' => 'btn btn-primary']) ?>
      
    </footer>             
<?php ActiveForm::end();
?>

<div id="dialog_simple" title="Dialog Simple Title">
    <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
    </p>
</div>


