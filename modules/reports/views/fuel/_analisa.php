<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>
<fieldset>
    <br>
    <h3>Data Analisa</h3>
    </br>
    <section>
        <label>Dirgen Migas Tahun</label>
        <?= $form->field($model, 'recommended3')->dropDownlist(['2013'=>'2013','2016'=>'2016','2019'=>'2019'])->label(false)?>
    </section>
    <div class="row">
        <section class="col col-4">
            <label>Appearance</label>
            <?= $form->field($model, 'appearance')->label(false)?>
        </section>
        <section class="col col-2">
            <label>Density</label>
            <?= $form->field($model, 'density')->label(false)?>
        </section>
        <section class="col col-2">
            <label>SG</label>
            <?=$form->field($model,'specific_gravity')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>AG</label>
            <?=$form->field($model,'api_gravity')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>TAN</label>
            <?= $form->field($model,'total_acid_number')->label(false)?>
        </section>
    </div>
    <div class="row">
        <section class="col col-2">
            <label>SAN</label>
            <?=$form->field($model,'strong_acid')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>Flash Point</label>
            <?=$form->field($model,'flash_point')->label(false) ?>
        </section>
        <section class="col col-4">
            <label>Viscosity 40 °C</label>
            <?=$form->field($model,'kinematic_viscosity')->label(false) ?>
        </section>

        <section class="col col-2">
            <label>KF</label>
            <?=$form->field($model,'water_content')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>Ash Content</label>
            <?=$form->field($model,'ash_content')->label(false) ?>
        </section>
    </div>
    <div class="row">
        <section class="col col-2">
            <label>Pour Point</label>
            <?=$form->field($model,'pour_point')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>Cloudn Point</label>
            <?=$form->field($model,'cloudn_point')->label(false) ?>
        </section>
        <section class="col col-3">
            <label>Cetane Index</label>
            <?=$form->field($model,'cetane_index')->label(false) ?>
        </section>
        <section class="col col-1">
            <label></label>
            <a id="cetan" class="btn btn-primary">Hitung CI</a>
        </section>
        <section class="col col-2">
            <label>CCR 10%</label>
            <?=$form->field($model,'conradson_carbon')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>IBP</label>
            <?=$form->field($model,'distillation_ibp')->label(false) ?>
        </section>
    </div>
    <div class="row">
        <section class="col col-2">
            <label>5 %</label>
            <?=$form->field($model,'distillation_5')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>10 % </label>
            <?=$form->field($model,'distillation_10')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>20 %</label>
            <?=$form->field($model,'distillation_20')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>30 %</label>
            <?=$form->field($model,'distillation_30')->label(false) ?>

        </section>
        <section class="col col-2">
            <label>40 %</label>
            <?=$form->field($model,'distillation_40')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>50 %</label>
            <?=$form->field($model,'distillation_50')->label(false) ?>
        </section>
    </div>
    <div class="row">
        <section class="col col-2">
            <label>60 %</label>
            <?=$form->field($model,'distillation_60')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>70 %</label>
            <?=$form->field($model,'distillation_70')->label(false) ?>

        </section>
        <section class="col col-2">
            <label>80 %</label>
            <?=$form->field($model,'distillation_80')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>90 %</label>
            <?=$form->field($model,'distillation_90')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>95 %</label>
            <?=$form->field($model,'distillation_95')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>EP</label>
            <?=$form->field($model,'distillation_ep')->label(false) ?>
        </section>
    </div>
    <div class="row">
        <section class="col col-2">
            <label>RC</label>
            <?=$form->field($model,'distillation_recovery')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>RS</label>
            <?=$form->field($model,'distillation_residue')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>L</label>
            <?=$form->field($model,'distillation_loss')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>300 °C</label>
            <?=$form->field($model,'distillation_recovery_300')->label(false) ?>
        </section>
        <section class="col col-4">
            <label>Sulphur Content </label>
            <?=$form->field($model,'sulphur_content')->label(false) ?>
        </section>
    </div>
    <div class="row">
        <section class="col col-2">
            <label>Sediment </label>
            <?=$form->field($model,'sediment')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>Colour</label>
            <?=$form->field($model,'colour')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>Copper Strip</label>
            <?=$form->field($model,'copper_corrosion')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>PC > 4 µm</label>
            <?=$form->field($model,'particles_4')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>PC > 6 µm</label>
            <?=$form->field($model,'particles_6')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>PC > 14 µm</label>
            <?=$form->field($model,'particles_14')->label(false) ?>
        </section>
    </div>
    <div class="row">
        <section class="col col-2">
            <label>FAME</label>
            <?=$form->field($model,'fame_content')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>Kestabilan</label>
            <?=$form->field($model,'rancimat')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>HFFR</label>
            <?=$form->field($model,'lubricity_of_diesel')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>Calorific Value1</label>
            <?=$form->field($model,'calorific_value')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>Calorific Value2</label>
            <?=$form->field($model,'calorific_value1')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>Cerosine Content</label>
            <?=$form->field($model,'cerosine_content')->label(false) ?>
        </section>
    </div>
    <div class="row">

        <section class="col col-2">
            <label>ISO 4406</label>
            <?=$form->field($model,'iso_4406')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>Silicon</label>
            <?=$form->field($model,'silicon')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>Alumunium</label>
            <?=$form->field($model,'alumunium')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>Particulat</label>
            <?=$form->field($model,'particulat')->label(false) ?>
        </section>
        <section class="col col-2">
            <label>Monoglyceride</label>
            <?=$form->field($model,'monoglyceride')->label(false) ?>
        </section>
        <section class="col col-6">
            <label>Recommended1</label>
            <?=$form->field($model,'recommended1')->textarea(['rows'=>'6'])->label(false) ?>
        </section>
    </div>
</fieldset>

