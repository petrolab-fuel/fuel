<?php
/**
 * Created by PhpStorm.
 * User: Feri
 * Date: 6/8/2019
 * Time: 19:32
 */

 use yii\helpers\Url;
use yii\helpers\Html;
use app\modelsDB\Attention;
$this->title = Yii::t('app', 'Analisa');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Analisa'), 'url' => Url::toRoute(['/reports/fuel'])];
// $this->params['breadcrumbs'][] = $this->title;
?>
<?= \yii\bootstrap\Modal::widget([
    'id' => 'modal-temp',
    'options' => ['tabindex' => false],
])?>


<section id="widget-grid" class="">
    <!-- START ROW -->
    <div class="row">

        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-2" data-widget-editbutton="false" data-widget-collapsed="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Filter  </h2>
                </header>
                <iframe id="my_iframe" style="display:none;"></iframe>
                <div>
                    <div class="widget-body no-padding">
                        <form class="smart-form" id="form-filter">
                            <fieldset>
                                <div class="row">
                                    <section class="col col-3">
                                        <label class="label">Receive Date</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" name="recv_date_begin" value="" class="datepr"
                                                   placeholder="Begin" autocomplete="off">
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">&nbsp;</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" name="recv_date_end" value="" class="datepr"
                                                   placeholder="End" autocomplete="off">
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">Report Date</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" name="report_date_begin" value="" class="datepr"
                                                   placeholder="Begin" autocomplete="off">
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">&nbsp;</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" name="report_date_end" value="" class="datepr"
                                                   placeholder="End" autocomplete="off">
                                        </label>
                                    </section>
                                </div>
                            </fieldset>
                            <footer>
                                <a id="search" class="btn btn-primary">Search</a>
                                <?= Html::a('<i class="fa fa-file-excel-o"></i> Export Excel',Url::toRoute(['/reports/fuel/excel']),['class'=>'btn btn-primary','data-action'=>'excel','target' => '_blank'])?>
                                <?= Html::a('<i class="fa fa-gear"></i> Upload to Server',Url::toRoute(['/reports/fuel/upload-data']),['class'=>'btn btn-success','data-action'=>'upload']) ?>
                                <?= Html::a('<i class="fa fa-gear"></i> Update Code',Url::toRoute(['/reports/fuel/eval-code']),['class'=>'btn btn-success','data-action'=>'update']) ?>


                            </footer>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-3" data-widget-editbutton="false">

                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Data Analisa  </h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="ibox-content collapse" id="uploadspase">

                            <?= \app\modules\reports\models\ReportTables::widget([
                                'columns' => [
                                    'action',
                                    'fuelStatus',
                                    'customerName',
                                    'attentionName',
                                    'po',
                                    'spb',
                                    'harga',
                                    'lab_number',
                                    'eng_builder',
                                    'eng_type',
                                    'eng_sn',
                                    'eng_location',
                                    'sampleDate',
                                    'receiveDate',
                                    'reportDate',
                                ],
                                'options'=>['order'=>"[7,'desc']"],
                                'model' => new \app\modules\reports\models\Fuel(),
                            ]) ?>
                        </div>
                        <!-- end widget content -->
                        <div class="ibox-content collapse" id="analisa">


                        </div>
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- END COL -->

    </div>

    <!-- END ROW-->

</section>
<!-- end widget grid -->
<?php
$url=Url::toRoute(['/reports/fuel/index']);
$jsCode=<<<JS
$(function() {
    let tbl=$('#w0');
    let table=tbl.DataTable();
    let form=$('#form-filter'); 
     let domOuttable = tbl.parent().parent().parent();
    $('#uploadspase').collapse('show');
    $('[data-action=add]').click(function(e) {
      e.preventDefault();
        $('#analisa').load($(this).attr('href'));
        $('#analisa').collapse('show');
        $('#uploadspase').collapse('hide');
    });
    domOuttable.on('click','tbody [data-action=edit]',function(e) {
      e.preventDefault();
        $('#analisa').load($(this).attr('href'));
        $('#analisa').collapse('show');
        $('#uploadspase').collapse('hide');
    });
    
    domOuttable.on('click','tbody [data-action=copy]',function(e) {
      e.preventDefault();
        $('#analisa').load($(this).attr('href'));
        $('#analisa').collapse('show');
        $('#uploadspase').collapse('hide');
    });
    
    $('#search').click(function() {
       let tBegin=form.find('[name=recv_date_begin]').val();
       let tEnd=form.find('[name=recv_date_end]').val();
       let rBegin=form.find('[name=report_date_end]').val();
       let rEnd=form.find('[name=report_date_end]').val();
       tbl.DataTable().ajax.url('$url'+'?recv_date_begin='+tBegin+'&recv_date_end='+tEnd+'&report_date_begin='+rBegin+'&report_date_end='+rEnd);
       tbl.DataTable().ajax.reload();
    });
    
    $('.datepr').datepicker({
        dateFormat: 'yy-mm-dd',
        autoclose:true
    });
    
    domOuttable.on('click','tbody [data-action=update-code]',function(e) {
        e.preventDefault();
         var elm=$(this);
         // console.log(elm.attr('href'));
            updatecode(elm);
      //   let d=table.row(this).index();
      //  // d.counter++;
      // console.log(d);
    });
    
    function DownloadUrl(url) {
    document.getElementById("my_iframe").src = url;
};
    
    $('[data-action=update]').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        var elm=$(this);
            updatecode(elm);
    });
    
    $('[data-action=excel]').click(function(e) {
        e.preventDefault();
        let param=tbl.DataTable().ajax.params();
        param.recv_date_begin = form.find('[name=recv_date_begin]').val();
        param.recv_date_end = form.find('[name=recv_date_end]').val();
        param.report_date_begin = form.find('[name=report_date_end]').val();
        param.report_date_end = form.find('[name=report_date_end]').val();
        var urlparm = $.param(param);
        
        // var url = $(e.target).attr('href');
        let elm=$(this);
        var urlparm = $.param(param);
        
        var url = elm.attr('href');
        
        DownloadUrl(url + '?' + urlparm);
    });
    
    $('[data-action=upload]').click(function(e) {
      e.preventDefault();
       let param=tbl.DataTable().ajax.params();
        param.recv_date_begin = form.find('[name=recv_date_begin]').val();
        param.recv_date_end = form.find('[name=recv_date_end]').val();
        param.report_date_begin = form.find('[name=report_date_end]').val();
        param.report_date_end = form.find('[name=report_date_end]').val();
        // console.log(param);
        // return false;
        var urlparm = $.param(param);
        $.ajax({
            url:$(this).attr('href'),
            data:param,
            method:'post',
        }).done(function(response) {
            console.log(response);
            alert_notif(response);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            let res={
                    status:'error',
                    message:'Fail'
            }
            alert_notif(res);
        })
    });
    
    function updatecode(elm){
         let param=tbl.DataTable().ajax.params();
         param.recv_date_begin = form.find('[name=recv_date_begin]').val();
         param.recv_date_end = form.find('[name=recv_date_end]').val();
         param.report_date_begin = form.find('[name=report_date_end]').val();
         param.report_date_end = form.find('[name=report_date_end]').val();
         $.ajax({
            url:elm.attr('href'),
            data:param,
            method:'post',
            beforeSend:function(){
                // $('#modal-temp').modal('show');
            },
            success:function(response) {
                table.ajax.reload();
                // table.rows().every(function() {
                //   let row=this;
                //        
                //       let a=row.data().fuelStatus;  
                //       console.log(row);
                // })
                // var row=table.row('#765OBF18');
                // console.log(row);
                // $.each(response.data, function (i, v) {
                //     console.log(i);
                //      var row = table.row('#' + i);
                //      console.log(row);
                //     if (row.data() !== undefined) {
                //         alert('ketemu');
                //         row.data().fuelStatus = v;
                //         row.invalidate();
                //     }
                // });
                 alert_notif(response);
              // console.log(response.message);
               // $('#modal-temp').modal('hide');
            },
            error:function(jqXHR, textStatus, errorThrown) {
              alert(jqXHR);
            }
         });
    }
     function alert_notif(res) {
        $.sound_on = false;
        var color = res.status === 'success' ? '#659265' : '#C46A69';
        var icon = res.status === 'success' ? 'fa-check' : 'fa-times';
        $.smallBox({
            title: res.status,
            content: res.message,
            color: color,
            iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
            timeout: 4000
        });
    }
});
JS;
$this->registerJS($jsCode,4,'dddd');
?>