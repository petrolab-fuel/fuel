<?php
use app\smartadmin\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
// app\modules\reports\assets\Analisa::register($this);  
 // app\smartadmin\assets\plugins\PluginsAssets::register($this);
 $this->title = Yii::t('app', 'Transaction');
 $this->params['breadcrumbs'][] = $this->title;
?>
<!-- widget grid -->
<section id="widget-grid" class="">
    <!-- START ROW -->
  <div class="row">
      <!-- NEW COL START -->
      <article class="col-sm-12 col-md-12 col-lg-12">
          <!-- Widget ID (each widget will need unique ID)-->
          <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
          
            <header>
                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
               <h2>Form Add Transaction </h2>
            </header>
              <!-- widget div-->
              <div>
                <div class="jarviswidget-editbox">
                      <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                  <!-- widget content -->  
                  <div class="widget-body no-padding">
                      <?php
                      $form = ActiveForm::begin([
                      'fieldConfig' => [
                            // 'template' => "{label}<label class='input'>{input}</label>{error}",
                            // 'inputOptions' => ['class' => 'form-control'],
                                  ],
                         'options' => [
                               'class' => 'smart-form',
                               'id'=>'form-input',
                               'enableAjaxValidation'=>true,
                         ],

                        ]);
                        ?>
                          <fieldset>

                                <section >
                                  <label>Lab No</label>
                                  <?= $form->field($model, 'lab_number')->textInput(['id'=>'lub_number'])->label(false)?>
                                </section>
                                <section>
                                    <label>Customer</label>
                                      <?= $form->field($model,'customer_id')->dropDownList($model->customer_id==''?[]:[$model->customer_id=>$model->customer->name],[
                                       'class'=>'select2','style'=>'width:100%',
                                        'prompt'=>'select..',
                                        'data-url'=>Url::toRoute(['/master/customers/default/list-customer']),
                                      // 'name'=>'customer_id',
//                                      'data-next'=>'att_id'
                                      ])->label(false)?>
                                </section>
                                <section >
                                    <label>Attentions</label>
                                    <?= $form->field($model,'attention_id')->dropDownList($model->attention_id==''?[]:[$model->attention_id=>$model->attention->name],
                                    ['prompt'=>'select..',
                                        'class'=>'select2','style'=>'width:100%',
                                        'data-url'=>Url::toRoute(['/master/customers/default/list-attention']),
                                        // 'name'=>'attention_id',
                                        'data-depend-from'=>'customer_id',
                                        'data-depend' => 'customer_id',
                                        'data-depend-id'=>'searchfuel-customer_id',
                                        'style'=>'width:100%'])->label(false)?>
                                </section>
                                <div class="row">
                                      <section class="col col-6">
                                            <label>Sampling Point / Sample Name</label>
                                            <?= $form->field($model, 'sludge_content')->dropDownlist($model->jenisFuel)->label(false)?>
                                      </section>
                                      <section class="col col-6">
                                          <label class="input">Sampling Date<i class="icon-append fa fa-calendar"></i>
                                              <?= $form->field($model, 'sampling_date')->textInput(['class'=>'datepicker','id'=>'sampling_date'])->label(false)?>
                                                 
                                      </section>
                                </div>  
                                <div class="row">
                                    <section class="col col-6">
                                         <label class="input">Received Date
                                                <?= $form->field($model, 'received_date',['template'=>'<i class="icon-append fa fa-calendar"></i>'])->textInput(['class'=>'form-control','id'=>'received_date'])->label(false)?>
                                          </label>
                                      </section>
                                      <section class="col col-6">
                                           <label class="input">Report Date<i class="icon-append fa fa-calendar"></i>
                                                  <?= $form->field($model, 'report_date')->textInput(['class'=>'form-control','id'=>'report_date'])->label(false)?>
                                            </label>
                                      </section>          
                                </div>
                                <div class="row">
                                  <section class="col col-6">
                                        <label>Serial Number</label>
                                        <?= $form->field($model, 'eng_sn')->label(false)?>
                                  </section>
                                  <section class="col col-6">
                                        <label>Compartment/(Loaksi Sampling)</label>
                                        <?= $form->field($model, 'compartment')->label(false)?>
                                  </section>
                                </div>
                                <div class="row">
                                  <section class="col col-6">
                                      <label>Eng Builder (Barge/Ponnton/Visel)</label>
                                      <?= $form->field($model, 'eng_builder')->label(false)?>
                                  </section>
                                  <section class="col col-6">
                                      <label>Type/etc(Type/Keterangan)</label>
                                      <?= $form->field($model, 'eng_type')->label(false)?>
                                  </section>
                                </div>
                                 <section>
                                       <label>Location (Lokasi)</label>
                                    <?= $form->field($model, 'eng_location')->label(false)?>
                                  </section>
                                 <section>
                                       <label>SPB</label>
                                    <?= $form->field($model, 'spb')->label(false)?>
                                  </section>
                                  <section>
                                       <label>No PO</label>
                                    <?= $form->field($model, 'po')->label(false)?>
                                  </section>
                                  <section>
                                       <label>Harga</label>
                                    <?= $form->field($model, 'harga')->label(false)?>
                                  </section>


                            </fieldset>
                            <fieldset>
                              <section>
                                 <label>Dirgen Migas Tahun</label>
                                    <?= $form->field($model, 'recommended3')->dropDownlist(['2013'=>'2013','2016'=>'2016','2019'=>'2019'])->label(false)?>
                              </section>
                              <div class="row">
                                   <section class="col col-4">
                                <label>Appearance</label>
                                <?= $form->field($model, 'appearance')->label(false)?>
                              </section>
                                  <section class="col col-2">
                                          <label>Density</label>
                                          <?= $form->field($model, 'density')->label(false)?>
                                  </section>
                                  <section class="col col-2">
                                      <label>SG</label>
                                      <?=$form->field($model,'specific_gravity')->label(false) ?>
                                  </section>
                                  <section class="col col-2">
                                      <label>AG</label>
                                      <?=$form->field($model,'api_gravity')->label(false) ?>
                                  </section>
                                  <section class="col col-2">
                                          <label>TAN</label>
                                          <?= $form->field($model,'total_acid_number')->label(false)?>
                                  </section>    
                              </div>
                              <div class="row">
                                  <section class="col col-2">
                                      <label>SAN</label>
                                      <?=$form->field($model,'strong_acid')->label(false) ?>
                                  </section> 
                                  <section class="col col-2">
                                    <label>Flash Point</label>
                                    <?=$form->field($model,'flash_point')->label(false) ?>
                                  </section>
                                  <section class="col col-4">
                                      <label>Viscosity 40 °C</label>
                                    <?=$form->field($model,'kinematic_viscosity')->label(false) ?>
                                  </section>

                                  <section class="col col-2">
                                      <label>KF</label>
                                      <?=$form->field($model,'water_content')->label(false) ?>
                                  </section>
                                 <section class="col col-2">
                                     <label>Ash Content</label>
                                    <?=$form->field($model,'ash_content')->label(false) ?>
                                </section>
                                </div>
                                <div class="row">
                                    <section class="col col-2">
                                      <label>Pour Point</label>
                                         <?=$form->field($model,'pour_point')->label(false) ?>
                                      </section>
                                      <section class="col col-2">
                                         <label>Cloudn Point</label>
                                         <?=$form->field($model,'cloudn_point')->label(false) ?>
                                      </section>
                                       <section class="col col-4">
                                            <label>Cetane Index</label>
                                          <?=$form->field($model,'cetane_index')->label(false) ?>
                                      </section>
                                       <section class="col col-2">
                                           <label>CCR 10%</label>
                                          <?=$form->field($model,'conradson_carbon')->label(false) ?>
                                      </section>
                                      <section class="col col-2">
                                          <label>IBP</label> 
                                            <?=$form->field($model,'distillation_ibp')->label(false) ?>
                                      </section>
                                </div>
                                <div class="row">
                                 <section class="col col-2">
                                  <label>5 %</label>
                                    <?=$form->field($model,'distillation_5')->label(false) ?>
                                  </section>
                                   <section class="col col-2">
                                       <label>10 % </label>
                                      <?=$form->field($model,'distillation_10')->label(false) ?>
                                  </section>
                                   <section class="col col-2">
                                        <label>20 %</label>
                                        <?=$form->field($model,'distillation_20')->label(false) ?>
                                    </section>
                                    <section class="col col-2">
                                        <label>30 %</label>
                                        <?=$form->field($model,'distillation_30')->label(false) ?>
                                        
                                    </section>
                                     <section class="col col-2">
                                        <label>40 %</label>
                                        <?=$form->field($model,'distillation_40')->label(false) ?>
                                    </section>
                                     <section class="col col-2">
                                        <label>50 %</label>
                                        <?=$form->field($model,'distillation_50')->label(false) ?>  
                                    </section>
                                  </div>
                                    <div class="row">
                                        <section class="col col-2">
                                            <label>60 %</label>
                                            <?=$form->field($model,'distillation_60')->label(false) ?>
                                        </section>    
                                            <section class="col col-2">
                                            <label>70 %</label>
                                            <?=$form->field($model,'distillation_70')->label(false) ?>
                                            
                                        </section>
                                          <section class="col col-2">
                                            <label>80 %</label>
                                            <?=$form->field($model,'distillation_80')->label(false) ?>
                                        </section>
                                        <section class="col col-2">
                                            <label>90 %</label>
                                            <?=$form->field($model,'distillation_90')->label(false) ?>
                                        </section>
                                            <section class="col col-2">
                                                <label>95 %</label>
                                                <?=$form->field($model,'distillation_95')->label(false) ?>
                                            </section>
                                             <section class="col col-2">
                                                <label>EP</label>
                                                <?=$form->field($model,'distillation_ep')->label(false) ?>
                                            </section>
                                        </div>
                                        <div class="row">
                                          <section class="col col-2">
                                              <label>RC</label>
                                              <?=$form->field($model,'distillation_recovery')->label(false) ?>
                                          </section>
                                          <section class="col col-2">
                                              <label>RS</label>
                                              <?=$form->field($model,'distillation_residue')->label(false) ?>
                                          </section>
                                           <section class="col col-2">
                                              <label>L</label>
                                              <?=$form->field($model,'distillation_loss')->label(false) ?>
                                          </section>
                                          <section class="col col-2">
                                              <label>300 °C</label>
                                              <?=$form->field($model,'distillation_recovery_300')->label(false) ?>
                                          </section>
                                          <section class="col col-4">
                                              <label>Sulphur Content </label>
                                            <?=$form->field($model,'sulphur_content')->label(false) ?>
                                           </section>
                                    </div>
                                    <div class="row">
                                        <section class="col col-2">
                                        <label>Sediment </label>
                                              <?=$form->field($model,'sediment')->label(false) ?>
                                        </section>
                                        <section class="col col-2">
                                           <label>Colour</label>
                                            <?=$form->field($model,'colour')->label(false) ?>
                                         </section>
                                          <section class="col col-2">
                                            <label>Copper Strip</label>
                                            <?=$form->field($model,'copper_corrosion')->label(false) ?>
                                        </section>
                                         <section class="col col-2">
                                           <label>PC > 4 µm</label>
                                          <?=$form->field($model,'particles_4')->label(false) ?>
                                         </section>
                                          <section class="col col-2">
                                              <label>PC > 6 µm</label>
                                             <?=$form->field($model,'particles_6')->label(false) ?>
                                          </section>
                                           <section class="col col-2">
                                                <label>PC > 14 µm</label>
                                                <?=$form->field($model,'particles_14')->label(false) ?>
                                           </section>
                                    </div>
                                    <div class="row">
                                         <section class="col col-2">
                                          <label>FAME</label>
                                          <?=$form->field($model,'fame_content')->label(false) ?>
                                        </section>
                                         <section class="col col-2">
                                            <label>Kestabilan</label>
                                            <?=$form->field($model,'rancimat')->label(false) ?>
                                        </section>
                                        <section class="col col-2">
                                            <label>HFFR</label>
                                            <?=$form->field($model,'lubricity_of_diesel')->label(false) ?>
                                        </section>
                                        <section class="col col-2">
                                            <label>Calorific Value1</label>
                                             <?=$form->field($model,'calorific_value')->label(false) ?>
                                        </section>
                                        <section class="col col-2">
                                               <label>Calorific Value2</label>
                                             <?=$form->field($model,'calorific_value1')->label(false) ?>
                                        </section>
                                        <section class="col col-2">
                                        <label>Cerosine Content</label>
                                          <?=$form->field($model,'cerosine_content')->label(false) ?>
                                        </section>   
                                  </div>
                                  <div class="row"> 
                        
                        <section class="col col-2">
                            <label>ISO 4406</label>
                            <?=$form->field($model,'iso_4406')->label(false) ?>
                        </section>
                        <section class="col col-2">
                            <label>Silicon</label>
                            <?=$form->field($model,'silicon')->label(false) ?>
                        </section>
                        <section class="col col-2">
                            <label>Alumunium</label>
                            <?=$form->field($model,'alumunium')->label(false) ?>
                        </section>
                        
                        <section class="col col-6">
                            <label>Recommended1</label>
                            <?=$form->field($model,'recommended1')->textarea(['rows'=>'6','name'=>'recomm1'])->label(false) ?>
                        </section>
                    </div>

                            </fieldset>
                        <footer>    
                        <?= Html::button('cancel', ['value' => Url::toRoute(['/reports/fuel']), 'title' => 'Add', 'class' => 'btn btn-success','id'=>'bCancelAdd']); ?>
                        <?= Html::submitButton(Yii::t('app', $model->isNewRecord ? 'Submit ' : 'Save '),['class' => 'btn btn-primary','id'=>'btn']) ?>
                      </footer>      

                    <?php ActiveForm::end();?>     
                  </div>
                  <!-- end widget content -->
              </div>
              <!-- end widget div -->  
          </div>
          <!-- end widget -->
      </article>
      <!-- END COL -->
  </div>
  <!-- END ROW -->
</section>
<!-- end widget grid -->
<?php
    $id_customer=Html::getInputId($model,'customer_id');
 $jsScript = <<< JS
$(function(){
    getlab();
    var form=$('#form-input');
    form.find('#searchfuel-customer_id').on('select2:select',function(e) {
      form.find('#searchfuel-attention_id').val(null).trigger('change');
    });
     let par = function () {
        return {
            ajax: {
                url: function (a) {
                    return this.attr('data-url');
                },
                data: function (params) {
                    // console.log()
                    let el = this;
                     console.log(el.attr('data-depend'));
                     console.log(el.attr('data-depend-from'));
//                     console.log('$id_customer');
                     
                    let query = {
                        q: params.term,
                        param: {
                            // console.log(el.attr('data-depend'));
                            col: el.attr('data-depend'),
                            fromcol: el.attr('data-depend-from'),
                            filter: form.find('#' + el.attr('data-depend-id')).val(),
                        }
                    };
                    return query;
                },
                processResults: function (data) {
                    return {
                        results: data.results
                    };
                }
            }
        }
    };
    form.find('.select2').select2(par());
      //   $(".select2").select2({ 
      //     theme: "bootstrap",
      // //      allowClear: true,
      // //      tags: true,
      // //     width: '100%'
      //  });
        function getlab(){
            $.ajax({
                url: base_url+"/reports/fuel/lab",
                // data: {value: value, obj: obj},
                type: "POST",
                    success: function(data) {
                      console.log(data);
                    $("#lub_number").val(data);
                }
            });
            console.log('test')
        }
      $('#bCancelAdd').click(function(e){
          e.preventDefault();
           $('#myModal').modal('hide');
      });
      $('#sampling_date').datepicker({
      dateFormat : 'yy-mm-dd',
      prevText : '<i class="fa fa-chevron-left"></i>',
      nextText : '<i class="fa fa-chevron-right"></i>',
      onSelect : function(selectedDate) {
        $('#received_date').datepicker('option', 'minDate', selectedDate);
      }
    });
    $('#received_date').datepicker({
      dateFormat : 'yy-mm-dd',
      prevText : '<i class="fa fa-chevron-left"></i>',
      nextText : '<i class="fa fa-chevron-right"></i>',
      onSelect : function(selectedDate) {
        $('#report_date').datepicker('option', 'minDate', selectedDate);
      }
    });
    $('#report_date').datepicker({
      dateFormat : 'yy-mm-dd',
      prevText : '<i class="fa fa-chevron-left"></i>',
      nextText : '<i class="fa fa-chevron-right"></i>',
      onSelect : function(selectedDate) {
        $('#sampling_date').datepicker('option', 'maxDate', selectedDate);
      }
    });
//      $("#customer_id").on("change", function() {
//       var value = $(this).val(),
//           obj = $(this).attr("id");
//     $.ajax({
//          url: base_url+"/reports/fuel/get",
//          data: {value: value, obj: obj},
//          type: "POST",
//          success: function(data) {
//              $("#att_id").html(data);
//          }
//     });
// });

    $("#form-input").on("beforeSubmit", function () {

         var form=$(this);
            $.ajax({
                type: form.attr("method"),
                 url: form.attr("action"),
                 data: form.serialize(),
                 success: function (data) {
                    alert_notif({status: "success", message: 'Berhasil di simpan'});
                 // alert('succeess save');
                  getlab();
                 $('#test').click();
                  },
                 error:function(xhr){
                    alert("failure"+xhr.readyState+this.url);
                 } 
              });   
           }).on('submit', function(e){
                e.preventDefault();
          });

        function alert_notif(res) {
        $.sound_on = false;
        var color = res.status === 'success' ? '#659265' : '#C46A69';
        var icon = res.status === 'success' ? 'fa-check' : 'fa-times';
        $.smallBox({
            title: res.status,
            content: res.message,
            color: color,
            iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
            timeout: 4000
        });
    }
       
})
JS;
$this->registerJs($jsScript);
?>