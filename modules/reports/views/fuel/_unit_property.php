<?php
use yii\helpers\Url;
?>
<fieldset>
    <h3>Unit Property</h3>
</br>
    <section>
        <?= $form->field($model, 'lab_number')?>
    </section>
    <section>
        <?= $form->field($model,'customer_id')->dropDownList($model->customer_id==''?[]:[$model->customer_id=>$model->customer->name],[
            'class'=>'select23','style'=>'width:100%',
            'prompt'=>'select..',
            'data-url'=>Url::toRoute(['/master/customers/default/list-customer']),
            // 'name'=>'customer_id',
//                                      'data-next'=>'att_id'
        ])->label('Customer')?>
    </section>
        <section>
            <?= $form->field($model,'attention_id')->dropDownList($model->attention_id==''?[]:[$model->attention_id=>$model->attention->name],
                ['prompt'=>'select..',
                    'class'=>'select23','style'=>'width:100%',
                    'data-url'=>Url::toRoute(['/master/customers/default/list-attention']),
                    // 'name'=>'attention_id',
                    'data-depend-from'=>'customer_id',
                    'data-depend' => 'customer_id',
                    'data-depend-id'=>'searchfuel-customer_id',
                    'style'=>'width:100%'])->label('Attention')?>
        </section>

    <div class="row">
        <section class="col col-6">
            <?= $form->field($model, 'sludge_content')->dropDownlist($model->jenisFuel)->label('Sampling Point / Sample Name')?>
        </section>
        <section class="col col-6">
            <?= $form->field($model, 'sampling_date', ['template' => '{label}
                        <div class="col-lg-12">{input}<i class="fa fa-calendar form-control-feedback" style="top: 12px;"></i>{error}</div>',
                'options' => ['class' => 'smart-form']
            ])->textInput(['class' => 'form-control datepct', 'autocomplete' => 'off'])->label('Sampling Date')?>
        </section>
    </div>
    <div class="row">
        <section class="col col-6">
            <?= $form->field($model, 'received_date', ['template' => '{label}
                        <div class="col-lg-12">{input}<i class="fa fa-calendar form-control-feedback" style="top: 12px;"></i>{error}</div>',
                'options' => ['class' => 'smart-form']
            ])->textInput(['class' => 'form-control datepct', 'autocomplete' => 'off'])?>
        </section>
        <section class="col col-6">
            <?= $form->field($model, 'report_date', ['template' => '{label}
                        <div class="col-lg-12">{input}<i class="fa fa-calendar form-control-feedback" style="top: 12px;"></i>{error}</div>',
                'options' => ['class' => 'smart-form']
            ])->textInput(['class' => 'form-control datepct', 'autocomplete' => 'off'])?>
        </section>
    </div>
    <div class="row">
        <section class="col col-6">
            <?= $form->field($model, 'eng_sn')->label('Serial Number')?>
        </section>
        <section class="col col-6">
            <?= $form->field($model, 'compartment')->label('Compartment/(Loaksi Sampling)')?>
        </section>
    </div>
    <div class="row">
        <section class="col col-6">
            <?= $form->field($model, 'eng_builder')->label('Eng Builder (Barge/Ponnton/Visel)')?>
        </section>
        <section class="col col-6">
            <?= $form->field($model, 'eng_type')->label('Type/etc(Type/Keterangan)')?>
        </section>
    </div>
    <section>
        <?= $form->field($model, 'eng_location')->label('Location (Lokasi)')?>
    </section>
    <section>
        <?= $form->field($model, 'spb')->label('SPB')?>
    </section>
    <section>
        <?= $form->field($model, 'po')->label('No PO')?>
    </section>
    <section>
        <?= $form->field($model, 'harga')->label('Harga')?>
    </section>

</fieldset>