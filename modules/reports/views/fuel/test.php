<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <?= \app\modules\reports\models\DataTables::widget([
        'columns' => [
            'action',
            'fuelStatus',
            'customerName',
            'attentionName',
            'po',
            'spb',
            'harga',
            'lab_number',
            'eng_builder',
            'eng_type',
            'eng_sn',
            'eng_location',
            'sampleDate',
            'receiveDate',
            'reportDate',
        ],
        'options'=>['order'=>"[7,'desc']"],
        'model' => new \app\modules\reports\models\Fuel(),
    ]) ?>

</article>