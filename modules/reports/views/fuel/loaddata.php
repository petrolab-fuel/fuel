<?php
use yii\helpers\Url;
?>
<div class="panel panel-info">
    <form action="http://localhost:88/petrolab/entry-data/web/master/customer/update" method="POST" target="_blank" id="form">
        <input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
        value="<?=Yii::$app->request->csrfToken?>" v-model="csrf_token"/>
        <input type="hidden" name="c_id"
        v-value="CustomerID" v-model="CustomerID">
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn btn-sm" data-toggle="collapse" data-target="#add_customer"><i class="fa fa-arrow-circle-down"> Add Customer</i></button>
            </div>
        </div>
        <div class="row">
            <div id="add_customer" class="collapse">
                <div class="col-md-10">

                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Customer Name</label>
                                <input type="text" name="Name" class="form-control input-sm" v-model="form.Name">
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" name="Address" class="form-control input-sm" v-model="form.Address">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Branch</label>
                                <select class="form-control input-sm select2" name="Branch" id="Branch" v-model="form.Branch">
                                    <option v-for="br in branchs" v-bind:value="br.branch">{{ br.branch }}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Owner</label>
                                <input type="text" name="owner" class="form-control input-sm" v-model="form.owner">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Lab Location</label>
                                <select class="form-control input-sm select2" name="location_code" id="location_code">
                                    <option value="B" selected>Balikpapan/B</option>
                                    <option value="S">Sangatta/S</option>
                                    <option value="J">Jakarta/J</option>
                                    <option value="T">Tanjung Redep/T</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <button id="btn" type="submit" class="btn btn-primary"><i class="fa fa-save"></i> {{ type_request.name }}</button>
                    <b v-if="btn_delete">
                        <button id="btn_delete" type="button" class="btn btn-danger btn-sm" v-on:click="delete_data()"><i class="fa fa-trash"></i> Delete</button>
                    </b>
                    <button id="btn_reset" type="button" class="btn btn-success btn-sm" v-on:click="reset_form()"><i class="fa fa-refresh"></i></button>
                    <b v-if="btn_unit">
                        <a :href="unit_url" id="btn_unit" type="button" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Add Unit</a>
                    </b>
                    <hr>
                </div>

            </div>
        </div>
    </form>
</div>

<div class="panel panel-info">

    <div class="row">
        <div class="col-md-12">
            <table id="table_customer" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Customer&nbsp;Name</th>
                        <th>Address</th>
                        <th>Branch</th>
                        <th>Owner</th>
                        <th>Created At</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

</div>
