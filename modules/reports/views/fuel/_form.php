<?php
use app\smartadmin\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

app\smartadmin\assets\plugins\PluginsAssets::register($this);
use yii\bootstrap\Tabs;

$form = ActiveForm::begin([
    'id' => 'form-analisa',
    'fieldConfig' => [
        'template' => "{label}<label class='input'>{input}</label>{error}",
        'inputOptions' => ['class' => 'form-control'],
    ],
    'options' => [
        'class' => 'smart-form',
    ],
]);
$itab = [];
$itab[] = [
    'label' => 'Unit Property',
    'content' => $this->render('_unit_property', ['model' => $model, 'form' => $form]),
    'active' => $itab == [],
];

$itab[] = [
    'label' => 'Data Analisa',
    'content' => $this->render('_analisa', ['model' => $model, 'form' => $form]),
    'active' => $itab == [],
];

echo Tabs::widget(['items' => $itab]);

?>

<footer>
    <?= Html::submitButton(Yii::t('app', $model->isNewRecord ? 'Create' : 'Save '),
        ['class' => 'btn btn-primary']) . Html::a('<i class="fa fa-rotate-left"></i> Go Back', Url::to(['/reports/fuel']), ['class' => 'btn btn-default', 'type' => "button"]) ?>
</footer>

<?php ActiveForm::end();
$url_lab=Url::toRoute(['/reports/fuel/lab']);
$index=Url::toRoute(['/reports/fuel']);
$jsCode=<<<JS
$(function() {
    let tbl=$('#uploadspase').find('#w0');
    let form=$('#form-analisa');
    function getlab(){
            $.ajax({
                url: '$url_lab',
                // data: {value: value, obj: obj},
                type: "POST",
                    success: function(data) {
                    $('#searchfuel-lab_number').val(data);
                }
            });
        }
  let par = function () {
        return {
             width: '100%',
            ajax: {
                url: function (a) {
                    return this.attr('data-url');
                },
                data: function (params) {
                    // console.log()
                    let el = this;
                    let query = {
                        q: params.term,
                        param: {
                            // console.log(el.attr('data-depend'));
                            col: el.attr('data-depend'),
                            fromcol: el.attr('data-depend-from'),
                            filter: form.find('#' + el.attr('data-depend-id')).val(),
                        }
                    };
                    return query;
                },
                processResults: function (data) {
                    return {
                        results: data.results
                    };
                }
            }
        }
    };
    form.find('.select23').select2(par());  
     form.find('#searchfuel-customer_id').on('select2:select',function(e) {
        form.find('#searchfuel-attention_id').val(null).trigger('change');
    });
     form.find('.datepct').datepicker({
            todayHighlight: true,
            dateFormat : 'yy-mm-dd',       
    });
     form.find('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        if ($(e.target).attr('href') == '#w0-tab2' && cho === 0) {
            var p = form.find($(e.target).attr('href'));
            select_detail(p);
            $.each(p.find('tr'), function (i, v) {
                let elm = $(v);
                if (elm.attr('data-id') !== undefined) {
                    genselectother(elm.attr('data-id'));
                }
            });
            cho++;
        }
    });
      $(form).on("beforeSubmit", function () {
          let formData = new FormData(this);
          let url=form.attr('action');
          $.ajax({
                url: form.attr('action') ,
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                }).done(function(response) {
                    if(response.status==='success'){
                       getlab();
                       alert_notif(response)
                    }else{
                         alert_notif(response)
                    }
                      if(url.indexOf('edit')>0){
                       $('#uploadspase').collapse('show');
                       $('#analisa').collapse('hide');
                       tbl.DataTable().ajax.reload();
                    }
                    
                }).fail(function(jqXHR, textStatus, errorThrown){
                    alert("gagal");
                })
     }).on('submit', function(e){
         e.preventDefault();
     });
    
      $('#cetan').click(function() {
          let den=form.find('#searchfuel-density').val();
          let des_50=form.find('#searchfuel-distillation_50').val();
        $.ajax({
            url:'$index'+'/cetan',
            method:'post',
            data:{density:den,dest_50:des_50},
        }).done(function(response) {
          form.find('#searchfuel-cetane_index').val(response);
        }).fail(function() {
          alert('fail');
        })
      });
       function alert_notif(res) {
        $.sound_on = false;
        var color = res.status === 'success' ? '#659265' : '#C46A69';
        var icon = res.status === 'success' ? 'fa-check' : 'fa-times';
        $.smallBox({
            title: res.status,
            content: res.message,
            color: color,
            iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
            timeout: 4000
        });
    }
})
JS;
$this->registerJS($jsCode,3,'blabla');
