<?php
/**
 * Created by
 * User: Wisard17
 * Date: 9/29/2017
 * Time: 4:11 PM
 */


/* @var $this yii\web\View */
/* @var $model \app\modules\master\customers\models\Customers */

use yii\helpers\Url;
// use app\modelsDB\Attention;
$this->title = Yii::t('app', 'Add');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Analisa'), 'url' => Url::toRoute(['/reports/fuel'])];
$this->params['breadcrumbs'][] = $this->title;
?>

<section id="widget-grid" class="">

    <!-- START ROW -->
    <div class="row">

        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-3" data-widget-editbutton="false">

                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                    <h2>Form  </h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body ">
                        <?= $this->render('_form', [
                            'model' => $model,
                        ]) ?>
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- END COL -->

    </div>

    <!-- END ROW-->

</section>
<!-- end widget grid -->
