<?php

/**
 * Created by
 * User     : Wisard17
 * Date     : 26/09/2019
 * Time     : 08.40 AM
 * File Name: ${FILE_NAME}
 **/

/* @var $this \yii\web\View */
/* @var $model \app\modules\report\models\data\Report */
/* @var $history \app\modules\master\models\data\Transaksi[] */

// $spaceTable = $model->spaceTable();
use yii\helpers\Url;

$draft = 0;
?>

<div class="line_top"></div>
<?= $this->render('_view_header_row', ['model' => $model]) ?>
<div class="br"></div>
<?= $this->render('_view_row_detail_', ['model' => $model]) ?>
