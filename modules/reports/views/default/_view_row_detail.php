<?php

use app\component\TA;

$type = 2;
$color = "#ababab";
$i = 0;
$dis = false;
$par = false;
$j = 0;
$dot1='';
$dot2='';
$dot3='';
$dot = ['*'=>'','**'=>'','***'=>''];


?>
<fieldset style="border:1.5px solid <?= $color ?>;">
    <table style="width: 100%;border-collapse: collapse;border-spacing: 0;">
        <tr style="background-color:#ebe9e9;">
            <th width="25" align="center" <?= TA::tHeadC($color) ?>>NO</th>
            <th width="250" align="center" <?= TA::tHeadC($color) ?>>PARAMETER</th>
            <th width="100" align="center" <?= TA::tHeadC($color) ?>>UNIT</th>
            <th width="150" align="center" <?= TA::tHeadC($color) ?>>METHOD</th>
            <th width="100" align="center" <?= TA::tHeadC($color) ?>>RESULT</th>
            <th align="center" <?= TA::tHeadEnd($color) ?>>TYPICAL *</th>
        </tr>
        <?php foreach ($model->renderParameter()  as $param) {
            $col = $val['col'];
            $code = $val['code'];
            $typical = $val['typical'];
            if ($model->$col == null || $model->$col == '') {
                continue;
            }
            $sk=$model->skMigas;
            $dot1=($col!='iso_4406' || $col!='cerosine_content')?'* '.$sk:$dot1;
            $dot2=$col == 'iso_4406'?'** Wordwide Fuel Charter 2019':$dot2;
            $dot3=$col == 'cerosine_content'?'*** Kesepakatan UT-Petrolab':$dot3;


            if (substr($col, 0, 12) == 'distillation' && $dis == false) { ?>
                <tr style="border-bottom:  solid <?= $color ?>;border-width: thin">
                    <td align="center" <?= TA::tbC($color) ?>><?= $i + 1 ?></td>
                    <td align="center" <?= TA::tbC2($color) ?>>Distillation Recovery Basis</td>
                    <td align="center" <?= TA::tbC($color) ?>>C</td>
                    <td align="center" <?= TA::tbC($color) ?>>ASTM 86-12</td>
                    <td align="center" <?= TA::tbC($color) ?>></td>
                    <td align="center" <?= TA::tbEnd($color) ?>></td>

                </tr>

            <?php $dis = true;
                $i++;
            } elseif (substr($col, 0, 9) == 'particles' && $par == false) {
            ?>
                <tr style="border-bottom:  solid <?= $color ?>;border-width: thin">
                    <td align="center" <?= TA::tbC($color) ?>><?= $i + 1 ?></td>
                    <td align="center" <?= TA::tbC2($color) ?>>Solid Particless (Particle size)</td>
                    <td align="center" <?= TA::tbC($color) ?>></td>
                    <td align="center" <?= TA::tbC($color) ?>></td>
                    <td align="center" <?= TA::tbC($color) ?>></td>
                    <td align="center" <?= TA::tbEnd($color) ?>></td>
                </tr>
            <?php
                $par = true;
            }
            $i = (substr($col, 0, 12) == 'distillation' || substr($col, 0, 9) == 'particles') ? $i : $i + 1;
            $align = ($col == 'iso_4406') ? "center" : "";
            ?>


            <tr style="border-bottom:  solid <?= $color ?>;border-width: thin">
                <td align="center" <?= TA::tbC($color) ?>><?= (substr($col, 0, 12) == 'distillation') ? '' : (substr($col, 0, 9) == 'particles' ? '' : ($col == 'iso_4406' ? '' : $i)) ?></td>
                <td align="<?= $align ?>" <?= $col == 'iso_4406' ? TA::tbC2($color) : TA::tbC($color) ?>><?= $val['nama'] ?></td>
                <td align="center" <?= TA::tbC($color) ?>><?= $val['satuan'] ?></td>
                <td align="center" <?= TA::tbC($color) ?>><?= $val['method'] ?></td>
                <td align="center" <?= TA::tbC2($color, TA::colorCode($model->$code)) ?>><?= $model->$col ?></td>
                <td align="center" <?= TA::tbEnd($color) ?>><?= $typical . ($col == 'iso_4406' ? ' **' : ($col == 'cerosine_content' ? '***' : '')) ?></td>
            </tr>

        <?php

        }
         $dot=[
                '*'=>$dot1,
                '**'=>$dot2,
                '***'=>$dot3
            ];
             ?>
    </table>
</fieldset>
<?= $this->render('_view_remark', ['model' => $model,'dot'=>$dot,'color'=>$color]) ?>