<?php
use app\component\TA;
use yii\helpers\Url;
$draft=0;
?>
<table <?= TA::tb_borderTB(5) ?>>
    <tr <?= TA::tR() ?>>
        <td style="font-size:10px;text-decoration:underline" align="left">Nama Pelanggan<br>
            <p style="font-style:oblique;text-decoration:none">(Customer Name)</p>
        </td>
        <td align="right" width="20">&nbsp;:<?= TA::space(5) ?></td>
        <td <?= TA::tHead() ?> width="300" align="left">
            <?= $model->customer_type==1? 'PT UNITED TRACTORS tbk': $model->branch ?>
        </td>
        <td style="font-size:14px;" colspan="3" align="center">Overal Analysis Result</td>

    </tr>
    <tr <?= TA::tR() ?>>
        <td style="font-size:10px;text-decoration:underline" align="left">Alamat<br>
            <p style="font-style:oblique;text-decoration:none">(Address)</p>
        </td>
        <td align="right" width="20">&nbsp;:<?= TA::space(5) ?></td>
        <td <?= TA::tHead() ?> align="left">
            <?= $model->customer_type==1?$model->branch:$model->address ?>
        </td>
        <td rowspan="3" colspan="3" align="center" width="100">

                <img width="34" height="55" src="<?='.'.  Url::to($model->logo($model->eval_code))?>">
        
            <p <?= TA::txt(14) ?>><?= $draft == 0 ? TA::codeStatus($model->eval_code) : '' ?></p>
        </td>
    </tr>
    <tr <?= TA::tR() ?>>
        <td style="font-size:10px;text-decoration:underline" align="left">Telepon/Fax<br>
            <p style="font-style:oblique;text-decoration:none">(Phone/Fax)</p>
        </td>
        <td align="right" width="20">&nbsp;:<?= TA::space(5) ?></td>
        <td <?= TA::tHead() ?> align="left">

        </td>
        <td> </td>
        <td> </td>
        <td> </td>
    </tr>
    <tr <?= TA::tR() ?>>
        <td style="font-size:10px;text-decoration:underline" align="left">Untuk Pelanggan<br>
            <p style="font-style:oblique;text-decoration:none">(Attention)</p>
        </td>
        <td align="right" width="20">&nbsp;:<?= TA::space(5) ?></td>
        <td <?= TA::tHead() ?> align="left">
             <?= $model->name ?>   
        </td>
        <td> </td>
        <td> </td>
        <td> </td>
    </tr>
    <tr <?= TA::tR() ?>>
        <td style="font-size:10px;text-decoration:underline" align="left">Nama Sample<br>
            <p style="font-style:oblique;text-decoration:none">(Sample Name)</p>
        </td>
        <td align="right" width="20">&nbsp;:<?= TA::space(5) ?></td>
        <td <?= TA::tHead() ?> align="left">
            <?= $model->type ?>
        </td>
        <td style="font-size:10px;text-decoration:underline" align="left">Lokasi Sampling<br>
            <p style="font-style:oblique;text-decoration:none">(Compartment)
        <td align="right" width="20">&nbsp;:<?= TA::space(5) ?></td>
        <td <?= TA::tHead() ?> align="left"><?= $model->compartment ?> </td>
    </tr>
    <tr <?= TA::tR() ?>>
        <td style="font-size:10px;text-decoration:underline" align="left">No Lab<br>
            <p style="font-style:oblique;text-decoration:none">(Lab No)</p>
        </td>
        <td align="right" width="20">&nbsp;:<?= TA::space(5) ?></td>
        <td <?= TA::tHead() ?> align="left">
            <?= $model->lab_number ?>
        </td>
        <td style="font-size:10px;text-decoration:underline" align="left">Barge/Ponton/Vissel<br>
            <p style="font-style:oblique;text-decoration:none">(Eng Builder)
        <td align="right" width="20">&nbsp;:<?= TA::space(5) ?></td>
        <td <?= TA::tHead() ?> align="left"><?= $model->eng_builder ?> </td>
    </tr>
    <tr <?= TA::tR() ?>>
        <td style="font-size:10px;text-decoration:underline" align="left">Tanggal Pengambilan Sample<br>
            <p style="font-style:oblique;text-decoration:none">(Sample Date)</p>
        </td>
        <td align="right" width="20">&nbsp;:<?= TA::space(5) ?></td>
        <td <?= TA::tHead() ?> align="left">
            <?= $model->sampleDate ?>
        </td>
        <td style="font-size:10px;text-decoration:underline" align="left">Type/Keterangan<br>
            <p style="font-style:oblique;text-decoration:none">(Type/Etc)
        <td align="right" width="20">&nbsp;:<?= TA::space(5) ?></td>
        <td <?= TA::tHead() ?> align="left"><?= $model->eng_type ?> </td>
    </tr>
    <tr <?= TA::tR() ?>>
        <td style="font-size:10px;text-decoration:underline" align="left">Tanggal Penerimaan Sample<br>
            <p style="font-style:oblique;text-decoration:none">(Received Date)</p>
        </td>
        <td align="right" width="20">&nbsp;:<?= TA::space(5) ?></td>
        <td <?= TA::tHead() ?> align="left">
            <?= $model->receiveDate ?>

        </td>
        <td style="font-size:10px;text-decoration:underline" align="left">No Seri<br>
            <p style="font-style:oblique;text-decoration:none">(Serial Number)
        <td align="right" width="20">&nbsp;:<?= TA::space(5) ?></td>
        <td <?= TA::tHead() ?> align="left"><?= $model->eng_sn ?> </td>
    </tr>
    <tr <?= TA::tR() ?>>
        <td style="font-size:10px;text-decoration:underline" align="left">Tanggal Report Analisis<br>
            <p style="font-style:oblique;text-decoration:none">(Analysis Date)</p>
        </td>
        <td align="right" width="20">&nbsp;:<?= TA::space(5) ?></td>
        <td <?= TA::tHead() ?> align="left">
            <?= $model->receiveDate . ' - ' . $model->reportDate ?>
        </td>
        <td style="font-size:10px;text-decoration:underline" align="left">Lokasi<br>
            <p style="font-style:oblique;text-decoration:none">(Location)
        <td align="right" width="20">&nbsp;:<?= TA::space(5) ?></td>
        <td <?= TA::tHead() ?> align="left"><?= $model->eng_location ?> </td>
    </tr>
</table>