<?php
/**
 * _view_row_detail_.php
 * Created By
 * feri_
 * 01/02/2023
 */
use app\component\TA;
use yii\bootstrap\Html;
$color = "#ababab";
$d2=0;
$d3=0;
$d1='';
?>
<fieldset style="border:1.5px solid #ababab;">
    <table style="width: 100%;border-collapse: collapse;border-spacing: 0;">
        <tr style="background-color:#ebe9e9;">
            <th width="25" align="center" <?= TA::tHeadC($color) ?>>NO</th>
            <th width="250" align="center" <?= TA::tHeadC($color) ?>>PARAMETER</th>
            <th width="100" align="center" <?= TA::tHeadC($color) ?>>UNIT</th>
            <th width="150" align="center" <?= TA::tHeadC($color) ?>>METHOD</th>
            <th width="100" align="center" <?= TA::tHeadC($color) ?>>RESULT</th>
            <th align="center" <?= TA::tHeadEnd($color) ?>>TYPICAL *</th>
        </tr>
        <tbody>
        <?php
        $i=0;
            foreach ($model->renderParameter()  as $param){
                $i++;
                $c=$param->col;
                $cd=$c.'_code';
                $m=$model->renderParamMatrix($param->parameter_id);
                $col=$model->hasAttribute($param->col)?$model->$c:'';
                $code=$model->hasAttribute($param->col)?$model->$cd:'';
                $align=$param->col==''?'center':'left';
                if($m!=''){
                    $d1=$m->jenis;
                    $exp1=explode('**',$m->B);
                    $exp2=explode('***',$m->B);
                    $d2=isset($exp1[1])?1:$d2;
                    $d3=isset($exp2[1])?1:$d3;
                }
//                if(strpos($m,'**')){
//                    $d2=1;
//                }
//                if(strpos($m,'***')){
//                    $d3=1;
//                }
//                echo Html::tag('tr',
//                Html::tag('td',$i).
//                Html::tag('td',$param->parameter->nama).
//                Html::tag('td',$param->parameter->unit).
//                Html::tag('td',$param->parameter->method).
//                Html::tag('td',$col).
//                Html::tag('td',$m==null?'':$m->B)
//                );
//                foreach ($param->parameter->renderChildParam() as $child){
//                    $c=$child->column_analisis_name;
//                    $m=$model->renderParamMatrix($child->id);
//                    $col=$model->hasAttribute($child->column_analisis_name)?$model->$c:'';
//                    echo Html::tag('tr',
//                        Html::tag('td','').
//                        Html::tag('td',$child->nama).
//                        Html::tag('td',$child->unit).
//                        Html::tag('td',$child->method).
//                        Html::tag('td',$col).
//                        Html::tag('td',$m==null?'':$m->B));
//                }
                ?>
                <tr style="border-bottom:  solid <?= $color ?>;border-width: thin">
                    <td align="center" <?= TA::tbC($color) ?>><?= $i?></td>
                    <td align="<?=$align ?>" <?= TA::tbC($color) ?>><?= $param->parameter->nama ?></td>
                    <td align="center" <?= TA::tbC($color) ?>><?= $param->parameter->unit ?></td>
                    <td align="center" <?= TA::tbC($color) ?>><?= $param->parameter->method ?></td>
                    <td align="center" <?= TA::tbC2($color, TA::colorCode($code)) ?>><?= $col ?></td>
                    <td align="center" <?= TA::tbEnd($color) ?>><?= $m==null?'':$m->B ?></td>
                </tr>
                <?php
                    foreach ($param->parameter->renderChildParam() as $child){
                        $c=$child->column_analisis_name;
                        $cd=$c.'_code';
                        $m=$model->renderParamMatrix($child->id);
                        $col=$model->hasAttribute($child->column_analisis_name)?$model->$c:'';
                        $code=$model->hasAttribute($child->column_analisis_name)?$model->$cd:'';
                        $align=$child->column_analisis_name==''?'center':'left';
                        if($m!=''){
                            $d1=$m->jenis;
                            // print_r($d1);
                            // exit;
                            $exp1=explode('**',$m->B);
                            $exp2=explode('***',$m->B);
                            $d2=isset($exp1[1])?1:0;
                            $d3=isset($exp2[1])?1:0;
                        }
//                        if(strpos($m,'**')){
//                            $d2=1;
//                        }
//                        if(strpos($m,'***')){
//                            $d3=1;
//                        }
                        ?>
                        <tr style="border-bottom:  solid <?= $color ?>;border-width: thin">
                            <td align="center" <?= TA::tbC($color) ?>></td>
                            <td align="<?=$align ?>" <?= TA::tbC($color) ?>><?= $child->nama ?></td>
                            <td align="center" <?= TA::tbC($color) ?>><?= $child->unit ?></td>
                            <td align="center" <?= TA::tbC($color) ?>><?= $child->method ?></td>
                            <td align="center" <?= TA::tbC2($color, TA::colorCode($code)) ?>><?= $col ?></td>
                            <td align="center" <?= TA::tbEnd($color) ?>><?= $m==null?'':$m->B ?></td>
                        </tr>
                    <?php } ?>
                ?>
            <?php }?>

        </tbody>
    </table>
</fieldset>
<?= $this->render('_view_remark', ['model' => $model,'color'=>$color,'d1'=>$d1,'d2'=>$d2,'d3'=>$d3]) ?>
