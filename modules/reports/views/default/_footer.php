<table style="font-size: 10px;width: 100%">
    <tr>
        <td width="7%" align="left">
            <p style="text-decoration: underline">Catatan :</p>
            <p style="font-style: italic">(Note)</p>        
        </td>
        <td style="font-style:oblique;" align="left">
            <p>-Data analisa hanya berlaku untuk sample yang di uji di laboratorium PT. Petrolab Service</p>
            <p>-Report tidak boleh di gandakan tanpa persetujuan tertulis dari laboratorium</p>
            <p>-Laboratorium PT. Petrolab Services tidak bertanggung jawab terhadap kegiatan sampling</p>
            <p>-Pengaduan tidak dilayani setelah 30 hari dari tanggal report di terbitkan</p>
        </td>
        <td width="15%" align="left">RK/7.8/04</td>
    </tr>
</table>