<?php

/**
 * Created by
 * User     : Wisard17
 * Date     : 27/09/2019
 * Time     : 01.19 PM
 * File Name: ${FILE_NAME}
 **/

/* @var $this \yii\web\View */

use yii\helpers\Url; ?>

<div style="position:relative;width:100%">
    <div style="float: left; margin: 0mm 0mm 0mm 0mm;">
        <img width="220px" src=".<?= Url::to('/img/reports/logo.png') ?>">
    </div>

    <div style="float: right;margin: -10mm 7mm 0 0;width: 77mm;text-align: center;font-size: 10px;">
        <div style="border:0.1mm solid #000000;
	background-color: #f0f2ff;
	background-gradient: linear #d8d3d6 #f0f2ff 0 2 0 0.5;
	border-radius: 2mm; padding: 5px;
	background-clip: border-box; text-align: center; vertical-align: center;">
            <h5 class="bold" style="color: #000000; margin: auto;">
                <span style="font-size: 17px">FUEL ANALYSIS REPORT</span>
            </h5>
        </div>
    </div>
    
</div>
<br>
<div style="float: right;padding:10px; margin: -10mm 7mm 0 0;width: 77mm;text-align: center;font-size: 10px;">
   <span style="font-size: 10px">Balikpapan, <?= date('d F-Y',strtotime($model->report_date)) ?> </span>
</div>
