<?php
use yii\helpers\Url;
use app\modules\reports\assets\TA;
use app\modelsDB\Parameter;
use app\modelsDB\ReportTypeHasParameter as RTP;
use app\modules\reports\helpers\Chart;
require_once(__DIR__.'/../../lib/phpchartdir.php');


$hm=Yii::$app->HM;
$i=0;
$no=0;
function tgl_w(){
	return 47;
};
$type=1;
$col_now=0;
$over=55;
$report_type=RTP::find()->where(['report_type_id'=>$type])->all();
$chart=new Chart;
$chart->deleteAllChart('oa');
$chart->makeChartOa(new XYChart(720, 195, 0xffffff, 0x000000, 0),$datas);
?>
<!DOCTYPE html>
<html>
<head>

</head>
<body>
	<div id="contoh2">
		<table>
			<tr>
				<td>
					<img <?= TA::logo() ?> src="<?= Yii::$app->HM->urlImg('reports/logo.png') ?>">
				</td>
				<td><?= TA::space(200) ?></td>
				<td align="center">
					<img <?= TA::logo() ?> src="<?= Yii::$app->HM->urlImg('reports/chart/default/oil_quality.png') ?>">
					<div <?= TA::txt(20) ?> >No.TR:0000777/TR/1/18</div>
				</td>
			</tr>
		</table>
		<div <?= TA::overLine() ?> ></div>
		<table <?= TA::tb_borderTB(9) ?>>
			<tr <?= TA::tR()?>>
				<td <?= TA::tHead() ?> align="left">Customer Name</td>
				<td align="right" width="100">&nbsp;:<?= TA::space(5) ?></td>
				<td <?= TA::tHead() ?> align="left"><?=$analisa['customer']['name']?></td>
				<td width="150"></td>
				<td <?= TA::tHead() ?> align="left">Operating Voltage</td>
				<td align="right" width="50">&nbsp;:<?= TA::space(5) ?></td>
				<td <?= TA::tHead() ?> align="left">
					<?php if(isset($analisa['unit'])){ ?>
					<?= $analisa['unit']['operating_voltage'] ?>
					<?php } ?>
				</td>
				<td><?= TA::space(9) ?></td>
				<td <?= TA::tHead() ?> align="left">KV</td>
			</tr>
			<tr <?= TA::tR()?>>
				<td <?= TA::tHead() ?> align="left">Attention For</td>
				<td align="right" width="100">&nbsp;:<?= TA::space(5) ?></td>
				<td <?= TA::tHead() ?> align="left">
					<?php if(isset($analisa['attention'])){ ?>
					<?= $analisa['attention']['name'] ?>
					<?php } ?>
				</td>

				<td></td>

				<td <?= TA::tHead() ?> align="left">Category</td>
				<td align="right" width="50">&nbsp;:<?= TA::space(5) ?></td>
				<td <?= TA::tHead() ?> align="left">
					<?php if(isset($analisa['unit']['category'])){ ?>
					<?= $analisa['unit']['category']['name'] ?>
					<?php } ?>
				</td>
			</tr>
			<tr <?= TA::tR()?>>
				<td <?= TA::tHead() ?> align="left">Location</td>
				<td align="right" width="100">&nbsp;:<?= TA::space(5) ?></td>
				<td <?= TA::tHead() ?> align="left">
					<?php if(isset($analisa['unit'])){ ?>
					<?= $analisa['unit']['location'] ?>
					<?php } ?>
				</td>

				<td><?= TA::space(25) ?></td>

				<td <?= TA::tHead() ?> align="left">Rating</td>
				<td align="right" width="50">&nbsp;:<?= TA::space(5) ?></td>
				<td <?= TA::tHead() ?> align="left">
					<?php if(isset($analisa['unit'])){ ?>
					<?= $analisa['unit']['rating'] ?>
					<?php } ?>
				</td>
				<td><?= TA::space(9) ?></td>
				<td <?= TA::tHead() ?> align="left">KVA</td>
			</tr>
			<tr <?= TA::tR()?>>
				<td <?= TA::tHead() ?> align="left">Transformer ID</td>
				<td align="right" width="100">&nbsp;:<?= TA::space(5) ?></td>
				<td <?= TA::tHead() ?> align="left">
					<?php if(isset($analisa['unit'])){ ?>
					<?= $analisa['unit']['transformer_id'] ?>
					<?php } ?>
				</td>

				<td><?= TA::space(25) ?></td>

				<td <?= TA::tHead() ?> align="left">Operating Temperature Oil</td>
				<td align="right" width="50">&nbsp;:<?= TA::space(5) ?></td>
				<td <?= TA::tHead() ?> align="left">
					<?php if(isset($analisa['unit'])){ ?>
					<?= $analisa['unit']['operating_temperature_of_oil'] ?>
					<?php } ?>
				</td>
				<td><?= TA::space(9) ?></td>
				<td <?= TA::tHead() ?> align="left">&deg;c</td>
			</tr>
			<tr <?= TA::tR()?>>
				<td <?= TA::tHead() ?> align="left">Transformer Manufacture</td>
				<td align="right" width="100">&nbsp;:<?= TA::space(5) ?></td>
				<td <?= TA::tHead() ?> align="left">
					<?php if(isset($analisa['unit'])){ ?>
					<?= $analisa['unit']['transformer_manufacture'] ?>
					<?php } ?>
				</td>

				<td><?= TA::space(25) ?></td>

				<td <?= TA::tHead() ?> align="left">Oil Volume</td>
				<td align="right" width="50">&nbsp;:<?= TA::space(5) ?></td>
				<td <?= TA::tHead() ?> align="left">
					<?php if(isset($analisa['unit'])){ ?>
					<?= $analisa['unit']['total_oil_volume'] ?>
					<?php } ?>
				</td>
				<td><?= TA::space(9) ?></td>
				<td <?= TA::tHead() ?> align="left">Kg</td>
			</tr>
			<tr <?= TA::tR()?>>
				<td <?= TA::tHead() ?> align="left">Serial Number</td>
				<td align="right" width="100">&nbsp;:<?= TA::space(5) ?></td>
				<td <?= TA::tHead() ?> align="left">
					<?php if(isset($analisa['unit'])){ ?>
					<?= $analisa['unit']['serial_number'] ?>
					<?php } ?>
				</td>

				<td><?= TA::space(25) ?></td>

				<td <?= TA::tHead() ?> align="left">Oil Brand</td>
				<td align="right" width="50">&nbsp;:<?= TA::space(5) ?></td>
				<td <?= TA::tHead() ?> align="left">
					<?php if(isset($analisa['unit'])){ ?>
					<?= $analisa['unit']['oil_brand'] ?>
					<?php } ?>
				</td>
			</tr>
			<tr <?= TA::tR()?>>
				<td <?= TA::tHead() ?> align="left"></td>
				<td>&nbsp;&nbsp;</td>
				<td <?= TA::tHead() ?> align="left"></td>

				<td><?= TA::space(25) ?></td>

				<td <?= TA::tHead() ?> align="left">Sample Point</td>
				<td align="right" width="50">&nbsp;:<?= TA::space(5) ?></td>
				<td <?= TA::tHead() ?> align="left">
					<?php if(isset($analisa['unit'])){ ?>
					<?= $analisa['unit']['sample_point'] ?>
					<?php } ?>
				</td>
			</tr>
			<tr <?= TA::tR()?>>
				<td <?= TA::tHead() ?> align="left"></td>
				<td>&nbsp;&nbsp;</td>
				<td <?= TA::tHead() ?> align="left"></td>

				<td><?= TA::space(25) ?></td>

				<td <?= TA::tHead() ?> align="left">Cooling System</td>
				<td align="right" width="50">&nbsp;:<?= TA::space(5) ?></td>
				<td <?= TA::tHead() ?> align="left">
					<?php if(isset($analisa['unit'])){ ?>
					<?= $analisa['unit']['cooling_system'] ?>
					<?php } ?>
				</td>
			</tr>
		</table>
		<div <?= TA::overLine() ?> ></div>
		<div <?= TA::txt(12) ?> >Test Details <?= TA::space(135) ?> Overall Analysis Result</div>
		<div>
			<fieldset style="border:1px solid black;">
				<table <?= TA::tb_borderTB(9) ?> >

					<tr>
						<td colspan="4" align="left" <?= TA::tb_border2(9,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?>>Lab&nbsp;Number</td>
						<?php foreach ($datas as $key => $value) {$i=$i+1;$col_now=$col_now+1; ?>
						<td align="center" width="<?=tgl_w()?>" <?= TA::tb_border(9) ?> >
							<?= $value['lab_number'] ?>
						</td>
						<?php } ?>
						<?=autoColumn($col_now)?>
					</tr>
					<tr>
						<td colspan="4" <?= TA::tb_border2(9,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >Sampling&nbsp;Date</td>
						<?php foreach ($datas as $key => $value) {$i=$i+1; ?>
						<td align="center" <?= TA::tb_border(9) ?> >
							<?= $hm->fDate('d-M-y',$value['sampling_date']) ?>
						</td>
						<?php } ?>
						<?=autoColumn($col_now)?>
						
					</tr>
					<tr>
						<td colspan="4" <?= TA::tb_border2(9,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >Receive&nbsp;Date</td>
						<?php foreach ($datas as $key => $value) {$i=$i+1; ?>
						<td align="center" <?= TA::tb_border(9) ?> >
							<?= $hm->fDate('d-M-y',$value['received_date']) ?>
						</td>
						<?php } ?>
						<?=autoColumn($col_now)?>
					</tr>
					<tr>
						<td colspan="4" <?= TA::tb_border2(9,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >Report&nbsp;Date</td>
						<?php foreach ($datas as $key => $value) { ?>
						<td align="center" <?= TA::tb_border(9) ?>>
							<?= $hm->fDate('d-M-y',$value['report_date']) ?>
						</td>
						<?php } ?>
						<?=autoColumn($col_now)?>
						<td align="center" colspan="4" width="<?=$over?>" <?= TA::tb_border2(9,"border-bottom: 0.5px solid black;") ?>></td>
						<td width="<?=$over?>" <?= TA::tb_border2(9,"border-bottom: 0.5px solid black;") ?>></td>
						<td width="<?=$over?>" <?= TA::tb_border2(9,"border-bottom: 0.5px solid black;") ?>></td>
					</tr>

					<tr <?= TA::tb_border(9) ?>>
						<th colspan="9" align="left" <?= TA::headContent(9) ?> >Phisycal Test</th>
						<th colspan="8" align="center" <?= TA::headContent(9) ?> >Limitation</th>

					</tr>
					</tr>

					<tr>
						<th align="center" width="5" <?= TA::headContent(9) ?> >No</th>
						<th align="center" width="150" <?= TA::headContent(9) ?> >Paramter Uji</th>
						<th align="center" width="40" <?= TA::headContent(9) ?> >Unit</th>
						<th align="center" width="70" <?= TA::headContent(9) ?> >Method</th>
						<th align="center" colspan="8" <?= TA::headContent(9) ?> >Result</th>
						<th align="center" width="<?=$over?>" <?= TA::headContent(9) ?> >Good</th>
						<th align="center" width="<?=$over?>" <?= TA::headContent(9) ?> >Fair</th>
						<th align="center" width="<?=$over?>" <?= TA::headContent(9) ?> >Poor</th>
					</tr>

					<?php foreach ($report_type as $key => $value) { ?>
					<?php
					$method=$value['parameter']['method'];
					$unit=$value['parameter']['satuan'];
					$param=$value['parameter']['column_analisis_name'];
					if($value['parameter']['column_analisis_name']!=""){
					$param_code=$value['parameter']['column_analisis_name']."_code";	
					}
					if($method){
						$no=$no+1;	
					}
					?>
					<tr <?= TA::tb_border2(9,"border-right: 0.5px solid black;") ?>>
						<td <?= TA::tb_border(9) ?>>
							<?php if($method){echo $no;}else{echo '';} ?>
						</td>
						<td><?=$value['parameter']['nama']?></td>
						<td align="center" <?= TA::tb_border2(9,"border-left: 0.5px solid black;") ?>><?=$unit ?></td>
						<td align="center" <?= TA::tb_border2(9,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?>><?=$method ?></td>
						<?php foreach ($datas as $key => $value) {$codes=colorCode($value[$param_code]);?>

						<td align="center" <?= TA::tb_border2(9,"border-right: 0.5px solid black;$codes") ?> width="<?=tgl_w()?>">
							<?php if(isset($value[$param])){ ?>
							<?= $value[$param] ?>
							<?php }else{echo '-';} ?>
						</td>
						<?php } ?>
						<?=autoColumn($col_now)?>
					</tr>
					<?php }?>

				</table>
			</fieldset>

			<div <?= TA::overLine2(3) ?> ></div>
			<br>
			<div id='chart'>
				<img src="/img/reports/chart/run/oa/chart.png">
			</div>
			<h6><b <?= TA::txt(9) ?>>Source of abnormality</b></h6>
			<fieldset style="border:0.5px outset black; width: 100%">
				
			</fieldset>
			<h6><b <?= TA::txt(9) ?>>Action to be taken</b></h6>
			<fieldset style="border:0.5px outset black; width: 100%">
				
			</fieldset>
			<table width="600">
				<tr>
					<td align="right" <?= TA::txt(10) ?>>
						Sr. Manager Teknis
						<?= TA::br(3) ?>
						Asbari. Ssi<?= TA::space(5) ?>
					</td>
				</tr>
			</table>
			<section style="width: 100%; background: #d6d6c2;">
				<p align="center" <?= TA::txt(7) ?>><?= "PT Petrolab Services"."<br>"."JJl Pisangan Lama III No 28 - Jakarta Timur 13230; Telp: +62 21 4717001; Fax: +62 21 4716985"."<br>"."petrolab@cbn.net.id;  http://www.petrolab.co.id" ?></p>
			</section>
		</div>
	</div>
</body>
</html>

<?php
function autoColumn($col){?>
<?php
$aa=0;
if($col==0){
	$aa=5;
}elseif($col==1){
	$aa=4;
}elseif($col==2){
	$aa=3;
}elseif($col==3){
	$aa=2;
}elseif($col==4){
	$aa=1;
}
if($col<5){
	for ($bb=0; $bb <$aa ; $bb++) { ?>
	<td align="center" width="<?=tgl_w()?>" <?= TA::tb_border2(9,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >-</td>
	<?php
}
}
}

function autoColspan($col,$i){
	$rr=0;
	$aa=0;
	if($col==0){
		$aa=5;
	}elseif($col==1){
		$aa=4;
	}elseif($col==2){
		$aa=3;
	}elseif($col==3){
		$aa=2;
	}elseif($col==4){
		$aa=1;
	}
	if($col<5){
		for ($bb=0; $bb <$aa ; $bb++) {
			$rr=$rr+1;
		}
	}
	return $i+$rr;
}
function colorCode($code){
	
	if(strtoupper($code)=='A'){
		
	}elseif(strtoupper($code)=='B'){
		return "background:#ffff00;";
	}elseif(strtoupper($code)=='C'){
		return "background:#ff0000;";
	}
}
	?>

<?php


?>