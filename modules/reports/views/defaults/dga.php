<?php
use yii\helpers\Url;
use app\modules\reports\assets\TA;
use app\modelsDB\Parameter;
use app\modelsDB\ReportTypeHasParameter as RTP;
use app\modules\reports\helpers\Chart;
use app\modules\reports\helpers\Parameter as Par;
use app\modules\reports\models\HelpMatric as Hm;
use app\modules\reports\helpers\Filter;
require_once(__DIR__.'/../../lib/phpchartdir.php');

$hm=Yii::$app->HM;
$i=0;
$no=0;
$no2=0;
function tgl_w2(){
	return 47;
};
$type=2;
$col_now=0;
$over=58;
$over_furan=45;
$report_type=RTP::find()
->where(['report_type_id'=>$type])
->andWhere(['!=','p_code','furan'])
->all();
$report_furan=RTP::find()
->where(['report_type_id'=>$type])
->andWhere(['p_code'=>'furan'])
->all();

$chart=new Chart;
$chart->deleteAllChart('dga');
$chart->makeChartDga(new XYChart(720, 195, 0xffffff, 0x000000, 0),$datas);
?>
<!DOCTYPE html>
<html>
<head>

</head>
<body>

	<div style="max-height: 1000px;">
		<?= theader(Yii::$app->HM->urlImg('reports/chart/default/dga.png'),150) ?>
		<div <?= TA::overLine() ?> ></div>
		<?= $this->render('header',['analisa'=>$analisa]) ?>
		<div <?= TA::overLine() ?> ></div>
		<div <?= TA::txt(12) ?> >Test Details <?= TA::space(135) ?> Overall Analysis Result</div>
		<div>
			<fieldset style="border:1px solid black;">
				<table <?= TA::tb_borderTB(11) ?> >

					<tr>
						<td colspan="4" align="left" <?= TA::tb_border2(11,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?>>Lab&nbsp;Number</td>
						<?php foreach ($datas as $key => $value) {$i=$i+1;$col_now=$col_now+1; ?>
						<td align="center" width="<?=tgl_w2()?>" <?= TA::tb_border(11) ?> >
							<b><?= $value['lab_number'] ?></b>
						</td>
						<?php } ?>
						<?=auto_column2($col_now)?>
						<td align="center" width="<?=$over?>" <?= TA::tb_border2(9,"border-bottom: 0.5px solid black;") ?>>
							<b>Status</b>
						</td>
						<td align="center" width="<?=$over?>" <?= TA::tb_border2(9,"border-left: 0.5px solid black;border-bottom: 0.5px solid black;") ?>>
							<b>Fault Type</b>
						</td>
						<td colspan="2" align="center" colspan="2" width="<?=$over?>" <?= TA::tb_border2(9,"border-left: 0.5px solid black;border-bottom: 0.5px solid black;") ?>>
							<b>Resampling Date</b>
						</td>
					</tr>
					<tr>
						<td colspan="4" <?= TA::tb_border2(11,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >Sampling&nbsp;Date</td>
						<?php foreach ($datas as $key => $value) {$i=$i+1; ?>
						<td align="center" <?= TA::tb_border(11) ?> >
							<?= $hm->fDate('d-M-y',$value['sampling_date']) ?>
						</td>
						<?php } ?>
						<?=auto_column2($col_now)?>
						<td rowspan="3" align="center" width="<?=$over?>" <?= TA::tb_border2(9,"border-bottom: 0 solid black;") ?>>
							<img class="logo1" width="20" height="35" <?=TA::logo_code1()?> src="<?= Yii::$app->HM->urlImg(Par::codeImg($analisa['eval_code_dga'])) ?>">
							<br>
							<b><?= Par::codeStatusDga($analisa['eval_code_dga']) ?></b>
						</td>
						<td rowspan="3" align="center" width="<?=$over?>" <?= TA::tb_border2(9,"border-left: 0.5px solid black;") ?>>
							<b style="color:red;">-</b>
						</td>
						<td rowspan="3" colspan="2" align="center" colspan="2" width="<?=$over?>" <?= TA::tb_border2(9,"border-left: 0.5px solid black;") ?>>
							<b style="color:red;"><?= Filter::resDate($analisa['report_date'],$analisa['resampling_dga']) ?></b>
						</td>
						
					</tr>
					<tr>
						<td colspan="4" <?= TA::tb_border2(11,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >Receive&nbsp;Date</td>
						<?php foreach ($datas as $key => $value) {$i=$i+1; ?>
						<td align="center" <?= TA::tb_border(11) ?> >
							<?= $hm->fDate('d-M-y',$value['received_date']) ?>
						</td>
						<?php } ?>
						<?=auto_column2($col_now)?>
					</tr>
					<tr>
						<td colspan="4" <?= TA::tb_border2(11,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >Report&nbsp;Date</td>
						<?php foreach ($datas as $key => $value) { ?>
						<td align="center" <?= TA::tb_border(11) ?>>
							<?= $hm->fDate('d-M-y',$value['report_date']) ?>
						</td>
						<?php } ?>
						<?=auto_column2($col_now)?>
						
					</tr>

					<tr <?= TA::tb_border(11) ?>>
						<th colspan="9" align="left" <?= TA::headContent(11) ?> >Dissolved Gas Analysis (DGA)</th>
						<th colspan="4" align="center" <?= TA::headContent2(11,"border-bottom:0.5px solid black;border-top:0.5px solid black;") ?> >Limit of Condition</th>
					</tr>
				</tr>

				<tr>
					<th align="center" width="5" <?= TA::headContent(11) ?> >No</th>
					<th align="center" width="120" <?= TA::headContent(11) ?> >Paramter of Test</th>
					<th align="center" width="40" <?= TA::headContent(11) ?> >Unit</th>
					<th align="center" width="70" <?= TA::headContent(11) ?> >Method</th>
					<th align="center" colspan="5" <?= TA::headContent(11) ?> >Test Result</th>
					<th align="center" width="<?=$over?>" <?= TA::headContent(11) ?> >1</th>
					<th align="center" width="<?=$over?>" <?= TA::headContent(11) ?> >2</th>
					<th align="center" width="<?=$over?>" <?= TA::headContent(11) ?> >3</th>
					<th align="center" width="<?=$over?>" <?= TA::headContent2(11,"border-bottom:0.5px solid black;") ?> >4</th>
				</tr>

				<?php foreach ($report_type as $key => $value) { ?>
				<?php
				$method=$value['parameter']['method'];
				$unit=$value['parameter']['satuan'];
				$param=$value['parameter']['column_analisis_name'];
				if($value['parameter']['column_analisis_name'] != ""){
					$param_code=$value['parameter']['column_analisis_name']."_code";	
				}elseif($value['parameter']['column_analisis_name'] != "furan_total"){
					$param_code=$value['parameter']['column_analisis_name']."_code";	
				}
				if($method){
					$no=$no+1;	
				}
				?>
				<tr <?= TA::tb_border2(11,"border-right: 0.5px solid black;") ?>>
					<td align="right" <?= TA::tb_border(11) ?>>
						<?php if($method){echo $no;}else{echo '';} ?>&nbsp;
					</td>
					<td><?=$value['parameter']['nama']?></td>
					<td align="center" <?= TA::tb_border2(11,"border-left: 0.5px solid black;") ?>><?=$unit ?></td>
					<td align="center" <?= TA::tb_border2(11,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?>><?=$method ?></td>
					<?php foreach ($datas as $key2 => $value2) {
						try{
							$codes=Par::colorCode($value2[$param_code]);	
						}catch(Exception $e){

						}

						?>
						<td align="center" <?= TA::tb_border2(11,"border-right: 0.5px solid black;$codes") ?> width="<?=tgl_w2()?>">
							<?= $value2[$param] ? $value2[$param] : "-"  ?>
						</td>
						<?php } ?>
						<?=auto_column2($col_now)?>
						<td align="center" width="<?=$over?>" <?= TA::tb_border2(11,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >
							<?= Par::matric(Hm::matric2(10,$value['parameter_id'])['condition1']) ?>
						</td>
						<td align="center" width="<?=$over?>" <?= TA::tb_border2(11,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >
							<?= Par::matric(Hm::matric2(10,$value['parameter_id'])['condition2']) ?>
						</td>
						<td align="center" width="<?=$over?>" <?= TA::tb_border2(11,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >
							<?= Par::matric(Hm::matric2(10,$value['parameter_id'])['condition3']) ?>
						</td>
						<td align="center" width="<?=$over?>" <?= TA::tb_border2(11,"border-left: 0.5px solid black;") ?> >
							<?= Par::matric(Hm::matric2(10,$value['parameter_id'])['condition4']) ?>
						</td>
					</tr>
					<?php }?>

				</table>
			</fieldset>

			<div <?= TA::overLine2(3) ?> ></div>
			<br>
			<div id='chart'>
				<img src="/img/reports/chart/run/dga/chart.png">
			</div>

			<?= recomend($analisa,'dga') ?>

			<?= $this->render('footer') ?>
		</div>
	</div>

	<div style="page-break-after: always;"></div>
	<?= $this->render('furan',['datas'=>$datas,'analisa'=>$analisa]) ?>

	<div style="page-break-after: always;"></div>
	<?= $this->render('oa_part',['datas'=>$datas,'analisa'=>$analisa]) ?>

</body>
</html>

<?php
function auto_column2($col){?>
<?php
$aa=0;
if($col==0){
	$aa=5;
}elseif($col==1){
	$aa=4;
}elseif($col==2){
	$aa=3;
}elseif($col==3){
	$aa=2;
}elseif($col==4){
	$aa=1;
}
if($col<5){
	for ($bb=0; $bb <$aa ; $bb++) { ?>
	<td align="center" width="<?=tgl_w2()?>" <?= TA::tb_border2(11,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >-</td>
	<?php
}
}
}


?>

<?php
function recomend($analisa,$type){
	$req1="";
	$req2="";
	if($type=='dga'){
		$req1=$analisa['recommended1_dga'];
		$req2=$analisa['recommended2_dga'];
	}
	if($type=='furan'){
		$req1=$analisa['recommended1_furan'];
		$req2=$analisa['recommended2_furan'];
	}
	if($type=='oa'){
		$req1=$analisa['recommended1_oil_quality'];
		$req2=$analisa['recommended2_oil_quality'];
	}

	?>
	<div> <b <?= TA::txt(11) ?>>Source of abnormality</b></div>
	<fieldset style="border:0.5px outset black; width: 100%">
		<p <?= TA::txt(11) ?>><?= $req1 ?></p>
	</fieldset>
	<div> <b <?= TA::txt(11) ?>>Action to be taken</b></div>
	<fieldset style="border:0.5px outset black; width: 100%">
		<p <?= TA::txt(11) ?>><?= $req2 ?></p>
	</fieldset>
	<?php }?>

	<?php
	function theader($img,$w){?>
	<table>
		<tr>
			<td>
				<img <?= TA::logo() ?> src="<?= Yii::$app->HM->urlImg('reports/logo.png') ?>">
			</td>
			<td width="180"><?= TA::space(50) ?></td>
			<td align="center" width="<?= $w ?>">
				<img <?= TA::logo() ?> src="<?= $img ?>">
				<div <?= TA::txt(12) ?> >No.TR:0000777/TR/1/18</div>
			</td>
		</tr>
	</table>
	<?php } ?>


