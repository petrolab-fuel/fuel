<?php
use yii\helpers\Url;
use app\modules\reports\assets\TA;
use app\modelsDB\Parameter;
use app\modelsDB\ReportTypeHasParameter as RTP;
use app\modules\reports\helpers\Chart;
use app\modules\reports\helpers\Parameter as Par;
use app\modules\reports\models\HelpMatric as Hm;
use app\modules\reports\helpers\Filter;
// require_once(__DIR__.'/../../lib/phpchartdir.php');

$hm=Yii::$app->HM;
$i=0;
$no=0;
$dot='';
$no2=0;
$col_now=0;
$nk=0;
$loop=0;
$par=true;
$ut=false;
$pc=false;
$mg=false;
$val='';
$tb1='';
//$nm= strpos($analisa['customer']['name'],'UNITED TRACTORS');
// if(strpos($data->customer->name,'PAMA')){
//     $customer='';
// }else{
//     $customer=$data->customer->name;

// }

function tgl_w2(){
	return 47;
};

$type=2;
$color="#ababab";

?>

<?= theader(Yii::$app->HM->urlImg('reports/chart/default/fuel.png'),150,$data) ?>
    <div <?= TA::overLine() ?> ></div>
<?= $this->render('header',['data'=>$data,'draft'=>$draft]) ?>
<div class="br"></div>
<div class="line_top"></div>
<div class="br"></div>
<div >
    <fieldset style="border:1.5px solid <?=$color?>;">

        <table style="width: 100%;border-collapse: collapse;border-spacing: 0;">
            <tr style="background-color:#ebe9e9;">
                <th width="25" align="center" <?= TA::tHeadC($color)?>>NO</th>
                <th width="250" align="center" <?= TA::tHeadC($color)?>>PARAMETER</th>
                <th width="100" align="center" <?= TA::tHeadC($color)?>>UNIT</th>
                <th width="150" align="center" <?= TA::tHeadC($color)?>>METHOD</th>
                <th width="100" align="center" <?= TA::tHeadC($color)?>>RESULT</th>
                <th align="center" <?= TA::tHeadEnd($color)?>>TYPICAL *</th>
            </tr>
            <?php foreach($data->colAnalisis as $parameter=>$code){
                $nilai=$data->$parameter=='0.0'?'0':$data->$parameter;
                if($data->$parameter=='' || $data->$parameter==null){
                    continue;
                }

                $no=$no+1;$col_now=$col_now+1;
                $nm=$parameter=='particulat'?'':substr($parameter,0,3);
                if ($parameter=='cerosine_content'){
                    $dot=($mg==false && $pc==false)?'*':($mg==true && $pc==false)|| ($mg==false && $pc==true)?'**':'***';
//                   if($ut==false)
////                    $val ='<td >'.$dot.'Kesepakatan UT-Petrolab</td>';
                   $ut=true;
                }elseif ($parameter=='iso_4406' || $parameter=='particles_4' || $parameter=='particles_6' || $parameter=='particles_14' ){
                    $dot=$mg==false?'*':'**';
//                    if($pc==false)
//                       $val .='<td width="30%">'.$dot.'Worldwide Fuel Charter 2005</td>';
                    $pc=true;
                }else{
                    $dot='*';
//                    $mg==false?$val.='<td width="30%">'.$dot. 'DIRJEN Migas No.28.K/10/DJM.T/2016</td>':$val='<td align="left">* DIRJEN Migas No.28.K/10/DJM.T/2016</td><td></td><td></td></td>';
                    $mg=true;
                }
                $i=$no-$loop;
                if ($nm=='dis'){
                    if ($loop==0){
                        ?>
                        <tr style="border-bottom:  solid <?=$color ?>;border-width: thin">
                            <td align="center" <?= TA::tbC($color)?>><?= $i ?></td>
                            <th <?= TA::tbC($color)?>>Distillation Recovery Basis</th>
                            <td align="center" <?= TA::tbC($color)?>>C</td>
                            <td align="center" <?= TA::tbC($color)?>>ASTM D 86-12</td>
                            <td align="center" <?= TA::tbC2($color,Par::colorCode($data->$code))?>></td>
                            <td align="center" <?= TA::tbEnd($color)?>></td>
                        </tr>
                        <?php
                        // $n=$n+1;
                    }
                    $loop++;
                }
                if ($nm=='par'){
                    if($par==true){
                        ?>
                        <tr style="border-bottom: solid <?=$color ?>;border-width: thin">
                            <td align="center" <?= TA::tbC($color)?>><?= ($loop==0)?$i:$i+1 ?></td>
                            <th <?= TA::tbC($color)?>>Solid Particles (Particle Size)</th>
                            <td align="center" <?= TA::tbC($color)?>></td>
                            <td align="center" <?= TA::tbC($color)?>></td>
                            <td align="center" <?= TA::tbC2($color,Par::colorCode($data->$code))?>></td>
                            <td align="center" <?= TA::tbEnd($color)?>></td>
                        </tr>
                        <?php
                    }
                    $par=false;
                    $loop++;
                }
                $vl='';
//                if(strpos($data->sludge_content,'FAME')){
////                    $v=$value['p_mtr1'];
///
//                    if( $data->mtr($data->prm($parameter)->id,$data->sludge_content,$data->recommended3)

                        $dt=$data->mtr($data->prm($parameter)->id,$data->sludge_content,$data->recommended3);
                        if ($parameter=='water_content'){
                            if($data->water_content<10){
                                $aa=$dt==NULL?'':$dt->condition2;
                                $aa=($aa/10000);
                                $v='Max '.$aa;
                            }else{
                                $v= $dt==NULL?'':$dt->condition3;
                            }
                        }else{
                            $v= $dt==NULL?'':$dt->condition3;
                        }
//                        $v= $dt==NULL?'':$dt->condition3;
//                        $v=$data->water_content>10 ? ($v==''or $v=NULL ?'':$v*1000):$v;
                        $sat= $dt==NULL?'':$dt->satuan;
                        if($parameter=='water_content'){
                            $sat=$data->water_content<10?'% wt':$sat;
                        }else{
                            $sat=$sat;
                        }
//                        $sat=$data->water_content<10?'% wt':$sat;
                        $method= $dt==NULL?'':$dt->method;

//                }else{
//                    $v = $data->mtr($data->prm($parameter)->id)->condition1;
//                }
//                $v=$value['p_jn']=='FUEL'?$value['p_mtr']:$value['p_mtr1'];
                if($parameter==='cerosine_content'){
                    $vl=$v.' ***';
                }elseif ($parameter==='iso_4406'){
                    $vl=$v.' **';
                }else{
                    $vl=$v;
                }
                ?>
                <tr style="border-bottom: solid <?=$color ?>;border-width: thin" >
                    <?php $nk=($loop==0)?$i:$i+1;?>

                    <td align="center" <?= TA::tbC($color)?>><?= ($nm=='iso'||$nm=='dis'||$nm=='par')?'':$nk?></td>
                    <?php if($nm=='iso'){?>
                        <th <?= TA::tbC($color)?>><?= $data->prm($parameter)->nama?></th>
                    <?php }else{?>
                        <td <?= TA::tbC($color)?>><?= $data->prm($parameter)->nama ?></td>
                    <?php }?>

                    <td align="center" <?= TA::tbC($color)?>><?= $sat ?></td>
                    <td align="center" <?= TA::tbC($color)?>><?= $method ?></td>
                    <td align="center" <?= TA::tbC($color)?>><?= $data->ColourLabel($data->$code,$nilai) ?></td>

                    <td align="center" <?= TA::tbEnd($color)?>><?= $vl ?></td>
                </tr>
                <?php
            }?>
<!--            --><?//=auto_column($col_now,$color)?>
        </table>

        <div <?= TA::overLine3(1,$color) ?> ></div>
    </fieldset>
    <table style="font-size: 10px;font-style:italic" width="100%">
        <tr>
            <?php
                if($ut==true && $mg==true && $pc==true ){
                    echo '<td align="left" width="30%">* '.$data->dirjen.'</td>
                            <td align="left" width="20%">** Worldwide Fuel Charter 2005</td>
                            <td align="left">*** Kesepakatan UT-Petrolab</td>';
                }elseif($ut==false && $mg==true && $pc==true ){
                    echo '<td align="left" width="30%">* '.$data->dirjen.'</td>
                            <td align="left" width="20%">** Worldwide Fuel Charter 2005</td>
                            <td align="left"></td>';
                }elseif($ut==false && $mg==true && $pc==false ){
                    echo '<td align="left" width="30%">* '.$data->dirjen.'</td>
                            <td align="left" width="20%"></td>
                            <td align="left"></td>';
                }elseif($ut==true && $mg==false && $pc==false ){
                    echo '<td align="left" width="30%">* Kesepakatan UT-Petrolab</td>
                            <td align="left" width="20%"></td>
                            <td align="left"></td>';
                }elseif($ut==true && $mg==false && $pc==true){
                    echo '<td align="left" width="30%">* Worldwide Fuel Charter 2005</td>
                            <td align="left" width="20%">**  Kesepakatan UT-Petrolab</td>
                            <td align="left"></td>';
                }elseif($ut==false && $mg==false && $pc==true){
                    echo '<td align="left" width="30%">* Worldwide Fuel Charter 2005</td>
                            <td align="left" width="20%"></td>
                            <td align="left"></td>';
                }
                
            ?>

        </tr>

    </table>
    <table style="width: 100%;border-collapse: collapse;border-spacing: 0;">
        <tr>
            <th style="font-size: 12px" width="100" align="left" >Keterangan</th>
            <th style="font-size: 12px" width="25" align="left">:</th>

            <th align="left" <?= TA::tHeadC1($color)?>><?= $data->recommended1?></th>
        </tr>
    </table>
    <table width="700">
        <tr>
            <td align="right" <?= TA::txt(12) ?>>
                Manager Teknis
            </td>
        </tr>

        <?php if($customer!==''){?>
        <tr>
            <td align="right" height="100%" <?= TA::txt(12) ?>>
                <img src=<?= Yii::$app->HM->urlImg('reports/sign.png')?>>
            </td>
        </tr>
            <?php
                }else
                    {?>
             <tr>
                <td>
                </td>
             </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
        <?php
                    }?>
        <tr>
            <td align="right" <?= TA::txt(12) ?>>
                Reko Sayogo, S.Si
            </td>
        </tr>
    </table>
</div>

<?php
    function recomend($data,$type){
//    $req1=$data->recommended1;
    $req2="";
    /*if($type=='dga'){
    $req1=$analisa['recommended1_dga'];
    $req2=$analisa['recommended2_dga'];
    }
    if($type=='furan'){
    $req1=$analisa['recommended1_furan'];
    $req2=$analisa['recommended2_furan'];
    }
    if($type=='oa'){
    $req1=$analisa['recommended1_oil_quality'];
    $req2=$analisa['recommended2_oil_quality'];
    }*/

    ?>


<?php }?>

	<?php
function theader($img,$w,$data){?>
<table>
    <tr>
        <td>
            <img <?= TA::logo() ?> src="<?= Yii::$app->HM->urlImg('reports/logo.png') ?>">
        </td>
        <td width="180"><?= TA::space(50) ?></td>
        <td align="center" width="<?= $w ?>">
            <img <?= TA::logo2() ?> src="<?= $img ?>">
            <div <?= TA::txt(12) ?> >Balikpapan, <?= Yii::$app->HM->fDate('d F Y',$data->report_date) ?></div>
        </td>
    </tr>
</table>
<?php } ?>