<?php
use yii\helpers\Url;
use app\modules\reports\assets\TA;
use app\modelsDB\Parameter;
use app\modelsDB\ReportTypeHasParameter as RTP;
use app\modules\reports\helpers\Chart;
use app\modules\reports\helpers\Parameter as Par;
use app\modules\reports\models\HelpMatric as Hm;

$hm=Yii::$app->HM;
$i=0;
$no=0;
$no2=0;
function tgl_w3(){
	return 47;
};
$type=2;
$col_now=0;
$over_furan=45;
$report_furan=RTP::find()
->where(['report_type_id'=>$type])
->andWhere(['p_code'=>'furan'])
->all();

?>
<div <?= TA::txt(12) ?> ><?= TA::space(150) ?> Overall Analysis Result</div>
<fieldset style="border:1px solid black;">
	<table <?= TA::tb_borderTB(9) ?> >
		<tr>
			<td colspan="4" align="left" <?= TA::tb_border2(9,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?>>Lab&nbsp;Number</td>
			<?php foreach ($datas as $key => $value) {$i=$i+1;$col_now=$col_now+1; ?>
			<td align="center" width="<?=tgl_w3()?>" <?= TA::tb_border(9) ?> >
				<?= $value['lab_number'] ?>
			</td>
			<?php } ?>
			<?=auto_column2($col_now)?>
			<td align="right" rowspan="4"  width="<?=$over_furan?>" <?= TA::tb_border2(9,"border-bottom: 0 solid black;") ?>>
				<img class="logo1" width="17" height="35" <?=TA::logo_code1()?> src="<?= Yii::$app->HM->urlImg(Par::codeImg($analisa['eval_code_furan'])) ?>">
			</td>
			<td rowspan="4" align="right" width="<?=$over_furan?>" <?= TA::tb_border2(16,"border-bottom: 0 solid black;") ?>>
				<?= Par::codeStatus($analisa['eval_code_furan']) ?>
			</td>
		</tr>
		<tr>
			<td colspan="4" <?= TA::tb_border2(9,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >Sampling&nbsp;Date</td>
			<?php foreach ($datas as $key => $value) {$i=$i+1; ?>
			<td align="center" <?= TA::tb_border(9) ?> >
				<?= $hm->fDate('d-M-y',$value['sampling_date']) ?>
			</td>
			<?php } ?>
			<?=auto_column2($col_now)?>

		</tr>
		<tr>
			<td colspan="4" <?= TA::tb_border2(9,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >Receive&nbsp;Date</td>
			<?php foreach ($datas as $key => $value) {$i=$i+1; ?>
			<td align="center" <?= TA::tb_border(9) ?> >
				<?= $hm->fDate('d-M-y',$value['received_date']) ?>
			</td>
			<?php } ?>
			<?=auto_column2($col_now)?>
		</tr>
		<tr>
			<td colspan="4" <?= TA::tb_border2(9,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >Report&nbsp;Date</td>
			<?php foreach ($datas as $key => $value) { ?>
			<td align="center" <?= TA::tb_border(9) ?>>
				<?= $hm->fDate('d-M-y',$value['report_date']) ?>
			</td>
			<?php } ?>
			<?=auto_column2($col_now)?>
		</tr>

		<tr <?= TA::tb_border(9) ?>>
			<th colspan="9" align="left" <?= TA::headContent(9) ?> >Furan Analysis</th>
			<th colspan="6" align="center" <?= TA::headContent(9) ?> >Limitation</th>
		</tr>

		<tr>
			<th align="center" width="5" <?= TA::headContent(9) ?> >No</th>
			<th align="center" width="120" <?= TA::headContent(9) ?> >Paramter Uji</th>
			<th align="center" width="40" <?= TA::headContent(9) ?> >Unit</th>
			<th align="center" width="70" <?= TA::headContent(9) ?> >Method</th>
			<th align="center" colspan="5" <?= TA::headContent(9) ?> >Test Result</th>
			<th align="center" width="<?=$over_furan?>" <?= TA::headContent(9) ?> >1</th>
			<th align="center" width="<?=$over_furan?>" <?= TA::headContent(9) ?> >2</th>
			<th align="center" width="<?=$over_furan?>" <?= TA::headContent(9) ?> >3</th>
			<th align="center" width="<?=$over_furan?>" <?= TA::headContent(9) ?> >4</th>
			<th align="center" width="<?=$over_furan?>" <?= TA::headContent(9) ?> >5</th>
		</tr>

		<?php foreach ($report_furan as $key => $value) { ?>
		<?php
		$method=$value['parameter']['method'];
		$unit=$value['parameter']['satuan'];
		$param=$value['parameter']['column_analisis_name'];
		if($value['parameter']['column_analisis_name'] != ""){
			$param_code=$value['parameter']['column_analisis_name']."_code";	
		}elseif($value['parameter']['column_analisis_name'] != "furan_total"){
			$param_code=$value['parameter']['column_analisis_name']."_code";	
		}
		if($method){
			$no2=$no2+1;	
		}
		?>
		<tr <?= TA::tb_border2(9,"border-right: 0.5px solid black;") ?>>
			<td <?= TA::tb_border(9) ?>>
				<?php if($method){echo $no2;}else{echo '';} ?>
			</td>
			<td><?=$value['parameter']['nama']?></td>
			<td align="center" <?= TA::tb_border2(9,"border-left: 0.5px solid black;") ?>><?=$unit ?></td>
			<td align="center" <?= TA::tb_border2(9,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?>><?=$method ?></td>
			<?php foreach ($datas as $key2 => $value2) {
				try{
					$codes=Par::colorCode($value2[$param_code]);	
				}catch(Exception $e){

				}

				?>

				<td align="center" <?= TA::tb_border2(9,"border-right: 0.5px solid black;$codes") ?> width="<?=tgl_w3()?>">
					<?= $value2[$param] ? $value2[$param] : "-"  ?>
				</td>
				<?php } ?>
				<?=auto_column2($col_now)?>
<td align="center" width="<?=$over_furan?>" <?= TA::tb_border2(9,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >
<?=Par::matric(Hm::matric($analisa['unit_id'],$value['parameter_id'])['condition1'])?>
</td>
<td align="center" width="<?=$over_furan?>" <?= TA::tb_border2(9,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >
<?= Par::matric(Hm::matric($analisa['unit_id'],$value['parameter_id'])['condition2']) ?>
</td>
<td align="center" width="<?=$over_furan?>" <?= TA::tb_border2(9,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >
<?= Par::matric(Hm::matric($analisa['unit_id'],$value['parameter_id'])['condition3']) ?>
</td>
<td align="center" width="<?=$over_furan?>" <?= TA::tb_border2(9,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >
<?= Par::matric(Hm::matric($analisa['unit_id'],$value['parameter_id'])['condition4']) ?>
</td>
<td align="center" width="<?=$over_furan?>" <?= TA::tb_border2(9,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >
<?= Par::matric(Hm::matric($analisa['unit_id'],$value['parameter_id'])['condition5']) ?>
</td>
			</tr>
			<?php }?>

		</table>
	</fieldset>
