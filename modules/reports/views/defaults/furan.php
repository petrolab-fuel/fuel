<?php
use yii\helpers\Url;
use app\modules\reports\assets\TA;
use app\modelsDB\Parameter;
use app\modelsDB\ReportTypeHasParameter as RTP;
use app\modules\reports\helpers\Chart;
use app\modules\reports\helpers\Parameter as Par;
use app\modules\reports\models\HelpMatric as Hm;
use app\modules\reports\helpers\Filter;
require_once(__DIR__.'/../../lib/phpchartdir.php');

$hm=Yii::$app->HM;
$i=0;
$no=0;
$no2=0;
function tgl_w3(){
	return 47;
};
$type=2;
$col_now=0;
$over_furan=45;
$report_furan=RTP::find()
->where(['report_type_id'=>$type])
->andWhere(['p_code'=>'furan'])
->all();

$chart=new Chart;
$chart->deleteAllChart('furan');
$chart->makeChartFuran(new XYChart(720, 230, 0xffffff, 0x000000, 0),$datas);

?>
<?= theader(Yii::$app->HM->urlImg('reports/chart/default/furane.png'),100) ?>
<div <?= TA::overLine() ?> ></div>
<?= $this->render('header',['analisa'=>$analisa]) ?>
<div <?= TA::overLine() ?> ></div>

<div <?= TA::txt(12) ?> ><?= TA::space(150) ?> Overall Analysis Result</div>
<fieldset style="border:1px solid black;">
	<table <?= TA::tb_borderTB(11) ?> >
		<tr>
			<td colspan="4" align="left" <?= TA::tb_border2(11,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?>>Lab&nbsp;Number</td>
			<?php foreach ($datas as $key => $value) {$i=$i+1;$col_now=$col_now+1; ?>
			<td align="center" width="<?=tgl_w3()?>" <?= TA::tb_border(11) ?> >
				<b><?= $value['lab_number'] ?></b>
			</td>
			<?php } ?>
			<?=auto_column2($col_now)?>
			<td colspan="3" align="center" width="<?=$over_furan?>" <?= TA::tb_border2(9,"border-bottom: 0.5px solid black;") ?>>
				<b>Status</b>
			</td>
			<td colspan="2" align="center" colspan="2" width="<?=$over_furan?>" <?= TA::tb_border2(9,"border-left: 0.5px solid black;border-bottom: 0.5px solid black;") ?>>
				<b>Resampling Date</b>
			</td>
		</tr>
		<tr>
			<td colspan="4" <?= TA::tb_border2(11,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >Sampling&nbsp;Date</td>
			<?php foreach ($datas as $key => $value) {$i=$i+1; ?>
			<td align="center" <?= TA::tb_border(11) ?> >
				<?= $hm->fDate('d-M-y',$value['sampling_date']) ?>
			</td>
			<?php } ?>
			<?=auto_column2($col_now)?>
			<td rowspan="3" colspan="3" align="center" width="<?=$over_furan?>" <?= TA::tb_border2(9,"border-bottom: 0 solid black;") ?>>
				<img class="logo1" width="20" height="35" <?=TA::logo_code1()?> src="<?= Yii::$app->HM->urlImg(Par::codeImg($analisa['eval_code_furan'])) ?>">
				<br>
				<b><?= Par::codeStatusFuran($analisa['eval_code_furan']) ?></b>
			</td>
			<td rowspan="3" colspan="2" align="center" colspan="2" width="<?=$over_furan?>" <?= TA::tb_border2(9,"border-left: 0.5px solid black;") ?>>
				<b style="color:red;"><?= Filter::resDate($analisa['report_date'],$analisa['resampling_oil_quality']) ?></b>
			</td>

		</tr>
		<tr>
			<td colspan="4" <?= TA::tb_border2(11,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >Receive&nbsp;Date</td>
			<?php foreach ($datas as $key => $value) {$i=$i+1; ?>
			<td align="center" <?= TA::tb_border(11) ?> >
				<?= $hm->fDate('d-M-y',$value['received_date']) ?>
			</td>
			<?php } ?>
			<?=auto_column2($col_now)?>
		</tr>
		<tr>
			<td colspan="4" <?= TA::tb_border2(11,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >Report&nbsp;Date</td>
			<?php foreach ($datas as $key => $value) { ?>
			<td align="center" <?= TA::tb_border(11) ?>>
				<?= $hm->fDate('d-M-y',$value['report_date']) ?>
			</td>
			<?php } ?>
			<?=auto_column2($col_now)?>
		</tr>

		<tr <?= TA::tb_border(11) ?>>
			<th colspan="9" align="left" <?= TA::headContent(11) ?> >Furan Analysis</th>
			<th colspan="6" align="center" <?= TA::headContent2(11,"border-bottom:0.5px solid black;border-top:0.5px solid black;") ?> >Limit</th>
		</tr>

		<tr>
			<th align="center" width="5" <?= TA::headContent(11) ?> >No</th>
			<th align="center" width="120" <?= TA::headContent(11) ?> >Paramter of Test</th>
			<th align="center" width="40" <?= TA::headContent(11) ?> >Unit</th>
			<th align="center" width="70" <?= TA::headContent(11) ?> >Method</th>
			<th align="center" colspan="5" <?= TA::headContent(11) ?> >Test Result</th>
			<th align="center" width="<?=$over_furan?>" <?= TA::headContent(11) ?> >1</th>
			<th align="center" width="<?=$over_furan?>" <?= TA::headContent(11) ?> >2</th>
			<th align="center" width="<?=$over_furan?>" <?= TA::headContent(11) ?> >3</th>
			<th align="center" width="<?=$over_furan?>" <?= TA::headContent(11) ?> >4</th>
			<th align="center" width="<?=$over_furan?>" <?= TA::headContent2(11,"border-bottom:0.5px solid black;") ?> >5</th>
		</tr>

		<?php foreach ($report_furan as $key => $value) { ?>
		<?php
		$method=$value['parameter']['method'];
		$unit=$value['parameter']['satuan'];
		$param=$value['parameter']['column_analisis_name'];
		if($value['parameter']['column_analisis_name'] != ""){
			$param_code=$value['parameter']['column_analisis_name']."_code";	
		}elseif($value['parameter']['column_analisis_name'] != "furan_total"){
			$param_code=$value['parameter']['column_analisis_name']."_code";	
		}
		if($method){
			$no2=$no2+1;	
		}
		?>
		<tr <?= TA::tb_border2(11,"border-right: 0.5px solid black;") ?>>
			<td align="right" <?= TA::tb_border(11) ?>>
				<?php if($method){echo $no2;}else{echo '';} ?>&nbsp;
			</td>
			<td><?=$value['parameter']['nama']?></td>
			<td align="center" <?= TA::tb_border2(11,"border-left: 0.5px solid black;") ?>><?=$unit ?></td>
			<td align="center" <?= TA::tb_border2(11,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?>><?=$method ?></td>
			<?php foreach ($datas as $key2 => $value2) {
				try{
					$codes=Par::colorCode($value2[$param_code]);	
				}catch(Exception $e){

				}

				?>

				<td align="center" <?= TA::tb_border2(11,"border-right: 0.5px solid black;$codes") ?> width="<?=tgl_w3()?>">
					<?= $value2[$param] ? $value2[$param] : "-"  ?>
				</td>
				<?php } ?>
				<?=auto_column2($col_now)?>
				<td align="center" width="<?=$over_furan?>" <?= TA::tb_border2(11,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >
					<?=Par::matric(Hm::matric2(11,$value['parameter_id'])['condition1'])?>
				</td>
				<td align="center" width="<?=$over_furan?>" <?= TA::tb_border2(11,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >
					<?= Par::matric(Hm::matric2(11,$value['parameter_id'])['condition2']) ?>
				</td>
				<td align="center" width="<?=$over_furan?>" <?= TA::tb_border2(11,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >
					<?= Par::matric(Hm::matric2(11,$value['parameter_id'])['condition3']) ?>
				</td>
				<td align="center" width="<?=$over_furan?>" <?= TA::tb_border2(11,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >
					<?= Par::matric(Hm::matric2(11,$value['parameter_id'])['condition4']) ?>
				</td>
				<td align="center" width="<?=$over_furan?>" <?= TA::tb_border2(11,"border-left: 0.5px solid black;") ?> >
					<?= Par::matric(Hm::matric2(11,$value['parameter_id'])['condition5']) ?>
				</td>
			</tr>
			<?php }?>

		</table>
	</fieldset>

	<div <?= TA::overLine2(3) ?> ></div>
	<br>
	<div <?= TA::txt(12) ?> ><b><u>Note<?= TA::space(6) ?></u></b></div>
	<table>
		<tr>
			<td width="10" <?= TA::txt(12) ?>><b>1.</b></td>
			<td <?= TA::txt(12) ?>>Normal</td>
			<td <?= TA::txt(12) ?>><b>4.</b></td>
			<td <?= TA::txt(12) ?>>High Risk</td>
			
		</tr>
		<tr>
			<td <?= TA::txt(12) ?>><b>2.</b></td>
			<td <?= TA::txt(12) ?>>Accelerated</td>
			<td <?= TA::txt(12) ?>><b>5.</b></td>
			<td <?= TA::txt(12) ?>>End of Expected</td>
		</tr>
		<tr>
			<td <?= TA::txt(12) ?>><b>3.</b></td>
			<td <?= TA::txt(12) ?>>Excessive</td>
		</tr>
	</table>
	<br>
	<div <?= TA::overLine2(3) ?> ></div>
	<br>
	<div id='chart'>
		<img src="/img/reports/chart/run/furan/chart.png">
	</div>
	<div <?= TA::overLine2(3) ?> ></div>
	<table>
		<tr>
			<td <?=TA::txt(11) ?>>
				5H2F (5-hydroxymethyl-2-furaldehyde) Caused by oxidation (aging and heating) of the paper
			</td>
		</tr>
		<tr>
			<td <?=TA::txt(11) ?>>
				2FOL (2-furfurol) caused by high moisture in the paper
			</td>
		</tr>
		<tr>
			<td <?=TA::txt(11) ?>>
				2FAL (2-furaldehyde) caused by overheating
			</td>
		</tr>
		<tr>
			<td <?=TA::txt(11) ?>>
				2ACF (2-acetylfuran) caused by lightning (rarely found in DGA)
			</td>
		</tr>
		<tr>
			<td <?=TA::txt(11) ?>>
				5M2F (5-methyl-2-furaldehyde) caused by local severe overheating (hotspot)
			</td>
		</tr>
	</table>

	<?= recomend($analisa,'furan') ?>

	<?= $this->render('footer') ?>
