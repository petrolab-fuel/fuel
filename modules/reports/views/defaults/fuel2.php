<?php
use app\modules\reports\assets\TA;
use app\modules\reports\helpers\Parameter as Par;
$color="#ababab";
$i=0;
?>
<table>
    <tr>
        <td>
            <img <?= TA::logo() ?> src="<?= Yii::$app->HM->urlImg('reports/logo.png') ?>">
        </td>
        <td width="180"><?= TA::space(50) ?></td>
        <td align="center" width="<?= 150 ?>">
            <img <?= TA::logo2() ?> src="<?= Yii::$app->HM->urlImg('reports/chart/default/fuel.png')?>">
            <div <?= TA::txt(12) ?> >Balikpapan, <?= Yii::$app->HM->fDate('d F Y',$data->report_date) ?></div>
        </td>
    </tr>
</table>
<div <?= TA::overLine() ?> ></div>
<?= $this->render('header',['data'=>$data]) ?>
<div class="br"></div>
<div class="line_top"></div>
<div class="br"></div>
<div>
    <fieldset style="border:1.5px solid <?=$color?>;">
        <table style="width: 100%;border-collapse: collapse;border-spacing: 0;">
            <tr style="background-color:#ebe9e9;">
                <th width="25" align="center" <?= TA::tHeadC($color)?>>NO</th>
                <th width="250" align="center" <?= TA::tHeadC($color)?>>PARAMETER</th>
                <th width="100" align="center" <?= TA::tHeadC($color)?>>UNIT</th>
                <th width="150" align="center" <?= TA::tHeadC($color)?>>METHOD</th>
                <th width="100" align="center" <?= TA::tHeadC($color)?>>RESULT</th>
                <th align="center" <?= TA::tHeadEnd($color)?>>TYPICAL *</th>
            </tr>
            <?php foreach ($data->paramAll() as $parameter=>$code){
                    if($data->cekParameter($parameter)){
                        continue;
                    }
                 $i++;
                 ?>
                <tr style="border-bottom:  solid <?=$color ?>;border-width: thin">
                    <td align="center" <?= TA::tbC($color)?>><?= $i ?></td>
                    <th <?= TA::tbC($color)?>><?=$data->prm($parameter)->nama ?></th>
                    <td align="center" <?= TA::tbC($color)?>>C</td>
                    <td align="center" <?= TA::tbC($color)?>>ASTM D 86-12</td>
                    <td align="center" <?= TA::tbC2($color,Par::colorCode($data->$code))?>><?=$data->$parameter ?></td>
                    <td align="center" <?= TA::tbEnd($color)?>></td>
                </tr>
            <?php }?>
        </table>
    </fieldset>

</div>
