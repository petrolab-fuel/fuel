<?php
use app\modules\reports\assets\TA;
?>

<table width="600">
	<tr>
		<td valign="top" align="left" <?= TA::txt(10) ?>>
			<p><u>Catatan:</u></p>
			<p>Note</p>
		</td>
		<td>
			<p <?= TA::txt(10) ?>>
				Data analisa hanya berlaku untuk sampel yang diuji di Laboraturium PT. Petrolab Services.
			</p>
			<p <?= TA::txt(10) ?>>
				Pengaduan tidak dilayani setelah 30 hari dari tanggal report diterbitkan
			</p>
			<p <?= TA::txt(10) ?>>
				*) Diluar ruang lingkup akreditasi
			</p>
		</td>
	</tr>
</table>
<footer style="width: 100%; background: #d6d6c2; position: absolute;">
	<p align="center" <?= TA::txt(7) ?>><?= "PT Petrolab Services"."<br>"."Komplek United Tractor. Jl. Jend Sudirman No. 874 Stal Kuda, Balikpapan; Telp: +62 542 766019; Fax: +62 542 762873"."<br>"."petrolab@cbn.net.id;  http://www.petrolab.co.id" ?></p>
</footer>