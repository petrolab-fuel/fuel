<?php
use yii\helpers\Url;
use app\modules\reports\assets\TA;
use app\modelsDB\Parameter;
use app\modelsDB\ReportTypeHasParameter as RTP;
use app\modules\reports\helpers\Chart;
use app\modules\reports\helpers\Parameter as Par;
use app\modules\reports\models\HelpMatric as Hm;
use app\modules\reports\helpers\Filter;
require_once(__DIR__.'/../../lib/phpchartdir.php');


$hm=Yii::$app->HM;
$i=0;
$no=0;
function tgl_w(){
	return 47;
};
$type=1;
$col_now=0;
$over=55;
$report_type=RTP::find()->where(['report_type_id'=>$type])->all();
$chart=new Chart;
$chart->deleteAllChart('oa');
$chart->makeChartOa(new XYChart(360, 155, 0xffffff, 0x000000, 0),$datas);
$chart->deleteAllChart('wc');
$chart->makeChartWc(new XYChart(360, 155, 0xffffff, 0x000000, 0),$datas);
?>
<div id="contoh2">
	<?= theader(Yii::$app->HM->urlImg('reports/chart/default/oqr.png'),100) ?>
	<div <?= TA::overLine() ?> ></div>
	<?= $this->render('header',['analisa'=>$analisa]) ?>
	<div <?= TA::overLine() ?> ></div>
	<div <?= TA::txt(12) ?> >Test Details <?= TA::space(135) ?> Overall Analysis Result</div>
	<div>
		<fieldset style="border:1px solid black;">
			<table <?= TA::tb_borderTB(9) ?> >

				<tr>
					<td colspan="4" align="left" <?= TA::tb_border2(10,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?>>Lab&nbsp;Number</td>
					<?php foreach ($datas as $key => $value) {$i=$i+1;$col_now=$col_now+1; ?>
					<td align="center" width="<?=tgl_w()?>" <?= TA::tb_border(10) ?> >
						<b><?= $value['lab_number'] ?></b>
					</td>
					<?php } ?>
					<?=autoColumn($col_now)?>
					<td align="center" width="<?=$over?>" <?= TA::tb_border2(9,"border-bottom: 0.5px solid black;") ?>>
						<b>Status</b>
					</td>
					<td align="center" colspan="2" width="<?=$over?>" <?= TA::tb_border2(9,"border-left: 0.5px solid black;border-bottom: 0.5px solid black;") ?>>
						<b>Resampling Date</b>
					</td>

				</tr>

				<tr>
					<td colspan="4" <?= TA::tb_border2(10,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >Sampling&nbsp;Date</td>
					<?php foreach ($datas as $key => $value) {$i=$i+1; ?>
					<td align="center" <?= TA::tb_border(10) ?> >
						<?= $hm->fDate('d-M-y',$value['sampling_date']) ?>
					</td>
					<?php } ?>
					<?=autoColumn($col_now)?>
					<td rowspan="3" align="center" width="<?=$over?>" <?= TA::tb_border2(9,"border-bottom: 0 solid black;") ?>>
						<img class="logo1" width="20" height="35" <?=TA::logo_code1()?> src="<?= Yii::$app->HM->urlImg(Par::codeImg($analisa['eval_code_oil_quality'])) ?>">
						<br>
						<b><?= Par::codeStatus($analisa['eval_code_oil_quality']) ?></b>
					</td>
					<td rowspan="3" align="center" colspan="2" width="<?=$over?>" <?= TA::tb_border2(9,"border-left: 0.5px solid black;") ?>>
						<b style="color:red;"><?= Filter::resDate($analisa['report_date'],$analisa['resampling_oil_quality']) ?></b>
					</td>

				</tr>
				<tr>
					<td colspan="4" <?= TA::tb_border2(10,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >Receive&nbsp;Date</td>
					<?php foreach ($datas as $key => $value) {$i=$i+1; ?>
					<td align="center" <?= TA::tb_border(10) ?> >
						<?= $hm->fDate('d-M-y',$value['received_date']) ?>
					</td>
					<?php } ?>
					<?=autoColumn($col_now)?>
				</tr>
				<tr>
					<td colspan="4" <?= TA::tb_border2(10,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >Report&nbsp;Date</td>
					<?php foreach ($datas as $key => $value) { ?>
					<td align="center" <?= TA::tb_border(10) ?>>
						<?= $hm->fDate('d-M-y',$value['report_date']) ?>
					</td>
					<?php } ?>
					<?=autoColumn($col_now)?>
				</tr>

				<tr <?= TA::tb_border(10) ?>>
					<th colspan="9" align="left" <?= TA::headContent(10) ?> >Phisycal and Chemical Test</th>
					<th colspan="4" align="center" <?= TA::headContent2(11,"border-bottom:0.5px solid black;border-top:0.5px solid black;") ?> >Limit</th>

				</tr>
			</tr>

			<tr>
				<th align="center" width="5" <?= TA::headContent(10) ?> >No</th>
				<th align="center" width="150" <?= TA::headContent(10) ?> >Paramter of Test</th>
				<th align="center" width="40" <?= TA::headContent(10) ?> >Unit</th>
				<th align="center" width="70" <?= TA::headContent(10) ?> >Method</th>
				<th align="center" colspan="5" <?= TA::headContent(10) ?> >Result</th>
				<th align="center" width="<?=$over?>" <?= TA::headContent(10) ?> >Good</th>
				<th align="center" width="<?=$over?>" <?= TA::headContent(10) ?> >Fair</th>
				<th align="center" width="<?=$over?>" <?= TA::headContent2(11,"border-bottom:0.5px solid black;") ?> >Poor</th>
			</tr>

			<?php foreach ($report_type as $key => $value) { ?>
			<?php
			$method=$value['parameter']['method'];
			$unit=$value['parameter']['satuan'];
			$param=$value['parameter']['column_analisis_name'];
			if($value['parameter']['column_analisis_name']!=""){
				$param_code=$value['parameter']['column_analisis_name']."_code";	
			}
			if($method){
				$no=$no+1;	
			}
			?>
			<tr <?= TA::tb_border2(10,"border-right: 0.5px solid black;") ?>>
				<td align="right" <?= TA::tb_border(10) ?>>
					<?php if($method){echo $no;}else{echo '';} ?>&nbsp;
				</td>
				<td <?= TA::txt(10) ?>><?=Par::cS($value['parameter']['nama'])?></td>
				<td align="center" <?= TA::tb_border2(10,"border-left: 0.5px solid black;") ?>><?=$unit ?></td>
				<td align="center" <?= TA::tb_border2(10,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?>><?=Par::cS($method) ?></td>
				<?php foreach ($datas as $key2 => $value2) {$codes=Par::colorCode($value2[$param_code]);?>

					<?php if(isset($value2[$param])){ $yoy=10;?>
					<?php if($value['parameter']['nama']=='Appearance'){$yoy=8;} ?>
					<td align="center" <?= TA::tb_border2($yoy,"border-right: 0.5px solid black;$codes") ?> width="<?=tgl_w()?>">
						
						<?= $value2[$param] ?>
						
					</td>
					<?php }else{ ?>
					<td align="center" <?= TA::tb_border2(10,"border-right: 0.5px solid black;$codes") ?> width="<?=tgl_w()?>">
						-
					</td>
					<?php } ?>

					<?php } ?>
					<?=autoColumn($col_now)?>
					<td align="center" width="<?=$over?>" <?= TA::tb_border2(8,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >
						<?= Par::matric(Hm::matric($analisa['unit_id'],$value['parameter_id'])['condition1']) ?>
					</td>
					<td align="center" width="<?=$over?>" <?= TA::tb_border2(8,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >
						<?= Par::matric(Hm::matric($analisa['unit_id'],$value['parameter_id'])['condition2']) ?>
					</td>
					<td align="center" width="<?=$over?>" <?= TA::tb_border2(8,"border-left: 0.5px solid black;") ?> >
						<?= Par::matric(Hm::matric($analisa['unit_id'],$value['parameter_id'])['condition3']) ?>
					</td>
				</tr>
				<?php }?>

			</table>
		</fieldset>

		<div <?= TA::overLine2(3) ?> ></div>
		<br>
		<div id='chart'>
			<table>
				<tr>
					<td>
						<img src="/img/reports/chart/run/oa/chart.png">
					</td>
					<td>
						<img src="/img/reports/chart/run/wc/chart.png">
					</td>
				</tr>
			</table>
			
		</div>
		<?= recomend($analisa,'oa') ?>
		<?= $this->render('footer') ?>
	</div>
</div>
</body>
</html>

<?php
function autoColumn($col){?>
<?php
$aa=0;
if($col==0){
	$aa=5;
}elseif($col==1){
	$aa=4;
}elseif($col==2){
	$aa=3;
}elseif($col==3){
	$aa=2;
}elseif($col==4){
	$aa=1;
}
if($col<5){
	for ($bb=0; $bb <$aa ; $bb++) { ?>
	<td align="center" width="<?=tgl_w()?>" <?= TA::tb_border2(9,"border-left: 0.5px solid black;border-right: 0.5px solid black;") ?> >-</td>
	<?php
}
}
}
?>