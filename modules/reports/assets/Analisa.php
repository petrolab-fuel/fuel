<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 3/16/2018
 * Time: 1:02 PM
 */

namespace app\modules\reports\assets;

use yii\web\AssetBundle;
class Analisa extends AssetBundle
{
    public $sourcePath = '@app/modules/reports/assets/app';
    public $css = [
            'app1.css'
    ];
    public $js = [
        'app1.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\smartadmin\assets\plugins\PluginsAssets',
    ];
}