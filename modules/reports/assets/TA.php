<?php

namespace app\modules\reports\assets;

/**
 * reports module definition class
 */
class TA
{

    public static function logo()
    {
        $data="width: 300px;";
        $data.="margin-top:1px;";
        return "style='$data'";
    }
    public static function logo2()
    {
        $data="width: 300px;";
        return "style='$data'";
    }
    public static function space($val)
    {
        $data="&nbsp;";
        for ($i=0; $i <$val ; $i++) { 
            $data=$data."&nbsp;";
        }
        return $data;
    }
    public static function br($val)
    {
        $data="<br>";
        for ($i=0; $i <$val ; $i++) { 
            $data=$data."<br>";
        }
        return $data;
    }
    public static function overLine(){
        $data="width: 100%;";
        $data.="height: 1.5px;";
        $data.="background-color: black;";
        return "style='$data'";
    }
    public static function overLine2($aa){
        $data="width: 100%;";
        $data.="height: $aa"."px;";
        $data.="background-color: black;";
        return "style='$data'";
    }
    public static function overLine3($aa,$color){
        $data="width: 100%;";
        $data.="height: $aa"."px;";
        $data.="background-color: $color;";
        return "style='$data'";
    }
    public static function tHead(){
        $data="font-size: 10px;";
        return "style='$data'";
    }
    public static function tHeadC($color){
        $data="font-size: 10px;";
        $data.="border-right: 1px solid $color;";
        $data.="border-bottom: 1px solid $color;";
        $data.="border-spacing: 0;";
        $data.="height: 25px;";
        return "style='$data'";
    }
    public static function tHeadC1($color){
        $data="font-size: 12px;";
        $data.="border-left: 1px solid $color;";
        $data.="border-top: 1px solid $color;";
        $data.="border-right: 1px solid $color;";
        $data.="border-bottom: 1px solid $color;";
        $data.="border-spacing: 0;";
        $data.="height: 25px;";
        return "style='$data'";
    }

    public static function tbC($color){
        $data="font-size: 10px;";
        $data.="border-right: 1px solid $color;";
        //$data.="border-bottom: 1px solid $color;";
        $data.="border-spacing: 0;";
        //$data.="height: 15px;";
        return "style='$data'";
    }
    public static function tbC2($color,$par=""){
        $data="font-size: 10px;";
        $data.="border-right: 1px solid $color;";
        //$data.="border-bottom: 1px solid $color;";
        $data.="border-spacing: 0;";
        $data.="$par;";
        return "style='$data'";
    }
    public static function tbEnd($color){
        $data="font-size: 10px;";
        return "style='$data'";
    }
    public static function tHeadEnd($color){
        $data="font-size: 10px;";
        $data.="border-bottom: 1px solid $color;";
        return "style='$data'";
    }
    public static function tHead2(){
        $data="font-size: 10px;";
        return "style='$data'";
    }
    public static function tR(){
        $data="padding:0px;";
        return "style='$data'";
    }
    public static function txt($font){
        $data="font-size: $font".";";
        return "style='$data'";
    }
    public static function txt2($font,$h){
        $data="font-size: $font".";";
        $data="line-height: $h".";";
        return "style='$data'";
    }
    public static function tb_border($font){
        $data="";
        $data.="font-size: $font".";";
        $data.="border-right: 0.5px solid black;";
        //$data.="border-collapse: collapse;";
        return "style='$data'";
    }
    public static function tb_border2($font,$data){
        $data.="font-size: $font".";";
        return "style='$data'";
    }
    public static function tb_borderTB($font){
        $data="";
        $data.="font-size: $font".";";
        $data.="border: 0 solid black;";
        $data.="border-collapse: collapse;";
        return "style='$data'";
    }
    public static function headContent($font){
        $data="";
        $data.="font-size: $font".";";
        $data.="border:0.5px solid black;";
        return "style='$data'";
    }
    public static function headContent2($font,$data=""){
        $data.="font-size: $font".";";
        return "style='$data'";
    }
    public static function logo_code1(){
        $data="";
        //$data.="position: absolute;";
        //$data.="margin-top: 100px;";
        //$data.="margin-left: 100px;";
        return "style='$data'";
    }
}
