<?php

/**
 *
 */
namespace app\modules\reports\models;


use app\component\ArrayHelper;
use app\modelsDB\DataAnalisa;
use app\models\Excel;
use yii\web\UploadedFile;

class UploadData extends DataAnalisa
{
    public $uploadFile;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['lab_number'], 'unique'],
            [['uploadFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xlxs, xls, csv', 'maxSize' => 10],
        ]);

    }

    /**
     * @param UploadedFile|null $image
     * @return bool
     * @throws \Throwable
     */
    public function upload(UploadedFile $image = null){
        $transaction =$this::getDb()->beginTransaction();
        try{
            $excel=new Excel();
            if($image !=null){
                $excel=$excel::renderData($image->tempName);
                $arrExcel=$excel->getActiveSheet()->toArray();
                $countInput = 0;
                $countError = 0;
                $header = [];
                $idxLabNo = '';
                $ls=['lab_number'=>'NoLab','recommended3'=>'Tahun','appearance'=>'appearance','density'=>'density','specific_gravity'=>'sg','api_gravity'=>'ag','total_acid_number'=>'tan','strong_acid'=>'san','flash_point'=>'flashpoint',
                    'kinematic_viscosity'=>'viscosity','water_content'=>'kf','ash_content'=>'AshContent','pour_point'=>'PourPoint','cloudn_point'=>'CloudnPoint','cetane_index'=>'CetaneIndex',
                    'conradson_carbon'=>'CCR10%','distillation_ibp'=>'IBP','distillation_5'=>'5%','distillation_10'=>'10%','distillation_20'=>'20%','distillation_30'=>'30%','distillation_40'=>'40%',
                    'distillation_50'=>'50%','distillation_60'=>'60%','distillation_70'=>'70%','distillation_80'=>'80%','distillation_90'=>'90%','distillation_95'=>'95%','distillation_ep'=>'ep',
                    'distillation_recovery'=>'RC','distillation_residue'=>'RS','distillation_loss'=>'L','distillation_recovery_300'=>'300C','sulphur_content'=>'SulphurContent',
                    'sediment'=>'sediment','colour'=>'Colour','copper_corrosion'=>'CopperStrip','particles_4'=>'PC>4µm','particles_6'=>'PC>6µm','particles_14'=>'PC>14µm','fame_content'=>'Fame','rancimat'=>'Kestabilan',
                    'lubricity_of_diesel'=>'hffr','calorific_value'=>'CalorificValue1','calorific_value1'=>'CalorificValue2','cerosine_content'=>'CerosineContent','iso_4406'=>'ISO4406','silicon'=>'silicon','alumunium'=>'Alumunium',
                    'particulat'=>'Particulat','recommended1'=>'recommended'
                    ];
                foreach ($arrExcel as $i=>$item){
                    if($i==0){
                        foreach ($item as $key=> $col){
                             if($this->hasAttribute($col)){
                                 if($col =='lab_number'){
                                     $idxLabNo=$key;
                                 }else{
                                    $header[$key]=$col;
                                 }
                             } else {
                                 $c2 = $this->indexOf($ls, str_replace(' ', '', $col));
                                 if ($c2) {
                                     if ($col == 'lab_number') {
                                         $idxLabNo = $key;
                                     } else {
                                         $header[$key] = $c2;
                                     }
                                 } else {
                                     $this->dataMessage .= "Kolom $col tidak terdaftar dalam table DataBase <br>";
                                 }
                             }
                        }
                    }else{
                        $labNo=isset($item[$idxLabNo])?$item[$idxLabNo]:'';
                        $report=self::findOne(['lab_number'=>$labNo]);
                        if($report==null){
                            $report=new self(['lab_number'=>$labNo]);
                        }
                        foreach ($header as $k=>$h){
                            if($report->hasAttribute($h)){
                                $report->$h=(string)$item[$k];
                            }
                        }
                        if($report->save()){
                            $countInput++;
                        }else{
                            $countError++;
                            $this->dataMessage .= "Lab No : $item[$idxLabNo] error dengan " . ArrayHelper::toString($report->errors) . ' <br>';
                        }
                    }
                }
                $this->status_save = $countInput > 0 ? 'success' : 'error';
                $this->message = "Upload berhasil dengan <br><br> Jumlah Success : $countInput <br>" .
                    ($countError > 0 ? "Jumlah Error : $countError <br>" : '') . '<br><hr>';
                $transaction->commit();
                return true;
            }
            $this->status_save = 'error';
            $this->message = 'Data Tidak disimpan, Coba lagi.<br>' . ArrayHelper::toString($this->errors);
            $transaction->rollBack();
            return false;
        }catch (\Exception $e){
            $transaction->rollBack();
            throw $e;
        }catch (\Throwable $e){
            $transaction->rollBack();
            throw  $e;
        }

    }

    /**
     * @param array $arr
     * @param $keyword
     * @return bool|int|string
     */

    protected function indexOf(array $arr, $keyword)
    {
        foreach ($arr as $idx => $item) {
            if(strtolower($item)==strtolower($keyword)){
//            if (strrpos($item, $keyword) !== false) {
                return $idx;
                break;
            }
        }
        return false;
    }
    public $message = '';

    public $status_save = '';

    public $class_message = '';

    public $dataMessage = '';
}