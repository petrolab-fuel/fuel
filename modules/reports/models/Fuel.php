<?php

/**
 * Created by PhpStorm.
 * User: HP
 * Date: 2/20/2018
 * Time: 1:57 PM
 */

namespace app\modules\reports\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Arrayhelper;
use yii\helpers\Url;
use app\modules\master\customers\models\Customers;
use app\modelsDB\Matrix;
use app\modelsDB\Parameter;
use yii\helpers\Json;
use app\modelsDB\Attention;
use app\modelsDB\Unit;

/**
 * Class Transformer
 * @package app\modules\monitoring\models
 *
 * @property string $fuelStatus
 * @property array $jenisFuel
 * @property string $custArray
 * @property void $cust
 * @property string $attArray
 * @property string $labArray
 * @property string $labNo
 * @property string $dirjen
 * @property string $action
 */

class Fuel extends \app\models\DataAnalisa
{

    public $_name;
    public $_attention;
    public $_address;
    public $_telp;
    //    public $_unitModel;
    //    public $_unitType;
    //    public $_unitSN;
    //    public $_unitLocation;

    private $allField = [
        'appearance' => 'appearance',
        'density' => 'density',
        'total_acid_number' => 'total_acid_number',
        'flash_point' => 'flash_point',
        'kinematic_viscosity' => 'kinematic_viscosity',
        'water_content' => 'water_content',
        'ash_content' => 'ash_content',
        'pour_point' => 'pour_point',
        'cetane_index' => 'cetane_index',
        'conradson_carbon' => 'conradson_carbon',
        'distillation_ibp' => 'distillation_ibp',
        'distillation_5' => 'distillation_5',
        'distillation_10' => 'distillation_10',
        'distillation_20' => 'distillation_20',
        'distillation_30' => 'distillation_30',
        'distillation_40' => 'distillation_40',
        'distillation_50' => 'distillation_50',
        'distillation_60' => 'distillation_60',
        'distillation_70' => 'distillation_70',
        'distillation_80' => 'distillation_80',
        'distillation_90' => 'distillation_90',
        'distillation_95' => 'distillation_95',
        'distillation_ep' => 'distillation_ep',
        'distillation_recovery' => 'distillation_recovery',
        'distillation_residue' => 'distillation_residue',
        'distillation_loss' => 'destillation_loss',
        'distillation_recovery_300' => 'destillation_recovery_300',
        'sulphur_content' => 'sulphur_content',
        'sediment' => 'sediment',
        'colour' => 'colour',
        'copper_corrosion' => 'copper_corrosion',
        'particles_4' => 'particles_4',
        'particles_6' => 'particles_6',
        'particles_14' => 'particles_14',
        'iso_4406' => 'iso_4406',
        'specific_gravity' => 'specific_gravity',
        'api_gravity' => 'api_gravity',
        'cloudn_point' => 'cloudn_point',
        'rancimat' => 'rancimat',
        'lubricity_of_diesel' => 'lubricity_of_diesel',
        'fame_content' => 'fame_content',
        'calorific_value' => 'calorific_value',
        'calorific_value1' => 'calorific_value1',
        'strong_acid' => 'strong_acid',
        'cerosine_content' => 'cerosine_content',


    ];

    /**
     * @return array
     */
    public function paramAll()
    {
        return [
            'appearance' => 'appearance_code',
            'density' => 'density_code',
            'api_gravity' => 'api_gravity_code',
            'specific_gravity' => 'specific_gravity_code',
            'total_acid_number' => 'total_acid_number_code',
            'strong_acid' => 'strong_acid_code',
            'calorific_value' => 'calorific_value_code',
            'calorific_value1' => 'calorific_value1_code',
            'flash_point' => 'flash_point_code',
            'kinematic_viscosity' => 'kinematic_viscosity_code',
            'water_content' => 'water_content_code',
            'ash_content' => 'ash_content_code',
            'cloudn_point' => 'cloudn_point_code',
            'pour_point' => 'pour_point_code',
            'cetane_index' => 'cetane_index_code',
            'conradson_carbon' => 'conradson_carbon_code',
            'distillation_ibp' => 'distillation_ibp_code',
            'distillation_5' => 'distillation_5_code',
            'distillation_10' => 'distillation_10_code',
            'distillation_20' => 'distillation_20_code',
            'distillation_30' => 'distillation_30_code',
            'distillation_40' => 'distillation_40_code',
            'distillation_50' => 'distillation_50_code',
            'distillation_60' => 'distillation_60_code',
            'distillation_70' => 'distillation_70_code',
            'distillation_80' => 'distillation_80_code',
            'distillation_90' => 'distillation_90_code',
            'distillation_95' => 'distillation_95_code',
            'distillation_ep' => 'distillation_ep_code',
            'distillation_recovery' => 'distillation_recovery_code',
            'distillation_residue' => 'distillation_residue_code',
            'distillation_loss' => 'distillation_loss_code',
            'distillation_recovery_300' => 'distillation_recovery_300_code',
            'sulphur_content' => 'sulphur_content_code',
            'rancimat' => 'rancimat_code',
            'lubricity_of_diesel' => 'lubricity_of_diesel_code',
            'fame_content' => 'fame_content_code',
            'cerosine_content' => 'cerosine_content_code',
            'sediment' => 'sediment_code',
            'colour' => 'colour_code',
            'copper_corrosion' => 'copper_corrosion_code',
            'particles_4' => 'particles_4_code',
            'particles_6' => 'particles_6_code',
            'particles_14' => 'particles_14_code',
            'iso_4406' => 'iso_4406_code',
            'silicon' => 'silicon_code',
            'alumunium' => 'alumunium_code',
            'monoglyceride' => 'monoglyceride_code',
            'free_glyceride' => 'free_glyceride_code',
            'total_glyceride' => 'total_glyceride_code',
            'particulat' => 'particulat_code',
        ];
    }
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'attentionName' => Yii::t('app', 'Attention'),
            'customerName' => Yii::t('app', 'Customer'),
            'action' => Yii::t('app', ''),
            'fuelStatus' => Yii::t('app', 'STATUS'),
            '_name' => Yii::t('app', 'Customer Name'),
            '_attention' => Yii::t('app', 'Attention'),
            '_address' => Yii::t('app', 'Address'),
            //            '_unitModel'=>Yii::t('app','Engine Builder'),
            //            '_unitType'=>Yii::t('app','Engine Type'),
            //            '_unitSN'=>Yii::t('app','Serial Number'),
            //            '_unitLocation'=>Yii::t('app','Serial Number'),
            //hssrmkoms
        ]);
    }

    /**
     * @return array
     */
    public function getJenisFuel()
    {
        return [
            'FUEL' => 'FUEL',
            'B20(CN 48)' => 'B20(CN 48)',
            'B30(CN 48)' => 'B30(CN 48)',
            'B30(CN 51)' => 'B30(CN 51)',
            'MFO' => 'MFO',
            'MFO 1' => 'MFO 1',
            'MFO 2' => 'MFO 2',
            'HSD' => 'HSD',
            'MDO' => 'MDO',
            'FAME/B100' => 'FAME/B100',
        ];
    }
    public function getAction()
    {
        //        $up = UploadMP_FC::findOne(['Lab_No' => $this->Lab_No]);
        return Html::tag('a', '<i class="fa fa-file-pdf-o"></i>', ['class' => 'btn btn-default btn-xs', 'title' => 'Print Draft', 'href' => Url::toRoute(["/reports/default/pdf", "lab_number" => $this->lab_number, 'draft' => 1]), 'target' => '_blank'])
            . Html::tag('a', '<i class="fa fa-file-pdf-o"></i>', ['class' => 'btn btn-default btn-xs', 'title' => 'Download Report', 'href' => Url::toRoute(["/reports/default/pdf", "lab_number" => $this->lab_number, 'draft' => 0]), 'target' => '_blank'])
            // . Html::tag('a','<i class="fa  fa-edit"></i>', ['class'=>'showModalButton','title'=>'Edit','href'=>Url::toRoute(["/reports/add/edit","id"=>$this->lab_number])])
            . Html::tag('a', '<i class="fa fa-edit"></i>', ['href' => Url::toRoute(['/reports/fuel/edit', 'id' => $this->lab_number]), 'data-action' => 'edit', 'title' => 'Edit', 'class' => 'btn btn-default btn-xs'])
            . Html::tag('a', '<i class="fa fa-copy"></i>', ['href' => Url::toRoute(['/reports/fuel/copy', 'id' => $this->lab_number]), 'data-action' => 'copy', 'title' => 'Copy', 'class' => 'btn btn-default btn-xs'])
            . Html::tag('a', '<i class="fa  fa-trash-o"></i>', [
                'class' => 'btn btn-default btn-xs', 'title' => 'Delete', 'href' => Url::toRoute(["/reports/add/del", "id" => $this->lab_number]), 'data-method' => 'post',
                'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
            ])
            //            . Html::tag('button','<i class="fa fa-copy"></i>', ['href'=> Url::toRoute(["/reports/fuel/copy","id"=>$this->lab_number]), 'title' => 'Copy', 'class' => 'showModalButton btn btn-xs'])
            . Html::tag('button', '<i class="fa fa-gear"></i>', ['href' => Url::toRoute(["/reports/fuel/eval-code", "lab_no" => $this->lab_number]), 'data-action' => 'update-code', 'title' => 'Update Eval code', 'class' => 'btn btn-default btn-xs'])
            // Html::tag('a','<i class="fa fa-search"></i>', ['class'=>'btn btn-primary btn-xs','title'=>'View Detail']).' '.
            // Html::tag('a','<i class="fa fa-share"></i>', ['class'=>'btn btn-primary btn-xs','title'=>'Follow Up','onclick'=>"openFollow('$this->Lab_No')"])
        ;
        //        return Html::tag('a', '<i class="fa fa-file-pdf-o" style="color: #ff0300;"></i>', [
        //            'href' => Url::toRoute(['/reports/default/pdf2', 'lab_number' => $this->lab_number]),
        //            'target' => '_blank',
        //            'data-method' => 'post',
        //            'class' => 'btn btn-default',
        //        ]).Html::tag('a', '<i class="fa fa-edit" style="color: #ff0300;"></i>', [
        //                'href' => Url::toRoute(['/reports/add/edit', 'id' => $this->lab_number]),
        //                               'data-method' => 'post',
        //                'class' => 'btn btn-default',
        //            ]).Html::tag('a', '<i class="fa fa-file-pdf-o" style="color: #ff0300;"></i>', [
        //                'href' => Url::toRoute(['/reports/default/pdf2', 'lab_number' => $this->lab_number]),
        //                'target' => '_blank',
        //                'data-method' => 'post',
        //                'class' => 'btn btn-default',
        //            ]);

        //        return Html::tag('div',
        //            Html::tag('a', 'Action <span class="caret"></span>', [
        //                'class' => "btn btn-default dropdown-toggle", 'data-toggle' => "dropdown", 'aria-expanded' => "false"
        //            ]) . Html::tag('ul',
        //                Html::tag('li', Html::tag('a', '<i class="fa fa-file-pdf-o"></i> view PDF', [
        //                    'href' => Url::toRoute(['/reports/default/pdf2', 'lab_number' => $this->lab_number]),
        //                    'target' => '_blank',
        //                    'data-method' => 'post',
        //                ])) .
        //                Html::tag('li', Html::tag('a', '<i class="fa fa-caret-square-o-right"></i> view Report', ['href' => Url::toRoute(['/orders/add', 'po_id' => $this->id])])) .
        //                Html::tag('li', Html::tag('a', '<i class="fa fa-print"></i> Print', ['href' => Url::toRoute(['//purchase/print', 'id' => $this->id]), 'target' => '_blank'])) .
        //                Html::tag('li', Html::tag('a', '<i class="fa fa-edit"></i> Edit', ['href' => Url::toRoute(['/reports/add/edit', 'id' => $this->lab_number])])) .
        //                Html::tag('li', Html::tag('a', '<i class="fa fa-window-close"></i> Delete',
        //                    ['href' => Url::toRoute(['/orders/purchase/del', 'id' => $this->id]),
        //                        'data-method' => 'post',
        //                        'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
        //                    ]))
        //
        //                , ['class' => "dropdown-menu"]), ['class' => "btn-group"]);
    }
    public function getFuelStatus()
    {
        $code = $this->eval_code == null ? 'A' : strtoupper($this->eval_code);
        $status = self::allStatus()['fuel']['status'];
        $lable = self::allStatus()['fuel']['label'];
        $txt = isset($status[$code]) ? $status[$code] : 'Not Run Evalcode';
        $cLabel = isset($lable[$code]) ? $lable[$code] : 'info';
        $listsColor = self::allStatus()['global_color'];
        $color = isset($listsColor[$code]) ? $listsColor[$code] : '#5bc0de';

        return '<span class="label label-' . $cLabel . '" style="background-color:' . $color . '">' . $txt . '</span>';
    }
    /**
     * @get string

     */
    public function getSampleDate()
    {
        return Yii::$app->formatter->asDate($this->sampling_date);
    }

    public function getReceiveDate()
    {
        return Yii::$app->formatter->asDate($this->received_date);
    }

    public function getReportDate()
    {
        return Yii::$app->formatter->asDate($this->report_date);
    }
    public function getCustArray()
    {
        $out = '';
        $cust = Customers::find()->asArray()->all();
        $out = ArrayHelper::map($cust, 'customer_id', 'name');
        return $out;
    }
    public function getLabArray()
    {
        $out = '';
        $lab = parent::find(['lab_number'])->asArray()->all();
        $out = ArrayHelper::map($lab, 'lab_number', 'lab_number');
        return $out;
    }

    public function getCustAttribut($id)
    {
        $model = Customers::findOne(['customer_id' => $id]);
        //        $model.=Customers->$this->getAttentionName();
        return $model;
    }
    public function getCust()
    {
        $this->_name = $this->customer->name;
        $this->_address = $this->customer->address;
        return;
    }

    public function getAttArray()
    {
        $out = '';
        $cust = Attention::find()->asArray()->all();
        $out = ArrayHelper::map($cust, 'id', 'name');
        return $out;
    }

    /** get lab No
     *
     */

    public function getLabNo()
    {
        $no = '';
        $dt = date('y');
        $n = self::find()->where(['right(lab_number,2)' => $dt])->max('lab_number');
        //       $n=self::find()->max('lab_number')->whre()
        $n = substr($n, 0, 4);
        $n = (int)$n + 1;
        return sprintf('%04s', $n) . "/OB/F/" . date('y');
        //       return $n;
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool

     */
    public function updateCode()
    {

        //        foreach ($this->aa as $aa){
        //
        //        }
        $count = 0;
        foreach ($this->paramAll() as $col => $code) {
            $value = $this->$col;
            if ($value == '' || $value == NULL || $value == '-') {
                continue;
            }
            $matrix = Matrix::find()->joinWith(['parameter'])->where(['matrix.tahun' => $this->recommended3, 'matrix.jenis' => $this->sludge_content])
                ->andWhere(['parameter.column_analisis_name' => $col])->one();
            $colname = '';
            $code = '';

            $evalCode = '';
            if ($matrix != NULL) {
                //                $code='';
                $min = $matrix->condition1;
                $max = $matrix->condition2;
                $batasan = $min != '' || $min != NULL ? $min : $max;
                $colname = $col . '_code';
                switch ($col) {
                    case 'appearance':
                        if ($value != '0' || $value != 0) {
                            $bat = explode('and', $batasan);
                            $code = sizeof(explode($bat[0], $value)) <= 1 ? 'B' : '';
                        }
                        break;
                    case 'copper_corrosion':
                        $code = sizeof(explode($batasan, $value)) <= 1 ? 'B' : '';
                        break;
                    case 'iso_4406':
                        $limit = explode('/', $batasan);
                        $val = explode('/', $value);
                        $code = $val[0] > $limit[0] || $val[1] > $limit[1] || $val[2] > $limit[2] ? 'B' : '';
                        break;
                    case 'water_content':
                        if ($value < 10) {
                            //                            $min=($min=='' or $min=NULL)?'':$min*1000;
                            //                            $max=($max==''or $max=NULL)?'':$max*1000;
                            $max = $max / 10000;
                        }
                        if ($max == '' || $max == NULL) {
                            $code = $value < $min ? 'B' : '';
                        } elseif ($min == '' || $min == NULL) {
                            $code = $value > $max ? 'B' : '';
                        } elseif (($max == '' || $max == NULL) && ($min == '' || $min == NULL)) {
                            $code = '';
                        } else {
                            $code = $value >= $min && $value <= $max ? '' : 'B';
                        }
                        break;
                    default:

                        if ($max == '' || $max == NULL) {
                            $code = $value < $min ? 'B' : '';
                        } elseif ($min == '' || $min == NULL) {
                            $code = $value > $max ? 'B' : '';
                        } elseif (($max == '' || $max == NULL) && ($min == '' || $min == NULL)) {
                            $code = '';
                        } else {
                            $code = $value >= $min && $value <= $max ? '' : 'B';
                        }
                }
            }
            if ($this->hasAttribute($colname)) {
                $this->$colname = $code;
            }
            if ($code > '') {
                $count++;
            }
        }
        $evalCode = $count > 0 ? 'B' : 'A';
        $this->eval_code = $evalCode;
        return $this->save();
    }
    //end function
    public function save($runValidation = true, $attributeNames = null)
    {

        $remark = '';
        $count = 0;
        // if ($this->appearance_code=='B'){
        //     $remark='Berdasarkan hasil dari parameter yang di ui, appearance sample solar keruh tidak sesuai nilai typical';
        // }elseif($this->iso_4406_code=='B'){
        //         $remark='Berdasarkan hasil dari parameter yang di uji, tingkat kebersihan minyak solar tidak sesuai nilai typical. Disarankan untuk melakukan filterisasi';
        // }elseif($this->appearance_code=='B' && $this->sediment_code=='B' && $this->colour_code=='B'){
        //         $remark='Berdasarkan hasil dari parameter yang di uji, appearance sample solar keruh, kandungan sediment, nilai Color ASTM tidak sesuai nilai typical.';
        // }elseif($this->sediment_code=='B' && $this->iso_4406_code=='B'){
        //         $remark='Kandungan sediment, tingkat kebersihan minyak solar tidak sesuai nilai typical. Disarankan untuk melakukan filterisasi.';
        // }else{
        //         $remark='Hasil analisa dari parameter yang diuji normal';
        //     }       


        //        if(Yii::$app->request->post('recomm1')){
        //          $this->recommended1=Yii::$app->request->post('recomm1');
        //        }

        $outParent = parent::save($runValidation, $attributeNames);
        $out = true;
        // $this->updateCode($this->lab_number);
        return $outParent && $out;
    }

    /**
     * @param $field
     * @return mixed
     */
    public function prm($field)
    {
        $data = Parameter::findOne(['column_analisis_name' => $field]);
        return $data;
    }

    /**
     * @param $id
     * @return Matrix|null
     */
    public function mtr($id, $jenis, $thn)
    {
        $data = Matrix::findOne(['parameter_id' => $id, 'jenis' => $jenis, 'tahun' => $thn]);
        return $data;
    }
    /**
     * @param $param
     * @return bool
     */
    public function cekParameter($param)
    {
        if ($this->$param == '-' || $this->$param == NULL || $this->$param == '') {
            return true;
        } else {
            return false;
        }
    }

    public function getDirjen()
    {
        $ket = '';
        switch ($this->recommended3) {
            case '2016':
                $ket = 'DIRJEN Migas No.28.K/10/DJM.T/2016';
                break;
            case '2019':
                $ket = 'DIRJEN Migas No.146.K/10/DJM/2020';
                break;
            default:
                $ket = 'DIRJEN Migas No.28.K/10/DJM.T/2013';
        }
        return $ket;
    }

    /**
     * @param $code
     * @param $label
     * @return string
     */
    public function ColourLabel($code, $label)
    {
        $out = '';
        $listsColor = self::allStatus()['global_color'];
        $color = isset($listsColor[$code]) ? $listsColor[$code] : 'white';
        switch ($code) {
            case 'B':
                $out = 'warning';
                break;
            case 'C':
                $out = 'danger';
                break;
            default:
                $out = 'default';
        }
        return  '<span class="label label-' . $out . '" style="background-color:' . $color . ';font-weight:bold">' . $label . '</span>';
        //        return $out;
    }
}
