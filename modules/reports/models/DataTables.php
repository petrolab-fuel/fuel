<?php
/**
 * Created by
 * User: wisard17
 * Date: 5/9/2017
 * Time: 4:32 PM
 */

namespace app\modules\reports\models;


use app\smartadmin\assets\plugins\DataTableAsset;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\View;

/**
 * Class DataTableView
 * @property string jsonData
 * @property string jsonDataTable
 * @package app\modules\report\models
 */
class DataTables extends Widget
{
    /**
     * @var array the HTML attributes for the container tag of the list view.
     * The "tag" element specifies the tag name of the container element and defaults to "div".
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];

    public $columns;

    /** @var  Model */
    public $model;

    public $request;

    public $ajaxUrl;


    /**
     * Initializes the view.
     */
    public function init()
    {
        parent::init();

        if ($this->columns === null) {
            throw new InvalidConfigException('The "columns" property must be set.');
        }

        if ($this->model === null) {
            throw new InvalidConfigException('The "model" property must be set.');
        }

        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
    }

    /**
     * Runs the widget.
     */
    public function run()
    {
        $assetTable = DataTableAsset::register($this->view);
        echo $this->loadTable();
        $this->runJs();
    }

    /**
     * @return string
     */
    public function loadTable()
    {
        $colspan = (isset($_GET['recv_date_begin']) || isset($_GET['recv_date_end'])) ? "false" : "true";
        // $levelId = Yii::$app->user->identity->level_id;
        $actionUrl = Url::toRoute(['/reports/fuel']);
        $content = '
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-2" data-widget-editbutton="false" data-widget-collapsed=' . $colspan . '>
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
        
                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"
        
                        -->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-filter"></i> </span>
                            <h2>Filter</h2>
                        </header>
        <iframe id="my_iframe" style="display:none;"></iframe>
<script>
function DownloadUrl(url) {
    document.getElementById("my_iframe").src = url;
};
</script>
                        <!-- widget div-->
                        <div>
        
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
        
                            </div>
                            <!-- end widget edit box -->
        
                            <!-- widget content -->
                            <div class="widget-body no-padding">
                                <form action  class="smart-form" method="get">
                                    <fieldset>
                                        <section class="col col-2">
                                            <label class="label">Receive Date</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-calendar"></i>
                                                <input type="text" id="recv_date_begin" name="recv_date_begin" value="' . @$_GET["recv_date_begin"] . '" placeholder="Begin">
                                            </label>
                                            
                                        </section>
                                        <section class="col col-2">
                                            <label class="label">&nbsp;</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-calendar"></i>
                                                <input type="text" id="recv_date_end" name="recv_date_end" value="' . @$_GET["recv_date_end"] . '" placeholder="End">
                                            </label>
                                        </section>
                                        <div class="row">
                                            <section class="col col-2">
                                            <label class="label">Report Date</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-calendar"></i>
                                                <input type="text" id="report_date_begin" name="report_date_begin" value="' . @$_GET["report_date_begin"] . '" placeholder="Begin">
                                            </label>
                                            
                                        </section>
                                        <section class="col col-2">
                                            <label class="label">&nbsp;</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-calendar"></i>
                                                <input type="text" id="report_date_end" name="report_date_end" value="' . @$_GET["report_date_end"] . '" placeholder="End">
                                            </label>
                                        </section>
                                        </div>
                                    </fieldset>
                                    <footer>
                                        ' . Html::button('Submit', ['class' => 'btn btn-primary', 'type' => "submit"]) . ' 
                                        ' . Html::a('<i class="fa fa-refresh"></i> Cancel', ['/reports/fuel'], ['class' => 'btn btn-default', 'type' => "button"]) . '
                                        ' . Html::a('<i class="fa fa-file-excel-o"></i> Excel Export', ['/ajax/export-excel'], ['class' => 'btn btn-default', 'type' => "button", 'data-action' => 'exportexcel', 'target' => '_blank',]) . '
                                         ' . Html::tag('a', '<i class="fa fa-file-pdf-o"></i> Download Report', ['class' => 'btn btn-danger', 'data-action' => 'export']) . '
                                          ' . Html::button('test', ['id'=>'test','class' => 'btn btn-primary', 'type' => "button",'style'=>'display: none']) . ' 
                                  </footer>
                                    </form>
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->


                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-3" data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
        
                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"
        
                        -->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Fuel</h2>
        
                        </header>
        
                        <!-- widget div-->
                        <div>
        
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
        
                            </div>
                            <!-- end widget edit box -->
        
                            <!-- widget content -->
                            <div class="widget-body no-padding">
                            <table id="' . $this->options['id'] . '" class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" width="100%">
                                <thead>
                                </thead>
                            </table>

                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->';
         

        return $content; 
        // Html::tag('table', "",
            // ['class' => "table table-responsive", 'width' => "100%", 'id' => $this->options['id']]);
    }


    /**
     * @return array
     */
    protected function loadColumnHeader()
    {
        $out = [];
        foreach ($this->columns as $column) {
            $showD = $column == 'Order_No' ? 'details-control' : '';
            $out[] = [
                'title' => $this->model->getAttributeLabel($column),
                'data' => "$column",
                'className' => $showD,
            ];
        }

        return $out;
    }

    /**
     * @return string
     */
    protected function loadColumnFilter()
    {
        $out = '';
        foreach ($this->columns as $column) {
            $ipt = '';
            if (!in_array($column, ['action',])) {
                if (in_array($column, ['fuelStatus'])) {
                    $ipt = $this->renderStatus('');
                } elseif ($column == 'sampleDate'||$column == 'receiveDate'||$column == 'reportDate') {
                    $ipt = Html::input('text', "filter_$column", '', [
                        'placeholder' => ' Filter ' . $this->model->getAttributeLabel($column),
                        'class' => 'form-control datejuifilter',
                        'style' => 'padding: 1px;height: 100%;width: 100%;',
                    ]);
                } else
                    $ipt = Html::input('text', "filter_$column", '', [
                        'placeholder' => ' Filter ' . $this->model->getAttributeLabel($column),
                        'class' => 'form-control',
                        'style' => 'padding: 1px;height: 100%;width: 100%;',
                    ]);
            }

            $out .= Html::tag('td', $ipt);
        }

        return Html::tag('tr', $out);
    }

    public function renderStatus($type)
    {

        $listStatus = [
                'A' => 'Normal',
                'B' => 'Attention',
                'C' => 'Urgent',
                'D' => 'Severe',
        ];
        $out = '<select class="form-control" style="padding: 1px;height: 100%;width: 100%;"><option value="">All</option>';
        foreach ($listStatus as $idx => $status) {
            $out .= '<option value="' . $idx . '">' . $status . '</option>';
        }
        return $out . "</select>";
    }

    /**
     * All JS
     */
    protected function runJs()
    {

        $csrf = Yii::$app->request->getCsrfToken();

        $loadingHtml = '<div align="center"><i class="fa fa-spinner fa-pulse fa-4x fa-fw margin-bottom"></i></div>';

        $ajaxUrl = Url::to(['index',
            'recv_date_begin' => isset($_GET['recv_date_begin']) ? $_GET['recv_date_begin'] : '',
            'recv_date_end' => isset($_GET['recv_date_end']) ? $_GET['recv_date_end'] : '',
            'report_date_begin' => isset($_GET['report_date_begin']) ? $_GET['report_date_begin'] : '',
            'report_date_end' => isset($_GET['report_date_end']) ? $_GET['report_date_end'] : '',
            '_csrf' => $csrf,
        ]);

        $idTable = $this->options['id'];
        $begin = isset($_GET['recv_date_begin']) ? $_GET['recv_date_begin'] : '';
        $end = isset($_GET['recv_date_end']) ? $_GET['recv_date_end'] : '';
        $rbegin=isset($_GET['report_date_begin']) ? $_GET['report_date_begin'] : '';
        $rend=isset($_GET['report_date_end']) ? $_GET['report_date_end'] : '';
        $columns = Json::encode($this->loadColumnHeader());



        $detailUrl = Url::to(['detail']);
       

        $colFilter = $this->loadColumnFilter();

        $detailUrl = Url::toRoute(['detail']);

        $urlState = Url::toRoute(['/monitoring/sample/set-state']);
        $url = Url::to(['add/getlabnumber']);
        $urlto = Url::to(['default/pdf2']);

        $order = isset($this->options['order']) ? $this->options['order'] : "[0, 'desc']";

        $jsScript = <<< JS
var delay2 = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();
// Date Range Picker
$("#recv_date_end").datepicker({
    defaultDate: "+1w",
    changeMonth: true,
    numberOfMonths: 3,
    dateFormat: 'yy-mm-dd',
    prevText: '<i class="fa fa-chevron-left"></i>',
    nextText: '<i class="fa fa-chevron-right"></i>',
    onClose: function (selectedDate) {
        $("#recv_date_begin").datepicker("option", "maxDate", selectedDate);
    }

});
$("#recv_date_begin").datepicker({
    defaultDate: "+1w",
    changeMonth: true,
    numberOfMonths: 3,
    dateFormat: 'yy-mm-dd',
    prevText: '<i class="fa fa-chevron-left"></i>',
    nextText: '<i class="fa fa-chevron-right"></i>',
    onClose: function (selectedDate) {
        $("#recv_date_end").datepicker("option", "minDate", selectedDate);
    }
});
$("#report_date_end").datepicker({
    defaultDate: "+1w",
    changeMonth: true,
    numberOfMonths: 3,
    dateFormat: 'yy-mm-dd',
    prevText: '<i class="fa fa-chevron-left"></i>',
    nextText: '<i class="fa fa-chevron-right"></i>',
    onClose: function (selectedDate) {
        $("#report_date_begin").datepicker("option", "maxDate", selectedDate);
    }

});
$("#report_date_begin").datepicker({
    defaultDate: "+1w",
    changeMonth: true,
    numberOfMonths: 3,
    dateFormat: 'yy-mm-dd',
    prevText: '<i class="fa fa-chevron-left"></i>',
    nextText: '<i class="fa fa-chevron-right"></i>',
    onClose: function (selectedDate) {
        $("#report_date_end").datepicker("option", "minDate", selectedDate);
    }
});
jQuery('.datejuifilter').datepicker({"altField": "yyyy-mm-dd", "dateFormat": "yy-mm-dd"});

var renderdata = (function () {
    // var dataSet = ;
    var domElement = $('#$idTable');
    var detailurl = "$detailUrl";

    var domOuttable = domElement.parent().parent().parent();

    var table = domElement.DataTable({
        sDom: "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
        "t" +
        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        scrollX: true,
        rowId: "Order_No",
        processing: true,
        serverSide: true,
        ajax: "$ajaxUrl",
        columns: $columns,
        order: [$order],
        "createdRow": function (row, data, index) {
            if (data.classRow !== '')
                $(row).addClass(data.classRow);
        },
        initComplete: function () {
            delay2(function () {
                var colfilter = $('$colFilter');
                colfilter.find('.datejuifilter').datepicker({
                    showButtonPanel: true,
                    altField: "yyyy-mm-dd",
                    dateFormat: "yy-mm-dd",
                    beforeShow: function (input) {
                        setTimeout(function () {
                            var buttonPane = $(input)
                                .datepicker("widget")
                                .find(".ui-datepicker-buttonpane");
                            $("<button>", {
                                text: "Reset",
                                click: function () {
                                    $.datepicker._clearDate(input);
                                }
                            }).appendTo(buttonPane).addClass("ui-datepicker-clear ui-state-default ui-priority-primary ui-corner-all");
                        }, 1);
                    },
                });
                domOuttable.find('thead').eq(0).append(colfilter);
                //domOuttable.find('.dataTables_scrollBody').attr('style', 'position: relative; width: 100%;');
            }, 1000);
        }

    });

    domOuttable.find('thead').eq(0).delegate('td input[type=text]', 'change', function () {
        var self = this;
        delay2(function () {
            table
                .column($(self).parent().index() + ':visible')
                .search(self.value)
                .draw();
        }, 1000);

    });

    domOuttable.find('thead').eq(0).delegate('td select', 'change', function () {
        var self = this;
        delay2(function () {
            table
                .column($(self).parent().index() + ':visible')
                .search(self.value)
                .draw();
        }, 1000);

    });

    domElement.on('click', 'tbody .details-control', function () {

        var tr = $(this).closest('tr');
        tr.toggleClass('selected');

        var row = table.row(tr);

        var orderNo = row.data().Order_No;


        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {

            if (row.child() === undefined || row.child().find('div').length < 2) {
                row.child('$loadingHtml');
                $.get(detailurl, {
                    "orderno": orderNo,
                    "_csrf": '$csrf'

                }, function (dataresponse, status, xhr) {
                    if (status === "success") {
                        row.child(dataresponse);
                        document.dzone_run($(row.child()).find(".dropzone"));
                    }

                    if (status === "error")
                        alert("Error: " + xhr.status + ": " + xhr.statusText);
                });
            }
            row.child.show();

            tr.addClass('shown');
        }
    });

    domElement.on('click', '[data-action=state-sample]', function () {
        var elmen = $(this);
        var tr = elmen.closest('tr');
        var td = elmen.closest('td');
        var row = table.row(tr);

        var status = elmen.attr('data-status');
        var orderNo = row.data().Order_No;

        $.ajax({
            type: 'POST',
            url: '$urlState',
            dataType: "json",
            data: {"_csrf": '$csrf', 'orderNo': orderNo, 'status': status},
            beforeSend: function () {
                // blockLoading(elmen);
            },
            error: function () {
                alert('silahkan ulangi ...');
                // $(elmen).unblock();
            },
            success: function (data) {
                if (data.state === 'yes') {
                    row.cell(row, 8).data(data.col_status);
                    row.cell(row, 1).data(data.col_action)
                    // console.log(row.cell(row,2).data());
                }
                // $(elmen).unblock();
            }
        });
    });

     $('[data-action=export]').click(function (e) {
         e.preventDefault();


          var urlto = "$urlto";
        var begin = "$begin";
        var end = "$end";
        var url = "$url";
        var data2 = {};
        // data2['group'] = group;
        data2['begin'] = begin;
        data2['end'] = end;
        
        var param = table.ajax.params();
        param.sm = data2;
        
        $.ajax({
            type: 'POST',
            url: url,
            data: param,
            success: function (data, textStatus, jqXHR) {
                // window.open('about:blank', '_blank');
                console.log(jqXHR.status);
                var arr=data.split(',');
                var i;
                for (i=0;i<arr.length;i++){
                    var urlfull = urlto + "?lab_number=" + arr[i];
                    window.open(urlfull, '_blank');
                }
                
                 
            }
        });

     });

    $('[data-action=exportexcel]').click(function (e) {
       
        e.preventDefault();
        
        var param = table.ajax.params();
        
        param._csrf = '$csrf';
        param.recv_date_begin = "$begin";
        param.recv_date_end = "$end"; 
        param.report_date_begin = "$rbegin";
        param.report_date_end = "$rend"; 
        
//        
        var urlparm = $.param(param);
        
        var url = $(e.target).attr('href');
        
        DownloadUrl(url + '?' + urlparm);
        console.log(urlparm);
    });

    $('#test').click(function(e){
            table.ajax.reload();
        });
    return {
        "test": function () {
            alert('daa');
        },
        table: table
    };
})();        

JS;


        $this->view->registerJs($jsScript, View::POS_READY, 'runfor_' . $this->options['id']);
    }


}