<?php


namespace app\modules\reports\models;

use app\models\DataAnalisa;
use app\modules\reports\models\search\SearchFuel;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use Curl\Curl;

class ImportData extends DataAnalisa
{

    /** @var string */
    public $message = '';

    /** @var string */
    public $status_save = '';

    /** @var array */
    public $data;

    public $dataApi=[];

    /**
     * @param $data
     */
    protected function saveApi($data){
        $url_api='https://pama.petrolab.co.id/api_erp/';
        $api=[];
        foreach ($data as $dataImp){
            $api['data'][]=$dataImp->toArray();
        }
        $c=new Curl();
        $c->setTimeout(10000);
        $c->post($url_api.'upload-fuel',['all'=>Json::encode($api)]);
        if ($c->error) {
//            $this->message=$c->response;
            return false;
        }
        if (isset($c->response->sv)) {
            $this->dataApi = [
                'save' => ArrayHelper::toArray($c->response->sv),
            ];
        }
        return true;
    }

    public function UpdateStatus($params){
        $model=parent::find()->where("customer_id=7");
        if (isset($params['recv_date_begin']) && $params['recv_date_begin'] != '') {
            $s = date('Y-m-d', strtotime($params['recv_date_begin']));
            if (isset($params['recv_date_end']) && $params['recv_date_end'] != '') {
                $e = date('Y-m-d', strtotime($params['recv_date_end']));
                $model->andWhere(" received_date >= '$s' and  received_date <=  '$e' ");
            } else
                $model->andWhere(" received_date >= '$s' and  received_date <=  '$s' ");
        }
        if (isset($params['report_date_begin']) && $params['report_date_begin'] != '') {
            $s = date('Y-m-d', strtotime($params['report_date_begin']));
            if (isset($params['report_date_end']) && $params['report_date_end'] != '') {
                $e = date('Y-m-d', strtotime($params['report_date_end']));
                $model->andWhere(" report_date >= '$s' and  report_date <=  '$e' ");
            } else
                $model->andWhere(" report_date >= '$s' and  report_date <=  '$s' ");
        }
        if (!isset($params['recv_date_begin']) || $params['recv_date_begin'] = ''){
            $this->message = 'Filter First';
            return false;
        }
//        $model->andWhere("received_date='2020-03-30'");
//        $model->noCache()->all();
//        foreach ($model as $data){
//            $this->dataApi[]=$model->lab_number;
//        }
//        return $this->dataApi;
//        $filter=$model->searchData($params,true,true);
//        $query=$model->searchData($params,true);
//        $query=self::find();
//        $query->where=$filter->where;
//        $query->joinWith=$filter->joinWith;
//
//        $this->message=$query->joinWith;

//        return $this->message;
//        $data=$query->noCache()->all();
//        return  $this->message=$data;
//        if ($this->lab_number != '') {
//            $query->andWhere(['lab_number' => $this->lab_number]);
//        }
//
//        if ($query->where == [] || $query->where == '') {
//            $this->message = 'Filter First';
//            return false;
//        }
        $data=$model->noCache()->all();
        if(!$this->saveApi($data)){
            $this->message='Not connect';
            return false;
        }
        $this->message='Upload Data';
        return true;
    }

}