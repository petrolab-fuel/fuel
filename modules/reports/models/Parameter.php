<?php
namespace app\modules\reports\models;
use Yii;
use app\modelsDB\DataAnalisa;
use app\modelsDB\Parameter as PR;
use app\modelsDB\Matrix;
/**
* 
*/
class Parameter
{

public function paramAll(){
	$param=[
		['param'=>'appearance','code'=>'appearance_code'],
		['param'=>'density','code'=>'density_code'],
		['param'=>'api_gravity','code'=>'api_gravity_code'],
		['param'=>'specific_gravity','code'=>'specific_gravity_code'],
		['param'=>'total_acid_number','code'=>'total_acid_number_code'],
        ['param'=>'strong_acid','code'=>'strong_acid_code'],
        ['param'=>'calorific_value','code'=>'calorific_value_code'],
        ['param'=>'calorific_value1','code'=>'calorific_value1_code'],
		['param'=>'flash_point','code'=>'flash_point_code'],
		['param'=>'kinematic_viscosity','code'=>'kinematic_viscosity_code'],
		['param'=>'water_content','code'=>'water_content_code'],
		['param'=>'ash_content','code'=>'ash_content_code'],
		['param'=>'cloudn_point','code'=>'cloudn_point_code'],
		['param'=>'pour_point','code'=>'pour_point_code'],
		['param'=>'cetane_index','code'=>'cetane_index_code'],
		['param'=>'conradson_carbon','code'=>'conradson_carbon_code'],
		['param'=>'distillation_ibp','code'=>'distillation_ibp_code'],
		['param'=>'distillation_5','code'=>'distillation_5_code'],
		['param'=>'distillation_10','code'=>'distillation_10_code'],
		['param'=>'distillation_20','code'=>'distillation_20_code'],
		['param'=>'distillation_30','code'=>'distillation_30_code'],
		['param'=>'distillation_40','code'=>'distillation_40_code'],
		['param'=>'distillation_50','code'=>'distillation_50_code'],
		['param'=>'distillation_60','code'=>'distillation_60_code'],
		['param'=>'distillation_70','code'=>'distillation_70_code'],
		['param'=>'distillation_80','code'=>'distillation_80_code'],
		['param'=>'distillation_90','code'=>'distillation_90_code'],
		['param'=>'distillation_95','code'=>'distillation_95_code'],
		['param'=>'distillation_ep','code'=>'distillation_ep_code'],
		['param'=>'distillation_recovery','code'=>'distillation_recovery_code'],
		['param'=>'distillation_residue','code'=>'distillation_residue_code'],
		['param'=>'distillation_loss','code'=>'distillation_loss_code'],
		['param'=>'distillation_recovery_300','code'=>'distillation_recovery_300_code'],
        ['param'=>'sulphur_content','code'=>'sulphur_content_code'],
        ['param'=>'rancimat','code'=>'rancimat_code'],
        ['param'=>'lubricity_of_diesel','code'=>'lubricity_of_diesel_code'],
        ['param'=>'fame_content','code'=>'fame_content_code'],
        ['param'=>'cerosine_content','code'=>'cerosine_content_code'],
        ['param'=>'sediment','code'=>'sediment_code'],
		['param'=>'colour','code'=>'colour_code'],
		['param'=>'copper_corrosion','code'=>'copper_corrosion_code'],
		['param'=>'particles_4','code'=>'particles_4_code'],
		['param'=>'particles_6','code'=>'particles_6_code'],
		['param'=>'particles_14','code'=>'particles_14_code'],
		['param'=>'iso_4406','code'=>'iso_4406_code'],
		['param'=>'silicon','code'=>'silicon_code'],
		['param'=>'alumunium','code'=>'alumunium_code'],
			
	];
	return $param;
}

//getdata
public function getData($labNumber){

$fixData=[];
foreach ($this->paramAll() as $key => $value) {
$data_analisa=DataAnalisa::find()
->where(['lab_number'=>$labNumber])->one();

if($data_analisa[$value['param']]){
$pr=PR::find()->where(['column_analisis_name'=>$value['param']])->one();
$mtr=Matrix::find()->where(['parameter_id'=>$pr['id']])->one();
if($pr){
array_push($fixData, ['p_label'=>$pr['nama'],'p_satuan'=>$pr['satuan'],'p_satuan1'=>$pr['satuan1'],'p_method'=>$pr['method'],'p_mtr'=>$mtr['condition1'],'p_mtr1'=>$mtr['condition4'],'p_mtr2'=>$mtr['condition6'],'p_col'=>$pr['column_analisis_name'],'p_name'=>$value['param'],'p_val'=>$data_analisa[$value['param']],'p_jn'=>$data_analisa['sludge_content'],'p_thn'=>$data_analisa['recommended3'],'p_code'=>$data_analisa[$value['code']]]);	
}

}

}
$fixData = array_map("unserialize", array_unique(array_map("serialize", $fixData)));
return $fixData;
}

}