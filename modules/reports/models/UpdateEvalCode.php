<?php


namespace app\modules\reports\models;
use app\component\ArrayHelper;
use app\modules\reports\models\search\SearchFuel;

class UpdateEvalCode extends Fuel
{
    public function evalCode($params){
        $model=new SearchFuel();
        $filter=$model->searchData($params,true);
        $query = self::find();
        $query->where = $filter->where;
        $query->joinWith = $filter->joinWith;
        if ($this->lab_number != '') {
            $query->andWhere(['lab_number' => $this->lab_number]);
        }
        if ($query->where == [] || $query->where == '') {
            $this->message = 'Filter First';
            return false;
        }
        $data = $query->noCache()->all();
        if ($data == null) {
            $this->message = 'No Data Found';
            return false;
        }
        $statusSuccees = 0;
        $out = true;
        $msg = '';
        foreach ($data as $record){
            if($record->updateCode()){
                $this->data[$record->rowId] = $record->fuelStatus;
                $statusSuccees++;
            }else{
                $out=false;
                $msg .=
                    "Lab No $record->lab_number error : " . ArrayHelper::toString($record->errors) . '<br>';
            }
        }
        $this->message .= $statusSuccees > 1 ? "Success diupdate : $statusSuccees <hr>" : '';
        $this->message .= $msg;
        return $out;
    }

    public $message;

    public $status_save = '';

    public $class_message = '';

    public $data = [];
}