<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 2/20/2018
 * Time: 5:41 PM
 */

namespace app\modules\reports\controllers;
use Yii;
use app\modules\users\models\Customer;
use app\smartadmin\helpers\Html;
use  app\modules\reports\models\Fuel;
use  app\modules\reports\models\search\SearchFuel;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\Json;
use app\modelsDB\Attention;
use yii\helpers\ArrayHelper;
class AddController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['detail', 'index', 'edit'],
                'rules' => [
                    [
                        'actions' => ['detail', 'index', 'edit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'del' => ['post'],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
         
         $model=new Fuel();
         if (Yii::$app->request->isAjax){
             $model->customer_id=Yii::$app->request->post('customer_id');
             $model->attention_id=Yii::$app->request->post('attention_id');
             
             if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                // $model->save();

                   if ($model->save()){
                     
                     return $this->asJson(['success' => true]);

                    }
               }else{
                return $this->renderAjax('index',['model'=>$model]);    
               }
         }else{
              return $this->renderAjax('index',['model'=>$model]);    
                // return ['model'=>$model];
        }
    }

    public function actionEdit($id)
    {
        $model = $this->findModel($id);     
        // $model->getCust();
        $model->received_date=date('d-m-Y',strtotime($model->received_date));
        $model->report_date=date('d-m-Y',strtotime($model->report_date));
        $model->sampling_date=date('d-m-Y',strtotime($model->sampling_date));
        $post = Yii::$app->request->post();
             // if (Yii::$app->request->isAjax){
                if ($model->load($post) && $model->validate()){
                    // print_r($model->customer_id);
                    // die;
                    $model->save(false);
                    return $this->redirect(Url::toRoute(['/reports/fuel']));
                        
                }else{
                 return $this->renderAjax('edit', ['model' => $model,]);
            }            
        }   

    public function actionDel($id)
    {
        $this->findModel($id)->delete();
            return $this->redirect(['/reports/fuel']);
    }


    public function actionGet(){
        $request = Yii::$app->request;
        $obj = $request->post('obj');
        $value1=$request->post('value1');
        $value = $request->post('value');
        switch ($obj) {
            case 'customer_id':
                $data = Attention::find()->where([$obj => $value])->all();
                $tagOptions = ['prompt' => "Select..."];
                return Html::renderSelectOptions([], ArrayHelper::map($data, 'id', 'name'), $tagOptions);
                break;
            case 'att_id':
                $data =Customer::find()
                     ->where(['customer_id' => $value])->one();
                    echo json::encode($data);
                    return;
               break;
//            case 'district_id':
//                $data = Village::find()->where([$obj => $value])->all();
//                break;
        }



    }
    public function actionLab(){
        $model=new Fuel;
             return $this->asJson([$model->labNo]);
    }

    public function actionGetlabnumber()
    {

        // $group = $_POST['sm']['group'];
        $begin = $_POST['sm']['begin'];
        $end = $_POST['sm']['end'];

        $searchModel = new SearchFuel();
        $request = Yii::$app->request->post();

        // $request['group'] = $_POST['sm']['group'];
        $request['recv_begin'] = $_POST['sm']['begin'];
        $request['recv_end'] = $_POST['sm']['end'];

        $query = $searchModel->searchData($request, true);

        $data = $query->select('lab_number')->distinct()->all();
        $labnumber = '';
        foreach ($data as $key => $value) {
            $labnumber .= $value->lab_number . ',';
        }
        return $labnumber;
    }
    public function actionPdf($labNumber){
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
      $connt=$this->renderPartial('report_process',['labNumber'=>$labNumber]);
      
      return $connt;
    }
//    public function actionAtt($id)
//    {
//        $model = Attention::find()
//            ->leftJoin('customer',[])
//            ->where(['customer_id' => $id])
//            ->orderBy('id DESC')
//            ->all();
//
////        if($model>0){
//
//
////        echo "<option>-</option>";
//            foreach($model as $post){
//                $tagOptions = ['prompt' => "=== Select ==="];
//                return Html::renderSelectOptions([],
//                    ArrayHelper::map($model, 'id', 'name'), $tagOptions);
//
//            }
////        }

//    }
//    public  function actionAtt(){
//        $out = [];
//        if (isset($_POST['depdrop_parents'])) {
//            $parents = $_POST['depdrop_parents'];
//            $selected='';
//            if ($parents != null) {
//                $cat_id = $parents[0];
//                $model = Attention::find()->asArray()
//                    ->where(['customer_id' => $cat_id])
//                    ->all();
//                if(!empty($_POST['depdrop_params'])){
//                    $param=$_POST['depdrop_params'];
//                    $id=$param[0];
//                    $id1=$param[1];
//                    foreach ($model as $key=>$value){
//                        $out[] = ['id'=>$value['id'],'name'=> $value['id'].' - '.$value['name']];
//                        if ($key==0){
//                            $selected =$value['id'];
//                        }
//                        $selected=$id1;
//                    }
//                }
//                echo Json::encode(['output' => $out, 'selected' => $selected]);
//                return;
//            }
//        }
//
//    }
//    public  function actionUnit(){
//        $out = [];
//        if (isset($_POST['depdrop_parents'])) {
//            $parents = $_POST['depdrop_parents'];
//            $selected='';
//            if ($parents != null) {
//                $cat_id = $parents[0];
//                $model = Units::find()->asArray()
//                    ->where(['customer_id' => $cat_id])
//                    ->all();
//                if(!empty($_POST['depdrop_params'])){
//                    $param=$_POST['depdrop_params'];
//                    $id=$param[0];
//                    $id1=$param[1];
//                    foreach ($model as $key=>$value){
//                        $out[] = ['id'=>$value['id'],'name'=> $value['id'].' - '.$value['type_unit']];
//                        if ($key==0){
//                            $selected =$value['id'];
//                        }
//                        $selected=$id1;
//                    }
//                }
//                echo Json::encode(['output' => $out, 'selected' => $selected]);
//                return;
//            }
//        }
//    }
//
//    public  function actionSlud(){
//        $out = [];
//        if (isset($_POST['depdrop_parents'])) {
//            $parents = $_POST['depdrop_parents'];
//            $selected='';
//            if ($parents != null) {
//                $cat_id = $parents[0];
//                $model = SamplingPoint::find()->asArray()
//                    ->where(['unit_id' => $cat_id])
//                    ->all();
//                if(!empty($_POST['depdrop_params'])){
//                    $param=$_POST['depdrop_params'];
//                    $id=$param[0];
//                    $id1=$param[1];
//                    foreach ($model as $key=>$value){
//                        $out[] = ['id'=>$value['id'],'name'=> $value['id'].' - '.$value['name']];
//                        if ($key==0){
//                            $selected =$value['id'];
//                        }
//                        $selected=$id1;
//                    }
//                }
//                echo Json::encode(['output' => $out, 'selected' => $selected]);
//                return;
//            }
//        }
//    }
//    public function actionUnitDetail($id) {
//        if($id){
//            $detail = Units::find()
//                ->where(['id' => $id])
//                ->one();
//            echo json::encode($detail);
//            return;
//        }
//    }

    protected function findModel($id)
    {
        if (($model = SearchFuel::findOne(['lab_number' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}