<?php

namespace app\modules\reports\controllers;


use app\modules\master\models\data\Analisa;
use yii\web\Controller;
use kartik\mpdf\Pdf;
use Codeception\Exception\ConfigurationException;
use app\modules\reports\models\Report;
use app\modules\reports\models\Parameter;
use yii\web\Response;
use app\component\Mpdf;
use app\modelsDB\DataAnalisa;
use Curl\Curl;
use yii\helpers\Json;
//use app\modelsDB\Parameter;
use Yii;

/**
 * Default controller for the `reports` module
 */
class DefaultController extends Controller
{
  /**
   * Renders the index view for the module
   * @return string
   */
  public $url_api = 'https://ut.petrolab.co.id/api_erp';
  public function actionIndex()
  {
  
      $model=Analisa::find()->where("received_date>='2021-03-01' and received_date<='2021-03-31'")->all();
      $api = [];
      foreach ($model as $updateStatus) {
          if ($updateStatus->publish == 0 && $updateStatus->customer_type == 1) {
              $updateStatus->publish = 1;
              $api['data'][] = $updateStatus->toArray();
          }
          
          //            $api['data'][$updateStatus->] = $updateStatus->toArray();
      } 
      $c = new Curl();
      $c->setOpt(CURLOPT_SSL_VERIFYPEER, false);
      $c->setTimeout(10000);
      $c->post($this->url_api . '/data-upload/fuel', ['all' => Json::encode($api)]);
     print_r ($c);
    exit;
      
   }
  
  public function actionPdf($lab)
  {
    Yii::$app->response->format = Response::FORMAT_RAW;
    $request = Yii::$app->request->queryParams;
    $rpt = Analisa::find()->where("lab_number='$lab'")->one();
//    print_r($rpt->renderParameter());
//    exit;
    $pdf = new Mpdf([
      'format' => [215, 330],
      'margin_left' => 5,
      'margin_right' => 5,
      'margin_top' => 18,
      'margin_bottom' => 5,
      'margin_header' => 5,
      'margin_footer' => 5,
    ]);

    // if ($draft) {
    //     $pdf->setDraft();
    // }

    $pdf->registerCssFile('@vendor/bower-asset/bootstrap/dist/css/bootstrap.css');
    $pdf->registerCssFile('@app/web/css/pdf.css');

    $pdf->SetTitle('Report PAP UT - Petrolab Services');
    $pdf->SetAuthor('Petrolab Services');

    $header = $this->renderPartial('_header', ['model' => $rpt]);
    $footer = $this->renderPartial('_footer');
    $pdf->SetHTMLHeader($header);
    $pdf->SetHTMLFooter($footer);

    // $count = count($trx);
    // foreach ($trx as $i => $report) {
    // $report->isPdf = true;
    // $report->draft = $draft;

    $content = $this->renderPartial('_pdf', [
      'model' => $rpt,
    ]);

    $pdf->WriteHTML($content);
    // if ($count > 1 && ($i+1) != $count) {
    //     $pdf->AddPage();
    // }
    // }
    $name = 'Report PAP-PDF.pdf';
    // if ($count == 1) {
    //     $t = $trx[0];
    //     $name = $t->Lab_No . '_' . $t->UNIT_NO . '_' . $t->component . '_' . $t->getTextEvalCode($t->EVAL_CODE) .'.pdf';
    // }
    $d = 'i';
    if (isset($request['download']) && $request['download'] == 1) {
      $d = 'd';
    }
    return $pdf->Output($name, $d);
  }
  /**
   * Renders the test pdf
   * @return string
   */
  //     public function actionPdf($lab_number,$draft)
  //     {
  // //        echo "$draft";
  // //        exit;

  //       Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
  // //      $par=new Parameter;
  // //      $lab_number=Yii::$app->request->post('lab_number');
  // //      $get_lab_number=Yii::$app->request->get('lab_number');
  //         $model=Analisa::findOne(['lab_number'=>$lab_number]);
  //         // return $model->SKmigas();

  // //        print_r($data);
  // //        exit;
  //         //data analisa one
  // //      $dataAnalisa=DataAnalisa::find();
  // //      if(isset($lab_number)){
  // //       $dataAnalisa->where(['lab_number'=>$lab_number]);
  // //     }elseif(isset($get_lab_number)){
  // //      $dataAnalisa->where(['lab_number'=>$get_lab_number]);
  // //    }
  // //    else{
  // //      $dataAnalisa->where(['lab_number'=>'2809/T/17']);
  // //    }
  // //
  // //        //data analisa all
  // //    if(isset($lab_number)){
  // //$alldata=$par->getData($lab_number);
  // //    }elseif(isset($get_lab_number)){
  // //      $alldata=$par->getData($get_lab_number);
  // //    }
  // //    else{
  // //   $alldata=$par->getData('2809/T/17');
  // //    }
  // //
  // //    $data['analisa']=$dataAnalisa->one();
  // //    $data['datas']=$alldata;
  // //        //return var_dump($data);
  //         // get your HTML raw content without any layouts or scripts
  //     $content=$this->renderPartial('_pdf',['model'=>$model,'draft'=>$draft]);
  //     $header = $this->renderPartial('_header');
  //     $footer = $this->renderPartial('_footer');
  //     // setup kartik\mpdf\Pdf component
  //     $pdf = new Pdf([
  //         // set to use core fonts only
  //       'mode' => Pdf::MODE_CORE, 
  //         // A4 paper format
  //       'format' => Pdf::FORMAT_A4, 
  //         // portrait orientation
  //       'orientation' => Pdf::ORIENT_PORTRAIT, 
  //         // stream to browser inline
  //       'destination' => Pdf::DEST_BROWSER, 
  //         // your html content input
  //       'content' => $content,  
  //         // format content from your own css file if needed or use the
  //         // enhanced bootstrap css built by Krajee for mPDF formatting
  //       // 'cssFile' =>'@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
  //       'cssFile'=>'@vendor/bower/bootstrap/dist/css/bootstrap.css',
  // //                   '@app/web/css/pdf.css'],
  //         // any css to be embedded if required
  // //      'cssInline' => '.kv-heading-1{font-size:18px;}.logo1{position:fixed}',
  //          // set mPDF properties on the fly
  //             //'options' => ['title' => 'Krajee Report Title'],
  //          // call mPDF methods on the fly
  //         'options' => [
  //             // 'title' => 'Krajee Report Title'
  //             'watermarkAngle' => 45,
  //             'showWatermarkText' => true,
  //             'watermarkTextAlpha' => 0.1,
  //         ],
  //         'methods' => [
  //             'RoundedRect' => [0, 7, 70, 13, 3],
  // //            'SetWatermarkText' => $trx->draft == 1 ? 'DRAFT' : '',
  //             'SetTitle' => 'Report PDF',
  //             'SetAuthor' => 'PT. Petrolab Services',
  //               //  'SetHeader'=>['Krajee Report Header'],
  //                 'SetHTMLHeader'=>$header,
  //                 'SetHTMLFooter' => $footer,
  //         ]
  //     ]);

  //     // return the pdf output as per the destination setting
  //         $pdf->marginTop = 0.5;
  //         $pdf->marginRight = 5;
  //         $pdf->marginLeft = 5;
  //         $pdf->marginBottom = 1;
  //         $pdf->marginHeader = 1;
  //         $pdf->marginFooter = 0.5;
  //     return $pdf->render(); 
  //   }
  /**
   * Renders the test pdf2
   * @return string
   */
  public function actionPdf2()
  {
    Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
    //       $col=Parameter::find()->all();

    //      die;
    $par = new Parameter;
    $lab_number = Yii::$app->request->post('lab_number');
    $get_lab_number = Yii::$app->request->get('lab_number');

    //data analisa one
    $dataAnalisa = DataAnalisa::find();
    if (isset($lab_number)) {
      $dataAnalisa->where(['lab_number' => $lab_number]);
    } elseif (isset($get_lab_number)) {
      $dataAnalisa->where(['lab_number' => $get_lab_number]);
    } else {
      $dataAnalisa->where(['lab_number' => '2809/T/17']);
    }

    //data analisa all
    if (isset($lab_number)) {
      $alldata = $par->getData($lab_number);
    } elseif (isset($get_lab_number)) {
      $alldata = $par->getData($get_lab_number);
    } else {
      $alldata = $par->getData('2809/T/17');
    }

    $data['analisa'] = $dataAnalisa->one();
    $data['datas'] = $alldata;
    //        return var_dump($data);
    //         get your HTML raw content without any layouts or scripts
    //     $content=$this->renderPartial('fuel2',$data);

    //     // setup kartik\mpdf\Pdf component
    //     $pdf = new Pdf([
    //         // set to use core fonts only
    //       'mode' => Pdf::MODE_CORE, 
    //         // A4 paper format
    //       'format' => Pdf::FORMAT_A4, 
    //         // portrait orientation
    //       'orientation' => Pdf::ORIENT_PORTRAIT, 
    //         // stream to browser inline
    //       'destination' => Pdf::DEST_BROWSER, 
    //         // your html content input
    //       'content' => $content,  
    //         // format content from your own css file if needed or use the
    //         // enhanced bootstrap css built by Krajee for mPDF formatting 
    //       'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
    //         // any css to be embedded if required
    //       'cssInline' => '.kv-heading-1{font-size:18px;}.logo1{position:fixed}', 
    //          // set mPDF properties on the fly
    //             //'options' => ['title' => 'Krajee Report Title'],
    //          // call mPDF methods on the fly
    //       'methods' => [ 
    //                 //'SetHeader'=>['Krajee Report Header'], 
    //                 //'SetFooter'=>['{PAGENO}'],
    //       ]
    //     ]);

    //     // return the pdf output as per the destination setting
    //     return $pdf->render(); 
    // $pdf = new FPDF('P', 'mm', 'Letter');
    // $pdf->AddPage();
    // $pdf->SetFont('Arial','B',16);
    // $pdf->Cell(40,10,'Hello World!');
    // $pdf->Output();
    // $pdf=new fpdf;
    // Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
    // print_r($data);
    // die;
    return  $this->renderPartial('fuel2', $data);
  }
}
