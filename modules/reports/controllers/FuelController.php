<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 2/20/2018
 * Time: 3:03 PM
 */

namespace app\modules\reports\controllers;
use app\assets\UploadBlueimpAsset;
use app\models\DataAnalisa;
use app\models\Excel;
use app\modules\master\models\data\FormDataUpdateStatus;
use app\modules\reports\models\ImportData;
use app\modules\reports\models\search\SearchFuel;
use  app\modules\reports\models\Fuel;
use app\modules\reports\models\UpdateEvalCode;
use app\modules\reports\models\UploadData;
use app\modules\users\models\Customer;
use app\modelsDB\Parameter;
use app\modelsDB\Matrix;
use yii\helpers\Json;
use app\modelsDB\Attention;
use yii\web\Controller;
use yii\data\Pagination;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use Curl\Curl;
use Yii;
use yii\web\Response;
use yii\web\UploadedFile;


class FuelController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SearchFuel();
        $request = Yii::$app->request->queryParams;
        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }
          return $this->render('index');
    }
    public function actionGet(){
        $request = Yii::$app->request;
        $obj = $request->post('obj');
        $value1=$request->post('value1');
        $value = $request->post('value');
        switch ($obj) {
            case 'customer_id':
                $data = Attention::find()->where([$obj => $value])->all();
                $tagOptions = ['prompt' => "Select..."];
                return Html::renderSelectOptions([], ArrayHelper::map($data, 'id', 'name'), $tagOptions);
                break;
            case 'att_id':
                $data =Customer::find()
                     ->where(['customer_id' => $value])->one();
                    echo json::encode($data);
                    return;
               break;
         }
       
    }
    public function actionLab(){
        $model=new Fuel;
            return $model->labNo;
    }
    protected function Lab(){
        $model=new Fuel;
        return $model->labNo;
    }
    public function actionCetan(){
        $density=Yii::$app->request->post('density');
        $des_50=Yii::$app->request->post('dest_50');
        $G=round(((141.5/(($density+0.5)*0.001))-131.5),1);
        $M=(((180*$des_50)+3200)*0.01);
        $G2=pow($G,2);
        $cetan=round((-420.34+(0.016*$G2))+(0.191*($G*log10($M)))+(65.01*(pow(log10($M),2)))-(0.0001809*pow($M,2)),1);
        return $cetan;
    }

    public function actionAdd()
    {
         $model=new SearchFuel();
         $req=Yii::$app->request->queryParams;
         $model->lab_number=$model->labNo;
         if ((Yii::$app->request->isAjax) && ($model->load(Yii::$app->request->post()))) {
                    if ($model->save()){
                        $model->updateCode();
                        return $this->asJson(['status' => 'success',
                                              'message'=>'Save Data'  ]);
                     }else{
                        return $this->asJson(['status'=>'errorr',
                                                'message'=>'Save Data']);
                    }
         }else{
              return $this->renderAjax('_form',['model'=>$model]);
        }
    }

    public function actionEdit($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if ((Yii::$app->request->isAjax) && ($model->load(Yii::$app->request->post()))) {
            if ($model->save()){
                $model->updateCode();
                return $this->asJson(['status' => 'success',
                    'message'=>'Edit Data']);
            }else{
                return $this->asJson(['status'=>'error',
                    'message'=>'Edit Data']);
            }
        }else{
            return $this->renderAjax('_form',['model'=>$model]);
        }
    }

    public function actionCopy($id){
        $model=new SearchFuel();
        $data = $this->findModel($id);
        $model->customer_id=$data->customer_id;
        $model->attention_id=$data->attention_id;
        $model->sludge_content=$data->sludge_content;
        $model->sampling_date=$data->sampling_date;
        $model->received_date=$data->received_date;
        $model->report_date=$data->report_date;
        $model->eng_sn=$data->eng_sn;
        $model->compartment=$data->compartment;
        $model->eng_builder=$data->eng_builder;
        $model->eng_type=$data->eng_type;
        $model->eng_location=$data->eng_location;
        $model->spb=$data->spb;
        $model->po=$data->po;
        $model->harga=$data->harga;

        if ((Yii::$app->request->isAjax) && ($model->load(Yii::$app->request->post()))){
            $lab=$model->lab_number;
            $model->updateCode();
            if ($model->save()){
                    return $this->asJson(['status' => 'success',
                        'message'=>'Save Data']);
                }else{
                    return $this->asJson(['status'=>'error',
                     'message'=>'Save Data']);
                }
            }else{
                return $this->renderAjax('_form',['model'=>$model]);
        }
    }

    public function actionParam()
    {
        $model= new SearchFuel();//::find(['lab_number'])->asArray()->all();
        if (Yii::$app->request->isAjax)
        {
            if ($model->load(Yii::$app->request->post())){
                     $id=Yii::$app->request->post('lab_no');
                    $par = $this->findModel($id);
                    $par->appearance=Yii::$app->request->post('app');
                    $par->density=Yii::$app->request->post('density');
                    $par->api_gravity=Yii::$app->request->post('api');
                    if ($par->save()){
                         return $this->asJson(['success'=>true]);
                    }
            }

            return $this->renderAjax('param',['model'=>$model]);
        }
        
    }
    protected function findModel($id)
    {
        if (($model = SearchFuel::findOne(['lab_number' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpload($id = '')
    {
       $form=new UploadData();
        if (Yii::$app->request->isAjax) {
            $fileUpload = UploadedFile::getInstance($form, 'uploadFile');
            Yii::$app->response->format = Response::FORMAT_JSON;
//            return ['data'=>$fileUpload];
            ini_set('memory_limit', '2048M');
            ini_set('max_execution_time', 3000);
            return [
                'response_type' => $form->upload($fileUpload) ? 'success' : 'error',
                'status_save' => $form->status_save,
                'message' => $form->message,
                'class_message' => $form->class_message,
                'data' => $form->dataMessage,
            ];
        }
        UploadBlueimpAsset::register($this->view);
        return $this->render('upload', ['model' => $form]);
    }



    public function actionEvalCode($lab_no = '')
    {

        $model = UpdateEvalCode::findOne(['lab_number' => $lab_no]);
        if ($model == null) {
            $model = new UpdateEvalCode();
        }
        $request = Yii::$app->request->queryParams;
        $request = array_merge($request, Yii::$app->request->post());
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax) {
            ini_set('memory_limit', '2048M');
            ini_set('max_execution_time', 1000);
            return [
                'status' => $model->evalCode($request) ? 'success' : 'warning',
                'message' => $model->message,
                'class_message' => $model->class_message,
                'data' => $model->data,
            ];
        }
        return '';
    }

    public function actionExcel(){
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', 300);

        $searchModel = new SearchFuel();
        $request = Yii::$app->request->queryParams;
        $query = $searchModel->searchData($request, true, false);
        if ($request == null)
            $query->limit(1000);
        $data = $query->select(['data_analisa.lab_number'=>'data_analisa.lab_number',
//           'data_analisa.lab_number',
            'data_analisa.spb'=>'data_analisa.spb',
            'data_analisa.po'=>'data_analisa.po',
            'data_analisa.Customer_id'=>'data_analisa.Customer_id',
            'customer.name'=>'customer.name',
            'attention.name as attention'=>'attention.name as attention',
            'customer.address'=>'customer.address',
            'data_analisa.eng_builder'=>'data_analisa.eng_builder',
            'data_analisa.eng_type'=> 'data_analisa.eng_type',
            'data_analisa.eng_sn'=>'data_analisa.eng_sn',
            'data_analisa.eng_location'=>'data_analisa.eng_location',
            'data_analisa.compartment as sampling_location'=>'data_analisa.compartment as sampling_location',
            'data_analisa.sludge_content as type'=>'data_analisa.sludge_content as type',
            'date_format(data_analisa.sampling_date,"%d-%m-%Y")as smpl_date'=>'date_format(data_analisa.sampling_date,"%d-%m-%Y")as smpl_date',
            'date_format(data_analisa.received_date,"%d-%m-%Y")as recv_date'=>'date_format(data_analisa.received_date,"%d-%m-%Y")as recv_date',
            'date_format(data_analisa.report_date,"%d-%m-%Y")as rpt_date'=>'date_format(data_analisa.report_date,"%d-%m-%Y")as rpt_date',
            'data_analisa.harga'=>'data_analisa.harga',
            'data_analisa.appearance'=>'data_analisa.appearance',
            'data_analisa.density'=>'data_analisa.density',
            'data_analisa.total_acid_number as TAN'=>'data_analisa.total_acid_number as TAN',
            'data_analisa.flash_point'=>'data_analisa.flash_point',
            'data_analisa.kinematic_viscosity'=>'data_analisa.kinematic_viscosity',
            'data_analisa.water_content'=>'data_analisa.water_content',
            'data_analisa.ash_content'=>'data_analisa.ash_content',
            'data_analisa.pour_point'=>'data_analisa.pour_point',
            'data_analisa.cetane_index'=> 'data_analisa.cetane_index',
            'data_analisa.conradson_carbon'=>'data_analisa.conradson_carbon',
            'data_analisa.distillation_recov'=>'data_analisa.distillation_recov',
            'data_analisa.distillation_ibp'=>'data_analisa.distillation_ibp',
            'data_analisa.distillation_5'=>'data_analisa.distillation_5',
            'data_analisa.distillation_10'=>'data_analisa.distillation_10',
            'data_analisa.distillation_20'=>'data_analisa.distillation_20',
            'data_analisa.distillation_30'=>'data_analisa.distillation_30',
            'data_analisa.distillation_40'=>'data_analisa.distillation_40',
            'data_analisa.distillation_50'=>'data_analisa.distillation_50',
            'data_analisa.distillation_60'=>'data_analisa.distillation_60',
            'data_analisa.distillation_70'=>'data_analisa.distillation_70',
            'data_analisa.distillation_80'=>'data_analisa.distillation_80',
            'data_analisa.distillation_90'=>'data_analisa.distillation_90',
            'data_analisa.distillation_95'=>'data_analisa.distillation_95',
            'data_analisa.distillation_ep'=>'data_analisa.distillation_ep',
            'data_analisa.distillation_recovery'=>'data_analisa.distillation_recovery',
            'data_analisa.distillation_residue'=>'data_analisa.distillation_residue',
            'data_analisa.distillation_loss'=>'data_analisa.distillation_loss',
            'data_analisa.distillation_recovery_300'=>'data_analisa.distillation_recovery_300',
            'data_analisa.sulphur_content'=>'data_analisa.sulphur_content',
            'data_analisa.sediment'=>'data_analisa.sediment',
            'data_analisa.colour'=>'data_analisa.colour',
            'data_analisa.copper_corrosion'=>'data_analisa.copper_corrosion',
            'data_analisa.particles_4'=>'data_analisa.particles_4',
            'data_analisa.particles_6'=>'data_analisa.particles_6',
            'data_analisa.particles_14'=>'data_analisa.particles_14',
            'data_analisa.iso_4406'=>'data_analisa.iso_4406',
            'data_analisa.specific_gravity'=>'data_analisa.specific_gravity',
            'data_analisa.api_gravity'=>'data_analisa.api_gravity',
            'data_analisa.cloudn_point'=>'data_analisa.cloudn_point',
            'data_analisa.rancimat'=>'data_analisa.rancimat',
            'data_analisa.lubricity_of_diesel'=>'data_analisa.lubricity_of_diesel',
            'data_analisa.fame_content'=>'data_analisa.fame_content',
            'data_analisa.calorific_value'=>'data_analisa.calorific_value',
            'data_analisa.strong_acid as SAN'=>'data_analisa.strong_acid as SAN',
            'data_analisa.cerosine_content'=>'data_analisa.cerosine_content',
            'data_analisa.recommended1 as recommended'=>'data_analisa.recommended1 as recommended'
        ])->leftjoin('customer','data_analisa.customer_id=customer.customer_id')->leftjoin('attention','data_analisa.attention_id=attention.id')->distinct()->asArray()->all();
        $e = new Excel();

        $p = $e->genExcelByArray($data, 'A1', 'sheet', true);

        $fileName = 'fuel_export_excel_' . date('d-m-Y_H-i-s');
        $e->download($p, $fileName);

    }

    public function actionUploadData($id='')
    {
//        $url_api='https://pama.petrolab.co.id/api_erp/';
//        $api=[];
//        if(i)
//        $model=DataAnalisa::find()->where("received_date >='2020-03-24' and received_date<='2020-04-05'")
//            ->andWhere("customer_id=7")
//            ->all();
//        foreach ($model as $data){
//            $api['data'][]=$data->toArray();
//        }
//
//        $c=new Curl();
//        $c->setTimeout(10000);
//        $c->post($url_api.'upload-fuel',['all'=>Json::encode($api)]);
//        if ($c->error) {
////            $this->message=$c->response;
//            return false;
//        }
//        if (isset($c->response->sv)) {
////            $this->dataApi = [
////                'save' => ArrayHelper::toArray($c->response->sv),
////            ];
//            print_r($c->response->sv);
//        }
//        exit;
//        return true;
        $st = ImportData::findOne(['lab_number' => $id]);
        if ($st == null) {
            $st = new ImportData();
        }
        if (Yii::$app->request->isAjax) {
            ini_set('max_execution_time', -1);
            ini_set('memory_limit', -1);

            $post = Yii::$app->request->post();
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $st->updateStatus($post) ? 'success' : 'error',
                'message' => $st->message,
               'data' => $st->dataApi,
            ];

        }
    }
}    


    