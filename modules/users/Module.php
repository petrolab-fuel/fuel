<?php

namespace app\modules\users;


//use Yii;
use yii\base\BootstrapInterface;
use yii\web\Application;

/**
 * users module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\users\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

    }


    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param \yii\base\Application $app the application currently running
     */
    public function bootstrap($app)
    {
        if ($app instanceof Application) {
            $app->getUrlManager()->addRules([
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id, 'route' => $this->id . '/default/index'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/add', 'route' => $this->id . '/default/add'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/<username:\w+>', 'route' => $this->id . '/default/profile'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/edit/<username:\w+>', 'route' => $this->id . '/default/edit'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/<controller:[\w\-]+>/<action:[\w\-]+>', 'route' => $this->id . '/<controller>/<action>'],
            ], false);
        }
    }
}
