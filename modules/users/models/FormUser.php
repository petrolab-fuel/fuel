<?php
/**
 * Created by
 * User: Wisard17
 * Date: 20/11/2017
 * Time: 03.35 PM
 */

namespace app\modules\users\models;

/**
 * Class FormUser
 * @package app\modules\users\models
 *
 * @property array $listCustomer
 * @property array $listRule
 */
class FormUser extends User
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', ], 'required'],
            [['status', 'access_rule_id'], 'integer'],
            [['time_create'], 'safe'],
            [['username', 'email'], 'string', 'max' => 100],
            [['password_hash', 'auth_key', 'access_token', 'password_reset_token'], 'string', 'max' => 200],
            [['username'], 'unique'],
            [['access_rule_id'], 'exist', 'skipOnError' => true,
                'targetClass' => AccessRule::className(), 'targetAttribute' => ['access_rule_id' => 'id']],
        ];
    }


    /**
     * @return array
     */
    public function getListRule()
    {
        $out = [];
        $rules = AccessRule::find()->orderBy('id DESC')->all();
        foreach ($rules as $rule) {
            $out[$rule->id] = $rule->role_name;
        }
        return $out;
    }

    /**
     * @param bool $insert
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->generateAuntKey();
            $current = self::findOne($this->index);
            if ($this->password_hash != $current->password_hash)
                $this->setPassword($this->password_hash);
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    public function getListCustomer()
    {
        $out = [];
        $customers = Customer::find()->orderBy('name ASC')->all();
        foreach ($customers as $customer) {
            $out[$customer->customer_id] = $customer->name;
        }

        return $out;
    }

    public $_customerId;

    public function save($runValidation = true, $attributeNames = null)
    {
        if (parent::save($runValidation, $attributeNames)) {
            if ($this->_customerId != null) {
                $listC = Customer::find()->where(['users_index' => $this->index])->all();
                foreach ($listC as $customer) {
                    $customer->users_index = null;
                    $customer->save();
                }
                $c = Customer::findOne(['customer_id' => $this->_customerId]);
                $c->save();
            }
            return true;
        }
        return false;
    }

    /**
     * @param mixed $customerId
     */
    public function setCustomerId($customerId)
    {
        $this->_customerId = $customerId;
    }
}