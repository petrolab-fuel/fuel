<?php
/**
 * Created by
 * User: Wisard17
 * Date: 17/11/2017
 * Time: 09.19 AM
 */

namespace app\modules\users\models;


use yii\helpers\Html;
use yii\helpers\Url;


/**
 * Class User
 * @package app\modules\users\models
 *
 * @property string $action
 */
class User extends \app\models\User
{

    /**
     * @return string
     */
    public function getAction()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => "btn btn-default dropdown-toggle", 'data-toggle' => "dropdown", 'aria-expanded' => "false"
            ]) . Html::tag('ul',
                Html::tag('li', Html::tag('a', '<i class="fa fa-caret-square-o-right"></i> view', ['href' => Url::toRoute(['/users/default/profile', 'username' => $this->username])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-print"></i> Print', ['href' => Url::toRoute(['/users/add/print', 'username' => $this->username]), 'target' => '_blank'])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-edit"></i> Edit', ['href' => Url::toRoute(['/users/default/edit', 'username' => $this->username])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-window-close"></i> Delete',
                    ['href' => Url::toRoute(['/users/add/del', 'username' => $this->username]),
                        'data-method' => 'post',
                        'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
                    ]))
                , ['class' => "dropdown-menu"]), ['class' => "btn-group", 'style' => 'overflow: visible;']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customer::className(), ['users_index' => 'index']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLogs()
    {
        return $this->hasMany(UserLog::className(), ['users_index' => 'index']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccessRule()
    {
        return $this->hasOne(AccessRule::className(), ['id' => 'access_rule_id']);
    }
}