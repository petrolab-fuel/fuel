<?php
/**
 * Created by
 * User: Wisard17
 * Date: 10/16/2017
 * Time: 9:59 AM
 */

use app\smartadmin\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\users\models\FormUser */
/* @var $form ActiveForm */
?>
<div class="add-_form">

    <?php $form = ActiveForm::begin([
        'id' => 'package-form',
//        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}<label class='input'>{input}</label>{error}",
//            'labelOptions' => ['class' => 'label  '],
            'inputOptions' => ['class' => 'form-control'],
        ],
        'options' => [
            'class' => 'smart-form',
        ],
        'enableAjaxValidation' => Yii::$app->request->isAjax,

    ]); ?>

    <header>
        Lengkapi field di bawah ini.
    </header>

    <fieldset>
        <div class="row">
            <section class="col col-6">
                <?= $form->field($model, 'username') ?>
            </section>
            <section class="col col-6">
                <div class="ipt"><?= $form->field($model, 'password_hash')->passwordInput() ?></div>
                <div class="btn btn-sm btn-default bbt" style="margin-top: 25px"><a onclick="showPass()">Change Password</a></div>
            </section>
        </div>

    </fieldset>

    <fieldset>
        <div class="row">
            <section class="col col-6">
                <?= $form->field($model, 'email') ?>
            </section>
        </div>
    </fieldset>

    <fieldset>
        <div class="row">
            <section class="col col-6">
                <?= $form->field($model, 'access_rule_id')->dropDownList($model->listRule) ?>
            </section>
        </div>
    </fieldset>

    <fieldset>
        <div class="row">
            <section class="col col-6">
                <?= $form->field($model, 'customerId')->dropDownList($model->listCustomer, ['prompt' => 'choose']) ?>
            </section>
        </div>
    </fieldset>

    <footer>
        <?= Html::submitButton(Yii::t('app', $model->isNewRecord ? 'Submit ' : 'Save '),
            ['class' => 'btn btn-primary']) ?>
    </footer>

    <?php ActiveForm::end(); ?>

</div>

<?php

$new = $model->isNewRecord ? 'y' : 'n';
$script = <<< JS
var view = $('.add-_form');

if ('$new' === 'y')
    view.find('.bbt').hide(300);
else 
    view.find('.ipt').hide(300);
function showPass() {
     view.find('.ipt').show(300);
     view.find('.bbt').hide(300);
};
JS;

$this->registerJs($script, 3, 'form-user');