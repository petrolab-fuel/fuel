<?php

namespace app\modules\users\controllers;

use app\modules\users\models\FormUser;
use app\modules\users\models\RuleAccess;
use app\modules\users\models\search\SearchUser;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Default controller for the `users` module
 */
class DefaultController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['detail', 'index', 'profile', 'edit'],
                'rules' => [
                    [
                        'actions' => ['detail', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['profile', 'edit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SearchUser();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }

        return $this->render('index');
    }

    /**
     * @param $username
     * @return string
     * @throws NotAcceptableHttpException
     * @throws NotFoundHttpException
     */
    public function actionProfile($username)
    {
        $model = $this->findModel($username);

        return $this->render('profile', [
            'model' => $model
        ]);
    }

    /**
     * @return array|string|Response
     *
     */
    public function actionAdd()
    {
        $model = new FormUser();

        $post = Yii::$app->request->post();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }

        if ($model->load($post)) {
            if ($model->save()) {
                return $this->redirect(Url::toRoute(['/users/default/profile', 'username' => $model->username]));
            }
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }

    /**
     * @param $username
     * @return array|string|Response
     * @throws NotAcceptableHttpException
     * @throws NotFoundHttpException
     */
    public function actionEdit($username)
    {
        $model = $this->findModel($username);


        $post = Yii::$app->request->post();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load($post)) {
            if ($model->save()) {
                return $this->redirect(Url::toRoute(['/users/default/profile', 'username' => $model->username]));
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('edit', [
                'model' => $model,
            ]);
        }

        return $this->render('edit', [
            'model' => $model
        ]);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $username
     * @return FormUser the loaded model
     * @throws NotFoundHttpException|NotAcceptableHttpException if the model cannot be found
     */
    protected function findModel($username)
    {
        if (($model = FormUser::findOne(['Username' => $username])) !== null) {
            if (!RuleAccess::accessSelfProfile($username))
                throw new NotAcceptableHttpException('The requested page not allowed.');
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
