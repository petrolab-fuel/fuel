<?php

namespace app\modules\master;

/**
 * master module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\master\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->modules=[
            'customers'=>[
                'class'=>'app\modules\master\customers\Module',
                ],
            'units'=>[
                'class'=>'app\modules\master\units\Module',
            ],
        ];

        // custom initialization code goes here
    }
}
