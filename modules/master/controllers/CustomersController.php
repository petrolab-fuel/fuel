<?php
/**
 * Created by PhpStorm.
 * User: Feri
 * Date: 10/23/2019
 * Time: 11:59
 */

namespace app\modules\master\controllers;

use app\modules\master\models\FormCustomers;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\widgets\ActiveForm;

class CustomersController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['detail', 'index', 'add', 'edit'],
                'rules' => [
                    [
                        'actions' => ['detail', 'index', 'add', 'edit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'del' => ['post'],
                ],
            ],
        ];
    }
    public function actionAdd(){
        $model=new FormCustomers();
        $post=Yii::$app->request->post();

        if(Yii::$app->request->isAjax && $model->load($post)){
            Yii::$app->response->format=Response::FORMAT_JSON;
                if($model->save()){
                    return ['data'=>'sucess'];
                }else{
                    return ['data'=>$model->getErrors()];
                }

            }
                return $this->render('create',['model'=>$model,]);
    }
}