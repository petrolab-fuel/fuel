<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 3/12/2018
 * Time: 10:02 AM
 */

namespace app\modules\master\controllers;
use app\modelsDB\Attention;
use app\modelsDB\Customer;
use yii\web\Controller;
use yii\helpers\Json;
use Yii;
class CustomerController extends Controller
{
    public function actionIndex(){
        return $this->render ('index');
    }

    public function actionViews(){
       \Yii::$app->response->format=\Yii\web\Response::FORMAT_JSON;
                $model = (new \yii\db\Query())
                ->select('customer.customer_id,customer.name,customer.address,attention.name as att')
                ->from('customer')
                ->leftJoin('attention','attention.customer_id=customer.customer_id');
                $comand=$model->createCommand();
                $data=$comand->queryAll();
                return  ['data'=>$data];

    }

    public function actionAdd(){
        \Yii::$app->response->format=\Yii\web\Response::FORMAT_JSON;
        $j=0;
        $model=new Customer();
        $model->name=strtoupper(Yii::$app->request->post('nama'));
        $model->address=strtoupper(Yii::$app->request->post('address'));
        $nm=Yii::$app->request->post('nm');
        $save=$model->save();
        if($save){
            $id=$model->customer_id;
            foreach($nm as $key){
                $att=new Attention();
                $att->customer_id=$id;
                $att->name=strtoupper($nm[$j]);
                $att->save();
                $j++;
            }

            return ['status'=>'success','message'=>'success'];
        }else{
            return ['status'=>'error','message'=>'error'];
        }
    }

    public function actionEdit(){

                $id=Yii::$app->request->post('id');
                $model=Customer::find()
                ->where(['customer_id'=>$id])
                ->one();
            echo Json::encode($model);
    }



}

