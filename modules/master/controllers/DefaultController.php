<?php

namespace app\modules\master\controllers;
use app\assets\UploadBlueimpAsset;
use yii\web\Controller;
use app\modules\master\models\data\search\SearchAnalisa;
use app\modules\master\models\data\FormAnalisa;
use app\modules\master\models\data\UpdateCode;
use app\modules\master\models\data\UpdateStatus;
use app\modules\master\models\data\Matrixs;
use app\modules\master\models\data\FormExcel;
use app\modules\master\models\data\UploadTransaksi;
use yii\web\UploadedFile;
use app\modelsDB\CustomersType;
use yii\web\Response;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use app\component\ArrayHelper;

use yii\helpers\Json;

class DefaultController extends Controller
{
    public function actionIndex()

    {

        $searchModel = new SearchAnalisa();
        $request = Yii::$app->request->queryParams;
        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
//            return $request;
        }

        return $this->render('index');
    }

    public function actionNew()
    {
        $model=new FormAnalisa();
        $post=Yii::$app->request->post();
        $model->lab_number=$model->labNo;

        if(Yii::$app->request->isAjax && isset($post['id'])){
            $model->type_report_id=(int)$post['id'];
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $this->renderAjax('_detail',['model'=>$model]);
        }
        if ($model->load($post) && $model->save()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => 'success',
                'labNo' => $model->labNo,
                'data_row' => $model->toArray(),
                'update_code' => $model->UpdateCode(),
            ];
        }
        return $this->render('create',['model'=>$model]);
//        $request = Yii::$app->request->queryParams;
//
////        $parameter = $this->getParameter();
//        $model = new Analisa();
//        $model->lab_number = $model->labNo;
//        $edit = false;
//        return $this->render('create', ['model' => $model, 'parameter' => $parameter, 'edit' => $edit]);
    }
    public function actionEdit($id)
    {
        $model = FormAnalisa::findOne(['id' => $id]);

        $post = Yii::$app->request->post();
        if(Yii::$app->request->isAjax && isset($post['id'])){
            $model->type_report_id=(int)$post['id'];
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $this->renderAjax('_detail',['model'=>$model]);
        }
        if ($model->load($post) && $model->save()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => 'success',
                'labNo' => $model->labNo,
                'data_row' => $model->toArray(),
                'update_code' => $model->UpdateCode(),
            ];
        }
        return $this->render('edit', ['model' => $model]);
    }

    protected function getParameter($type = 'Fuel')
    {
        $model = Matrixs::find()->joinWith(['parameters'])
            ->where("matrix.jenis='$type'")
            ->all();
        return $model;
    }

    public function actionSave()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $post = Yii::$app->request->post();
        $lab = '';
        if (isset($post['FormAnalisa']['lab_number'])) {
            $lab = $post['FormAnalisa']['lab_number'];
        }
        $model = FormAnalisa::find()->where("lab_number='$lab'")->one();
        if ($model == null) {
            $model = new FormAnalisa();
        }
        if ($model->load($post) && $model->save()) {
            return [
                'status' => 'success',
                'labNo' => $model->labNo,
                'data_row' => $model->toArray(),
                'update_code' => $model->UpdateCode(),
            ];
        }
        return ['status' => 'error', 'message' => ArrayHelper::toString($model->errors)];
    }




    protected function findName($lab_number)
    {
        $a = Analisa::find()->where("lab_number='$lab_number'")->one();
        return $a == null ? '' : $a->customerName;
    }

    public function actionSearch()
    {
        $request = Yii::$app->request->get();
      
        $search = isset($request['search']) ? $request['search'] : '';
        $data = Analisa::find()->where("lab_number='$search'")->one();
        Yii::$app->response->format = Response::FORMAT_JSON;
        // return  $request;
        if ($data == null) {
            return [
                'status' => 'error',
                'data' => null,
                'parameter' => '',
                'message' => 'Data Not Found',
            ];
        }
        return [
            'status' => 'success',
            'data' => $data->toArray(),
            'parameter' => $data->colAnalisis,
            'message' => 'This data Found',
        ];
    }

    public function actionGetLabnumber()
    {

        Yii::$app->response->format = Response::FORMAT_JSON;
        $a = (new Analisa())->labNo;
        return [
            'labno' => $a,
        ];
    }

    public function actionUpdateCode($lab_no = '')
    {

        $model = UpdateCode::find()->where(['lab_number' => $lab_no])->one();
        if ($model == null) {
            $model = new UpdateCode();
        }
        $request = Yii::$app->request->queryParams;
        $request = array_merge($request, Yii::$app->request->post());
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            ini_set('memory_limit', '2048M');
            ini_set('max_execution_time', 1000);
            return [
                'status' => $model->evalCode($request) ? 'success' : 'warning',
                'message' => $model->message,
                // 'class_message' => $model->class_message,
                'data' => $model->data,
            ];
        }
        return '';
    }

    public function actionUpdateStatus($lab_no = '')
    {
        $model = UpdateStatus::findOne(['lab_number' => $lab_no]);
        if ($model == null) {
            $model = new UpdateStatus();
        }
        $request = Yii::$app->request->queryParams;
        $request = array_merge($request, Yii::$app->request->post());
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax) {
            ini_set('memory_limit', '2048M');
            ini_set('max_execution_time', 1000);
            return [
                'status' => $model->updateStatus($request) ? 'success' : 'warning',
                'message' => $model->message,
                // 'class_message' => $model->class_message,
                'data' => $model->data,
            ];
        }
        return '';
    }

    public function actionDownloadExcel($csv = false)
    {
        $request = Yii::$app->request->queryParams;
        $model = new FormExcel();

        ini_set('max_execution_time', -1);
        ini_set('memory_limit', -1);
        header('Set-Cookie: fileDownload=true; path=/');
        return $model->generateExcel($request, $csv);
    }

    public function actionCustomerType()
    {
        $out = [];
        $q = isset($_GET['q']) ? $_GET['q'] : '';
        Yii::$app->response->format = Response::FORMAT_JSON;
        $query=CustomersType::find()->limit(10);
        $query->where("description like '%$q%'");
        if($query->count()>0)
        {
            foreach($query->all() as $record)
            {
                $out[]=[
                    'id'=>$record->id,
                    'text'=>$record->description,
                ];
            }
        }else{
            $out[]=[
                'id'=>0,
                'text'=>'data not found',
            ];
        
        }
        return [
            'results' => $out,
        ];
    }

    public function actionUploadData(){
        $form = new UploadTransaksi();
        if (Yii::$app->request->isAjax) {
            $fileUpload = UploadedFile::getInstance($form, 'uploadFile');
            Yii::$app->response->format = Response::FORMAT_JSON;
            ini_set('memory_limit', '2048M');
            ini_set('max_execution_time', 3000);
            return [
                'response_type' => $form->upload($fileUpload) ? 'success' : 'error',
                'status_save' => $form->status_save,
                'message' => $form->message,
                'class_message' => $form->class_message,
                'data' => $form->dataMessage,
            ];
        }
        UploadBlueimpAsset::register($this->view);
        return $this->render('upload_data', ['model' => $form]);
    }

    /**
     * @return float
     */
    public function actionCetan(){
        $density=Yii::$app->request->post('density');
        $des_50=Yii::$app->request->post('destilasi');
        $G=round(((141.5/(($density+0.5)*0.001))-131.5),1);
        $M=(((180*$des_50)+3200)*0.01);
        $G2=pow($G,2);
        $cetan=round((-420.34+(0.016*$G2))+(0.191*($G*log10($M)))+(65.01*(pow(log10($M),2)))-(0.0001809*pow($M,2)),1);
        return $cetan;
    }

//    public function actionRenderReport(){
//
//    }

}
