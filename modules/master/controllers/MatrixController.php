<?php

namespace app\modules\master\controllers;

use yii\web\Controller;
use app\modules\master\models\data\search\SearchMatrix;
use app\modules\master\models\data\FormMatrix;
use Yii;
use yii\helpers\Url;
use yii\web\Response;

class MatrixController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new FormMatrix();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->renderAjax($request);
        }
        return $this->render('index');
    }

    public function actionNew()
    {
        $model = new FormMatrix();
        $post=Yii::$app->request->post();
        if(Yii::$app->request->isAjax && isset($post['new_row'])){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model->renderAjaxParameterMatrix($post['new_row']);
        }
        if($model->load($post)){
            if($model->save()){
                return $this->redirect(Url::to(['/master/matrix']));
            }else{
                print_r($model->errors);
                exit;
            }
        }
        return $this->render('create',['model'=>$model]);
    }

    public function actionEdit($id)
    {
        $model = FormMatrix::findOne(['id' => $id]);
        $post=Yii::$app->request->post();
        if(Yii::$app->request->isAjax && isset($post['new_row'])){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model->renderAjaxParameterMatrix($post['new_row']);
        }
        if($model->load($post)&& $model->save()){
            return $this->redirect(Url::to(['/master/matrix']));
        }
        return $this->render('edit',['model'=>$model]);
    }

    public function actionUpdateSkMigas()
    {
        $post = Yii::$app->request->post();
        $dir = Yii::getAlias('@app/web');
        $file = $dir . '/img/dirjen.json';
        if (isset($post['post'])) {
            $data = ['sk' => $post['post']];
            $json = json_encode($data);
            $byte = file_put_contents($file, $json);
            if ($byte) {
                return json_encode(['status' => 'success']);
            } else {
                return json_encode(['status' => 'error']);
            }
        } else {
            $data = file_get_contents($file);
            return $data;
        }
    }
}
