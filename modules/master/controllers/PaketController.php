<?php
/**
 * PaketController.php
 * Created By
 * feri_
 * 30/01/2023
 */

namespace app\modules\master\controllers;
use app\modules\master\models\data\FormTypeReport;
use Yii;
use yii\web\Response;
use yii\helpers\Url;
class PaketController extends \yii\web\Controller
{
    public function actionIndex(){
        $params=Yii::$app->request->queryParams;
        $model=new FormTypeReport();
        if(Yii::$app->request->isAjax){
            return $model->renderAjax($params);
        }
        return $this->render('index',['model'=>$model]);
    }
    public function actionNew(){
        $model=new FormTypeReport();
        $post=Yii::$app->request->post();
        if(Yii::$app->request->isAjax && isset($post['new_row'])){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model->renderAjaxTypeParameter($post['new_row']);
        }
        if($model->load($post)&& $model->save()){
            return $this->redirect(Url::to(['/master/paket']));
        }
        return $this->render('create',['model'=>$model]);

    }

    public function actionEdit($id){
        $model=FormTypeReport::find()->where(['id'=>$id])->one();
        $post=Yii::$app->request->post();
        if(Yii::$app->request->isAjax && isset($post['new_row'])){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model->renderAjaxTypeParameter($post['new_row']);
        }
        if($model->load($post)&& $model->save()){
            return $this->redirect(Url::to(['/master/paket']));
        }
        return $this->render('edit',['model'=>$model]);
    }
}