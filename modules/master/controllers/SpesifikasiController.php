<?php
    namespace app\modules\master\controllers;

    use app\modelsDB\Spesifikasi;
    use yii\web\Controller;
    use app\modules\master\models\data\FormSpesifikasi;
    use yii\helpers\Url;
    use Yii;
    use yii\web\Response;

    class SpesifikasiController extends Controller
    {
        public function actionIndex(){
            $model=new FormSpesifikasi();
            $params=Yii::$app->request->queryParams;
            if(Yii::$app->request->isAjax){
                return $model->renderAjax($params);
            }
            return $this->render('index',['model'=>$model]);
        }

        public function actionNew(){
            $model=new FormSpesifikasi();
            $post=Yii::$app->request->post();
            if(Yii::$app->request->isAjax && isset($post['new_row'])){
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $model->renderAjaxSpecParameteri($post['new_row']);
            }
            if($model->load($post)&& $model->save()){
                return $this->redirect(Url::to(['/master/spesifikasi']));
            }
            return $this->render('create',['model'=>$model]);

        }

        public function actionEdit($id){
            $model=FormSpesifikasi::find()->where(['id'=>$id])->one();
            $post=Yii::$app->request->post();
            if(Yii::$app->request->isAjax && isset($post['new_row'])){
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $model->renderAjaxSpecParameteri($post['new_row']);
            }
            if($model->load($post)&& $model->save()){
                return $this->redirect(Url::to(['/master/spesifikasi']));
            }
            return $this->render('edit',['model'=>$model]);
        }
    }
?>