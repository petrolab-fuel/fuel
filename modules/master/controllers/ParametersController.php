<?php

namespace app\modules\master\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use app\modules\master\models\data\FormParameters;
use app\modules\master\models\data\search\SearchParameters;
use yii\web\Response;

class ParametersController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new SearchParameters();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }

        return $this->render('index');
    }

    public function actionAdd()
    {
        $model = new FormParameters();
        $post=Yii::$app->request->post();
        if(Yii::$app->request->isAjax && isset($post['new_row'])){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model->renderAjaxChild($post['new_row']);
        }
        if($model->load($post)&& $model->save()){
            return $this->redirect(Url::to(['/master/parameters']));
        }
        return $this->render('create',['model'=>$model]);
    }

    public function actionEdit($id)
    {
        $model = FormParameters::findOne(['id' => $id]);
        $post = Yii::$app->request->post();
        if(Yii::$app->request->isAjax && isset($post['new_row'])){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model->renderAjaxChild($post['new_row']);
        }
        if($model->load($post)&& $model->save()){
            return $this->redirect(Url::to(['/master/parameters']));
        }
        return $this->render('edit', ['model' => $model]);
    }

    public function actionParameterList()
    {
        $get = Yii::$app->request->get();

        Yii::$app->response->format = Response::FORMAT_JSON;
        // return $get;
        $out = [];
        $data = FormParameters::find()->limit(30)->all();
        if (isset($get['q'])) {
            $q = $get['q'];
            $data = FormParameters::find()->where("nama like '%$q%'")->limit(30)->all();
            $data == null ? [] : $data;
        }
        foreach ($data as $col) {
            $out[] = [
                'id' => $col->id,
                'text' => $col->nama,
            ];
        }
        return [
            'results' => $out,
        ];
    }

}
