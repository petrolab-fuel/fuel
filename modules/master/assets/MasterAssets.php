<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 3/12/2018
 * Time: 9:51 AM
 */

namespace app\modules\master\assets;


use yii\web\AssetBundle;

class MasterAssets extends AssetBundle
{
    public $sourcePath = '@app/modules/master/assets/app';
        public $css = [

        ];
    public $js = [
        'app.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}