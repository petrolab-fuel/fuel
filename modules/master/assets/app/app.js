//datatables
var responsiveHelper_dt_basic = undefined;
var responsiveHelper_datatable_fixed_column = undefined;
var responsiveHelper_datatable_col_reorder = undefined;
var responsiveHelper_datatable_tabletools = undefined;
var status;
var urut=1;
var breakpointDefinition = {
    tablet : 1024,
    phone : 480
};
/* COLUMN SHOW - HIDE */
tb=$('#table_customer').dataTable({
    "scrollX": true,
    "searching":true,
    "searchDelay":1000,
    "ajax": {
        "url": base_url+"/master/customer/views",
        'type':'get'
    },
    "columnDefs": [
        /*	{ "width": "20px", "targets": 0 },*/
    ],
    columns:[
        {
            "className":"aaa",
            "orderable":false,
            "data":"id_send_email_status",
            "render":function(data,type,full,meta){return '<button class="btn btn-xs btn-danger" alert="edit" onclick="edit_customer('+full.customer_id+');"><i class="fa fa-edit"></i> </button>'+
                 '<button class="btn btn-xs btn-danger" onclick="delete_customer('+full.customer_id+');"><i class="fa fa-trash-o"></i> </button>';
            }
        },
        {data:"customer_id"},
        {data:"name"},
        {data:"address"},
        {data:"att"},
          ],
    select:true,
    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
    "t"+
    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
    "autoWidth" : true,
    "oLanguage": {
        "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
    },

    "preDrawCallback" : function() {
        // Initialize the responsive datatables helper once.
        if (!responsiveHelper_datatable_col_reorder) {
            responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#table_customer'), breakpointDefinition);
        }
    },
    "rowCallback" : function(nRow) {
        responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
    },
    "drawCallback" : function(oSettings) {
        responsiveHelper_datatable_col_reorder.respond();
    }

});

/* END COLUMN SHOW - HIDE */

function alert_success(res){
    $.sound_on = false;
    /*$.sound_path = '/sound';*/
    $.smallBox({
        title : res.status,
        content : res.message,
        color : "#659265",
        iconSmall : "fa fa-check fa-2x fadeInRight animated",
        timeout : 4000
    });
}
//mesage box success
function alert_error(){
    $.sound_on = false;
    /*$.sound_path ='sound/';*/
    $.smallBox({
        title : "Error",
        content : "found error in proccess",
        color : "#C46A69",
        iconSmall : "fa fa-times fa-2x fadeInRight animated",
        timeout : 4000
    });
}

$('#btn_add').click(function (e) {
    e.preventDefault();
    status='new';
    $('#myModal').modal('show');
})
function edit_customer(id){
    status='edit';
    $.ajax({
        type:'POST',
        url: base_url + '/master/customer/edit',
        dataType:'JSON',
        data:'id='+id,
        success:function (res) {
            $('#nama').val(res.name);
            $('#address').val(res.address);
            $('#myModal').modal('show');
        }
    })

}
function delete_customer(id){
    alert(id);
}

$('#add_att').click(function (e) {
    e.preventDefault();
    html='<tr>'
        + '<td> <button type="button" id="del_att" name="del_att" class="btn btn-primary btn-xs">Del</button></td>'
        + '<td><input type="text" class="input-xs" style="width: 95%" id="nm" name="nm[]"></td>'
        + '</tr>';
    $('#tbl tbody').append(html);
    urut++;
    $('#urut').val(urut);
})

$(document).on('click', '#del_att', function(e){
    e.preventDefault();
        $(this).parent().parent().remove();
    })

$("#frm").on('submit',function (e) {
    e.preventDefault();
    var data=$('#frm').serialize();
    var nama=$('#nama').val();
    var address=$('#address').val();
    var nm =$('#nm').val();
    var url;
    if (status=='edit'){
        url=base_url + '/master/customer/update';
    }else{
        url=base_url + '/master/customer/add';
    }
    $.ajax({
        type: 'POST',
        url: url,
        dataType: 'JSON',
        data:data
    })    
        .done(function (res) {
            if(res.status=='success'){
                alert_success(res);
            }else{
                alert_error();
            }
            clearForm();
            tb.api().ajax.reload();
        })
        .fail(function() {
            alert_error();
        })
})

function clearForm(){
    $('#nama').val("");
    $('#address').val("");
}