<?php
/**
 * Created by PhpStorm.
 * User: MY PC
 * Date: 2/2/2018
 * Time: 11:37 AM
 */

namespace app\modules\master\customers;


class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\master\customers\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

}