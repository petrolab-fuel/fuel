<?php

use app\smartadmin\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use conquer\select2;

/* @var $this yii\web\View */
/* @var $model app\modules\master\customers\models\Customers */
/* @var $form ActiveForm */
?>
<div class="add-_form">
    
    <?php $form = ActiveForm::begin([

        'fieldConfig' => [
            'template' => "{label}<label class='input'>{input}</label>{error}",
            'inputOptions' => ['class' => 'form-control'],
        ],
        'options' => [
            'class' => 'smart-form',
            'id'=>'form-customer',
        ],
    ]);
    // Html::tag('td', $form->field($cust, 'name', ['template' => '<label class="input">{input}</label>{error}']));
    ?>

    <header>
        Lengkapi field di bawah ini.
    </header>

    <fieldset>
        <div class="row">
            <section>
              
                <?= $form->field($cust, 'name') ?>
            </section>
            <section>
                <?= $form->field($cust, 'address')->textarea(['rows'=>'3']) ?>
            </section>
        </div>
    </fieldset>

    <fieldset>
        <div class="form-group">
            <label class="col-lg-2 control-label">Search Attention <sup>*</sup></label>
            <div class="col-lg-8">
                <?= Html::input('text','searchtext','',['id'=>'searchtext','class'=>'form-control','placeholder'=>'Attention Name']) ?>

            </div>
            <div class="col-lg-2">
                <?= Html::button('Search',['class'=>'btn btn-info btn-sm','id'=>'cari']) ?>
            </div>
        </div>
    </fieldset>
    <fieldset>
            <div class="row">
            <section class="col col-12">
               <?= $cust->renderTable($form)?>
                <a data-action="add_row_table" class="btn btn-primary btn-xs btn-block">
            <i class="fa fa-plus-circle"></i> Add Attention
        </a>
            </section>

        </div>
    </fieldset>



    <footer>
        <?= Html::submitButton(Yii::t('app', $cust->isNewRecord ? 'Submit ' : 'Save '),
            ['class' => 'btn btn-primary']) ?>

        <?= \app\smartadmin\helpers\Html::a('<i class="fa fa-back"></i>
             Cancel', Url::toRoute(['/master/customers']), ['class' => 'btn btn-primary']) ?>
    </footer>

    <?php ActiveForm::end(); ?>

</div>
<?php
$id=$cust->customer_id;
$jsScript = <<< JS
$(function(){
    function alert_notif(res) {
        $.sound_on = false;
        var color = res.status === 'success' ? '#659265' : '#C46A69';
        var icon = res.status === 'success' ? 'fa-check' : 'fa-times';
        $.smallBox({
            title: res.status,
            content: res.message,
            color: color,
            iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
            timeout: 4000
        });
    } ;
   var form=$('#form-customer');
   form.delegate('[data-action=add_row_table]','click',function() {
     var elm = $(this);
     var tbl = elm.closest('fieldset').find('table');
     var form = elm.closest('form');
        $.ajax({
            // url: '',
            method: 'post',
            data: {'reg_new_row': (tbl.find('tr').length - 1)},
            
            error: function (response) {
                alert_notif({status: "error", message: 'tidak bisa save'});
            },
            success: function (response) {
               console.log(response);
                var t = $(response.data);
                if ($.fn.select2 !== undefined)
                    t.find('select').select2({"minimumInputLength": 2});
                tbl.find('tbody').append(t);
                // $(form).yiiActiveForm(response.jsAdd, []);
                $.each(response.jsAdd, function (i, v) {
                    var vali = eval("var f = function(){ return " + v.validate.expression + ";}; f() ;");
                    v.validate = vali;
                     $(form).yiiActiveForm('add', v);
                });
            }
        });
   })  
    form.delegate('[data-action=delate_row]', 'click', function () {
        var elm = $(this);
        var row = elm.closest('tr');
        p = '$cust->attForm';
        p = p.replace('[new]', '[' + row.attr('data-id') + ']');

        row.html('<input type="hidden" name="' + p + '[actionId]" value="del" >');

    });
   form.delegate('#cari','click',function(e) {
     e.preventDefault();
        // console.log(form.find('#searchtext').val());
        var text=form.find('#searchtext').val();
        $.ajax({
            method:'post',
            url:base_url+'/master/customers/add/search',
            data:{'id':'$id','searchtext':text},
            error:function() {
              
            },
            success:function(response) {
                
               alert_notif({status: response.status, message: response.message});
              console.log(response);
            }
        });
   })
});
JS;
$this->registerJs($jsScript, 4, 'formcustomer');
?>


