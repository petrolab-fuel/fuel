<?php
/**
 * Created by
 * User: Wisard17
 * Date: 10/16/2017
 * Time: 1:48 PM
 */


/**
 * @var $model \app\modules\customer\models\Customer
 * @var $this yii\web\View
 *
 */


use yii\helpers\Url;

$this->title = Yii::t('app', ' ' . $cust->name . '');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Customer'), 'url' => Url::toRoute(['/master/customers'])];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">

    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-6">
        <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-user-circle "></i>
            <?= $this->title ?>
        </h1>
    </div>


    <!-- right side of the page with the sparkline graphs -->
    <!-- col -->
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-6">
        <!-- sparks -->
        <ul id="sparks">
            <li class="sparks-info .bg-color-teal">
                <?= \app\smartadmin\helpers\Html::a('<i class="fa fa-plus-circle"></i>
                    Add', \yii\helpers\Url::toRoute(['/master/customers/add']), ['class' => 'btn btn-primary'])?>
            </li>
        </ul>
        <!-- end sparks -->
    </div>
    <!-- end col -->

</div>

<section id="widget-grid" class="well well-light well-sm no-margin no-padding">

    <!-- row -->
    <div class="row">



        <div class="col-sm-12">

            <div class="row">

                <div class="col-sm-3 profile-pic">

                    
                </div>
                <div class="col-sm-6">
                    <h1> <span class="semi-bold"><?= $cust->name?></span>
                        <br>
                        <small>  <?= $cust->name?></small></h1>

                    <ul class="list-unstyled">
                        
                         <li>
                            <p class="text-muted">
                                <i class="fa fa-home"></i>&nbsp;Address :&nbsp;<?= $cust->address?>
                            </p>
                        </li>

                    </ul>
                    <br>
                    <a href="javascript:void(0);" class="btn btn-default btn-xs"><i class="fa fa-envelope-o"></i> Send Message</a>
                    <br>
                    <br>

                </div>
                <div class="col-sm-3">
                    <h1><small>Contact</small></h1>
                   <ul class="list-inline friends-list">
                        <?= $cust->contact ?>

<!--                        
                            <li><img src="img/avatars/1.png" alt="friend-1">-->
<!--                        </li>-->
<!--                        <li><img src="img/avatars/2.png" alt="friend-2">-->
<!--                        </li>-->
<!--                        <li><img src="img/avatars/3.png" alt="friend-3">-->
<!--                        </li>-->
<!--                        <li><img src="img/avatars/4.png" alt="friend-4">-->
<!--                        </li>-->
<!--                        <li><img src="img/avatars/5.png" alt="friend-5">-->
<!--                        </li>-->
<!--                        <li><img src="img/avatars/male.png" alt="friend-6">-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="javascript:void(0);">413 more</a>-->
<!--                        </li>-->
                    </ul>



                </div>

            </div>
            <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-sm-8 ">

                    <div class="btn-group">
                        <a href="<?= Url::toRoute(['/master/customers/add/edit', 'id' => $cust->customer_id]) ?>"
                           class="btn btn-sm btn-primary"> <i class="fa fa-edit"></i> Edit </a>
                    </div>

                    <div class="btn-group">
                        <a href="<?= Url::toRoute(['/master/customers/add/del', 'id' => $cust->customer_id]) ?>"
                           class="btn btn-sm btn-danger" data-method='post'> <iclass="fa fa-close"></i> Delete </a>
                    </div>

                </div>
            </div>
            <br>
            <br>
        </div>

    </div>

    <!-- end row -->

</section>