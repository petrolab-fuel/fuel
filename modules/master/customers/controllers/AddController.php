<?php
/**
 * Created by
 * User: Wisard17
 * Date: 10/16/2017
 * Time: 2:00 PM
 */

namespace app\modules\master\customers\controllers;

use app\modules\master\customers\models\Attentions;
use app\modules\master\customers\models\RunJSModel;
use Yii;
use app\modules\master\customers\models\Customers;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\web\Response;

class AddController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['detail', 'index', 'edit'],
                'rules' => [
                    [
                        'actions' => ['detail', 'index', 'edit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'del' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return string|\yii\web\Response
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionIndex()
    {
        $cust = new Customers();

        $post = Yii::$app->request->post();

        if (Yii::$app->request->isAjax && isset($post['reg_new_row'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $cust->renderRowAjax($post['reg_new_row']);
        }

        if ($cust->load($post)) {
            if ($cust->validate()) {
                $cust->save(false);
                return $this->redirect(Url::toRoute(['/master/customers/default']));
            }
        }
//        RunJSModel::getRunJs(new Attentions());
        return $this->render('create', [
            'cust' => $cust,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionEdit($id)
    {
        $cust = $this->findModel($id);
        $post = Yii::$app->request->post();
        if (Yii::$app->request->isAjax && isset($post['reg_new_row'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $cust->renderRowAjax($post['reg_new_row']);
        }
        if ($cust->load($post)) {
//            print_r($post);die;
            if ($cust->validate()) {
                $cust->save(false);
                return $this->redirect(Url::toRoute(['/master/customers']));
            }
        }
//        RunJSModel::getRunJs(new Attentions());
        return $this->render('edit', [
            'cust' => $cust,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $cust = $this->findModel($id);

        return $this->render('view', [
            'cust' => $cust,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDel($id)

    {
//        Attentions::deleteAll(['customer_id' =>$id]);
        // $att=Attentions::find()->where(['customer_id'=>$id])->all();
        // $att->delete();
        $this->findModel($id)->delete();

        return $this->redirect(['/master/customers']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FormCustomer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Customers::findOne(['customer_id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSearch(){
        $post=Yii::$app->request->post();
        Yii::$app->response->format=Response::FORMAT_JSON;
        $model=Customers::find()->select(['attention.name','attention.id'])->joinWith('attentions')
            ->where(['attention.customer_id'=>$post['id']])
            ->andWhere("attention.name like '%".$post['searchtext']."%'")
            ->one();
        if($model==null){
            return['status'=>'success',
                    'message'=>'Data Belum Ada'];
        }else{
            return['status'=>'error',
                'message'=>'Data Atention sudah ada'];
        }
//        exit;
//        $model=Attentions::find()->select('name')->where([])
    }
}