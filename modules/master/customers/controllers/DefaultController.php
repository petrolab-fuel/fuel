<?php
/**
 * Created by PhpStorm.
 * User: MY PC
 * Date: 2/2/2018
 * Time: 2:26 PM
 */

namespace app\modules\master\customers\controllers;
use app\modules\master\customers\models\Attentions;
use app\modules\master\customers\models\Customers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;
use app\modelsDB\TblBranchUT;
use app\modelsDB\TblBranchPAMA;
use app\modelsDB\TblBranchOTHERS;
use app\modelsDB\TblCustomersUT;
use app\modelsDB\TblCustomersPAMA;
use app\modelsDB\TblCustomersOTHERS;
use app\modules\master\customers\models\search\SearchCustomer;
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['detail', 'index', 'add', 'edit'],
                'rules' => [
                    [
                        'actions' => ['detail', 'index', 'add', 'edit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'del' => ['post'],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
        $searchModel = new SearchCustomer();
       $request = Yii::$app->request->queryParams;
//
        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }

        return $this->render('index');

    }
    public function actionListCustomer(){
        $get=Yii::$app->request->get('param');
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        $q = isset($_GET['q']) ? $_GET['q'] : '';
//        if (isset($_GET['type']))  {
            $query = Customers::find()->limit(20);
            $query->where("name like '%$q%'" );
//            $query->andWhere("(name like '%$q%')");
            if($query->count()>0){
                foreach ($query->all() as $record) {
                    $out[] = [
                        'id' => $record->customer_id,
                        'text' => $record->name,
                    ];
                }
            }else{
                $out[]=[
                    'id'=>0,
                    'text'=>'Customers Not Found',
                ];
            }
//        } else {
//
//            $out[] = [
//                'id' => '',
//                'text' => "-- Please select Items --",
//            ];
//        }
        return [
            'results' => $out,
        ];
    }
    public function actionListAttention(){
        $get=Yii::$app->request->get('param');
        $filter=isset($get['filter'])?$get['filter']:'';
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        $q = isset($_GET['q']) ? $_GET['q'] : '';
//        if (isset($_GET['type']))  {
        $query = Attentions::find()->limit(20);
        $query->where(['customer_id'=>$filter]);
            $query->andWhere("(name like '%$q%')");
        if($query->count()>0){
            foreach ($query->all() as $record) {
                $out[] = [
                    'id' => $record->id,
                    'text' => $record->name,
                ];
            }
        }else{
            $out[]=[
                'id'=>0,
                'text'=>'Attention Not Found',
            ];
        }
//        } else {
//
//            $out[] = [
//                'id' => '',
//                'text' => "-- Please select Items --",
//            ];
//        }
        return [
            'results' => $out,
        ];
    }

    public function actionBranch()
    {
        $get = Yii::$app->request->get('param');
        $id=$get['filter'];
        Yii::$app->response->format=Response::FORMAT_JSON;
        $out = [];
        $q = isset($_GET['q']) ? $_GET['q'] : '';
        if ($id==1)
        {
            $query = TblBranchUT::find()->limit(20);
        }elseif($id==2)
        {
            $query = TblBranchPAMA::find()->limit(20);
        }else{
            $query = TblBranchOTHERS::find()->limit(20);
        }
        $query->where("branch like '%$q%'");
        if($query->count()>0)
        {
            foreach($query->all() as $record)
            {
                $out[]=[
                    'id'=>$record->branch,
                    'text'=>$record->branch,
                ];
            }
        }else{
            $out[]=[
                'id'=>0,
                'text'=>'data not found',
            ];
        
        }
        return [
            'results' => $out,
        ];
    }

    public function actionCustomer()
    {

        $get = Yii::$app->request->get('param');
        $id=$get['fromcol'];
        $branch=$get['filter'];
        Yii::$app->response->format=Response::FORMAT_JSON;
        $out = [];
        $q = isset($_GET['q']) ? $_GET['q'] : '';
        if ($id==1)
        {
            $query = TblCustomersUT::find()->limit(20);
        }elseif($id==2)
        {
            $query = TblCustomersPAMA::find()->limit(20);
        }else{
            $query = TblCustomersOTHERS::find()->limit(20);
        }
        $query->where("Branch='$branch'");
        $query->andWhere("Name like '%$q%'");
        if($query->count()>0)
        {
            foreach($query->all() as $record)
            {
                $out[]=[
                    'id'=>$record->CustomerID,
                    'text'=>$record->Name,
                ];
            }
        }else{
            $out[]=[
                'id'=>0,
                'text'=>'data not found',
            ];
        
        }
        return [
            'results' => $out,
        ];
    }
}