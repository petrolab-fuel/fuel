<?php
/**
 * Created by PhpStorm.
 * User: Wisard17
 * Date: 10/16/2017
 * Time: 10:48 AM
 */

namespace app\modules\master\customers\models\search;


use app\modules\master\customers\models\Attentions;
use app\modules\master\customers\models\Customers;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class SearchCustomer
 * @package app\modules\customer\models\search
 */
class SearchCustomer extends Customers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'customer_id',
                'name',
                'address',
                'type',
                'time_create',
                'time_update',
                'Factory_Phone',],
                'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public $allField = [
        'customer_id' => 'customer_id',
        'name' => 'name',
        'address' => 'address',
        'type' => 'type',
        'time_create'=>'time_create',
        'time_update'=>'time_update',
        'attention'=>'customer_id',
    ];

    public function ordering($params)
    {
        $ncol = isset($params['order'][0]['column']) ? $params['order'][0]['column'] : 0;
        $col = (isset($params['columns'][$ncol]) && array_key_exists($params['columns'][$ncol]['data'], $this->allField)) ?
            $this->allField[$params['columns'][$ncol]['data']] : '';
        $argg = isset($params['order'][0]['dir']) ? $params['order'][0]['dir'] : 'asc';
        $table = 'customer';

        return $col !== '' ? "$table.$col $argg " : '';
    }


    public $allData;

    public $currentData;

    /**
     * @param Query $query
     * @return int
     */
    public function calcAllData($query)
    {
        return $this->allData == null ? $query->count() : $this->allData;
    }

    /**
     * @param Query $query
     * @param $params
     * @return Query
     */
    public function defaultFilterByUser($query, $params)
    {


        return $query;
    }

    /**
     * @param $params
     * @param bool $export
     * @param bool $join
     * @return $this|\yii\db\ActiveQuery|Query
     */
    public function searchData($params, $export = false, $join = true)
    {

        $odr = $this->ordering($params);

        $query = self::find();

        if (!$join)
            $query = self::find();

        $query = $this->defaultFilterByUser($query, $params);

        $query->orderBy($odr);

        $start = isset($params['start']) ? $params['start'] : 0;
        $lang = isset($params['length']) ? $params['length'] : 10;

        $this->allData = $this->calcAllData($query);

        $fltr = '';
        $fltr2 = '';
        $s = 0;
        if (isset($params['columns']))
            foreach ($params['columns'] as $col) {
                if (isset($params['search']) && $params['search']['value'] != '') {
                    $lst[] = $col['data'];
                    if (array_key_exists($col['data'], $this->allField) &&
                        !array_key_exists($col['data'], $lst) && $this->allField[$col['data']] != '') {
                        $fltr .= $fltr !=='' ? ' or ' : '';
                        $fltr .= ' `customer`.' . $this->allField[$col['data']] . " like '%" . $params['search']['value'] . "%' ";
//                        $s++;
                    }
                    if ($col['data'] == 'attention') {
                        $c = Attentions::find()->where("name like '%" . $params['search']['value'] . "%'")->distinct()->select(['customer_id'])->all();
                        $fltr .= $fltr !== '' ? ' or ' : '';
                        $fltr .= ' `' . self::tableName() . "`.customer_id IN ('" . join("','", ArrayHelper::getColumn($c, 'customer_id')) . "') ";
                    }

                }
                if ($col['searchable'] == 'true' && $col['search']['value'] != '' &&
                    array_key_exists($col['data'], $this->allField) && $this->allField[$col['data']] != '') {
                    if ($col['data'] == 'attention') {
                        $c = Attentions::find()->where("name like '%" . $col['search']['value'] . "%'")->distinct()->select(['customer_id'])->all();
                        $query->andWhere(' `' . self::tableName() . "`.customer_id IN ('" . join("','", ArrayHelper::getColumn($c, 'customer_id')) . "') ");
                    } else {
                        $query->andFilterWhere(['like', '`' . self::tableName() . '`.' . $this->allField[$col['data']], $col['search']['value']]);
                    }
                }

            }

        $query->andWhere($fltr);


        $this->load($params);

        if ($export) {
            return $query;
        }

        $this->currentData = $query->count();

        $query->limit($lang)->offset($start);
        return $query;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function searchDataTable($params)
    {
        $data = $this->searchData($params);
        return Json::encode([
            "draw" => isset ($params['draw']) ? intval($params['draw']) : 0,
            "recordsTotal" => intval($this->allData),
            "recordsFiltered" => intval($this->currentData),
            "data" => $this->renderData($data, $params),
        ]);
    }

    /**
     * @param $query Query
     * @param $params
     * @return array
     */
    public function renderData($query, $params)
    {
        $out = [];

        /** @var self $model */
        foreach ($query->all() as $model) {

            $out[] = array_merge(ArrayHelper::toArray($model), [
                'action' => $model->action,
                'attention'=>$model->phones,

            ]);

        }
        return $out;
    }

}