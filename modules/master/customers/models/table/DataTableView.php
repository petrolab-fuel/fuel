<?php
/**
 * Created by .
 * User: wisard17
 * Date: 5/9/2017
 * Time: 4:32 PM
 */

namespace app\modules\master\customers\models\table;


use app\smartadmin\assets\plugins\DataTableAsset;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\View;

/**
 * Class DataTableView
 * @property string jsonData
 * @property string jsonDataTable
 * @package app\modules\report\models
 */
class DataTableView extends Widget
{
    /**
     * @var array the HTML attributes for the container tag of the list view.
     * The "tag" element specifies the tag name of the container element and defaults to "div".
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];

    public $columns;

    /** @var  Model */
    public $model;

    public $request;

    public $ajaxUrl;


    /**
     * Initializes the view.
     */
    public function init()
    {
        parent::init();

        if ($this->columns === null) {
            throw new InvalidConfigException('The "columns" property must be set.');
        }

        if ($this->model === null) {
            throw new InvalidConfigException('The "model" property must be set.');
        }

        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
    }

    /**
     * Runs the widget.
     */
    public function run()
    {
        $assetTable = DataTableAsset::register($this->view);
        echo $this->loadTable();
        $this->runJs();
    }

    /**
     * @return string
     */
    public function loadTable()
    {
        $content = '';

        return Html::tag('table', "",
            ['class' => "table table-striped table-bordered table-hover table-responsive", 'width' => "100%", 'id' => $this->options['id']]);
    }


    /**
     * @return array
     */
    protected function loadColumnHeader()
    {
        $out = [];
        foreach ($this->columns as $column) {
            $showD = $column == '' ? 'details-control' : '';
            $out[] = [
                'title' => $this->model->getAttributeLabel($column),
                'data' => "$column",
                'className' => $showD,
            ];
        }

        return $out;
    }

    /**
     * @return string
     */
    protected function loadColumnFilter()
    {
        $out = '';
        foreach ($this->columns as $column) {
            $ipt = '';
            if (!in_array($column, ['action', 'followup', 'actions'])) {
                if (in_array($column, ['sampleDate', 'receiveDate', 'reportDate',], true)) {
                    $ipt = Html::input('text', "filter_$column", '', [
                        'placeholder' => ' Filter ' . $this->model->getAttributeLabel($column),
                        'class' => 'form-control datejuifilter',
                        'style' => 'padding: 1px;height: 100%;width: 100%;',
                    ]);
                } elseif ($column === 'status') {
                    $ipt = '<select name="filter_' . $column . '"><option value="" selected>Select All</option><option value="N">Normal</option><option value="B">Attention</option><option value="C">Urgent</option><option value="D">Severe</option></select>';
                } else {
                    $ipt = Html::input('text', "filter_$column", '', [
                        'placeholder' => ' Filter ' . $this->model->getAttributeLabel($column),
                        'class' => 'form-control',
                        'style' => 'padding: 1px;height: 100%;width: 100%;',
                    ]);
                }
            }

            $out .= Html::tag('td', $ipt);
        }

        return Html::tag('tr', $out);
    }

    /**
     * All JS
     */
    protected function runJs()
    {

        $csrf = Yii::$app->request->getCsrfToken();

        $loadingHtml = '<div align="center"><i class="fa fa-spinner fa-pulse fa-4x fa-fw margin-bottom"></i></div>';

        $ajaxUrl = Url::to(['index',
            '_csrf' => $csrf,
        ]);

        $idTable = $this->options['id'];

        $columns = Json::encode($this->loadColumnHeader());

        $colFilter = $this->loadColumnFilter();

        $detailUrl = Url::to(['detail']);
        $order = isset($this->options['order']) ? $this->options['order'] : "[0, 'desc']";


        $jsScript = <<< JS
var delay2 = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();
var renderdata = (function () {
    // var dataSet = ;
    var domElement = $('#$idTable');
    var detailurl = "$detailUrl";
    var domOuttable = domElement.parent().parent().parent();
    var table = domElement.DataTable({
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'>i<'col-sm-3 pull-right col-xs-12 hidden-xs'>>" +
                "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'rl><'col-xs-12 col-sm-6'p>>",
        scrollX: true,
        //"scrollY": "400px",
        // "scrollCollapse": true,
        stateSave: true,
        rowId: 'rowId',
        processing: true,
        serverSide: true,
        ajax: "$ajaxUrl",
        columns: $columns,
        order: [$order],
        createdRow: function (row, data, index) {
            if (data.classRow !== '')
                $(row).addClass(data.classRow);
        },
        initComplete: function () {
            delay2(function () {
                var colfilter = $('$colFilter');
                colfilter.find('.datejuifilter').datepicker({
                    showButtonPanel: true,
                    altField: "yyyy-mm-dd",
                    dateFormat: "yy-mm-dd",
                    beforeShow: function (input) {
                        setTimeout(function () {
                            var buttonPane = $(input)
                                .datepicker("widget")
                                .find(".ui-datepicker-buttonpane");
                            $("<button>", {
                                text: "Reset",
                                click: function () {
                                    $.datepicker._clearDate(input);
                                }
                            }).appendTo(buttonPane).addClass("ui-datepicker-clear ui-state-default ui-priority-primary ui-corner-all");
                        }, 1);
                    },
                });
                // Restore state
                var state = table.state.loaded();
                if (state) {
                    table.columns().eq(0).each(function (colIdx) {
                        var colname = table.column(colIdx).dataSrc();
                        var colSearch = state.columns[colIdx].search;
                        if (colSearch.search) {
                            console.log(colname);
                            console.log(colSearch.search);
                            colfilter.find('[name=filter_' + colname + ']').val(colSearch.search);
                        }
                    });
                }
                domOuttable.find('thead').eq(0).append(colfilter);
                //domOuttable.find('.dataTables_scrollBody').attr('style', 'position: relative; width: 100%;');
            }, 1000);
        }
    });
    domOuttable.find('thead').eq(0).delegate('td input[type=text]', 'change', function () {
        var self = this;
        delay2(function () {
            table
                .column($(self).parent().index() + ':visible')
                .search(self.value)
                .draw();
        }, 1000);

    });

    domOuttable.find('thead').eq(0).delegate('td select', 'change', function () {
        var self = this;
        delay2(function () {
            table
                .column($(self).parent().index() + ':visible')
                .search(self.value)
                .draw();
        }, 1000);

    });

    domElement.on('click', 'tbody .details-control', function () {

        var tr = $(this).closest('tr');
        tr.toggleClass('selected');

        var row = table.row(tr);

        var orderNo = row.data().Order_No;


        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {

            if (row.child() === undefined || row.child().find('div').length < 2) {
                row.child('$loadingHtml');
                $.get(detailurl, {
                    "orderno": orderNo,
                    "_csrf": '$csrf'

                }, function (dataresponse, status, xhr) {
                    if (status === "success")
                        row.child(dataresponse);
                    if (status === "error")
                        alert("Error: " + xhr.status + ": " + xhr.statusText);
                });
            }
            row.child.show();
            tr.addClass('shown');
        }
    });

    return {
        table: table
    };
})();        

JS;


        $this->view->registerJs($jsScript, View::POS_READY, 'runfor_' . $this->options['id']);
    }


}