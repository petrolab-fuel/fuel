<?php
/**
 * Created by PhpStorm.
 * User: MY PC
 * Date: 2/2/2018
 * Time: 12:00 PM
 */

namespace app\modules\master\customers\models;
use app\smartadmin\ActiveForm;
use app\modelsDB\Attention;
use app\modelsDB\Customer;
use yii\helpers\Html;
use yii\helpers\Url;
use yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;


/**
 *
 * @property mixed $att
 * @property mixed $contact
 * @property mixed $attForm
 * @property mixed $attArray
 * @property string $action
 * @property mixed $phones
 * @property \yii\db\ActiveQuery|array|\app\modules\master\customers\models\PIC[] $allAtt
 * @property mixed $aksi
 * @property mixed $email
 */
class Customers extends Customer
{

    /**
     * @inheritdoc
     */
      public function rules()
    {
        return [
            [['address'], 'string'],
            [['time_create', 'time_update'], 'safe'],
            [['users_index'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 4],
            [['name'],'required'],
        ];
    }


 public function load($data, $formName = null)
    {
        $data = $data == null ? [] : $data;
        if (isset($data['Attentions'])) {
                      
            foreach ($data['Attentions'] as $idx => $datum) {
                $expIdx = explode('_', $idx);
                if (sizeof($expIdx) > 1)
                    if (!is_numeric($expIdx[0])) {
                        $this->_attArray[$idx] = new Attentions($datum);
                    } else {
                        $item = Attentions::findOne(['customer_id' => $expIdx[0], 'id' => $expIdx[1]]);
                        $item->load(['Data' => $datum], 'Data');
                        $item->actionId = isset($datum['actionId']) ? $datum['actionId'] : null;
                        $this->_attArray[$idx] = $item;
                    }
            }
        }
        $ld = parent::load($data, $formName);
        return $ld;
    }

 public function save($runValidation = true, $attributeNames = null)
    {
        $n=0;
        $outParent = parent::save($runValidation, $attributeNames);
        $out = true;
        foreach ($this->allAtt as $item) {
            $item->customer_id = $this->customer_id;
            if (!$item->isNewRecord && $item->actionId == 'del') {
               $simpan  = $item->delete();
            } else {
                $simpan = $item->save();
            }
            $out = $out && $simpan;
            // $out=$simpan;
            $n++;
        }
      
        return $outParent && $out;
    }

    public $_att;
    public function getAtt()
    {
        if ($this->_att == null)
            $this->_att = $this->hasMany(Attentions::className(), ['customer_id' => 'customer_id']);
        return $this->_att;
    }


     public function getContact()
        {
        # code...
            $out='';
             foreach($this->attentions as $attention)
             {

                 $out .= Html::tag('li', 'Name : '.$attention->name.' Telp:'.$attention->phone.' Email:'.$attention->email);
             }
                return Html::tag('ul', $out, ['class' => "friends-list"]);
        }

    


     public function  getPhones()
    {
        $out='';
        foreach($this->attentions as $attention)
        {
             $out .= Html::tag('li', $attention->name);
        }
         return Html::tag('ul', $out, ['class' => "friends-list"]);
    }


public function  getEmail()
    {
        $out='';
        foreach($this->attentions as $attention)
        {
           $out .=Html::tag('li', $attention->name.' - '.$attention->email);
        }
        return Html::tag('ul', $out, ['class' => "friends-list"]);
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return Html::tag('div',
            Html::tag('a','<i class="fa  fa-search"></i>', ['class'=>'btn btn-default btn-xs','title'=>'View','href'=>Url::toRoute(["/master/customers/add/view","id"=>$this->customer_id])]).
            Html::tag('a','<i class="fa  fa-edit"></i>', ['class'=>'btn btn-default btn-xs','title'=>'Edit','href'=>Url::toRoute(["/master/customers/add/edit","id"=>$this->customer_id])]).
            Html::tag('a','<i class="fa  fa-trash-o"></i>', ['class'=>'btn btn-default btn-xs','title'=>'Delete','href'=>Url::toRoute(["/master/customers/add/del","id"=>$this->customer_id]),'data-method' => 'post',
                'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',])
            ,['class'=>'btn-group']);

//        return '<div class="btn-group">
//                <button type="button" class="btn btn-default">
//                    <i class="fa fa-align-left"></i>
//                </button>
//                <button type="button" class="btn btn-default">
//                    <i class="fa fa-align-center"></i>
//                </button>
//                <button type="button" class="btn btn-default">
//                    <i class="fa fa-align-right"></i>
//                </button>
//                <button type="button" class="btn btn-default">
//                    <i class="fa fa-align-justify"></i>
//                </button>
//            </div>';
//        return Html::tag('div',
//            ['class'=>"btn-group"]);

//        return Html::tag('div',
//            Html::tag('a', 'Action <span class="caret"></span>', [
//                'class' => "btn btn-default dropdown-toggle", 'data-toggle' => "dropdown", 'aria-expanded' => "false"
//            ]) . Html::tag('ul',
//                Html::tag('li', Html::tag('a', '<i class="fa fa-caret-square-o-right"></i> view', ['href' => Url::toRoute(['/master/customers/add/view', 'id' => $this->customer_id])])) .
//                Html::tag('li', Html::tag('a', '<i class="fa fa-edit"></i> Edit', ['href' => Url::toRoute(['/master/customers/add/edit', 'id' => $this->customer_id])])) .
//                Html::tag('li', Html::tag('a', '<i class="fa fa-window-close"></i> Delete',
//                    ['href' => Url::toRoute(['/master/customers/add/del', 'id' => $this->customer_id]),
//                        'data-method' => 'post',
//                        'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
//                    ]))
//
//                , ['class' => "dropdown-menu"]), ['class' => "btn-group"]);
    }
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(),[
           'customer_id' => Yii::t('app', 'ID'),
        ]);
    }

     public function getAksi()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => "btn btn-default dropdown-toggle", 'data-toggle' => "dropdown", 'aria-expanded' => "false"
            ]) . Html::tag('ul',
//                Html::tag('li', '<a>Edit</a>') .
                Html::tag('li', '<a data-action="delate_row">Delete</a>')

                , ['class' => "dropdown-menu"]), ['class' => "btn-group"]);
    }

    public $_attArray;
    public $n=0;

    public function getAttArray()
    {
        if ($this->_attArray == null)
            $this->_attArray = $this->att;

        return $this->_attArray;
    }

    /**
     * @param array|PIC $pic
     */
    public function setAttArray($att)
    {
        $this->_attArray = $att;
    }

    /**
     * @return PIC[]|array|\yii\db\ActiveQuery
     */
    public function getAllAtt()
    {
        $att = $this->attArray;
        return $att == null ? [0 => new Attentions()] : $att;
    }

    public function renderTable($form)
        {
        $i=0;
        $out='';
        // if $this->attentions->isNewRe
        foreach ($this->allAtt as $att) {
            # code...
            if ($i==0){
               $out .= Html::tag('thead', Html::tag('tr',
//                    Html::tag('th', '') .
                        Html::tag('th', 'action') .
                         Html::tag('th','Name', ['style' => 'width:45%;']) .
                         Html::tag('th','Phone',['style'=>'width:25%;']) .
                        Html::tag('th', 'Email',['style'=>'width:25%;']) .'<tbody>'
                        ));       
                }
                $att->idName = $att->id == null ? 'newrow_' . $i : $att->customer_id . '_' . $att->id;
                $out .= Html::tag('tr',
                Html::tag('td', $this->aksi) .
                Html::tag('td', $form->field($att, 'name', ['template' => '<label class="input">{input}</label>{error}'])) .
                Html::tag('td', $form->field($att, 'phone', ['template' => '<label class="input">{input}</label>{error}'])) .
                Html::tag('td', $form->field($att, 'email', ['template' => '<label class="input">{input}</label>{error}'])) ,
                ['data-id' => $att->idName]
                );

                $i++;
             }
              return Html::tag('div', Html::tag('table', $out . '</tbody>', ['class' =>
            'table table-bordered table-striped table-condensed table-hover smart-form has-tickbox'
        ]), ['class' => 'table-responsive','id'=>'tbl']);
    }


     public function renderRowAjax($rowTo = 0)
    {
        $form = ActiveForm::begin();
        $att = new Attentions();
        
       $att->idName = 'newrow_' . $rowTo;

        $out = Html::tag('tr',
            Html::tag('td', $att->action) .
            Html::tag('td', $form->field($att, 'name', ['template' => '<label class="input">{input}</label>{error}'])) .
            Html::tag('td', $form->field($att, 'phone', ['template' => '<label class="input">{input}</label>{error}'])) .
            Html::tag('td', $form->field($att, 'email', ['template' => '<label class="input">{input}</label>{error}'])),
            ['data-id' => $att->idName]
        );


        return [
            'data' =>$out,
            'jsAdd' => ArrayHelper::toArray($form->attributes),
        ];
    }
    public function getAttForm()
    {
        return (new Attentions())->formName();
    }


}