<?php
/**
 * Created by PhpStorm.
 * User: MY PC
 * Date: 2/7/2018
 * Time: 2:11 PM
 */

namespace app\modules\master\units\controllers;
use yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\web\Response;
use app\modules\master\units\models\Units;
use app\modules\master\units\models\SamplingPoint;
use app\modules\master\units\models\RunJSModel;



class AddController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['detail', 'index', 'edit'],
                'rules' => [
                    [
                        'actions' => ['detail', 'index', 'edit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'del' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return string|\yii\web\Response
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionIndex()
    {
        
        $model = new Units();
        $post = Yii::$app->request->post();
        if (Yii::$app->request->isAjax && isset($post['reg_new_row'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model->renderRowAjax($post['reg_new_row']);
        }
        if ($model->load($post)){
            if ($model->validate()){
                $model->save(false);
                return $this->redirect(Url::toRoute(['/master/units/add/view', 'id' => $model->id]));
            }
        }
        RunJSModel::getRunJs(new SamplingPoint());
        return $this->render('create', ['model' => $model,]);
    }

    public function actionEdit($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if (Yii::$app->request->isAjax && isset($post['reg_new_row'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model->renderRowAjax($post['reg_new_row']);
        }
        if ($model->load($post)) {
            if ($model->validate()) {
                $model->save(false);
               return $this->redirect(Url::toRoute(['/master/units/add/view', 'id' => $model->id]));
            }
        }
        RunJSModel::getRunJs(new SamplingPoint());
        return $this->render('edit', [
            'model' => $model,
        ]);
    }
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }
    public function actionDel($id)

    {
        $this->findModel($id)->delete();

        return $this->redirect(['/master/units']);
    }
    protected function findModel($id)
    {
        if (($model = Units::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }



}