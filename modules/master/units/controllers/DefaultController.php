<?php
/**
 * Created by PhpStorm.
 * User: MY PC
 * Date: 2/2/2018
 * Time: 2:26 PM
 */

namespace app\modules\master\units\controllers;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use app\modules\master\units\models\search\SearchUnits;
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['detail', 'index', 'add', 'edit'],
                'rules' => [
                    [
                        'actions' => ['detail', 'index', 'add', 'edit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'del' => ['post'],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
        $searchModel = new SearchUnits();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }

        return $this->render('index');

    }
}