<?php

use app\smartadmin\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use conquer\select2;

/* @var $this yii\web\View */
/* @var $model app\modules\master\units\models\Units */
/* @var $form ActiveForm */
app\smartadmin\assets\plugins\PluginsAssets::register($this);
?>
<div class="add-_form">

    <?php $form = ActiveForm::begin([

        'fieldConfig' => [
            'template' => "{label}<label class='input'>{input}</label>{error}",
            'inputOptions' => ['class' => 'form-control'],
        ],
        'options' => [
            'class' => 'smart-form',
        ],
    ]);
    // Html::tag('td', $form->field($cust, 'name', ['template' => '<label class="input">{input}</label>{error}']));
    $jsScript = <<< JS

$('.select2').select2()
JS;
    $this->registerJS($jsScript);
    ?>



    <header>
        Lengkapi field di bawah ini.
    </header>

    <fieldset>
        <div class="row">
            <section class="col col-4">
                 <?php $urlData = Url::toRoute(['/master/units/add']); ?>

                <?= $form->field($model,'customer_id')->dropDownList(
                        $model->getCustArray(),
                        ['prompt'=>'select customer','class'=>'select2','style'=>'width:100%'] )?>
            </section>
            <section class="col col-3">
                <?= $form->field($model, 'model_unit') ?>
            </section>
            <section class="col col-md-3">
                <?= $form->field($model, 'type_unit') ?>
            </section>
            <section class="col col-md-2">
                <?= $form->field($model, 'serial_number') ?>
            </section>
        </div>

    </fieldset>

    <fieldset>
        <div class="row">
            <section class="col col-10">
                <?= $form->field($model, 'location') ?>
            </section>


        </div>

    </fieldset>

    <fieldset>
        <div class="row">
            <section class="col col-12">
                <?= $model->renderTable($form)?>
                <a data-action="add_row_table" class="btn btn-primary btn-xs btn-block">
                    <i class="fa fa-plus-circle"></i> Add Sampling Point
                </a>
            </section>

        </div>
    </fieldset>


    <footer>
        <?= Html::submitButton(Yii::t('app', $model->isNewRecord ? 'Submit ' : 'Save '),
            ['class' => 'btn btn-primary']) ?>
    </footer>

    <?php ActiveForm::end(); ?>

</div>
<?php

?>


