<?php
/**
 * Created by
 * User: Wisard17
 * Date: 10/16/2017
 * Time: 1:48 PM
 */


/**
 * @var $model \app\modules\master\units\models\Units
 * @var $this yii\web\View
 *
 */


use yii\helpers\Url;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Units'), 'url' => Url::toRoute(['/master/units'])];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">

    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-6">
        <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-user-circle "></i>
            <?= $this->title ?>
        </h1>
    </div>


    <!-- right side of the page with the sparkline graphs -->
    <!-- col -->
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-6">
        <!-- sparks -->
        <ul id="sparks">
            <li class="sparks-info .bg-color-teal">
                <?= \app\smartadmin\helpers\Html::a('<i class="fa fa-plus-circle"></i>
                    Add', \yii\helpers\Url::toRoute(['/master/units/add']), ['class' => 'btn btn-primary'])?>
            </li>
        </ul>
        <!-- end sparks -->
    </div>
    <!-- end col -->

</div>

<section id="widget-grid" class="well well-light well-sm no-margin no-padding">

    <!-- row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-3 profile-pic">
                </div>
                <div class="col-sm-3">
                    <h1> <span class="semi-bold"><?= $model->customerName?></span>
                        <br>
                        <small> Location : <?= $model->location?></small>
                        <br>
                        <small>Unit Model : <?= $model->model_unit?></small>
                        <br>
                        <small>Unit Type : <?= $model->type_unit?></small>
                        <br>
                        <small> Serial Number : <?= $model->serial_number?></small>
                    </h1>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-sm-8 ">

                    <div class="btn-group">
                        <a href="<?= Url::toRoute(['/master/units/add/edit', 'id' => $model->id]) ?>"
                           class="btn btn-sm btn-primary"> <i class="fa fa-edit"></i> Edit </a>
                    </div>

                    <div class="btn-group">
                        <a href="<?= Url::toRoute(['/master/units/add/del', 'id' => $model->id]) ?>"
                           class="btn btn-sm btn-danger" data-method='post'> <iclass="fa fa-close"></i> Delete </a>
                    </div>

                </div>
            </div>
            <br>
            <br>
        </div>

    </div>

    <!-- end row -->

</section>