<?php
/**
 * Created by PhpStorm.
 * User: MY PC
 * Date: 2/7/2018
 * Time: 11:48 AM
 */

namespace app\modules\master\units;


class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\master\units\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}