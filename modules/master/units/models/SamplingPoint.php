<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 3/15/2018
 * Time: 2:51 PM
 */

namespace app\modules\master\units\models;
use yii\helpers\Html;
use yii\helpers\Url;
use yii;
class SamplingPoint extends \app\modelsDB\SamplingPoint
{
    public function getAction()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => "btn btn-default dropdown-toggle", 'data-toggle' => "dropdown", 'aria-expanded' => "false"
            ]) . Html::tag('ul',
//                Html::tag('li', '<a>Edit</a>') .
                Html::tag('li', '<a data-action="delate_row">Delete</a>')

                , ['class' => "dropdown-menu"]), ['class' => "btn-group"]);
    }
    public $_idName;

    /**
     * @return string
     */
    public function getIdName()
    {
        if ($this->_idName == null)
            $this->_idName  = 'new';
        return $this->_idName;
    }

    /**
     * @param string $idName
     */
    public function setIdName($idName)
    {
        $this->_idName = $idName;
    }

    /**
     * @return string
     */
    public function formName()
    {
        return parent::formName() . "[$this->idName]";
    }

    /** @var  string|int */
    public $actionId;
}