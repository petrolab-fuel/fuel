<?php
/**
 * Created by PhpStorm.
 * User: MY PC
 * Date: 2/5/2018
 * Time: 5:40 PM
 */

namespace app\modules\master\units\models;
use Yii;
use yii\base\Model;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\View;

class RunJSModel extends Model
{
    /**
     * void
     * @param $model Model
     */
    static public function getRunJs($model)
    {
        $csrf = Yii::$app->request->getCsrfToken();

        $loadingHtml = '<div align="center"><i class="fa fa-spinner fa-pulse fa-4x fa-fw margin-bottom"></i></div>';

        $modelForm = $model->formName();

        // $paket = Json::encode(Package::find()->select([
            // 'Package_ID',
            // 'Package_Name',
            // 'Description',
            // 'Price',
        // ])->indexBy('Package_ID')->all());

         $urlAddPackage = Url::toRoute(['/orders/package/add']);
         $urlLabNote = Url::toRoute(['/monitoring/sample/lab-note']);

        $jsScript = <<< JS
var renderdata_po = (function () {
    var body = $('body');
    var p = 'string';

    

    function alert_notif(res) {
        $.sound_on = false;
        var color = res.status === 'success' ? '#659265' : '#C46A69';
        var icon = res.status === 'success' ? 'fa-check' : 'fa-times';
        $.smallBox({
            title: res.status,
            content: res.message,
            color: color,
            iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
            timeout: 4000
        });
    }

    body.delegate('[data-action=delate_row]', 'click', function () {
        var elm = $(this);
        var row = elm.closest('tr');
        p = '$modelForm';
        p = p.replace('[new]', '[' + row.attr('data-id') + ']');

        row.html('<input type="hidden" name="' + p + '[actionId]" value="del" >');

    });

    body.delegate('[data-action=add_row_table]', 'click', function () {
        var elm = $(this);
        var tbl = elm.closest('fieldset').find('table');
        var form = elm.closest('form');
       
        $.ajax({
            // url: '',
            method: 'post',
            data: {'reg_new_row': (tbl.find('tr').length - 1)},
            
            error: function (response) {
                alert_notif({status: "error", message: 'tidak bisa save'});
            },
            success: function (response) {
               
                var t = $(response.data);
                if ($.fn.select2 !== undefined)
                    t.find('select').select2({"minimumInputLength": 2});
                tbl.find('tbody').append(t);
                // $(form).yiiActiveForm(response.jsAdd, []);
                $.each(response.jsAdd, function (i, v) {
                    var vali = eval("var f = function(){ return " + v.validate.expression + ";}; f() ;");
                    v.validate = vali;
                     $(form).yiiActiveForm('add', v);
                });
            }
        });

    });

    body.delegate('[data-action=change_paket]', 'change', function () {
        var elm = $(this);
        var row = elm.closest('tr');

        var v = elm.val();

        p = '$modelForm';
        p = p.replace('[new]', '[' + row.attr('data-id') + ']');
        console.log(p);
        $('[name="' + p + '[Laboratory_Service_Description]"]').val(pack[v].Description);
        $('[name="' + p + '[Sales_Price]"]').val(pack[v].Price);
        // console.log(pack[v].Price);
    });

    body.delegate('[href="$urlAddPackage-i"]', 'click', function (a) {
        a.preventDefault();
        var elm = $(this);

        var modalvar = $('#modal-temp');
        modalvar.removeData('bs.modal');
        modalvar.modal({remote: '$urlAddPackage'});
        modalvar.modal('show');
    });

    var modalvar = $('#modal-temp');

    body.delegate('[data-action=note_lab]', 'click', function (a) {
        a.preventDefault();
        var elm = $(this);
        var orderNo = elm.attr('data-id');
        modalvar.removeData('bs.modal');
        modalvar.modal({remote: '$urlLabNote' + '?orderNo=' + orderNo});
        modalvar.modal('show');
        // alert_notif({status: "success", message: 'test'});

    });

    body.delegate('[data-action=submit_note_lab]', 'click', function (a) {
        a.preventDefault();
        a.stopPropagation();
        var elm = $(this);

        var form = modalvar.find('form');

        var data = $(form).serializeArray();

        $.ajax({
            url: $(form).attr('action'),
            method: $(form).attr('method'),
            data: data,
            error: function (response) {
                alert_notif({status: "error", message: 'tidak bisa save'});
                modalvar.modal('hide');
            },
            success: function (response) {
                console.log(response);
                alert_notif(response);
                modalvar.modal('hide');
            },

        });

    });

    return {
        "test": function () {
            alert('daa');
        }
    };
})();        

JS;


        Yii::$app->getView()->registerJs($jsScript, View::POS_READY, 'runfor_purchase');
    }
}