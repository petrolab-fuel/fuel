<?php
/**
 * Created by PhpStorm.
 * User: MY PC
 * Date: 2/9/2018
 * Time: 4:41 PM
 */

namespace app\modules\master\units\models\search;
use app\modules\master\units\models\Units;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use Yii;

class SearchUnits extends Units
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'id',
                'category_id',
                'customer_id',
                'location',
                'serial_number',
                'type_unit',
                'model_unit',
            ],
                'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public $allField = [

        'id',
        'customer_id'=>'customer_id',
        'location'=>'location',
        'serial_number'=>'serial_number',
        'model_unit'=>'model_unit',
        'type_unit'=>'type_unit',

        'customerName'=>'',

    ];

    public function ordering($params)
    {
        $ncol = isset($params['order'][0]['column']) ? $params['order'][0]['column'] : 0;
        $col = (isset($params['columns'][$ncol]) && array_key_exists($params['columns'][$ncol]['data'], $this->allField)) ?
            $this->allField[$params['columns'][$ncol]['data']] : '';
        $argg = isset($params['order'][0]['dir']) ? $params['order'][0]['dir'] : 'asc';
        $table = self::tableName();
        if (isset($params['columns'][$ncol]['data']) && $params['columns'][$ncol]['data'] == 'customerName') {
            $col = 'name';
            $table = 'customer';
        }

        return $col !== '' ? "$table.$col $argg " : '';
    }


    public $allData;

    public $currentData;

    /**
     * @param Query $query
     * @return int
     */
    public function calcAllData($query)
    {
        return $this->allData == null ? $query->count() : $this->allData;
    }

    /**
     * @param Query $query
     * @param $params
     * @return Query
     */
    public function defaultFilterByUser($query, $params)
    {
        $user = Yii::$app->user->identity;

        if ($user->access_rule_id == 4)
            $query->andWhere([self::tableName() . '.customer_id' => $user->customerId]);
        return $query;
    }

    /**
     * @param $params
     * @param bool $export
     * @param bool $join
     * @return $this|\yii\db\ActiveQuery|Query
     */
    public function searchData($params, $export = false, $join = true)
    {

        $odr = $this->ordering($params);

        $query = self::find()->joinWith(['customer']);//->where('Package_ID > 123');

        if (!$join)
            $query = self::find();

//        $query->andWhere(['report_type_id' => 1]);

        $query = $this->defaultFilterByUser($query, $params);

        $query->orderBy($odr);

        $start = isset($params['start']) ? $params['start'] : 0;
        $lang = isset($params['length']) ? $params['length'] : 10;

        $this->allData = $this->calcAllData($query);

        $table = self::tableName();

        $fltr = '';

        if (isset($params['columns']))
            foreach ($params['columns'] as $col) {
                if (isset($params['search']) && $params['search']['value'] != '') {
                    $lst[] = $col['data'];
                    if (array_key_exists($col['data'], $this->allField) &&
                        !array_key_exists($col['data'], $lst) && $this->allField[$col['data']] != '') {
                        $fltr .= $fltr != '' ? ' or ' : '';
                        $fltr .= ' `' . $table . '`.' . $this->allField[$col['data']] . " like '%" . $params['search']['value'] . "%' ";
                    }

                    if ($col['data'] == 'customerName') {
                        $fltr .= $fltr != '' ? ' or ' : '';
                        $fltr .= ' `customer`.name ' . " like '%" . $params['search']['value'] . "%' ";
                    }
                }
                if ($col['searchable'] == 'true' && $col['search']['value'] != '' &&
                    array_key_exists($col['data'], $this->allField)) {
                    if (in_array($col['data'], ['serialNumber', 'location', 'transformerId', 'attentionName', 'customerName'])) {
                        if ($col['data'] == 'customerName') {
                            $query->andFilterWhere(['like', '`customer`.name', $col['search']['value']]);
                        }
                    } elseif ($this->allField[$col['data']] != '')
                        $query->andFilterWhere(['like', '`' . $table . '`.' . $this->allField[$col['data']], $col['search']['value']]);
                }

            }

        $query->andWhere($fltr);


        $this->load($params);

        if ($export) {
            return $query;
        }

        $this->currentData = $query->count();

        $query->limit($lang)->offset($start);
        return $query;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function searchDataTable($params)
    {
        $data = $this->searchData($params);
        return Json::encode([
            "draw" => isset ($params['draw']) ? intval($params['draw']) : 0,
            "recordsTotal" => intval($this->allData),
            "recordsFiltered" => intval($this->currentData),
            "data" => $this->renderData($data, $params),
        ]);
    }

    /**
     * @param $query Query
     * @param $params
     * @return array
     */
    public function renderData($query, $params)
    {
        $out = [];

        /** @var self $model */
        foreach ($query->all() as $model) {

            $out[] = array_merge(ArrayHelper::toArray($model), [
                'action' => $model->action,
                'customerName' => $model->customerName,
              ]);

        }
        return $out;
    }
}
