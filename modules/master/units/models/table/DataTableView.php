<?php
/**
 * Created by .
 * User: wisard17
 * Date: 5/9/2017
 * Time: 4:32 PM
 */

namespace app\modules\master\units\models\table;


use app\smartadmin\assets\plugins\DataTableAsset;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\View;

/**
 * Class DataTableView
 * @property string jsonData
 * @property string jsonDataTable
 * @package app\modules\report\models
 */
class DataTableView extends Widget
{
    /**
     * @var array the HTML attributes for the container tag of the list view.
     * The "tag" element specifies the tag name of the container element and defaults to "div".
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];

    public $columns;

    /** @var  Model */
    public $model;

    public $request;

    public $ajaxUrl;


    /**
     * Initializes the view.
     */
    public function init()
    {
        parent::init();

        if ($this->columns === null) {
            throw new InvalidConfigException('The "columns" property must be set.');
        }

        if ($this->model === null) {
            throw new InvalidConfigException('The "model" property must be set.');
        }

        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
    }

    /**
     * Runs the widget.
     */
    public function run()
    {
        $assetTable = DataTableAsset::register($this->view);
        echo $this->loadTable();
        $this->runJs();
    }

    /**
     * @return string
     */
    public function loadTable()
    {
        $content = '';

        return Html::tag('table', "",
            ['class' => "table table-responsive", 'width' => "100%", 'id' => $this->options['id']]);
    }


    /**
     * @return array
     */
    protected function loadColumnHeader()
    {
        $out = [];
        foreach ($this->columns as $column) {
            $showD = $column == '' ? 'details-control' : '';
            $out[] = [
                'title' => $this->model->getAttributeLabel($column),
                'data' => "$column",
                'className' => $showD,
            ];
        }

        return $out;
    }

    /**
     * @return string
     */
    protected function loadColumnFilter()
    {
        $out = '<tr>';
        foreach ($this->columns as $column) {
            if (!in_array($column, [
                'actions',
                'followup',
                'status',
            ])) {
                $out .= "<th class='hasinput' rowspan='1' colspan='1'><input type='text' class='' placeholder=' Filter " . $this->model->getAttributeLabel($column) . "' /></th>";
            } else {
                $out .= "<th class='hasinput' rowspan='1' colspan='1'></th>";
            }
        }

        return $out . '</tr>';
    }

    /**
     * All JS
     */
    protected function runJs()
    {

        $csrf = Yii::$app->request->getCsrfToken();

        $loadingHtml = '<div align="center"><i class="fa fa-spinner fa-pulse fa-4x fa-fw margin-bottom"></i></div>';

        $ajaxUrl = Url::to(['index',
            '_csrf' => $csrf,
        ]);

        $idTable = $this->options['id'];

        $columns = Json::encode($this->loadColumnHeader());

        $colFilter = $this->loadColumnFilter();

        $detailUrl = Url::to(['detail']);



        $jsScript = <<< JS
var renderdata = (function () {
    // var dataSet = ;
    var domElement = $('#$idTable');
    var detailurl = "$detailUrl";


    var table = domElement.DataTable({
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
        "t" +
        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        "scrollX": true,
        rowId: "Order_No",
        "processing": true,
        "serverSide": true,
        "ajax": "$ajaxUrl",
        columns: $columns,
        order: [[0, "desc"]],
        "createdRow": function (row, data, index) {
            if (data.classRow !== '')
                $(row).addClass(data.classRow);
        },
        initComplete: function () {

        }

    });

    domElement.on('click', 'tbody .details-control', function () {

        var tr = $(this).closest('tr');
        tr.toggleClass('selected');

        var row = table.row(tr);

        var orderNo = row.data().Order_No;


        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {

            if (row.child() === undefined || row.child().find('div').length < 2) {
                row.child('$loadingHtml');
                $.get(detailurl, {
                    "orderno": orderNo,
                    "_csrf": '$csrf'

                }, function (dataresponse, status, xhr) {
                    if (status === "success")
                        row.child(dataresponse);
                    if (status === "error")
                        alert("Error: " + xhr.status + ": " + xhr.statusText);
                });
            }
            row.child.show();

            tr.addClass('shown');
        }
    });

    return {
        table: table
    };
})();        

JS;


        $this->view->registerJs($jsScript, View::POS_READY, 'runfor_' . $this->options['id']);
    }


}