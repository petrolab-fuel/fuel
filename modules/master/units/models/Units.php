<?php
/**
 * Created by PhpStorm.
 * User: MY PC
 * Date: 2/7/2018
 * Time: 11:54 AM
 */

namespace app\modules\master\units\models;
use yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\modules\master\units\models\SamplingPoint;
use app\modules\master\customers\models\Customers;
use app\modelsDB\Unit;
use app\smartadmin\ActiveForm;
class Units extends Unit
{

    public $customer_name;
        /**
         * @inheritdoc
         */

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(),[
            'customer_name' => Yii::t('app', 'Customer Name'),
        ]);
    }

    public function getAction()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => "btn btn-default dropdown-toggle", 'data-toggle' => "dropdown", 'aria-expanded' => "false"
            ]) . Html::tag('ul',
                Html::tag('li', Html::tag('a', '<i class="fa fa-caret-square-o-right"></i> view', ['href' => Url::toRoute(['/master/units/add/view', 'id' => $this->id])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-edit"></i> Edit', ['href' => Url::toRoute(['/master/units/add/edit', 'id' => $this->id])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-window-close"></i> Delete',
                    ['href' => Url::toRoute(['/master/units/add/del', 'id' => $this->id]),
                        'data-method' => 'post',
                        'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
                    ]))
                , ['class' => "dropdown-menu"]), ['class' => "btn-group"]);
    }


//    public function getCategoryName()
//    {
//        return $this->category->name;
//    }

    public function load($data, $formName = null)
    {
        $data = $data == null ? [] : $data;
        if (isset($data['SamplingPoint'])) {

            foreach ($data['SamplingPoint'] as $idx => $datum) {
                $expIdx = explode('_', $idx);
                if (sizeof($expIdx) > 1)
                    if (!is_numeric($expIdx[0])) {
                        $this->_samplPointArray[$idx] = new SamplingPoint($datum);
                    } else {
                        $item =SamplingPoint::findOne(['unit_id' => $expIdx[0], 'id' => $expIdx[1]]);
                        $item->load(['Data' => $datum], 'Data');
                        $item->actionId = isset($datum['actionId']) ? $datum['actionId'] : null;
                        $this->_samplPointArray[$idx] = $item;
                    }
            }
        }
        $ld = parent::load($data, $formName);
        return $ld;
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $n=0;
        $outParent = parent::save($runValidation, $attributeNames);
        $out = true;
        foreach ($this->getAllSmplPoint() as $item) {
            $item->unit_id = $this->id;
            if (!$item->isNewRecord && $item->actionId == 'del') {
                $simpan  = $item->delete();
            } else {
                $simpan = $item->save();
            }
            $out = $out && $simpan;
            // $out=$simpan;
            $n++;
        }

        return $outParent && $out;
    }

    public $_smplPoint;
    public function getSmplPoint()
    {
        if ($this->_smplPoint == null)
            $this->_smplPoint = $this->hasMany(SamplingPoint::className(), ['unit_id' => 'id']);
        return $this->_smplPoint;
    }

    public function getCustomerName()
    {
        return $this->customer->name;
    }

    public function getName($id)
    {
        $model=Customers::findOne(['customer_id'=>$id]);
        return $model;
    }

//    public function getCatArray()
//    {
//       $out='';
//        $cats=Category::find()->all();
//        $out= ArrayHelper::map( $cats,'id','name');
//        return $out;
//
//    }
    public function getCustArray()
    {
        $out='';
        $cust=Customers::find()->all();
       $out=ArrayHelper::map($cust,'customer_id','name');
//          foreach($cust as $cus){
//              $out.=$cus->customer_id;
//          }
        return $out;
    }

//    public function getCategori()
//       {
//           $result='';
//            $type=$this->type_of_equipment;
//
//            $travo=$this->operating_voltage;
//            $travo=(float)$travo;
//            $penetu = PenentuanCategory::find()->where(['type_of_equipment'=>$type])
//               ->all();
//           foreach ($penetu as $category) {
//               $vlt = str_replace(',', '.', $category->voltage);
//               $vlt = str_replace('≤', '<=', $vlt);
//               $vlt = str_replace('≥', '>=', $vlt);
//               $a = '';
//               $exp = explode('-', $vlt);
//                  if (sizeof($exp) < 2) {
//                      $p = '$a = ' . $travo . $vlt . ' ? true : false;';
//                      eval($p);
//                    }else {
//                       $min = $exp[0] < $exp[1] ? $exp[0] : $exp[1];
//                       $max = $exp[0] > $exp[1] ? $exp[0] : $exp[1];
//                       $a = $travo <= $min && $travo >= $max ? true : false;
//                      }
//                 if($a==true){
//                        $result=$category->category_id;
//                     }
//                 // /$result.=' '.$travo.' '.$type.' '.$category->voltage.' '.$a.'<br>';
//           }
//          return $result;
//       }
    public function getAksi()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => "btn btn-default dropdown-toggle", 'data-toggle' => "dropdown", 'aria-expanded' => "false"
            ]) . Html::tag('ul',
//                Html::tag('li', '<a>Edit</a>') .
                Html::tag('li', '<a data-action="delate_row">Delete</a>')

                , ['class' => "dropdown-menu"]), ['class' => "btn-group"]);
    }
    public $_samplPointArray;
    public $n=0;

    public function getSmplPointArray()
    {
        if ($this->_samplPointArray == null)
            $this->_samplPointArray = $this->getSmplPoint();

        return $this->_samplPointArray;
    }

    /**
     * @param array|PIC $pic
     */
    public function setSmplPointArray($smplPoint)
    {
        $this->_samplPointArray = $smplPoint;
    }

    /**
     * @return PIC[]|array|\yii\db\ActiveQuery
     */
    public function getAllSmplPoint()
    {
        $smplPoint = $this->smplPointArray;
        return $smplPoint == null ? [0 => new SamplingPoint()] : $smplPoint;
    }
    public function renderTable($form)
    {
        $i=0;
        $out='';
        // if $this->attentions->isNewRe
        foreach ($this->allSmplPoint as $att) {
            # code...
            if ($i==0){
                $out .= Html::tag('thead', Html::tag('tr',
//                    Html::tag('th', '') .
                    Html::tag('th', 'action') .
                    Html::tag('th','Name', ['style' => 'width:95%;']) .'<tbody>'

                ));
            }
            $att->idName = $att->id == null ? 'newrow_' . $i : $att->unit_id . '_' . $att->id;
            $out .= Html::tag('tr',
                Html::tag('td', $this->aksi) .
                Html::tag('td', $form->field($att, 'name', ['template' => '<label class="input">{input}</label>{error}'])) ,
                ['data-id' => $att->idName]
            );

            $i++;
        }
        return Html::tag('div', Html::tag('table', $out . '</tbody>', ['class' =>
            'table table-bordered table-striped table-condensed table-hover smart-form has-tickbox'
        ]), ['class' => 'table-responsive','id'=>'tbl']);
    }


    public function renderRowAjax($rowTo = 0)
    {
        $form = ActiveForm::begin();
        $att = new SamplingPoint();

        $att->idName = 'newrow_' . $rowTo;

        $out = Html::tag('tr',
            Html::tag('td', $att->action) .
            Html::tag('td', $form->field($att, 'name', ['template' => '<label class="input">{input}</label>{error}'])) ,

            ['data-id' => $att->idName]
        );


        return [
            'data' =>$out,
            'jsAdd' => ArrayHelper::toArray($form->attributes),
        ];
    }
}