<?php


namespace app\modules\master\models\search;
use Yii;
use app\modules\master\models\Matrixs;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;


class SearchMatrix extends Matrixs
{
    public $allField=[
        'id' => 'ID',
        'parameter_id' =>  'parameter_id',
        'condition1' => 'condition1',
        'condition2' => 'condition2',
        'condition3' => 'condition3',
        'condition4' => 'condition4',
        'condition5' => 'condition5',
        'recom1' => 'recom1',
        'rekom2' => 'rekom2',
        'rekom3' => 'rekom3',
        'rekom4' => 'rekom4',
        'tahun'=>'tahun',
        'jenis'=>'jenis',
        'satuan'=>'satuan',
        'method'=>'method',
        'namaParameter'=>'parameter_id',
    ];

    public function ordering($params)
    {
        $ncol = isset($params['order'][0]['column']) ? $params['order'][0]['column'] : 0;
        $col = (isset($params['columns'][$ncol]) && array_key_exists($params['columns'][$ncol]['data'], $this->allField)) ?
            $this->allField[$params['columns'][$ncol]['data']] : '';
        $argg = isset($params['order'][0]['dir']) ? $params['order'][0]['dir'] : 'asc';
        $table = self::tableName();

//        if (isset($params['columns'][$ncol]['data']) && $params['columns'][$ncol]['data'] === 'customerName') {
//            $col = 'Name';
//            $table = 'tbl_customers';
//        }

        return $col !== '' ? "$table.$col $argg " : '';
    }


    public $allData;

    public $currentData;

    /**
     * @param Query $query
     * @return int
     */
    public function calcAllData($query)
    {
        return $this->allData === null ? $query->count() : $this->allData;
    }

    /**
     * @param Query $query
     * @param $params
     * @return Query
     */
    public static function defaultFilterByUser($query, $params = '')
    {
        $user = Yii::$app->user->identity;

        if ($user->ruleAccess === -1) {
            return $query;
        }


        return $query;
    }

    /**
     * @param $params
     * @param bool $export
     * @param bool $join
     * @return $this|\yii\db\ActiveQuery|Query
     */
    public function searchData($params, $export = false, $join = true)
    {

        $odr = $this->ordering($params);

//        $query = self::find();
        $query = self::find()->joinWith(['parameter']);

        if (!$join) {
            $query = self::find();
        }

        $query = self::defaultFilterByUser($query, $params);

//        $query->andWhere("type is null");

        $query->orderBy($odr);

        $start = isset($params['start']) ? $params['start'] : 0;
        $lang = isset($params['length']) ? $params['length'] : 10;

        $this->allData = $this->calcAllData($query);

        $fltr = '';
        if (isset($params['columns']))
            foreach ($params['columns'] as $col) {
                if (isset($params['search']) && $params['search']['value'] !== '') {
                    $lst[] = $col['data'];
                    if (array_key_exists($col['data'], $this->allField) &&
                        !array_key_exists($col['data'], $lst) && $this->allField[$col['data']] != '') {
                        $fltr .= $fltr !== '' ? ' or ' : '';
                        $fltr .= ' `' . self::tableName() . '`.' . $this->allField[$col['data']] . " like '%" . $params['search']['value'] . "%' ";
                    }
                    if ($col['data'] === 'namaParameter') {
                        $fltr .= $fltr !== '' ? ' or ' : '';
                        $fltr .= ' `parameter`.nama ' . " like '%" . $params['search']['value'] . "%' ";
                    }
                }
                if ($col['searchable'] === 'true' && $col['search']['value'] !== '' &&
                    array_key_exists($col['data'], $this->allField) && $this->allField[$col['data']] != '') {
                    if ($col['data'] === 'namaParameter') {
                        $query->andFilterWhere(['like', '`parameter`.nama', $col['search']['value']]);
                    } else {
                        $query->andFilterWhere(['like', '`' . self::tableName() . '`.' . $this->allField[$col['data']], $col['search']['value']]);
                    }
                }
            }

        $query->andWhere($fltr);

        $this->load($params);

        if ($export) {
            return $query;
        }

        $this->currentData = $query->count();

        $query->limit($lang)->offset($start);
        return $query;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function searchDataTable($params)
    {
        $data = $this->searchData($params);
        return Json::encode([
            'draw' => isset ($params['draw']) ? intval($params['draw']) : 0,
            'recordsTotal' => intval($this->allData),
            'recordsFiltered' => intval($this->currentData),
            'data' => $this->renderData($data, $params),
        ]);
    }

    /**
     * @param $query Query
     * @param $params
     * @return array
     */
    public function renderData($query, $params)
    {
        $out = [];

        /** @var self $model */
        foreach ($query->all() as $model) {
            $out[] = array_merge(ArrayHelper::toArray($model), [
                'actions' => $model->actions,
                'namaParameter'=>$model->namaParameter,
            ]);

        }
        return $out;
    }
}