<?php
/**
 * Created by PhpStorm.
 * User: Feri
 * Date: 10/23/2019
 * Time: 10:48
 */

namespace app\modules\master\models;


use app\modelsDB\Customer;
use yii\helpers\Url;
use yii\helpers\Html;


/**
 *
 * @property string $actions
 */
class Customers extends Customer
{
    /**
     * @return string
     */
    public function getActions()
    {
        return
            Html::tag('a', '<i class="fa fa-external-link"></i> View', ['class' => 'btn btn-default btn-xs',
                'title' => 'View Customer', 'href' => Url::to(['/master/customer/view', 'id' => $this->customer_id]),]) .
            Html::tag('a', '<i class="fa fa-edit"></i> Edit', ['class' => 'btn btn-default btn-xs',
                'title' => 'Edit Customer', 'href' => Url::to(['/master/customer/edit', 'labNo' => $this->Lab_No])]) .
            Html::tag('a', '<i class="fa fa-trash-o"></i> Delete', ['class' => 'btn btn-default btn-xs',
                'title' => 'Delete Customer', 'href' => Url::to(['/master/customer/del', 'id' => $this->Lab_No]), 'data-method' => 'post',
                'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',]);
//           . '<a class="btn btn-default btn-xs" title="View History" href='.Url::to(["/master/transaction/view/$this->Lab_No","page"=>1]).' style=""><i class="fa fa-eye"></i></a>'
            // Html::tag('a','<i class="fa fa-search"></i>', ['class'=>'btn btn-primary btn-xs','title'=>'View Detail']).' '
            // Html::tag('a','<i class="fa fa-share"></i>', ['class'=>'btn btn-primary btn-xs','title'=>'Follow Up','onclick'=>"openFollow('$this->Lab_No')"])

    }



}