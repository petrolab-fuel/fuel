<?php
namespace app\modules\master\models\data;
use app\modelsDB\Spesifikasi;
use yii\helpers\Url;
use yii\helpers\Html;
use app\component\DataTableAjax;

/**
 *
 * @property-read mixed $specParameter
 * @property-read string $specParamaterForm
 * @property-read mixed $actions
 */

class Specification extends Spesifikasi{

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(),[
            'name'=>'Spesifikasi',
            'note'=>'No SK Migas'
        ]);
    }

    public function getActions(){
        return Html::tag('div',
        Html::tag('a','<i class="fa fa-edit"> ',['href'=>Url::to(['/master/spesifikasi/edit','id'=>$this->id]),'class'=>'btn btn-default btn-sm']),
        [
            'class'=>'group'
        ]);
    }
    public function renderAjax($params){
        $q=self::find();
        $total=$q->count('id');
        if(isset($params['columns'])){
            foreach($params['columns'] as $col){
                if (isset($params['search']['value']) && $params['search']['value'] != '') {

                }
                if ($col['searchable'] == 'true' && $col['search']['value'] != '') {

                }
            }
        }
        if (isset($params['order'])) {
            $sort = [];
            foreach ($params['order'] as $item) {
                $arr = isset($item['dir']) ? $item['dir'] : 'asc';
                $col = isset($item['column']) && isset($param['columns'][$item['column']]) ? $param['columns'][$item['column']]['data'] : '0';
                if ((new self())->hasAttribute($col)) {
                    $sort = self::tableName() . '.' . $col . ' ' . $arr;
                }
                // if ($col == 'start') {
                //     $sort = self::tableName() . '.date_start ' . $arr;
                // }
                // if ($col == 'end') {
                //     $sort = self::tableName() . '.date_end ' . $arr;
                // }
                // if ($col == 'hour') {
                //     $q->addSelect('*');
                //     $q->addSelect(['timediff(date_start,date_end) deff_h']);
                //     $sort = 'deff_h ' . $arr;
                // }
                // if ($col == 'projectTask') {
                //     $sort = self::tableName() . '.project_id ' . $arr;
                // }
            }
            $q->orderBy($sort);
        }
        return DataTableAjax::renderAjax($q, ['allData' => $total]);
    }

    /**
     * @var
     */
    public $_specParameter=[];

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecParameter(){
        if($this->_specParameter==[]){
            $this->_specParameter=$this->hasMany(SpecParameters::className(),['spesifikasi_id'=>'id']);
        }
        return $this->_specParameter;
    }
    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getSpecParamaterForm()
    {
        return (new SpecParameters())->formName();
    }
}
?>