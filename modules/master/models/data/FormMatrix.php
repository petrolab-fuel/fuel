<?php

namespace app\modules\master\models\data;

use app\component\ArrayHelper;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use Yii;

class FormMatrix extends Matrixs
{
    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {

            if (parent::save($runValidation, $attributeNames)) {
                foreach ($this->parameterMatrix as $item) {
                    if ($item->actionId == 'del') {
                        $item->delete();
                    } else {
                        $item->matrix = $this->id;
                        if (!$item->save()) {
                            $this->addError('parameterMatrix', 'kesalahan pada  :' . ArrayHelper::toString($item->errors));
                            $transaction->rollBack();
                            return false;
                        }
                    }
                }
                $transaction->commit();
                return true;
            }
            $transaction->rollBack();
            return false;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    public function load($data, $formName = null)
    {
        $data = $data == null ? [] : $data;
        $ld = parent::load($data, $formName);
        if (isset($data['ParameterMatrix'])) {
            foreach ($data['ParameterMatrix'] as $idx => $datum) {
                $pca = explode('_', $idx);
                $matrix = isset($pca[0]) ? $pca[0] : 0;
                $parameterId = isset($pca[1]) ? $pca[1] : 0;
                $item = ParameterMatrix::findOne(['matrix' => $matrix, 'parameter_id' => $parameterId]);
                if ($item == null) {
                    $item = new ParameterMatrix($datum);
                }
                $item->load(['Data' => $datum], 'Data');
                $item->actionId = isset($datum['actionId']) ? $datum['actionId'] : null;
                $this->_parameterMatrix[$idx] = $item;
            }
        }
        return $ld;
    }

    /**
     * @param $form
     * @return string
     */
    public function renderParameterMatrix($form){
        $out='<thead>
                <tr>
                    <th style="width:5%">Option</th>
                    <th>Name</th>
                     <th style="width:10%">Min</th>
                      <th style="width:10%">Max</th>
                      <th style="width:10%">Show PDF</th>       
                 </tr>
                </thead><tbody>';
                foreach ($this->parameterMatrix as $idx=>$item){
                    $item->idName = $item->id == '_' ? 'newrow_' . $idx : $item->id;
                    $out.=Html::tag('tr',
                        Html::tag('td',$item->action).
                        Html::tag('td',$form->field($item,'parameter_id')->dropDownList($item->parameter_id==null?[]:
                            [$item->parameter_id=>$item->parameterName],
                            ['class'=>'form-control select2ajax',
                                'data-url'=>Url::to(['/master/parameters/parameter-list'])])->label(false)).
                        Html::tag('td',$form->field($item,'min')->label(false)).
                        Html::tag('td',$form->field($item,'max')->label(false)).
                        Html::tag('td',$form->field($item,'B')->label(false))
                        ,['data-id'=>$item->idName]);
                }
                return Html::tag('table',$out.'</tbody>',['class'=>'table','width'=>'100%']);
    }

    /**
     * @param $rowTo
     * @return array
     */
    public function renderAjaxParameterMatrix($rowTo=0){
        $form = ActiveForm::begin([
            'fieldConfig' => [
                'template' => '<label class="input">{input}</label>{error}',
            ]
        ]);
        $item=new ParameterMatrix();
        $item->idName='newrow_' . $rowTo;
        $out=Html::tag('tr',
            Html::tag('td',$item->action).
            Html::tag('td',$form->field($item,'parameter_id')->dropDownList($item->parameter_id==null?[]:
                [$item->parameter_id=>$item->parameterName],
                ['class'=>'form-control select2ajax',
                    'data-url'=>Url::to(['/master/parameters/parameter-list'])])->label(false)).
            Html::tag('td',$form->field($item,'min')->label(false)).
            Html::tag('td',$form->field($item,'max')->label(false)).
            Html::tag('td', $form->field($item, 'B')->label(false))
            ,['data-id'=>$item->idName]);

        return [
            'data' => $out,
            'jsAdd' => ArrayHelper::toArray($form->attributes),
        ];
    }
}
