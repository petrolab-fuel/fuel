<?php

/**
 * Created by
 * User: Wisard17
 * Date: 08/09/2018
 * Time: 10.02 AM
 */

namespace app\modules\master\models\data;


use app\smartadmin\assets\plugins\DataTableAsset;
use app\smartadmin\assets\plugins\FileDownloadAssets;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/**
 * Class DataTableView
 * @package app\modules\master\models\data
 */
class DataTableView extends Widget
{
    /**
     * @var array the HTML attributes for the container tag of the list view.
     * The "tag" element specifies the tag name of the container element and defaults to "div".
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];

    /** @var */
    public $columns;

    /** @var  Model */
    public $model;

    /** @var */
    public $request;
    /** @var */
    public $ajaxUrl;


    /**
     * Initializes the view.
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if ($this->columns === null) {
            throw new InvalidConfigException('The "columns" property must be set.');
        }

        if ($this->model === null) {
            throw new InvalidConfigException('The "model" property must be set.');
        }

        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
    }

    /**
     * Runs the widget.
     */
    public function run()
    {
        $assetTable = DataTableAsset::register($this->view);
        $asserJSDownload = FileDownloadAssets::register($this->view);
        echo $this->loadTable();
        $this->runJs();
    }

    /**
     * @return string
     */
    public function loadTable()
    {
        $content = '';

        return Html::tag(
            'table',
            $content,
            ['class' => 'table table-responsive', 'width' => '100%', 'id' => $this->options['id']]
        );
    }


    /**
     * @return array
     */
    protected function loadColumnHeader()
    {
        $out = [];
        foreach ($this->columns as $column) {
            $showD = $column == '' ? 'details-control' : '';
            $out[] = [
                'title' => $this->model->getAttributeLabel($column),
                'data' => (string)$column,
                'name' => (string)$column,
                'className' => $showD,
                'orderable' => !in_array($column, ['actions', 'followup', 'status', 'statusPublish']),
            ];
        }

        return $out;
    }

    /**
     * @return string
     */
    protected function loadColumnFilter()
    {
        $out = '';
        foreach ($this->columns as $column) {
            $ipt = '';
            if (!in_array($column, ['actions', 'followup',])) {
                if (in_array($column, ['sampleDate', 'receiveDate', 'reportDate',], true)) {
                    $ipt = Html::input('text', "filter_$column", '', [
                        'placeholder' => ' Filter ' . $this->model->getAttributeLabel($column),
                        'class' => 'form-control datejuifilter', 'autocomplete' => 'off',
                        'style' => 'padding: 1px;height: 100%;width: 100%;',
                    ]);
                } elseif ($column === 'status') {
                    $ipt = '<select name="filter_' . $column . '"><option value="" selected>Select All</option><option value="N">Normal</option><option value="B">Attention</option><option value="C">Urgent</option><option value="D">Severe</option></select>';
                } elseif ($column === 'publishData') {
                    $ipt = '<select name="filter_' . $column . '"><option value="" selected>Select All</option><option value=1>Publish</option><option value=0>Unpublish</option></select>';
                } elseif ($column === 'typeCustomer') {
                    $ipt = '<select name="filter_' . $column . '"><option value="" selected>Select All</option><option value=UT>UT</option><option value=PAMA>PAMA</option><option value=OTHERS>OTHERS</option></select>';
                } else {
                    $ipt = Html::input('text', "filter_$column", '', [
                        'placeholder' => ' Filter ' . $this->model->getAttributeLabel($column),
                        'class' => 'form-control',
                        'style' => 'padding: 1px;height: 100%;width: 100%;',
                    ]);
                }
            }

            $out .= Html::tag('td', $ipt);
        }

        return Html::tag('tr', $out);
    }

    public $url_ajax;

    /**
     * All JS
     */
    protected function runJs()
    {

        $csrf = Yii::$app->request->getCsrfToken();

        $loadingHtml = '<div align="center"><i class="fa fa-spinner fa-pulse fa-4x fa-fw margin-bottom"></i></div>';

        $ajaxUrl = $this->url_ajax == null ? Url::to(['index', '_csrf' => $csrf], true) : $this->url_ajax;

        $idTable = $this->options['id'];

        $columns = Json::encode($this->loadColumnHeader());

        $colFilter = $this->loadColumnFilter();

        $detailUrl = Url::to(['detail']);

        $urlFollowup = Url::toRoute(['/reports/default/followup']);
        $urlExportExcel = isset($this->options['excel-export']) ? $this->options['excel-export'] : Url::toRoute(['/reports/default/export-excel']);

        $order = isset($this->options['order']) ? $this->options['order'] : "[0, 'desc']";

        // Select2Asset::register($this->view);

        $jsScript = <<< JS
var delay2 = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

var renderdata = (function () {

    function alert_notif(res) {
        $.sound_on = false;
        var color = res.status === 'success' ? '#659265' : '#C46A69';
        var icon = res.status === 'success' ? 'fa-check' : 'fa-times';
        if (res.status === 'warning') {
            color = '#f0ad4e';
            icon = 'fa-warning';
        }
        $.smallBox({
            title: res.status,
            content: res.message,
            color: color,
            iconSmall: "fa " + icon + " fadeInRight animated",
            timeout: res.status === 'success' ? 4000 : 15000
        });
    }

    // var dataSet = ;
    var domElement = $('#$idTable');
    var detailurl = "$detailUrl";
    var domOuttable = domElement.parent().parent().parent();
    var filter = $('#form-filter');
    
    var table = domElement.DataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'btn-add col-sm-3 col-xs-12 hidden-xs'>i<'col-sm-3 pull-right col-xs-12 hidden-xs'>>" +
                "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'rl><'col-xs-12 col-sm-6'p>>",
            scrollX: true,
            //"scrollY": "400px",
            // "scrollCollapse": true,
            stateSave: true,
            stateSaveParams: function (settings, data) {
                data.filter = table.ajax.params().filter;
                },
            rowId: 'id',
            processing: true,
            serverSide: true,
            ajax: {
                url:"$ajaxUrl",
                data:function(d){
                    d.filter = {
                                r_start: filter.find('[name=recv_date_begin]').val(),
                                r_end: filter.find('[name=recv_date_end]').val(),
                                rp_start: filter.find('[name=rpt_date_begin]').val(),
                                rp_end:  filter.find('[name=rpt_date_end]').val(),
                                // group: filter.find('[name=group_log]').val(),
                                // customer_name: filter.find('[name=customer_name]').val(),
                                // component_name: filter.find('[name=component_name]').val(),
                            };
                }},
            columns: $columns,
            order: [$order],
            createdRow: function (row, data, index) {
                if (data.classRow !== '')
                    $(row).addClass(data.classRow);
            }
            ,
            initComplete: function () {
                delay2(function () {
                    var colfilter = $('$colFilter');
                    colfilter.find('.datejuifilter').datepicker({   
                        todayHighlight: true,                    
                        format: "yyyy-mm-dd",                      
                    });
                    // Restore state
                    var state = table.state.loaded();
                    if (state) {
                         filter.find('[name=recv_date_begin]').val(state.filter.r_start);
                         filter.find('[name=recv_date_end]').val(state.filter.r_end);
                          filter.find('[name=rpt_date_begin]').val(state.filter.rp_start);
                         filter.find('[name=rpt_date_end]').val(state.filter.rp_end);
                        //  filter.find('[name=group_log]').val(state.filter.group);
                        //  filter.find('[name=customer_name]').val(state.filter.customer_name);
                        //  filter.find('[name=component_name]').val(state.filter.component_name);
                        $.each(state.filter, function(r, d) {
                            if (d.length > 0) {
                                filter.closest('#wid-id-2').removeClass('jarviswidget-collapsed').children('div').slideDown('fast');        
                            }
                        });
                        
                        table.columns().eq(0).each(function (colIdx) {
                            var colname = table.column(colIdx).dataSrc();
                            var colSearch = state.columns[colIdx].search;
                            if (colSearch.search) {
                                colfilter.find('[name=filter_' + colname + ']').val(colSearch.search);
                            }
                        });
                    }
                    domOuttable.find('thead').eq(0).append(colfilter);
                    domOuttable.find('table').attr('style', 'min-height: 200px;');
                    //domOuttable.find('.dataTables_scrollBody').attr('style', 'position: relative; width: 100%;');
                }, 1000);
            }

        });



     $('[data-action=update_status]').click(function (e) {
        e.preventDefault();
        e.stopPropagation();

        var elm = $(this);
        update_status(elm);
    });

    domOuttable.delegate('[data-action=update_status]', 'click', function (a) {
        a.preventDefault();
        a.stopPropagation();
        let elm = $(this);
        
        update_status(elm);
    });
    
    function update_status(elm) {
        let param = table.ajax.params();
        param._csrf = '$csrf';
        let urlparm = $.param(param);
        let icon = elm.find('i');
        icon.addClass('fa-spin');
        $.ajax({
            url: elm.attr('href'),
            method: 'post',
            data: param,
            error: function (response) {
                alert_notif({
                    status: "error",
                    message: 'tidak bisa diupdate data terlalu besar filter kembali dan coba lagi. '
                });
                icon.removeClass('fa-spin');
            },
            success: function (response) {
                // console.log(response);
                $.each(response.data, function (i, v) {
                    var row = table.row('#' + i);
                    if (row.data() !== undefined) {
                        console.log(v);
                        row.data().statusPublish = v;
                        row.invalidate();
                    }
                });
                icon.removeClass('fa-spin');
                alert_notif(response);
            },

        });
        return false;
    }

     

    filter.find('#generate').on('click', function () {
        table.ajax.reload();
    });
    
    domOuttable.find('thead').eq(0).delegate('td input[type=text]', 'change', function () {
        var self = this;
        delay2(function () {
            table
                .column($(self).parent().index() + ':visible')
                .search(self.value)
                .draw();
        }, 1000);
    });

    domOuttable.find('thead').eq(0).delegate('td select', 'change', function () {
        var self = this;
        delay2(function () {
            table
                .column($(self).parent().index() + ':visible')
                .search(self.value)
                .draw();
        }, 1000);
    });

    domElement.on('click', 'tbody .details-control', function () {

        var tr = $(this).closest('tr');
        tr.toggleClass('selected');

        var row = table.row(tr);

        var orderNo = row.data().Order_No;

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            if (row.child() === undefined || row.child().find('div').length < 2) {
                row.child('$loadingHtml');
                $.get(detailurl, {
                    "orderno": orderNo,
                    "_csrf": '$csrf'

                }, function (dataresponse, status, xhr) {
                    if (status === "success")
                        row.child(dataresponse);
                    if (status === "error")
                        alert("Error: " + xhr.status + ": " + xhr.statusText);
                });
            }
            row.child.show();

            tr.addClass('shown');
        }
    });

    var modalvar = $('#modal-temp');
    var td = null;
    domElement.on('click', 'tbody [data-action=update_followup]', function (a) {
        a.preventDefault();
        var elm = $(this);
        var tr = elm.closest('tr');
        td = elm.closest('td');

        var row = table.row(tr);
        var labNo = row.data().Lab_No;

        modalvar.removeData('bs.modal');
        modalvar.modal({remote: '$urlFollowup' + '?labNo=' + labNo});
        modalvar.modal('show');
    });

    modalvar.delegate('[data-action=submit_update_followup]', 'click', function (a) {
        a.preventDefault();
        a.stopPropagation();
        var elm = $(this);

        var form = modalvar.find('form');

        var cll = table.cell(td);
        var data = $(form).serializeArray();

        $.ajax({
            url: $(form).attr('action'),
            method: $(form).attr('method'),
            data: data,
            error: function (response) {
                alert_notif({status: "error", message: 'tidak bisa save'});
                modalvar.modal('hide');
            },
            success: function (response) {
                console.log(response);
                cll.data(response.button);
                alert_notif(response);
                if (response.status === 'success') {
                    modalvar.modal('hide');
                }
            },

        });

    });

    modalvar.delegate('[data-action=export_excel]', 'click', function (a) {
        a.preventDefault();
        a.stopPropagation();
        var elm = $(this);

        var form = modalvar.find('form');
        var data = $(form).serializeArray();

        var param = table.ajax.params();
        param._csrf = '$csrf';
        var urlparm = $.param(param);

        $.fileDownload($(form).attr('action') + '?' + urlparm, {
            preparingMessageHtml: "We are preparing your report, please wait...",
            failMessageHtml: "There was a problem generating your report, the report is to big, filter first and please try again.",
            httpMethod: $(form).attr('method'),
            data: data
        });
        modalvar.modal('hide');
        return false;
    });

    $('[data-action=admin_excel]').click(function (a) {
        a.preventDefault();
        a.stopPropagation();
        var elm = $(this);

        modalvar.removeData('bs.modal');
        modalvar.modal().find('.modal-content').load(elm.attr('href'), function () {
            var modal = $(this);
            modal.find('.select2param').select2({width: "100%",});
        });
        modalvar.modal('show');
    });
    
    $('[data-action=admin_pdf]').click(function (a) {
        a.preventDefault();
        a.stopPropagation();
        let elm = $(this);

        let param = table.ajax.params();
        console.log(param);
        window.open(elm.attr('href') + '?json=' + JSON.stringify(param), '_blank');
    });
    
    $('[data-action=excel_default]').click(function (a) {
        a.preventDefault();
        a.stopPropagation();
        var elm = $(this);
        console.log(elm.attr('href'));

        var param = table.ajax.params();
        param._csrf = '$csrf';
        // param.format_lama = 1;
        var urlparm = $.param(param);
        console.log(param);
        // $.ajax({
        //     url: elm.attr('href'),
        //     type: 'GET',
        //     data: param,
        //     error: function (response) {
        //         alert_notif({status: "error", message: 'tidak bisa save'});
        //         // modalvar.modal('hide');
        //     },
        //     success: function (response) {
        //         console.log(response);
        //         // cll.data(response.button);
        //         // alert_notif(response);
        //         // if (response.status === 'success') {
        //         //     modalvar.modal('hide');
        //         // }
        //     },

        // });
        $.fileDownload(elm.attr('href'), {
            preparingMessageHtml: "We are preparing your report, please wait...",
            failMessageHtml: "There was a problem generating your report, the report is to big, filter first and please try again.",
            // httpMethod: 'post',
            data: param,
        });
        return false
    });

    domOuttable.on('click', 'tbody [data-action=update-code]', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var elm = $(this);
        
         update_evalCode(elm);
    });
    

    $('[data-action=update_code]').click(function (e) {
        e.preventDefault();
        e.stopPropagation();

        var elm = $(this);
        // console.log('test');
         update_evalCode(elm);
    });



    function update_evalCode(elm) {
        var param = table.ajax.params();
        param._csrf = '$csrf';
        console.log(param);
        var urlparm = $.param(param);
        var icon = elm.find('i');
        icon.addClass('fa-spin');
        $.ajax({
            url: elm.attr('href'),
            method: 'post',
            data: param,
            error: function (response) {
                alert_notif({
                    status: "error",
                    message: 'tidak bisa diupdate data terlalu besar filter kembali dan coba lagi. '
                });
                icon.removeClass('fa-spin');
            },
            success: function (response) {
                // console.log(response);
                $.each(response.data, function (i, v) {
                    var row = table.row('#' + i);
                    if (row.data() !== undefined) {
                        row.data().status = v;
                        console.log(v)
                        row.invalidate();
                    }
                });
                icon.removeClass('fa-spin');
                alert_notif(response);
            },

        });
        return false;
    }
    domOuttable.on('click', 'tbody [data-action=update-status]', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var elm = $(this);
        update_status(elm);
    });

    $('[data-action=update-status]').click(function (e) {
        e.preventDefault();
        e.stopPropagation();

        var elm = $(this);
        // console.log('test');
         update_status(elm);
    });

    function update_status(elm){
        var param = table.ajax.params();
        param._csrf = '$csrf';
        var urlparm = $.param(param);
        var icon = elm.find('i');
        icon.addClass('fa-spin');
        $.ajax({
            url: elm.attr('href'),
            method: 'post',
            data: param,
            error: function (response) {
                alert_notif({
                    status: "error",
                    message: 'tidak bisa diupdate data terlalu besar filter kembali dan coba lagi. '
                });
                icon.removeClass('fa-spin');
            },
            success: function (response) {
                // console.log(response);
                $.each(response.data, function (i, v) {
                    var row = table.row('#' + i);
                    if (row.data() !== undefined) {
                        row.data().publishData = v;
                        console.log(v)
                        row.invalidate();
                    }
                });
                icon.removeClass('fa-spin');
                alert_notif(response);
            },

        });
        return false;
    }


     domOuttable.on('click', 'tbody [data-action=update_oil]', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var elm = $(this);
        update_oil(elm);
    });
    
    $('[data-action=update_oil]').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        var elm=$(this);
         update_oil(elm);
    })
    
    function update_oil(elm) {
        var param = table.ajax.params();
        param._csrf = '$csrf';
        
        var urlparm = $.param(param);
        var icon = elm.find('i');
        icon.addClass('fa-spin');
        $.ajax({
            url: elm.attr('href'),
            method: 'post',
            data: param,
            error: function (response) {
                 alert_notif({
                    status: "error",
                    message: 'tidak bisa diupdate data terlalu besar filter kembali dan coba lagi. '
                });
                icon.removeClass('fa-spin');
            },
            success: function (response) {
               console.log(response);
                icon.removeClass('fa-spin');
                alert_notif(response);
            },

        });
        return false;
    }

    return {
        "test": function () {
            alert('daa');
        },
        table: table
    };
})();        

JS;


        $this->view->registerJs($jsScript, 4, 'runfor_' . $this->options['id']);
    }
}
