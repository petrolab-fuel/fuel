<?php

namespace app\modules\master\models\data;

use app\modelsDB\Customer;
use app\modelsDB\DataAnalisaBaru;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use Codeception\Exception\ConfigurationException;

/**
 * @property typeCustomer 
 */
class Analisa extends DataAnalisaBaru

{
    public function rules(){
        return array_merge(parent::rules(),[
            [['calcium_ca','zn_zinc','phosphor_p','calcium_ca_code','zn_zinc_code','phosphor_p_code','sodium','sodium_code'],'string', 'max' => 45]
        ]);
    }
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), ['publishData' => 'Publish', 'typeCustomer' => 'Customer Type']);
    }
    public function getActions()
    {
        return Html::tag(
            'div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => 'btn btn-default btn-xs dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-expanded' => 'false'
            ]) . Html::tag(
                'ul',
                // Html::tag('li', Html::tag('a', '<i class="fa fa-caret-square-o-right"></i> view', [
                //     'href' => Url::toRoute(['/customers/default/view', 'id' => $this->CustomerID])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-edit"></i> Edit', [
                    'href' => Url::toRoute(['/master/default/edit', 'id' => $this->id])
                ])) .
                    Html::tag('li', Html::tag('a', '<i class="fa fa-file-pdf-o"></i> pdf', [
                        'href' => Url::toRoute(['/reports/default/pdf', 'lab' => $this->lab_number]), 'target' => '_blank'
                    ])) .
                    Html::tag('li', Html::tag('a', '<i class="fa fa-gear"></i> Update Code', [
                        'href' => Url::toRoute(['/master/default/update-code', 'lab_no' => $this->lab_number]), 'data-action' => 'update-code'
                    ])),
                ['class' => 'dropdown-menu']
            ),
            ['class' => 'btn-group']
        );
    }

    /**
     * Query Nama
     *
     * @return void
     */
    // public function getCustomerName()
    // {
    //     return $this->customers == null ? '' : $this->customers->Name;
    // }
    public function getKeteranganCustomer()
    {
        return $this->customerType == null ? '' : $this->customerType->description;
    }

    public function logo($code)
    {

        if ($code == 'A') {
            return '/img/reports/e_code/a.png';
        }
        if ($code == 'N') {
            return '/img/reports/e_code/a.png';
        }

        if ($code == 'B') {
            return '/img/reports/e_code/b.png';
        }

        if ($code == 'C') {
            return '/img/reports/e_code/c.png';
        }

        if ($code == 'D') {
            return '/img/reports/e_code/d.png';
        }

        if ($code == 'E') {
            return '/img/reports/e_code/e.png';
        }

        return '/img/blank.jpg';
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        $code = $this->eval_code;
        $status = $this->getTextEvalCode($code);
        $label = '';
        if (($code == 'C') || ($code == 'U') || $code == 'D') {
            $label = 'label-danger';
        }
        if ($code == 'B' || $code == 'A') {
            $label = 'label-warning';
        }
        if ($code == 'N') {
            $label = 'label-success';
        }
        if ($code == 'A') {
            $label = 'label-success';
        }
        return $this->eval_code == '' ? '' : Html::tag('span', $status, ['class' => 'label ' . $label]);
    }

    /**
     * @param $code
     * @return string
     */
    public function getTextEvalCode($code)
    {
        $code = strtoupper($code);
        $eval = '';
        if ($code == 'D') {
            $eval = 'SEVERE';
        }
        if (($code == 'C') || ($code == 'U')) {
            $eval = 'URGENT';
        }
        if ($code == 'B' || $code == 'A') {
            $eval = 'ATTENTION';
        }
        if ($code == 'N') {
            $eval = 'NORMAL';
        }
        if ($code == 'A') {
            $eval = 'NORMAL';
        }
        return $eval;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getTypeCustomer()
    {
        return $this->customerType == null ? '' : $this->customerType->description;
    }

    public function ColourLabel($code, $label)
    {
        $out = '';
        $listsColor = $this->global_color();
        $color = isset($listsColor[$code]) ? $listsColor[$code] : 'white';
        switch ($code) {
            case 'B':
                $out = 'warning';
                break;
            case 'C':
                $out = 'danger';
                break;
            default:
                $out = 'default';
        }
        return  '<span class="label label-' . $out . '" style="background-color:' . $color . ';font-weight:bold">' . $label . '</span>';
        //        return $out;
    }
    protected function global_color()
    {
        return [
            //                'A' => '#00B500',
            'B' => '#FFED00',
            'C' => '#DF6240',
            //                'D' => '#ff2828',
            'E' => 'black',
        ];
    }

    // public function getJenisFuel()
    // {
    //     return [
    //         'FUEL' => 'FUEL',
    //         'B20(CN 48)' => 'B20(CN 48)',
    //         'B30(CN 48)' => 'B30(CN 48)',
    //         'B30(CN 51)' => 'B30(CN 51)',
    //         'MFO' => 'MFO',
    //         'MFO 1' => 'MFO 1',
    //         'MFO 2' => 'MFO 2',
    //         'HSD' => 'HSD',
    //         'MDO' => 'MDO',
    //         'FAME/B100' => 'FAME/B100',
    //     ];
    // }

    public function getListSpesifikasi(){
        return ArrayHelper::map(Specification::find()->all(),'name','name');
    }

    public function getSkMigas(){
        return $this->spesifikasi==null?'':$this->spesifikasi->note;
    }
    /**
     * Undocumented function
     *
     * @return void
     */
    public function getAction()
    {
        return Html::tag(
            'div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => 'btn btn-default btn-sm dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-expanded' => 'false'
            ]) . Html::tag(
                'ul',
                Html::tag('li', Html::tag('a', '<i class="fa fa-caret-square-o-right"></i> view', [
                    'href' => Url::toRoute(['/fuel/default/delete', 'id' => $this->lab_number]), 'data-action' => 'delete'
                ])) .
                    Html::tag('li', Html::tag('a', '<i class="fa fa-edit"></i> Edit', [
                        'href' => Url::toRoute(['/fuel/default/edit', 'id' => $this->lab_number]), 'data-action' => 'edit'
                    ])),
                ['class' => 'dropdown-menu']
            ),
            ['class' => 'btn-group btn-sm']
        );
    }

    public function getColAnalisis()
    {
        return (new Parameters())->colomnAnalisis;
        // return (new self(['query' => $query]))->searchDataTable();
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getSampleDate()
    {
        return ($this->sampling_date == '' || $this->sampling_date == null) ? '-' : date('d-m-Y', strtotime($this->sampling_date));
    }

    public function getReceiveDate()
    {
        return ($this->received_date == '' || $this->received_date == null) ? '-' : date('d-m-Y', strtotime($this->received_date));
    }

    public function getReportDate()
    {
        return ($this->report_date == '' || $this->report_date == null) ? '-' : date('d-m-Y', strtotime($this->report_date));
    }

    public function getAddress()
    {
        $alamat=$this->getCustomers($this->customer_type,$this->customer_id);
        return $alamat == null ? '' : $alamat->Address;
//        return $this->customer_id;
    }


    public function PropertyColomn($type = 'FUEL')
    {
        $out = [];
        foreach ((new Parameters())->colomnAnalisis as $col => $col_code) {
            $p = Parameters::findOne(['column_analisis_name' => $col]);
            $id = $p == null ? 0 : $p->id;
            $m = Matrixs::findOne(['parameter_id' => $id, 'jenis' => $type]);
            $out[] = [
                'nama' => $p == null ? '' : $p->nama,
                //             'id' => $p->id,
                'satuan' => $m == null ? '' : $m->satuan,
                'method' => $m == null ? '' : $m->method,
                'typical' => $m == null ? '' : $m->typical,
                'col'    => $col,
                'code' => $col_code,
                //             'min' => $m == null ? '' : $m->min,
                //             'max' => $m == null ? '' : $m->max,
            ];
        }
        return $out;
    }
    /**
     * Undocumented function
     *
     * @return void
     */
    public function getLabNo()
    {
        $no = '';
        $dt = date('y');
        $n = self::find()->where(['right(lab_number,2)' => $dt])->max('lab_number');
        $n = substr($n, 0, 4);
        $n = (int)$n + 1;
        return sprintf('%04s', $n) . "/OB/F/" . date('y');
    }

    public function UpdateCode()
    {
        $eval_code = '';
        $count = 0;
        if($this->paramMatrix==null){
            return false;
        }
        foreach ($this->paramMatrix as $param) {
            $col=$param->parameters==null?'':$param->parameters->column_analisis_name;
            if($col==''){
                continue;
            }
            $min=$param->min;
            $max=$param->max;
            $batasan=$param->B;
            
//            $m = Matrixs::find()->joinWith('parameters')->where(['matrix.jenis' => $this->type])
//                ->andWhere(['parameter.column_analisis_name' => $col])->one();
//            $min = $m == null ? '' : $m->min;
//            $max = $m == null ? '' : $m->max;
            $val = $this->$col;
            $col_code=$col.'_code';
            $code = '';


            if ($val != null || $val != '') {
                switch ($col) {
                    case 'appearance':
                        $val = strtolower($val);
                        $val = str_replace(' ', '', $val);
                        $batasan = str_replace(' ', '', $batasan);
                        $batasan = strtolower($batasan);
                        // $value=strtolower($val);
                        $code = sizeof(explode($val,$batasan))>1?'':'B';
                        // $code = $val == $batasan ? '' : 'B';
                        break;
                    case 'copper_corrosion':
                        $val = strlen($val) > 1 ? $val[0] : $val;
                        if ($min = '' || $min = null) {
                            $code = $val > $max ? 'B' : '';
                        } elseif ($max = '' || $max = null) {
                            $code = $val < $min ? 'B' : '';
                        }
                        break;
                    case 'iso_4406':
                        $batasan = ($min != '' || $min != null) ? $min : $max;
                        $limit = explode('/', $batasan);
                        $val = explode('/', $val);
                        $code = $val[0] > $limit[0] || $val[1] > $limit[1] || $val[2] > $limit[2] ? 'B' : '';
                        break;
                    case 'water_content':
//                        if ($val < 10) {
//                            //                            $min=($min=='' or $min=NULL)?'':$min*1000;
//                            //                            $max=($max==''or $max=NULL)?'':$max*1000;
//                            $max = ($max != '' || $max != null) ? $max / 10000 : '';
//                        }
                        if ($max == '' || $max == NULL) {
                            $code = $val < $min ? 'B' : '';
                        } elseif ($min == '' || $min == NULL) {
                            $code = $val > $max ? 'B' : '';
                        } elseif (($max == '' || $max == NULL) && ($min == '' || $min == NULL)) {
                            $code = '';
                        } else {
                            $code = $val >= $min && $val <= $max ? '' : 'B';
                        }
//                        $code='';
                        break;
                    default:
                        if ($max == '' || $max == NULL) {
                            $code = $val < $min ? 'B' : '';
                        } elseif ($min == '' || $min == NULL) {
                            $code = $val > $max ? 'B' : '';
                        } elseif (($max == '' || $max == NULL) && ($min == '' || $min == NULL)) {
                            $code = '';
                        } else {
                            $code = $val >= $min && $val <= $max ? '' : 'B';
                        }
                }
                if ($this->hasAttribute($col_code)) {
                    $this->$col_code = $code;
                }
                if($min==null && $max==null){
                    // continue;
                    $this->$col_code = null;
                    $code=null;
                }
                if ($code != '') {
                    $count++;
                }
                // $count = $code==''?$count:
            }
        }
        $eval_code = $count > 0 ? 'B' : 'N';
        $this->eval_code = $eval_code;
        return $this->save();
    }

    public function getDirBerkas()
    {
        $dir = Yii::getAlias('@app/web/assets');
        if (!file_exists($dir . '/json') && !mkdir($concurrentDirectory = $dir . '/json', 0777, true) && !is_dir($concurrentDirectory)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }
        $dirFile = $dir . '/json/dirjen.json';
        // if (!file_exists($dirFile) && !mkdir($dirFile, 0777, true) && !is_dir($dirFile)) {
        //     throw new \RuntimeException(sprintf('Directory "%s" was not created', $dirFile));
        // }

        // $dirCart = $dirFile . '/' . $this->model->unit_id . '_' . $this->model->ComponentID;
        // if (!file_exists($dirCart) && !mkdir($dirCart, 0777, true) && !is_dir($dirCart)) {
        //     throw new \RuntimeException(sprintf('Directory "%s" was not created', $dirCart));
        // }

        // return $dirCart;
        return $dirFile;
    }
    // public function SKmigas()
    // {
    //     // $dir = Yii::getAlias();
    //     // $file=$dir.'/image/dirjen.json';
    //     $place = $this->checkDir('@app/web/img/dirjen.json');
    //     // $stylesheet = file_get_contents($place);
    //     $data = file_get_contents($place);
    //     $a = json_decode($data, true);
    //     return $a;
    // }
    public function getRowId()
    {
        return str_replace(['/', ' '], '', $this->lab_number);
    }

    public function getPublishData()
    {
        if ($this->publish == 1) {
            return Html::a('<i class="fa fa-gear-circle"></i>
            Publish', Url::toRoute(['/master/default/update-status', 'lab_no' => $this->lab_number]), ['class' => 'btn btn-xs btn-success', 'data-action' => 'update-status']);
        } else {
            return Html::a('<i class="fa fa-gear-circle"></i>
            Unpublish', Url::toRoute(['/master/default/update-status', 'lab_no' => $this->lab_number]), ['class' => 'btn btn-xs btn-warning', 'data-action' => 'update-status']);
        }
    }

    protected function checkDir($dir)
    {
        //        Yii::$app->assetManager->publish($dir);
        $directory = Yii::getAlias($dir);

        if (!file_exists($directory)) {
            throw new ConfigurationException('check dir or file');
        }
        return $directory;
        //        return Yii::$app->assetManager->getPublishedUrl($dir);
    }

    public function __get($name)
    {
        $v=parent::__get($name);
        if($name=='name'){
            return ($v==null || $v=='')?$this->attentionName:parent::__get($name);
        }
        if($name=='branch'){
             return $v==null?$this->customerName:$v;
            // return 'ASU';
        }
            return parent::__get($name);
                
    }

    public function getCustomerName(){
        $q=$this->customerAll;
        return $q==null?'':$q->name;
    }

    public function getAttentionName(){
        $q=$this->attentionAll;
        return $q==null?'':$q->name;
    }

    /**
     * @return array
     */
    public function getListReportType(){
        return ArrayHelper::map(TypeReports::find()->all(),'id','name');
    }

    /**
     * @return array
     */
    public function getListMatrix(){
        return ArrayHelper::map(Matrixs::find()->all(),'id','jenis');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParamMatrix(){
        return $this->hasMany(ParameterMatrix::className(),['matrix'=>'matrix_id']);
    }

    public function getHasParameter(){
        return $this->hasMany(TypeReportParameters::className(),['type_report_id'=>'type_report_id'])->orderBy('urut');
    }

    public function renderParameter()
    {
        $q=TypeReportParameters::find()->joinWith('parameter')->where(['parameter_has_type_report.type_report_id'=>$this->type_report_id]);
            $q->indexBy('parameter_id')->orderBy('urut');
        return $q->all();
    }

    /**
     * @param $id
     * @return ParameterMatrix|array|\yii\db\ActiveRecord|null
     */
    public function renderParamMatrix($id){
        $q=ParameterMatrix::find()->where(['matrix'=>$this->matrix_id])->andWhere(['parameter_id'=>$id]);
//        $q->select(['B','jenis']);
        return $q->one();
    }

}
