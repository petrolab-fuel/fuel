<?php
/**
 * ParameterMatrix.php
 * Created By
 * feri_
 * 31/01/2023
 */

namespace app\modules\master\models\data;

use yii\bootstrap\Html;

class ParameterMatrix extends \app\modelsDB\ParameterHasMatrix
{
    public function getAction()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => "btn btn-default dropdown-toggle", 'data-toggle' => "dropdown", 'aria-expanded' => "false"
            ]) . Html::tag('ul',
//                Html::tag('li', '<a>Edit</a>') .
                Html::tag('li', '<a data-action="delete_row">Delete</a>')

                , ['class' => "dropdown-menu"]), ['class' => "btn-group"]);
    }

    public function getId(){
        return $this->matrix. '_' .$this->parameter_id;
    }
    public $_idName;

    /**
     * @return string
     */
    public function getIdName()
    {
        if ($this->_idName == null) {
            $this->_idName = 'new';
        }
        return $this->_idName;
    }

    /**
     * @param string $idName
     */
    public function setIdName($idName)
    {
        $this->_idName = $idName;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function formName()
    {
        return parent::formName() . "[$this->idName]";
    }

    /** @var  string|int */
    public $actionId;

    /**
     * @return string
     */
    public function getParameterName(){
        return $this->parameters==null?'':$this->parameters->nama;
    }

    public function getParameters(){
        return $this->hasOne(Parameters::className(),['id'=>'parameter_id']);
    }

    public function getDataMatrix(){
        return $this->hasOne(Matrixs::className(),['id'=>'matrix']);
    }
    public function getJenis(){
        return $this->dataMatrix==null?'':$this->dataMatrix->jenis;
    }
}