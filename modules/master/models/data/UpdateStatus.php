<?php

namespace app\modules\master\models\data;

use app\component\ArrayHelper;
use app\modules\master\models\data\search\SearchAnalisa;
use Yii;
use Curl\Curl;
use yii\helpers\Json;

class UpdateStatus extends Analisa
{
    public $url_api = 'https://ut.petrolab.co.id/api_erp';

    /** @var array  */
    public $dataApi = [];

    public function updateStatus($params)
    {
        $col=$params['columns'][3]['search']['value'];
//        $this->message=$params['columns'][3]['search']['value'];
//        return false;
        $model = new SearchAnalisa();
        $filter = $model->searchData($params, true);
        $query = self::find();
        $query->where = $filter->where;
        $query->joinWith = $filter->joinWith;
//        $this->message=$params;
//        return false;
        if ($this->lab_number != '') {
            $query->andWhere(['lab_number' => $this->lab_number]);
        }
        if($col==''){
            $this->message='Pilih Type Customernya';
            return false;
        }
        if ($query->where == [] || $query->where == '') {
            $this->message = 'Filter First';
            return false;
        }
        $data = $query->noCache()->all();

        if ($data == null) {
            $this->message = 'No Data Found';
            return false;
        }
        // $this->message = $data;
        // return true;

        if ($col=='UT'){
            $this->url_api = 'https://ut.petrolab.co.id/api_erp';
        }elseif($col=='PAMA'){
            $this->url_api = 'https://pama.petrolab.co.id/api_erp';
        }else{
            $this->url_api = 'https://report.petrolab.co.id/api_erp';
        }
        if (!$this->saveApi($data)) {
            $this->message = 'error to import data';
        }

        $statusSuccees = 0;
        $out = true;
        $msg = '';
        foreach ($data as $record) {
            if (isset($this->dataApi['save'][$record->lab_number])) {
                if ($record->saveStatus()) {
                    $this->data[$record->id] = $record->publishData;
                    $statusSuccees++;
                } else {
                    $out = false;
                    $msg .=
                        "Lab Number $record->lab_number error : " . ArrayHelper::toString($record->errors) . '<br>';
                }
            }
        }
        $this->message .= $statusSuccees > 1 ? "Success diupdate : $statusSuccees <hr>" : '';
        $this->message .= $msg;
        return $out;
    }
    public $message = '';
    public $data = [];
    protected $dataStatus = [];

    public function saveStatus()
    {
        $this->publish = 1;
        return $this->save();
    }

    public function saveApi($data)
    {
        // $this->url_api = Yii::$app->params['api_erp'];
        $api = [];
        foreach ($data as $updateStatus) {
            if ($updateStatus->publish == 0 ) {
                $updateStatus->publish = 1;
                $api['data'][] = $updateStatus->toArray();
            }
            //            $api['data'][$updateStatus->] = $updateStatus->toArray();
        }
        // $this->message = $api;
        // return true;
        if ($api == []) {
            return true;
        }
        $c = new Curl();
        $c->setOpt(CURLOPT_SSL_VERIFYPEER, false);
        $c->setTimeout(10000);
        $c->post($this->url_api . '/data-upload/fuel', ['all' => Json::encode($api)]);
        if ($c->error) {
            return false;
        }
        if (isset($c->response->sv)) {
            $this->dataApi = [
                'save' => ArrayHelper::toArray($c->response->sv),
            ];
            // $dataStatus = ArrayHelper::toArray($c->response->sv);
        }
        return true;
    }


    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (parent::save($runValidation, $attributeNames)) {
                $transaction->commit();
                return true;
            }
            $transaction->rollBack();
            return false;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}
