<?php
/**
 * SpecParameters.php
 * Created By
 * feri_
 * 29/01/2023
 */

namespace app\modules\master\models\data;

use yii\helpers\Html;

/**
 *
 * @property string $idName
 * @property-read \yii\db\ActiveQuery $spesifikasi
 * @property-read string $action
 * @property-read string $parameterName
 * @property-read \yii\db\ActiveQuery $parameters
 */
class SpecParameters extends \app\modelsDB\SpesifikasiParameters
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpesifikasi(){
        return self::hasOne(Specification::className(),['id'=>'spesifikasi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameters(){
        return self::hasOne(Parameters::className(),['id'=>'parameter_id']);
    }

    public function getParameterName(){
        return $this->parameters==null?'':$this->parameters->nama;
    }
    /**
     * @return string
     */
    public function getAction()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => "btn btn-default dropdown-toggle", 'data-toggle' => "dropdown", 'aria-expanded' => "false"
            ]) . Html::tag('ul',
//                Html::tag('li', '<a>Edit</a>') .
                Html::tag('li', '<a data-action="delete_row">Delete</a>')

                , ['class' => "dropdown-menu"]), ['class' => "btn-group"]);
    }

    public $_idName;

    /**
     * @return string
     */
    public function getIdName()
    {
        if ($this->_idName == null) {
            $this->_idName = 'new';
        }
        return $this->_idName;
    }

    /**
     * @param string $idName
     */
    public function setIdName($idName)
    {
        $this->_idName = $idName;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function formName()
    {
        return parent::formName() . "[$this->idName]";
    }

    /** @var  string|int */
    public $actionId;
}