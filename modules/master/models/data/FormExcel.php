<?php

namespace app\modules\master\models\data;

use app\modules\master\models\data\search\SearchAnalisa;
use app\models\Excel;
use PHPExcel_Style_Border;

class FormExcel extends SearchAnalisa
{
    protected $row;
    protected $col;
    protected $exc;
    protected  $arr;
    protected $csv = false;

    protected $attribute = [
        'lab_number', 'branch', 'name', 'type', 'eng_type', 'eng_sn', 'eng_location','eng_builder','compartment', 'sampling_date', 'received_date', 'report_date','harga','spb','po', 'eval_code', 'recommended1'
    ];
    protected $parameter = [
        'appearance', 'density', 'total_acid_number', 'flash_point', 'kinematic_viscosity', 'water_content', 'ash_content', 'pour_point', 'cetane_index', 'conradson_carbon',
        'distillation_ibp', 'distillation_5', 'distillation_10', 'distillation_20', 'distillation_30', 'distillation_40', 'distillation_50', 'distillation_60',
        'distillation_70', 'distillation_80', 'distillation_90', 'distillation_95', 'distillation_ep', 'distillation_recovery', 'distillation_residue', 'distillation_loss',
        'distillation_recovery_300', 'sulphur_content', 'sediment', 'colour', 'copper_corrosion', 'particles_4', 'particles_6', 'particles_14', 'iso_4406', 'specific_gravity',
        'cloudn_point', 'rancimat', 'lubricity_of_diesel', 'fame_content', 'calorific_value', 'strong_acid', 'cerosine_content', 'silicon', 'alumunium', 'monoglyceride', 'free_glyceride',
        'total_glyceride', 'particulat',
    ];
    public function generateExcel($params = null, $csv = false, $style = [])
    {
        $countFilter = 0;
        if (isset($params['columns']))
            foreach ($params['columns'] as $col) {
                if (isset($params['search']) && $params['search']['value'] !== '') {
                    $countFilter++;
                }
                if ($col['searchable'] === 'true' && $col['search']['value'] !== '') {
                    $countFilter++;
                }
            }

        $query = $this->searchData($params, true);
        $e = new Excel();
        $this->exc = $e->newExcel();
        $this->arr = [];
        $this->row = 2;
        $this->col = 'A';
        $data = $query->all();
        $this->LoopingData($data);
        // $this->renderStyle($style);
        $fileName = 'Export_excel_' . date('d-m-Y_H-i-s');
        $this->csv = $csv;
        if ($this->csv) {
            $e->downloadCsv($this->exc, $fileName);
            return '';
        }
        $e->download($this->exc, $fileName);
        return '';
        // $e->download($this->exc, $fileName);
        // return '';

    }

    protected function renderStyle($style = [])
    {
        foreach ($style as $cell => $item) {
            if ($cell == 'all') {
                $cell = 'A1:' . $this->col . $this->row;
            }
            $this->p->getActiveSheet()->getStyle($cell)->applyFromArray($item);
        }
    }
    protected function LoopingData($data)
    {
        foreach ($data as $rec) {

            foreach ($this->attribute as $att) {
                if (!isset($this->arr[$att])) {
                    $this->arr[$att] = $this->col;
                    $this->exc->getActiveSheet()->setCellValue($this->arr[$att] . 1, $att == 'customerName'?'Name':$att);
                    // $this->exc->getActiveSheet()->getStyle($this->arr[$value] . 1)->applyFromArray($this->styleFillHeader);
                    $this->col++;
                }
                $this->exc->getActiveSheet()->setCellValue(
                    $this->arr[$att] . $this->row,
                    $rec->hasAttribute($att) ? $rec->$att : ($att == 'customerName' ? $rec->$att : '')
                );
                // $this->exc->getActiveSheet()->getStyle($this->arr[$value] . $this->row)->applyFromArray($styleBorder);

            }
            foreach ($this->parameter as $par) {
                if (!isset($this->arr[$par])) {
                    $this->arr[$par] = $this->col;
                    $this->exc->getActiveSheet()->setCellValue($this->arr[$par] . 1, $par);
                    $this->col++;
                }
                $this->exc->getActiveSheet()->setCellValue(
                    $this->arr[$par] . $this->row,
                    $rec->hasAttribute($par) ? $rec->$par : ''
                );
            }
            $this->row++;
        }
    }
}
