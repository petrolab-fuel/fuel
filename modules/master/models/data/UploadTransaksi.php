<?php
namespace app\modules\master\models\data;
use app\component\ArrayHelper;
use app\models\Excel;
use phpDocumentor\Reflection\Types\Self_;
use yii\web\UploadedFile;
class UploadTransaksi extends Analisa{
    public $uploadFile;
    public function rules(){
        return array_merge(parent::rules(),[
            [['lab_number'], 'unique'],
            [['uploadFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xlxs, xls, csv', 'maxSize' => 10],
        ]);
    }
    public function upload(UploadedFile $image=null){
        $transaction=$this::getDb()->beginTransaction();
        try{
            $excel = new Excel();
            if($image !=null){
               $excel=$excel->renderData($image->tempName); 
               $arrExcel=$excel->getActiveSheet()->toArray();
               $countInput = 0;
               $countError = 0;
               $header = [];
               $idxLabNo = '';
               foreach($arrExcel as $i=>$item){
                    if($i==0){
                        foreach($item as $key=>$value){
                            if($this->hasAttribute($value)){
                                if($value=='lab_number'){
                                    $idxLabNo=$key;
                                }else{
                                    $header[$key]=$value;
                                }
                            }else{
                                $this->dataMessage .= "Kolom $value tidak terdaftar dalam table DataBase <br>";
                            }
                        }
                    }else{
                        $labNo=isset($item[$idxLabNo])?$item[$idxLabNo]:'';
                        $report=self::findOne(['lab_number'=>$labNo]);
                        if($report==null){
                            $report= new Self(['lab_number'=>$labNo]);
                        }
                        foreach($header as $k=>$h){
                            if ($report->hasAttribute($h)) {
                                if (in_array($h, ['sampling_date', 'received_date', 'report_date'], true)){
                                    if (is_numeric($item[$k])) {
                                        /** @noinspection SummerTimeUnsafeTimeManipulationInspection */
                                        $time = ($item[$k] - 25569) * 86400;
                                        $report->$h = date('Y-m-d',  $time);
                                    } else {
                                        $report->$h = date('Y-m-d', strtotime($item[$k]));
                                    }
                                } else {
                                    $report->$h = (string)$item[$k];
                                }
                            }
                        }
                        if ($report->save()) {
                            $countInput++;
                        } else {
                            $countError++;
                            $this->dataMessage .= "Lab No : $item[$idxLabNo] error dengan " . ArrayHelper::toString($report->errors) . ' <br>';
                        }
                    }
                }
                $this->status_save = $countInput > 0 ? 'success' : 'error';
                $this->message = "Upload berhasil dengan <br><br> Jumlah Success : $countInput <br>" .
                    ($countError > 0 ? "Jumlah Error : $countError <br>" : '') . '<br><hr>';
                $transaction->commit();
                return true;
            }
            $this->status_save = 'error';
            $this->message = 'Data Tidak disimpan, Coba lagi.<br>' . ArrayHelper::toString($this->errors);
            $transaction->rollBack();
            return false;
        }catch(\Exception $e){
            $transaction->rollBack();
            throw $e;
        }catch(\Throwable $e){
            $transaction->rollBack();
            throw $e;
        }
    }
    public $message = '';

    public $status_save = '';

    public $class_message = '';

    public $dataMessage = '';
}