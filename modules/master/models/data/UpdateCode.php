<?php
namespace app\modules\master\models\data;
use app\components\ArrayHelper;
use app\modules\master\models\data\search\SearchAnalisa;
class UpdateCode extends Analisa
{
    public function evalCode($params)
    {
        $model=new SearchAnalisa();
        $filter=$model->searchData($params,true);
        $query=self::find();
        $query->where=$filter->where;
        $query->joinWith=$filter->joinWith;
        
        if ($this->lab_number != '') {
            $query->andWhere(['lab_number' => $this->lab_number]);
        }

        if ($query->where == [] || $query->where == '') {
            $this->message = 'Filter First';
            return false;
        }
        $data = $query->noCache()->all();

        if ($data == null) {
            $this->message = 'No Data Found';
            return false;
        }
        $statusSuccees = 0;
        $out = true;
        $msg = '';
        foreach ($data as $record){
            if($record->UpdateCode()){
                $this->data[$record->id] = $record->status;
                $statusSuccees++;
            }else{
                $out=false;
                $msg .=
                    "Lab Number $record->lab_number error : " . ArrayHelper::toString($record->errors) . '<br>';
            }
        }
        $this->message .= $statusSuccees > 1 ? "Success diupdate : $statusSuccees <hr>" : '';
        $this->message .= $msg;
        return $out;
    }
    public $message='';
    public $data = [];
}

