<?php

namespace app\modules\master\models\data\search;

use app\component\ArrayHelper;
use Yii;
use yii\db\Query;
use yii\helpers\Json;
use app\modules\master\models\data\Analisa;

class SearchAnalisa extends Analisa
{
    public $allField = [
        'id' => 'ID',
        'received_date' => 'received_date',
        'start_analisa' => 'start_analisa',
        'end_analisa' => 'end_analisa',
        'report_date' => 'report_date',
        'sampling_date' => 'sampling_date',
        'po' => 'po',
        'spb' => 'spb',
        'harga' => 'harga',
        'lab_number' => 'lab_number',
        'eval_code' => 'eval_code',
        'customer_id' => 'customer_id',
        'unit_id' => 'unit_id',
        'eng_builder' => 'eng_builder',
        'eng_type' => 'eng_type',
        'eng_sn' => 'eng_sn',
        'eng_location' => 'eng_location',
        'branch' => 'branch',
        'report_type_id' => 'report_type_id',
        'appearance' => 'appearance',
        'appearance_code' => 'appearance_code',
        'density' => 'density',
        'density_code' => 'density_code',
        'total_acid_number' => 'total_acid_number',
        'total_acid_number_code' => 'total_acid_number_code',
        'flash_point' => 'flash_point',
        'flash_point_code' => 'flash_point_code',
        'kinematic_viscosity' => 'kinematic_viscosity',
        'kinematic_viscosity_code' => 'kinematic_viscosity_code',
        'water_content' => 'water_content',
        'water_content_code' => 'water_content_code',
        'ash_content' => 'ash_content',
        'ash_content_code' => 'ash_content_code',
        'pour_point' => 'pour_point',
        'pour_point_code' => 'pour_point_code',
        'cetane_index' => 'cetane_index',
        'cetane_index_code' => 'cetane_index_code',
        'conradson_carbon' => 'conradson_carbon',
        'conradson_carbon_code' => 'conradson_carbon_code',
        'distillation_recov' => 'distillation_recov',
        'distillation_ibp' => 'distillation_ibp',
        'distillation_ibp_code' => 'distillation_ibp_code',
        'distillation_5' => 'distillation_5',
        'distillation_5_code' => 'distillation_5_code',
        'distillation_10' => 'distillation_10',
        'distillation_10_code' => 'distillation_10_code',
        'distillation_20' => 'distillation_20',
        'distillation_20_code' => 'distillation_20_code',
        'distillation_30' => 'distillation_30',
        'distillation_30_code' => 'distillation_30_code',
        'distillation_40' => 'distillation_40',
        'distillation_40_code' => 'distillation_40_code',
        'distillation_50' => 'distillation_50',
        'distillation_50_code' => 'distillation_50_code',
        'distillation_60' => 'distillation_60',
        'distillation_60_code' => 'distillation_60_code',
        'distillation_70' => 'distillation_70',
        'distillation_70_code' => 'distillation_70_code',
        'distillation_80' => 'distillation_80',
        'distillation_80_code' => 'distillation_80_code',
        'distillation_90' => 'distillation_90',
        'distillation_90_code' => 'distillation_90_code',
        'distillation_95' => 'distillation_95',
        'distillation_95_code' => 'distillation_95_code',
        'distillation_ep' => 'distillation_ep',
        'distillation_ep_code' => 'distillation_ep_code',
        'distillation_recovery' => 'distillation_recovery',
        'distillation_recovery_code' => 'distillation_recovery_code',
        'distillation_residue' => 'distillation_residue',
        'distillation_residue_code' => 'distillation_residue_code',
        'distillation_loss' => 'distillation_loss',
        'distillation_loss_code' => 'distillation_loss_code',
        'distillation_recovery_300' => 'distillation_recovery_300',
        'distillation_recovery_300_code' => 'distillation_recovery_300_code',
        'sulphur_content' => 'sulphur_content',
        'sulphur_content_code' => 'sulphur_content_code',
        'sediment' => 'sediment',
        'sediment_code' => 'sediment_code',
        'colour' => 'colour',
        'colour_code' => 'colour_code',
        'copper_corrosion' => 'copper_corrosion',
        'copper_corrosion_code' => 'copper_corrosion_code',
        'type' => 'type',
        'particles_4' => 'particles_4',
        'particles_4_code' => 'particles_4_code',
        'particles_6' => 'particles_6',
        'particles_6_code' => 'particles_6_code',
        'particles_14' => 'particles_14',
        'particles_14_code' => 'particles_14_code',
        'iso_4406' => 'iso_4406',
        'iso_4406_code' => 'iso_4406_code',
        'specific_gravity' => 'specific_gravity',
        'specific_gravity_code' => 'specific_gravity_code',
        'api_gravity' => 'api_gravity',
        'api_gravity_code' => 'api_gravity_code',
        'cloudn_point' => 'cloudn_point',
        'cloudn_point_code' => 'cloudn_point_code',
        'rancimat' => 'rancimat',
        'rancimat_point' => 'rancimat_point',
        'lubricity_of_diesel' => 'lubricity_of_diesel',
        'lubricity_of_diesel_code' => 'lubricity_of_diesel_code',
        'fame_content' => 'fame_content',
        'fame_content_code' => 'fame_content_code',
        'calorific_value' => 'calorific_value',
        'calorific_value_code' => 'calorific_value_code',
        'strong_acid' => 'strong_acid',
        'strong_acid_code' => 'strong_acid_code',
        'cerosine_content' => 'cerosine_content',
        'cerosine_content_code' => 'cerosine_content_code',
        'recommended1' => 'recommended1',
        'recommended2' => 'recommended2',
        'recommended3' => 'recommended3',
        'equipment_condition' => 'equipment_condition',
        'equipment_condition_code' => 'equipment_condition_code',
        'silicon' => 'silicon',
        'silicon_code' => 'silicon_code',
        'alumunium' => 'alumunium',
        'alumunium_code' => 'alumunium_code',
        'monoglyceride' => 'monoglyceride',
        'monoglyceride_code' => 'monoglyceride_code',
        'free_glyceride' => 'free_glyceride',
        'free_glyceride_code' => 'free_glyceride_code',
        'total_glyceride' => 'total_glyceride',
        'total_glyceride_code' => 'total_glyceride_code',
        'particulat' => 'particulat',
        'particulat_code' => 'particulat_code',
        'actions' => '',

        // 'customerName' => 'customer_id',
        'reportDate' => 'report_date',
        'sampleDate' => 'sampling_date',
        'receiveDate' => 'received_date',
        'status' => 'eval_code',
        'publishData'=>'publish',
        'typeCustomer'=>'customer_type',

    ];

    /**
     * @param $params
     * @return string
     */
    public function ordering($params)
    {
        $ncol = isset($params['order'][0]['column']) ? $params['order'][0]['column'] : 0;
        $col = (isset($params['columns'][$ncol]) && array_key_exists($params['columns'][$ncol]['data'], $this->allField)) ?
            $this->allField[$params['columns'][$ncol]['data']] : '';
        $argg = isset($params['order'][0]['dir']) ? $params['order'][0]['dir'] : 'asc';
        $table = self::tableName();

        if (isset($params['columns'][$ncol]['data']) && $params['columns'][$ncol]['data'] === 'typeCustomer') {
            $col = 'description';
            $table = 'customers_type';
        }

        return $col !== '' ? "$table.$col $argg " : '';
    }


    public $allData;

    public $currentData;

    /**
     * @param Query $query
     * @return int
     */
    public function calcAllData($query)
    {
        return $this->allData === null ? $query->count() : $this->allData;
    }

    /**
     * @param Query $query
     * @param $params
     * @return Query
     */
    public static function defaultFilterByUser($query, $params = '')
    {
        // $user = Yii::$app->user->identity;
        // $dataId = $user->data_id;
        // $role = $user->ruleAccess;
        // // if ($user->ruleAccess === -1) {
        // //     return $query;
        // // }

        // // if ($user->ruleAccess === 2) {

        // // }

        // if ($role === 3) {
        //     $query->andWhere(['Branch' => $dataId]);
        // }

        // if ($role === 4) {
        //     $where = "CustomerID IN ($dataId)";
        //     $query->andWhere($where);
        // }

        //        $query->andWhere('publish is not null');

        return $query;
    }

    /**
     * @param $params
     * @param bool $export
     * @param bool $join
     * @return $this|\yii\db\ActiveQuery|Query
     */
    public function searchData($params, $export = false, $join = true)
    {

        $odr = $this->ordering($params);

        $query = self::find()->joinWith(['customerType']);

        if (!$join) {
            $query = self::find();
        }

        $query = self::defaultFilterByUser($query, $params);

        $query->orderBy($odr)->noCache();

        $start = isset($params['start']) ? $params['start'] : 0;
        $lang = isset($params['length']) ? $params['length'] : 10;

        if (!$export) {
            $this->allData = $this->calcAllData($query);
        }
        // $this->allData = $this->calcAllData($query);

        if (isset($params['filter'])) {
            foreach ($params['filter'] as $idx => $filter) {
                if ($filter == '') {
                    continue;
                }
                if ($idx == 'r_start') {
                    $query->andWhere(['>=', 'received_date', $filter]);
                }
                if ($idx == 'r_end') {
                    $query->andWhere(['<=', 'received_date', $filter]);
                }
                if ($idx == 'rp_start') {
                    $query->andWhere(['>=', 'report_date', $filter]);
                }
                if ($idx == 'rp_end') {
                    $query->andWhere(['<=', 'report_date', $filter]);
                }
            }
        }

        $fltr = '';
        if (isset($params['columns']))
            foreach ($params['columns'] as $col) {
                if (isset($params['search']) && $params['search']['value'] !== '') {
                    $lst[] = $col['data'];
                    if (
                        array_key_exists($col['data'], $this->allField) &&
                        !array_key_exists($col['data'], $lst) && $this->allField[$col['data']] != ''
                    ) {
                        $fltr .= $fltr !== '' ? ' or ' : '';
                        $fltr .= ' `' . self::tableName() . '`.' . $this->allField[$col['data']] . " like '%" . $params['search']['value'] . "%' ";
                    }
                    if ($col['data'] === 'typeCustomer') {
                        $fltr .= $fltr !== '' ? ' or ' : '';
                        $fltr .= ' `customers_type`.description ' . " like '%" . $params['search']['value'] . "%' ";
                    }
                    // // if ($col['data'] == 'para') {
                    //     $c = Customers::find()->where("Name like '%" . $params['search']['value'] . "%'")->distinct()->select(['Branch'])->all();
                    //     $fltr .= $fltr !== '' ? ' or ' : '';
                    //     $fltr .= ' `' . self::tableName() . "`.branch IN ('" . join("','", ArrayHelper::getColumn($c, 'Branch')) . "') ";
                    // }
                }
                if (
                    $col['searchable'] === 'true' && $col['search']['value'] !== '' &&
                    array_key_exists($col['data'], $this->allField) && $this->allField[$col['data']] != ''
                ) {
                    if ($col['data'] === 'typeCustomer') {
                        $query->andFilterWhere(['like', '`customers_type`.description', $col['search']['value']]);
                        // } elseif ($col['data'] == 'listAttention') {
                        // $c = Customers::find()->where("Name like '%" . $col['search']['value'] . "%'")->distinct()->select(['Branch'])->all();
                        // $query->andWhere(' `' . self::tableName() . "`.branch IN ('" . join("','", ArrayHelper::getColumn($c, 'Branch')) . "')");
                    } else {
                        $query->andFilterWhere(['like', '`' . self::tableName() . '`.' . $this->allField[$col['data']], $col['search']['value']]);
                    }
                }
            }

        $query->andWhere($fltr);

        $this->load($params);

        if ($export) {
            return $query;
        }

        $this->currentData = $query->count();

        $query->limit($lang)->offset($start);
        return $query;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function searchDataTable($params)
    {
        $data = $this->searchData($params);
        return Json::encode([
            'draw' => isset($params['draw']) ? intval($params['draw']) : 0,
            'recordsTotal' => intval($this->allData),
            'recordsFiltered' => intval($this->currentData),
            'data' => $this->renderData($data, $params),
        ]);
    }

    /**
     * @param $query Query
     * @param $params
     * @return array
     */
    public function renderData($query, $params)
    {
        $out = [];

        /** @var self $model */
        foreach ($query->all() as $model) {
            $out[] = array_merge(ArrayHelper::toArray($model), [
                'actions' => $model->actions,
                'status' => $model->status,
                'publishData'=>$model->publishData,
                'typeCustomer'=>$model->typeCustomer,
                // 'customerName' => $model->customerName,
                'sampleDate'=>$model->sampleDate,
                'receiveDate'=>$model->receiveDate,
                'reportDate'=>$model->reportDate,
                // 'listUnit'=>$model->listUnit,
                // 'listAttention' => $model->listAttention,
                // 'status' => $status = ($model->active)? 'Active':'Inactive',
            ]);
        }
        return $out;
    }
}
