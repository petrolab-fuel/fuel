<?php
/**
 * TypeReports.php
 * Created By
 * feri_
 * 30/01/2023
 */

namespace app\modules\master\models\data;

use app\component\DataTableAjax;
use yii\bootstrap\Html;
use yii\helpers\Url;

/**
 *
 * @property-read \yii\db\ActiveQuery $typeParameter
 */
class TypeReports extends \app\modelsDB\TypeReport
{
    public $_typeParameter;

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeParameter(){
        if($this->_typeParameter==null){
            $this->_typeParameter=$this->hasMany( TypeReportParameters::className(),['type_report_id'=>'id']);
        }
        return $this->_typeParameter;
    }

    public function getActions(){
        return \yii\helpers\Html::tag('div',
            Html::tag('a','<i class="fa fa-edit"> ',['href'=>Url::to(['/master/paket/edit','id'=>$this->id]),'class'=>'btn btn-default btn-sm']),
            [
                'class'=>'group'
            ]);
    }
    public function renderAjax($params){
        $q=self::find();
        $total=$q->count('id');
        if(isset($params['columns'])){
            foreach($params['columns'] as $col){
                if (isset($params['search']['value']) && $params['search']['value'] != '') {

                }
                if ($col['searchable'] == 'true' && $col['search']['value'] != '') {

                }
            }
        }
        if (isset($params['order'])) {
            $sort = [];
            foreach ($params['order'] as $item) {
                $arr = isset($item['dir']) ? $item['dir'] : 'asc';
                $col = isset($item['column']) && isset($param['columns'][$item['column']]) ? $param['columns'][$item['column']]['data'] : '0';
                if ((new self())->hasAttribute($col)) {
                    $sort = self::tableName() . '.' . $col . ' ' . $arr;
                }
                // if ($col == 'start') {
                //     $sort = self::tableName() . '.date_start ' . $arr;
                // }
                // if ($col == 'end') {
                //     $sort = self::tableName() . '.date_end ' . $arr;
                // }
                // if ($col == 'hour') {
                //     $q->addSelect('*');
                //     $q->addSelect(['timediff(date_start,date_end) deff_h']);
                //     $sort = 'deff_h ' . $arr;
                // }
                // if ($col == 'projectTask') {
                //     $sort = self::tableName() . '.project_id ' . $arr;
                // }
            }
            $q->orderBy($sort);
        }
        return DataTableAjax::renderAjax($q, ['allData' => $total]);
    }
}