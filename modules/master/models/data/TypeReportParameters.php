<?php
/**
 * TypeReportParameters.php
 * Created By
 * feri_
 * 30/01/2023
 */

namespace app\modules\master\models\data;

use yii\bootstrap\Html;

class TypeReportParameters extends \app\modelsDB\ParameterHasTypeReport
{
    public function getAction()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => "btn btn-default dropdown-toggle", 'data-toggle' => "dropdown", 'aria-expanded' => "false"
            ]) . Html::tag('ul',
//                Html::tag('li', '<a>Edit</a>') .
                Html::tag('li', '<a data-action="delete_row">Delete</a>')

                , ['class' => "dropdown-menu"]), ['class' => "btn-group"]);
    }

    public $_idName;

    /**
     * @return string
     */
    public function getIdName()
    {
        if ($this->_idName == null) {
            $this->_idName = 'new';
        }
        return $this->_idName;
    }

    /**
     * @param string $idName
     */
    public function setIdName($idName)
    {
        $this->_idName = $idName;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function formName()
    {
        return parent::formName() . "[$this->idName]";
    }

    public function getParameter()
    {
        return $this->hasOne(Parameters::className(), ['id' => 'parameter_id']);
    }

    /** @var  string|int */
    public $actionId;

    /**
     * @return string
     */
    public function getNama(){
        return $this->parameter==null?'':$this->parameter->nama;
    }

    /**
     * @return string
     */
    public function getCol(){
        return $this->parameter==null?'':$this->parameter->column_analisis_name;
    }
    public function getUnit(){
        return $this->parameter==null?'':$this->parameter->unit;
    }
    public function getMethod(){
        return $this->parameter==null?'':$this->parameter->method;
    }
    /**
     * @return array
     */
    public function getNoOrder(){
        $out=[];
        for($i=1;$i<50;$i++){
            $out[$i]=$i;
        }
        return $out;
    }
}