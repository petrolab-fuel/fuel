<?php
/**
 * FormSpesifikasi.php
 * Created By
 * feri_
 * 29/01/2023
 */

namespace app\modules\master\models\data;
use Yii;
use app\component\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
class FormSpesifikasi extends Specification
{
    public function rules()
    {

        return array_merge(parent::rules(),[
            [['name'], 'required'],
        ]);
        // TODO: Change the autogenerated stub
    }

    /**
     * @param $data
     * @param $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        if(parent::load($data, $formName)){
            if(isset($data['SpecParameters'])){
                foreach ($data['SpecParameters'] as $idx=>$data){

                    $item = SpecParameters::findOne($idx);
                    if ($item == null) {
                        $item = new SpecParameters(ArrayHelper::merge($data, ['parameter_id' => $this->id]));
                    }
                    $item->load(['Data' => $data], 'Data');
                    $item->actionId = isset($data['actionId']) ? $data['actionId'] : null;
                    $this->_specParameter[$idx] = $item;
                }
            }
            return true;
        }
        return false;
        // TODO: Change the autogenerated stub
    }

    /**
     * @param $runValidation
     * @param $attributeNames
     * @return bool
     * @throws \yii\db\Exception
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try{
            if(parent::save($runValidation, $attributeNames)){
                foreach ($this->specParameter as $item){
                    $item->spesifikasi_id=$this->id;
                    if($item->actionId=='del'){
                        if(!$item->isNewRecord){
                            $item->delete();
                        }
                    }else{
                        if(!$item->save()){
                            $transaction->rollBack();
                            return false;
                        }
                    }
                }
                $transaction->commit();
                return true;
            }
            $transaction->rollBack();
            return false;
        }
        catch (\Exception $e){
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @param $form
     * @return string
     */
    public function renderSpecParameter($form){
           $out=Html::tag('thead',
           Html::tag('tr',
               Html::tag('th','Option',['width'=>'10%']).
               Html::tag('th','Parameter')
           )).'<tbody>';
           foreach ($this->specParameter as $idx=>$item){
//               $component->idName = $component->ComponentID == null ? 'newrow_' . $idx : $component->ComponentID;
               $item->idName=$item->id==null?'newrow_'.$idx:$item->id;
               $out.=Html::tag('tr',
               Html::tag('td',$item->action).
               Html::tag('td',$form->field($item,'parameter_id', ['template' => '<label class="select">
                   {input}</label>{error}'])->dropDownList($item->parameter_id==null?[]:[$item->parameter_id=>$item->parameterName],[
                       'class'=>'form-control select2',
                       'data-url'=>Url::toRoute(['/master/parameters/parameter-list'])
                   ])->label(false))
               ,['data-id'=>$item->idName]);
           }
           return Html::tag('table',$out.'</tbody>',['class'=>'table table-responsive']);
    }

    /**
     * @param $rowTo
     * @return array
     */
    public function renderAjaxSpecParameteri($rowTo=0){
        $form = ActiveForm::begin([
            'fieldConfig' => [
                'template' => '<label class="input">{input}</label>{error}',
            ]
        ]);
        $item=new SpecParameters();
        $item->idName='newrow_' . $rowTo;
        $out=Html::tag('tr',Html::tag('td',$item->action).
                Html::tag('td',$form->field($item,'parameter_id', ['template' => '<label class="select">
                   {input}</label>{error}'])->dropDownList($item->parameter_id==null?[]:[$item->parameter_id=>$item->parameterName],[
                    'class'=>'form-control select2',
                    'data-url'=>Url::toRoute(['/master/parameters/parameter-list'])
                ])->label(false))
        ,['data-id'=>$item->idName]);

        return [
            'data' => $out,
            'jsAdd' => ArrayHelper::toArray($form->attributes),
        ];
    }
}