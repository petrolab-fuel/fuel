<?php

namespace app\modules\master\models\data;

use app\modelsDB\ParameterBaru;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use app\modelsDB\DataAnalisaBaru;

class Parameters extends ParameterBaru
{
    /**
     * Undocumented function
     *
     * @return void
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), ['column_analisis_name' => 'Coloumn Analisa']);
    }

    /**
     * Undocumented function
     *
     * @return void
     */

    public function getActions()
    {
        return Html::tag(
            'div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => 'btn btn-default dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-expanded' => 'false'
            ]) . Html::tag(
                'ul',
                // Html::tag('li', Html::tag('a', '<i class="fa fa-caret-square-o-right"></i> view', [
                //     'href' => Url::toRoute(['/customers/default/view', 'id' => $this->CustomerID])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-edit"></i> Edit', [
                    'href' => Url::toRoute(['/master/parameters/edit', 'id' => $this->id])
                ])),
                ['class' => 'dropdown-menu']
            ),
            ['class' => 'btn-group']
        );
    }

    public function getColomnAnalisis()
    {
        $schema=DataAnalisaBaru::getTableSchema();
        $out=[];
        foreach ($schema->columns as $idx=>$value){
            if(strpos($idx,'_code')){
                $label=str_replace('_code','',$idx);
                if($label !='eval' && $label!='eval_oil_quality'){
                    $out[$label]=$idx;
                }
            }
        }
        return $out;
//        return [
//            'appearance' => 'appearance_code',
//            'density' => 'density_code',
//            'api_gravity' => 'api_gravity_code',
//            'specific_gravity' => 'specific_gravity_code',
//            'total_acid_number' => 'total_acid_number_code',
//            'strong_acid' => 'strong_acid_code',
//            'calorific_value' => 'calorific_value_code',
//            'calorific_value1' => 'calorific_value1_code',
//            'flash_point' => 'flash_point_code',
//            'kinematic_viscosity' => 'kinematic_viscosity_code',
//            'water_content' => 'water_content_code',
//            'ash_content' => 'ash_content_code',
//            'cloudn_point' => 'cloudn_point_code',
//            'pour_point' => 'pour_point_code',
//            'cetane_index' => 'cetane_index_code',
//            'conradson_carbon' => 'conradson_carbon_code',
//            'distillation_ibp' => 'distillation_ibp_code',
//            'distillation_5' => 'distillation_5_code',
//            'distillation_10' => 'distillation_10_code',
//            'distillation_20' => 'distillation_20_code',
//            'distillation_30' => 'distillation_30_code',
//            'distillation_40' => 'distillation_40_code',
//            'distillation_50' => 'distillation_50_code',
//            'distillation_60' => 'distillation_60_code',
//            'distillation_70' => 'distillation_70_code',
//            'distillation_80' => 'distillation_80_code',
//            'distillation_90' => 'distillation_90_code',
//            'distillation_95' => 'distillation_95_code',
//            'distillation_ep' => 'distillation_ep_code',
//            'distillation_recovery' => 'distillation_recovery_code',
//            'distillation_residue' => 'distillation_residue_code',
//            'distillation_loss' => 'distillation_loss_code',
//            'distillation_recovery_300' => 'distillation_recovery_300_code',
//            'sulphur_content' => 'sulphur_content_code',
//            'rancimat' => 'rancimat_code',
//            'lubricity_of_diesel' => 'lubricity_of_diesel_code',
//            'fame_content' => 'fame_content_code',
//            'cerosine_content' => 'cerosine_content_code',
//            'sediment' => 'sediment_code',
//            'colour' => 'colour_code',
//            'copper_corrosion' => 'copper_corrosion_code',
//            'particles_4' => 'particles_4_code',
//            'particles_6' => 'particles_6_code',
//            'particles_14' => 'particles_14_code',
//            'iso_4406' => 'iso_4406_code',
//            'silicon' => 'silicon_code',
//            'alumunium' => 'alumunium_code',
//            'monoglyceride' => 'monoglyceride_code',
//            'free_glyceride' => 'free_glyceride_code',
//            'total_glyceride' => 'total_glyceride_code',
//            'particulat' => 'particulat_code',
//            'cetan_number' => 'cetan_number_code',
//            'sulphated_ash'=>'sulphated_ash_code',
//            'phospor'=>'phospor_code',
//            'methyl_ester'=>'methyl_ester_code',
//            'iodium_number'=>'iodium_number_code',
//            'metal_i'=>'metal_i_code',
//            'metal_ii'=>'metal_ii_code',
//            'cfpp'=>'cfpp_code',
//            'calcium_ca'=>'calcium_ca_code',
//            'zn_zinc'=>'zn_zinc_code',
//        ];
    }
    public function getAttParameters(){
        $schema=DataAnalisaBaru::getTableSchema();
        $out=[];

        foreach ($schema->columns as $idx=>$value){
            if(strpos($idx,'_code')){
                $label=str_replace('_code','',$idx);
                if($label !='eval' && $label!='eval_oil_quality'){
                    $out[$label]=$label;
                }
            }
        }
        return $out;
//        $schema=SearchAnalisa::getTableSchema();
//        $a=[];
//        foreach ($schema->columns as $idx => $val){
//            if(strpos($idx,'code')){
//                $label=str_replace('_code','',$idx);
//                $a[$label]=$label;
//            }
//
//        }
//        print_r($a);
//        exit;
//        return [
//            'appearance' => 'Appearance',
//            'density' => 'Density',
//            'total_acid_number' => 'Total Acid Number',
//            'flash_point' => 'Flash Point',
//            'kinematic_viscosity' => 'Kinematic Viscosity',
//            'water_content' => 'Water Content',
//            'ash_content' => 'Ash Content',
//            'pour_point' => 'Pour Point',
//            'cetane_index' => 'Cetane Index',
//            'conradson_carbon' => 'Conradson Carbon',
//            'distillation_recov' => 'Distillation Recov',
//            'distillation_ibp' => 'Distillation Ibp',
//            'distillation_5' => 'Distillation 5',
//            'distillation_10' => 'Distillation 10',
//            'distillation_20' => 'Distillation 20',
//            'distillation_30' => 'Distillation 30',
//            'distillation_40' => 'Distillation 40',
//            'distillation_50' => 'Distillation 50',
//            'distillation_60' => 'Distillation 60',
//            'distillation_70' => 'Distillation 70',
//            'distillation_80' => 'Distillation 80',
//            'distillation_90' => 'Distillation 90',
//            'distillation_95' => 'Distillation 95',
//            'distillation_ep' => 'Distillation Ep',
//            'distillation_recovery' => 'Distillation Recovery',
//            'distillation_residue' => 'Distillation Residue',
//            'distillation_loss' => 'Distillation Loss',
//            'distillation_recovery_300' => 'Distillation Recovery 300',
//            'sulphur_content' => 'Sulphur Content',
//            'sediment' => 'Sediment',
//            'colour' => 'Colour',
//            'copper_corrosion' => 'Copper Corrosion',
//            'particles_4' => 'Particles 4',
//            'particles_6' => 'Particles 6',
//            'particles_14' => 'Particles 14',
//            'iso_4406' => 'Iso 4406',
//            'specific_gravity' => 'Specific Gravity',
//            'api_gravity' => 'Api Gravity',
//            'cloudn_point' => 'Cloudn Point',
//            'rancimat' => 'Rancimat',
//            'lubricity_of_diesel' => 'Lubricity Of Diesel',
//            'fame_content' => 'Fame Content',
//            'calorific_value' => 'Calorific Value',
//            'calorific_value1' => 'Calorific Value',
//            'strong_acid' => 'Strong Acid',
//            'cerosine_content' => 'Cerosine Content',
//            'silicon'=>'silicon',
//            'alumunium'=>'alumunium',
//            'particulat'=>'Particulat',
//            'monoglyceride'=>'Monoglyceride',
//            'free_glyceride' => 'free_glyceride',
//            'total_glyceride' => 'total_glyceride',
//            'cetan_number' => 'cetan_number',
//            'sulphated_ash'=>'sulphated_ash',
//            'phospor'=>'phospor',
//            'methyl_ester'=>'methyl_ester',
//            'iodium_number'=>'iodium_number',
//            'metal_i'=>'metal_i',
//            'metal_ii'=>'metal_ii',
//            'cfpp'=>'cfpp',
//            'calcium_ca'=>'Calcium',
//            'zn_zinc'=>'Zinc'
//        ];
    }

    /**
     * @var
     */
    public $_childParameter;
    public function getChildParameter(){
        if($this->_childParameter==null){
            $this->_childParameter=$this->hasMany(ChildParameters::className(),['parent'=>'id']);
        }
        return $this->_childParameter;
    }

    /**
     * @return ChildParameters[]|array|\yii\db\ActiveRecord[]
     */
    public function renderChildParam(){
        $q=ChildParameters::find()->where(['parent'=>$this->id])->orderBy('id')->all();
        return $q;
    }

}
