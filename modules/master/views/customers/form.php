<?php
/**
 * Created by PhpStorm.
 * User: Feri
 * Date: 10/23/2019
 * Time: 14:21
 */
use app\smartadmin\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
 $form = ActiveForm::begin([
    'id' => 'form-customer',
//    'layout' => 'form-group',
    'fieldConfig' => [
        'template' => "{label}<label class='input'>{input}</label>{error}",
        'labelOptions' => ['class' => 'control-label'],
    ],
     'options' => [
         'class' => 'smart-form',
         'id'=>'form-customer',
         ]
]); ?>

<fieldset>
    <legend>
        Lengkapi Data
    </legend>
    <div class="row">
        <div class="col col-4">
            <?= $form->field($model,'name')?>
        </div>
        <div class="col col-2">
            <?= $form->field($model,'phone')?>
        </div>
        <div class="col col-6">
            <?= $form->field($model,'address')?>
        </div>
    </div>

    <footer>
        <?= Html::submitButton(Yii::t('app', $model->isNewRecord ? 'Submit ' : 'Save '),
            ['class' => 'btn btn-primary']) ?>

        <?= \app\smartadmin\helpers\Html::a('<i class="fa fa-back"></i>
             Cancel', Url::toRoute(['/master/customers']), ['class' => 'btn btn-primary']) ?>
    </footer>

</fieldset>

<?php ActiveForm::end(); ?>

<?php
$jscode= <<< JS
$(function(){
    var form=$('#form-customer');
        form.submit(function(e) {
          e.preventDefault();
          e.stopImmediatePropagation();
          var formData=form.serialize();
          
          // console.log(formData);
          
          $.ajax({
                url:form.attr('action'),
                type:'POST',
                data:formData,
                // contentType: false,
                // processData: false,
                success:function(data, textStatus, jqXHR) {
                  console.log(data);
                  alert(data);
                },
                error:function(jqXHR, textStatus, errorThrown) {
                  console.log(jqXHR);
                }
          });
          return false;
          
        });
    // console.log('test');
});
JS;
$this->registerJs($jscode,4,'custcode');
?>

