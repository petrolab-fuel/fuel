<?php
/**
 * Created by PhpStorm.
 * User: Feri
 * Date: 10/23/2019
 * Time: 13:55
 */
use yii\helpers\Url;
// use app\modelsDB\Attention;
$this->title = Yii::t('app', 'Add Customer');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Customers'), 'url' => Url::toRoute(['/master/customers'])];
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="row">
        <!-- col -->
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h4 class="page-title txt-color-blueDark">
                <!-- PAGE HEADER -->
                <i class="fa-fw fa fa-home"></i>
                <?= $this->title ?>
            </h4>
        </div>
        <!-- end col -->
    </div>
    <!-- widget grid -->
    <section id="widget-grid" class="">
        <div class="col-sm-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                    <h2>customer</h2>
                </header>
                <!-- widget div-->
                <div>
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                        <input class="form-control" type="text">
                    </div>
                    <!-- end widget edit box -->
                    <!-- widget content -->
                    <div class="widget-body">
                        <?= $this->render('form',['model'=>$model])?>
                    </div>
                </div>
            </div>
        </div>
    </section>
