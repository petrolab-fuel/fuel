<?php

use yii\helpers\Html;
?>
<div class="col-md-8">
    <div class="param panel panel-primary">
        <table style="width: 100%;">
            <tr class="param title-header">
                <th>Parameter</th>
                <th>Unit</th>
                <th>Method</th>
                <th>Value</th>
                <th></th>
            </tr>
            <?php foreach ($model->renderParameter() as $param) {

//                $btn = $param->parameter->column_analisis_name == 'cetane_index' ? "<button type='button' class='btn btn-default' id='cetane'>Math</button>" : "";
                    echo Html::tag('tr',
                        Html::tag('td',$param->nama).
                                    Html::tag('td',$param->unit).
                                    Html::tag('td',$param->method).
                                    Html::tag('td',$param->col==''?'':Html::activeTextInput($model, $param->col, ['class' => 'form-control input-sm'])));

                foreach ($param->parameter->renderChildParam() as $child){
                     echo Html::tag('tr',
                        Html::tag('td',$child->nama).
                                    Html::tag('td',$child->unit).
                                    Html::tag('td',$child->method).
                                    Html::tag('td',$child->column_analisis_name==''?'':Html::activeTextInput($model, $child->column_analisis_name, ['class' => 'form-control input-sm'])));
                    }
                }
           ?>
        </table>
    </div>
</div>
<div class="col-md-4">
    <div class="param panel panel-primary">
        <table style="width: 100%;">
            <tr class="param title-header">
                <th>Recomendation</th>
                <th>Value</th>
            </tr>
            <tr>
                <td>Recomendation</td>
                <td>
                    <?= Html::activeTextarea($model, 'recommended1', ['class' => 'form-control input-sm', 'rows' => '10']) ?>
                </td>
            </tr>
        </table>
    </div>
</div>