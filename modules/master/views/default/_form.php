<?php

/**
 * Created by
 * User: Doni46
 * Date: 08/29/2019
 * Time: 4:16 PM
 */


use app\smartadmin\ActiveForm;
// use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\smartadmin\assets\plugins\DatePickerAssets;
// use app\smartadmin\assets\plugins\DataTableAsset;
// use conquer\select2\Select2Asset;
use yii\helpers\Json;

app\smartadmin\assets\plugins\PluginsAssets::register($this);
/* @var $this yii\web\View */
/* @var $model \app\modules\customer\models\Customer */
/* @var $form ActiveForm */
?>

<form class="" id="form-transaksi" action="<?= Url::to() ?>">
    <div class="row">
        <div class="col-sm-4">
            <div class=="form-group">
                <label>Paket</label>
                <?= Html::activeDropDownList($model, 'type_report_id', $model->listReportType, [
                    'class' => 'select2',
                    'style' => 'width:100%',
                    'prompt'=>'Pilih Paket'
                ]) ?>

            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <div class=="form-group">
                <label>Customer Type</label>
                <?= Html::activeDropDownList($model, 'customer_type', $model->customer_type == '' ? [] :
                    [$model->customer_type => $model->keteranganCustomer], [
                    'class' => 'select2ajax',
                    'style' => 'width:100%',
                    'data-url' => Url::toRoute(['/master/default/customer-type'])
                ]) ?>

            </div>

        </div>
        <div class="col-sm-3">
            <div class=="form-group">
                <label>SPB</label>
                <?= Html::activeTextInput($model, 'spb', ['class' => 'form-control input-sm']) ?>
            </div>
        </div>
        <div class="col-sm-3">
            <div class=="form-group">
                <label>PO</label>
                <?= Html::activeTextInput($model, 'po', ['class' => 'form-control input-sm']) ?>
            </div>
        </div>
        <div class="col-sm-3">
            <div class=="form-group">
                <label>Harga</label>
                <?= Html::activeTextInput($model, 'harga', ['class' => 'form-control input-sm']) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <div class="form-group">
                <label>Lab Number</label>
                <div class="input-group">
                    <?= Html::activeTextInput($model, 'lab_number', ['class' => 'form-control input-sm']) ?> <span class="input-group-btn">
                        <button class="btn btn-default btn-sm" type="button" id="bLab" data-url="<?= Url::toRoute(['/master/default/get-labnumber']) ?>">
                            <i class="fa fa-search"></i>
                        </button> </span>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class=="form-group">
                <label>Group Loc</label>
                <?= Html::activeTextInput($model, 'grouploc', ['class' => 'form-control input-sm']) ?>
            </div>
        </div>
        <div class="col-sm-3">
            <div class=="form-group">
                <label>Branch</label>
                <?= Html::activeDropDownList($model, 'branch', $model->branch == '' ? [] :
                    [$model->branch => $model->branch], [
                    'class' => 'form-control  select2 select2ajax',
                    'data-url' => Url::toRoute(['/master/customers/default/branch']),
                    'data-depend' => 'branch', 'data-depend-id' => 'formanalisa-customer_type'
                ]) ?>
            </div>
        </div>
        <div class="col-sm-3">
            <div class=="form-group">
                <label>Customer</label>
                <?= Html::activeDropDownList($model, 'customer_id', $model->customer_id == '' ? [] :
                    [$model->customer_id => $model->name], [
                    'class' => 'form-control select2ajax',
                    'data-url' => Url::toRoute(['/master/customers/default/customer']),
                    'data-depen' => 'branch',
                    'data-depend-from' => 'formanalisa-customer_type',
                    'data-depend-id' => 'formanalisa-branch'

                ]) . Html::activeHiddenInput($model, 'name', ['value' => $model->name]) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group">
                <label>Eng Builder</label>
                <?= Html::activeTextInput($model, 'eng_builder', ['class' => 'form-control input-sm']) ?>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label>Eng Type/Model</label>
                <?= Html::activeTextInput($model, 'eng_type', ['class' => 'form-control input-sm']) ?>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label>SN</label>
                <?= Html::activeTextInput($model, 'eng_sn', ['class' => 'form-control input-sm']) ?>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label>Location</label>
                <?= Html::activeTextInput($model, 'eng_location', ['class' => 'form-control input-sm']) ?>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label>Compartment/(Loaksi Sampling)</label>
                <?= Html::activeTextInput($model, 'compartment', ['class' => 'form-control input-sm']) ?>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label>Sample Name </label>
                <?= Html::activeDropDownList($model, 'type', $model->listSpesifikasi, [
                    'class' => 'form-control  select2 ajaxselect',
                    'data-url' => Url::toRoute(['/customers/default/list-branch'])
                ]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <div class="form-group">
                <label>Matrix </label>
                <?= Html::activeDropDownList($model, 'matrix_id', $model->listMatrix, [
                    'class' => 'form-control  select2',
                    'prompt'=>'Pilih Matrix',
                ]) ?>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label>Sampling Date </label>
                <div class="input-group">
                    <?= Html::activeTextInput($model, 'sampling_date', ['class' => 'form-control input-sm datepic', 'autocomplete' => 'off']) ?>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label>Received Date </label>
                <div class="input-group">
                    <?= Html::activeTextInput($model, 'received_date', ['class' => 'form-control input-sm datepic', 'autocomplete' => 'off']) ?>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label>Report Date </label>
                <div class="input-group">
                    <?= Html::activeTextInput($model, 'report_date', ['class' => 'form-control input-sm datepic', 'autocomplete' => 'off']) ?>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-2">
                <button class="btn btn-default" type="reset">
                    Cancel
                </button>
                <button class="btn btn-primary" type="submit">
                    <i class="fa fa-save"></i>
                    Submit
            </div>
            <div class="col-md-10">
                <button type="button" class="btn btn-sm pull-right" data-toggle="collapse" data-target="#more_field"><i class="fa fa-arrow-circle-down"> More Field</i></button>
            </div>
        </div>
    </div>
    <div class="row">
        <div id="more_field" class="col-md-12 collapse">
                <?= $this->render('_detail',['model'=>$model]) ?>
        </div>
    </div>
</form>

<?php
// Select2Asset::register($this);
DatePickerAssets::register($this);
// DataTableAsset::register($this);
$url_route=Url::toRoute(['/master/default']);
$url = Url::toRoute(['/master/default/save']);
$url_base = Url::toRoute(['/master/default/index']);
$cls = Json::encode($model::className());

$jsCode = <<< JS
$(function () {
    let form = $('#form-transaksi');
    let par = function () {
        return {
            width: '100%',
            ajax: {
                url: function (a) {
                    return this.attr('data-url');
                },
                data: function (params) {
                    let el = this;
                    let query = {
                        q: params.term,
                        param: {
                            col: el.attr('data-depend'),
                            fromcol: form.find('#'+ el.attr('data-depend-from')).val(),
                            filter: form.find('#' + el.attr('data-depend-id')).val(),
                            model: $cls,
                        }
                    };
                    return query;
                },
                processResults: function (data) {
                    return {
                        results: data.results
                    };
                }
            }
        }
    };
    
    form.find('.select2ajax').select2(par())
    form.find('.ajaxselect').select2({
        width: '100%',
        placeholder:'pilih',
    })
    // form.find('.select2').select2({
    //     width: '100%',
    //     // placeholder:'pilih',
    // })
    $('#formanalisa-type_report_id').on('select2:select',function (e){
        let data=e.params.data;
        $.ajax({
            method:'POST',
            data:{id:data.id},
            success:function (res){
                console.log(res);
                // return false
                $('#more_field').html(res)
            }
        })
    })
    form.find('#formanalisa-customer_id').on('select2:select',function(e){
        let data=e.params.data;
        form.find('#formanalisa-name').val(data.text)
        // console.log($('#analisa-name').val());

    });
    form.find('.datepic').datepicker({
                autoclose: true,
			    // dateFormat: 'dd.mm.yy',
                format: 'yyyy-mm-dd',
			    prevText: '<i class="fa fa-chevron-left"></i>',
			    nextText: '<i class="fa fa-chevron-right"></i>',
			    onSelect: function (selectedDate) {
			        $('#finishdate').datepicker('option', 'minDate', selectedDate);
		}
	});	

    $('#search').click(function(e){
        e.preventDefault();
        let textSearch=$('#cari').val();
        console.log(textSearch);
        $.ajax({
            url:$(this).attr('data-url'),
            type:'GET',
            data:{search:textSearch},
            success:function(res){
                //alert_notif(res)
                loadField(res.parameter,res.data)
            },
            error:function(jqXHR, textStatus, errorThrown){
                alert_notif({status:'error',message:errorThrown})
                // console.log(textStatus)

            }
        })
        
    })

    // let table = form.find('#place_table').dataTable({
    //     ajax: {
    //         data: function (d) {
    //             d.day = form.find('[name=day]').val()
    //         }
    //     },
    //     // serverSide: true,
    //     rowId: 'lab_number',
    //     columns: [
    //         {data: 'action', title: 'Action'},
    //         {data: 'lab_number', title: 'Lab Number'},
    //         {data: 'customerName', title: 'Customer'},
    //         {data: 'branch', title: 'Branch'},
    //         {data:'eng_type',title:'Unit'},
    //         {data:'eng_sn',title:'Serial No'},
    //         {data:'eng_location',title:'Location'},
    //         // {data: 'MODEL', title: 'Model'},
    //         // {data: 'UNIT_NO', title: 'Unit Number'},
    //         // {data: 'COMPONENT', title: 'Component'},
    //         {data: 'sampling_date', title: 'Sampling Date'},
    //         {data: 'received_date', title: 'Receive Date'},
    //         {data: 'report_date', title: 'Report Date'},
            
    //     ],
    // });
    $('#sparks').delegate('.btn-primary','click',function(e){
        e.preventDefault();
        $('#tbl-show').collapse('hide');
        $('#input-data').collapse('show');
    })
    form.find('[name=day]').on('change', function (e) {
        table.api().ajax.reload();
    });

    form.find('[name=day]').datepicker({
        autoclose: true,
			    // dateFormat: 'dd.mm.yy',
        format: 'yyyy-mm-dd'		  
    })

    function cetan(density,destilasi){
//         let G=Math.round(((parseFloat(141.5) /((parseFloat(density)+ parseFloat(0.5))* parseFloat(0.001)))-parseFloat(131.5)),1);
//             // let G=((parseFloat(141.5) /((parseFloat(density)+ parseFloat(0.5))* parseFloat(0.001)))-parseFloat(131.5));
//        //     let G=(density+0.5);
//                 // G=parseFloat(density)+parseFloat(0.5);
//        // console.log(G);
//        let M=(((parseInt (180)*  parseFloat(destilasi))+parseInt(3200) )* parseFloat(0.01));
//        let G2=Math.pow(G,parseInt(2));
//        // let result=Math.round(( parseFloat(-420.34)+(parseFloat( 0.016)*G2))+(parseFloat(0.191) *(G*Math.log10(M)))+(parseFloat(65.01) *(Math.pow(Math.log10(M),2)))-(parseFloat(0.0001809) *Math.pow(M,2)),1);
//         let result=( parseFloat(-420.34)+(parseFloat( 0.016)*G2))+(parseFloat(0.191) *(G*Math.log10(M)))+(parseFloat(65.01) *(Math.pow(Math.log10(M),2)))-(parseFloat(0.0001809) *Math.pow(M,2));

        // return result;
    }
    $('#cetane').on('click',function(e){
        let density=$('#formanalisa-density').val();
        let destilasi=$('#formanalisa-distillation_50').val();
        $.ajax({
            url:'$url_route'+'/cetan',
            type:'POST',
            data:{density:density,destilasi:destilasi},
            beforeSend:function (){
              if(density=='' || destilasi==''){
                   alert_notif({status: 'error', message: 'density atau destilasi 50 % empty'});
                   return false;
              }  
            },
            success:function (response){
                     form.find('#formanalisa-cetane_index').val(response);
            },
            error:function (jqXHR, textStatus, errorThrown){
                 alert_notif({status: 'error', message: jqXHR});
            }
        })
    })
    
    $(form).on('submit',function(e){
        e.preventDefault();
        let action=($(this).attr('action'));
        let formData = new FormData(this);
          $.ajax({
                url: $(this).attr('action') + '?token=' + Math.random(),
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                }).done(function(response) {
                     alert_notif(response);
                    // table.api().row.add(response.data_row).draw();
                            
                        if (action.indexOf('edit')>0){
                            window.open('$url_base','_self');
                                  return false;  
                        }
                     form.find('#formanalisa-lab_number').val(response.labNo);
                }).fail(function(jqXHR, textStatus, errorThrown){
                    alert_notif({status: 'error', message: jqXHR});
                })
        return false
    });

     $('#bLab').on('click',function(e){
        e.preventDefault();
        $.ajax({
            url:$(this).attr('data-url'),
            success: function(res){
                console.log(res);
                form.find('#formanalisa-lab_number').val(res.labno);
            },
            error: function(jqXHR, textStatus, errorThrown){

            }
        })
    });


    function alert_notif(res) {
        $.sound_on = false;
        var color = res.status === 'success' ? '#659265' : '#C46A69';
        var icon = res.status === 'success' ? 'fa-check' : 'fa-times';
        $.smallBox({
            title: res.status,
            content: res.message,
            color: color,
            iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
            timeout: 4000
        });
    }
    function loadField(params,data){
        if(data==null){

            form.find('#analisa-lab_number').val(null);
            form.find('#analisa-type').val(null).trigger('change');
            form.find('#analisa-customer_type').val(null).trigger('change');
            form.find('#analisa-sampling_date').val(null);
            form.find('#analisa-received_date').val(null);
            form.find('#analisa-report_date').val(null);
            form.find('#analisa-eng_type').val(null);  
            form.find('#analisa-eng_sn').val(null);
            form.find('#analisa-eng_location').val(null);
            form.find('#analisa-grouploc').val(null);
            form.find('#analisa-eng_builder').val(null);
            form.find('#analisa-compartment').val(null);
            form.find('#analisa-harga').val(null);
            form.find('#analisa-po').val(null);

        }else{
            form.find('#formanalisa-lab_number').val(data.lab_number);
            if (form.find("#formanalisa-branch option[value='" + data.branch + "']").length > 0) {
                form.find('#formanalisa-branch').val(data.branch).trigger('change');
            } else {
                form.find('#formanalisa-branch').append(new Option(data.branch, data.branch)).val(data.branch).trigger('change');
            }
            if (form.find("#formanalisa-customer_id option[value='" + data.customer_id + "']").length > 0) {
                form.find('#formanalisa-customer_id').val(data.customer_id).trigger('change');
            } else {
                form.find('#formanalisa-customer_id').append(new Option(data.name, data.customer_id)).val(data.customer_id).trigger('change');
            }
            form.find('#formanalisa-type').val(data.type).trigger('change');
            form.find('#formanalisa-tahun_matrix').val(data.tahun_matrix).trigger('change');
            form.find('#formanalisa-sampling_date').val(data.sampling_date);
            form.find('#formanalisa-received_date').val(data.received_date);
            form.find('#formanalisa-report_date').val(data.report_date);  
            form.find('#formanalisa-eng_type').val(data.eng_type);  
            form.find('#formanalisa-eng_sn').val(data.eng_sn);
            form.find('#formanalisa-eng_location').val(data.eng_location);
            form.find('#formanalisa-eng_location').val(data.grouploc);
            form.find('#formanalisa-eng_location').val(data.eng_builder);
            form.find('#formanalisa-eng_location').val(data.compartment);
            form.find('#formanalisa-harga').val(data.harga);
            form.find('#formanalisa-harga').val(data.po);
        } 
        // form.find('#analisa-branch').val(data.branch).trigger('change');
        var hasil,a
        $.each(params, function(key, value){
            let p="data."+key
                form.find('#analisa-'+key).val(eval(p))
           
        })
      } 

}); 
JS;

$this->registerJs($jsCode, 4, 'parameters');
