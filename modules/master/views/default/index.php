<?php

/**
 * Created by
 * User: Doni46
 * Date: 08/29/2019
 * Time: 4:16 PM
 */

/* @var $this yii\web\View */
/* @var $model \app\modules\customer\models\FormCustomer */

use yii\helpers\Url;
use yii\helpers\Html;
use app\smartadmin\assets\plugins\DatePickerAssets;

$this->title = Yii::t('app', 'Analisa');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">

    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-shopping-cart "></i>
            <?= $this->title ?>
        </h1>
    </div>

    <!-- right side of the page with the sparkline graphs -->
    <!-- col -->
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <!-- sparks -->
        <ul id="sparks">
            <li class="sparks-info .bg-color-teal">
                <?= Html::a('<i class="fa fa-plus-circle"></i>
                Add', Url::toRoute(['/master/default/new']), ['class' => 'btn btn-primary']) ?>
            </li>

        </ul>
        <!-- end sparks -->
    </div>
    <!-- end col -->

</div>

<!-- end row -->
<section id="widget-grid" class="">

    <!-- START ROW -->
    <div class="row">

        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-2" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-collapsed="false" data-widget-fullscreenbutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-filter"></i> </span>
                    <h2>Filter</h2>
                </header>
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <form action="" class="smart-form" id="form-filter" method="get">
                            <fieldset>
                                <div class="row">
                                    <section class="col col-4">
                                        <label class="label">Receive Date</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" name="recv_date_begin" value="" class="datepr" placeholder="Begin" autocomplete="off">
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">&nbsp;</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" name="recv_date_end" value="" class="datepr" placeholder="End" autocomplete="off">
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-4">
                                        <label class="label">Report Date</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" name="rpt_date_begin" value="" class="datepr" placeholder="Begin" autocomplete="off">
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">&nbsp;</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" name="rpt_date_end" value="" class="datepr" placeholder="End" autocomplete="off">
                                        </label>
                                    </section>
                                </div>


                            </fieldset>
                            <footer>
                                <a id="generate" class="btn btn-xs btn-primary">Search</a>
                                <?= Html::tag('a', '<i class="fa fa-gear"></i>Update Code', [
                                    'title' => 'Download Excel', 'data-action' => 'update_code', 'class' => 'btn btn-xs btn-default',
                                    'href' => Url::toRoute(['/master/default/update-code']),
                                ]) ?>
                                <?= Html::tag('a', '<i class="fa fa-file-audio-o"></i> Publish', [
                                    'title' => 'Download Excel', 'data-action' => 'update-status', 'class' => 'btn btn-xs btn-default',
                                    'href' => Url::toRoute(['/master/default/update-status']),
                                ]) ?>
                                <?= Html::tag('a', '<i class="fa fa-file-excel-o"></i> Download Excel', [
                                    'title' => 'Download Excel', 'data-action' => 'excel_default', 'class' => 'btn btn-xs btn-default',
                                    'href' => Url::toRoute(['/master/default/download-excel', 'csv' => false]),
                                ]) ?>
                            </footer>
                        </form>
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>


            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget well">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                    <h2>Form </h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content data show-->
                    <div id="data-show" class="widget-body no-padding">
                        <?= \app\modules\master\models\data\DataTableView::widget([
                            'columns' => [
                                'actions',
                                'status',
                                'publishData',
                                'typeCustomer',
                                'lab_number',
                                'name',
                                'branch',
                                'sampleDate',
                                'receiveDate',
                                'reportDate',
                                'type',
                                'eng_builder',
                                'eng_type',
                                'eng_sn',
                                'eng_location',
                                'harga',
                                'spb',
                                // 'jenis',
                                // 'tahun',
                                // 'min',
                                // 'max',
                                // 'typical',
                                // // 'Address',
                                // 'listUnit',
                                // 'status'
                                // 'listAttention',

                            ],
                            'options' => ['order' => "[8, 'desc']", 'id' => 'tbl-fuel'],
                            'model' => new \app\modules\master\models\data\Analisa(),
                        ]) ?>
                    </div>
                    <!-- end widget content data show-->
                    <!-- widget content data input-->

                    <!-- end widget content data show-->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget data data input-->



        </article>
        <!-- END COL -->

    </div>

    <!-- END ROW-->

</section>
<!-- end widget grid -->
<?php
DatePickerAssets::register($this);
$jsCode = <<< JS
$(function () {
    let form = $('#form-filter');
    
    console.log(form);
   form.find('.datepr').datepicker({
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        autocomplete:false,       
   });

    function alert_notif(res) {
        $.sound_on = false;
        var color = res.status === 'success' ? '#659265' : '#C46A69';
        var icon = res.status === 'success' ? 'fa-check' : 'fa-times';
        $.smallBox({
            title: res.status,
            content: res.message,
            color: color,
            iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
            timeout: 4000
        });
    }
});
JS;

$this->registerJs($jsCode, 4, 'ppr');
