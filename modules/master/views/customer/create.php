<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use app\smartadmin\Alert;

$this->title = 'Import Data';
$this->params['breadcrumbs'][] = 'Monitoring';
$this->params['breadcrumbs'][] = $this->title;
app\smartadmin\assets\plugins\PluginsAssets::register($this);
app\modules\master\assets\MasterAssets::register($this);
?>

