<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use app\smartadmin\Alert;

$this->title = 'Customer';
$this->params['breadcrumbs'][] = 'Master';
$this->params['breadcrumbs'][] = $this->title;
app\smartadmin\assets\plugins\PluginsAssets::register($this);
app\smartadmin\assets\plugins\DataTableAsset::register($this);
app\modules\master\assets\MasterAssets::register($this);

?>
<div>
    <section id="widget-grid">
        <!-- row -->
        <div class="row">
            <!-- SINGLE GRID -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <div class="jarviswidget jarviswidget-color-darken" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
                    <header>
                        <h2><strong>Customer</strong></h2>
                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget content -->
                        <div class="widget-body">

                            <button id="btn_add" class="btn btn-primary btn-sm">
                                <span class="fa fa-plus"></span> Add Customer</button>
                            <table id="table_customer" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <!-- <th>Options</th> -->
                                    <th>Option</th>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Attention</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article><!-- GRID END -->
        </div><!-- end row -->
    </section>

</div>


<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:750px;">

        <!-- Modal content-->
        <div class="modal-content">
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                            <!-- widget options:
                            usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                            data-widget-colorbutton="false"
                            data-widget-editbutton="false"
                            data-widget-togglebutton="false"
                            data-widget-deletebutton="false"
                            data-widget-fullscreenbutton="false"
                            data-widget-custombutton="false"
                            data-widget-collapsed="true"
                            data-widget-sortable="false"

                            -->
                            <header>
                                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                                <h2>Customer </h2>

                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->

                                </div>
                                <!-- end widget edit box -->

                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <form class="smart-form" id="frm">
                                        <fieldset>
                                            <section>
                                                <label class="label">Name</label>
                                                <label class="input">
                                                    <input type="text" class="input-sm" id="nama" name="nama">
                                                </label>
                                            </section>
                                            <section>
                                                <label class="label">Adress</label>
                                                <label class="input">
                                                    <input type="text" class="input-sm" id="address" name="address">
                                                </label>

                                            </section>
                                            <section>
                                                <table id="tbl" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th style="width: 95%">Name</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                           <td> <button type="button" id="del_att" name="add_att" class="btn btn-primary btn-xs">Del</button></td>
                                                            <td><input type="text" class="input-xs" style="width: 95%" id="nm" name="nm[]"></td>
                                                            <input type="hidden" id="urut" name="urut">
                                                        </tr>
                                                    </tbody>
                                                 <tfoot>
                                                    <tr>
                                                        <td colspan="2"><button type="button" id="add_att" name="add_att" class="btn btn-primary btn-xs">Attention</button></td>
                                                    </tr>
                                                 </tfoot>
                                                </table>

                                            </section>
                                        </fieldset>
                                        <footer>
                                            <button type="submit" class="btn btn-primary">
                                                Save
                                            </button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                Cancel
                                            </button>
                                        </footer>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </section>
        </div>

    </div>
</div>