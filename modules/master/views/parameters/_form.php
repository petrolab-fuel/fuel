<?php

/**
 * Created by
 * User: Doni46
 * Date: 08/29/2019
 * Time: 4:16 PM
 */


use app\smartadmin\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \app\modules\customer\models\Customer */
/* @var $form ActiveForm */
?>
<div class="add-_form">

    <?php $form = ActiveForm::begin([
        // 'layout' => 'horizontal',
        'id' => 'form-parameters',
        'fieldConfig' => [
            'template' => "{label}<label class='input'>{input}</label>{error}",
            'inputOptions' => ['class' => 'form-control'],
        ],
        'options' => [
            'class' => 'smart-form',
        ],
    ]); ?>

    <header>
        Lengkapi field di bawah ini.
    </header>

    <fieldset>
        <div class="row">
            <section class="col col-4">
                <?= $form->field($model, 'nama') ?>
            </section>
            <section class="col col-2">
                <?= $form->field($model, 'unit') ?>
            </section>
            <section class="col col-3">
                <?= $form->field($model, 'method') ?>
            </section>
            <section class="col col-3">
                <?= $form->field($model, 'column_analisis_name')->dropDownList($model->attParameters,
                 [
                     'prompt'=>'Pilih Column Analisa',
                    'class' => 'form-control select2ajax',
                    'data-url' => Url::toRoute(['/fuel/parameters/column-name']),
                ]) ?>
            </section>

        </div>
        <div class="row">
            <section id="tbl">
                <?= $model->renderChild($form) ?>
                <a data-action="add_row_table" class="btn btn-primary btn-xs btn-block">
                    <i class="fa fa-plus-circle"></i> Add Parameter
                </a>
            </section>
        </div>

    </fieldset>

    <footer>
        <?= Html::submitButton(
            Yii::t('app', $model->isNewRecord ? 'Submit ' : 'Save '),
            ['class' => 'btn btn-primary']
        ) ?>
    </footer>

    <?php ActiveForm::end(); ?>

</div>
<?php

$jsCode = <<< JS
$(function () {

    let form = $('#form-parameters');
    let par = function () {
        return {
            ajax: {
                url: function (a) {
                    return this.attr('data-url');
                },
                
                processResults: function (data) {
                    return {
                        results: data.results
                    };
                }
            }
        }
    };
       const select_detail=(t)=>{
            t.find('.select2ajax').select2({
                width:'100%'
            });   
       }
    form.find('.select2ajax').select2({
        width:'100%'
    })

            $('#tbl').delegate('[data-action=add_row_table]','click',function (e){
            e.preventDefault();    
            let elm=$(this);
            let table=$('section').find('table');
            let form=elm.closest('form');
                 $.ajax({
                method: 'post',
                data: {'new_row': (table.find('tr').length - 1)},
                error: function (response) {
                    console.log(response)
                    // alert_notif({status: "error", message: 'tidak bisa save'});
                },
                success: function (response) {
                    var t = $(response.data);
                    console.log(t);
                    select_detail(t)
                    // genselectother(t.attr('data-id'));
                    table.find('tbody').append(t);
    
                    $.each(response.jsAdd, function (i, v) {
                        var vali = eval("var f = function(){ return " + v.validate.expression + ";}; f() ;");
                        v.validate = vali;
                        $(form).yiiActiveForm('add', v);
                    });
                }
            });
        })
        $('#tbl').delegate('[data-action=delete_row]', 'click', function () {
        var elm = $(this);
        var row = elm.closest('tr');
        p ='ChildParameters[new]';
        p = p.replace('[new]', '[' + row.attr('data-id') + ']');
        row.html('<input type="hidden" name="' + p + '[actionId]" value="del" >');
        // $(form).yiiActiveForm('remove','paramater_id-'+ row.attr('data-id'));
    });

    
});
JS;

$this->registerJs($jsCode, 4, 'parameters');
