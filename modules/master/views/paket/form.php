<?php
use app\smartadmin\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
?>

<div>
    <?php $form= ActiveForm::begin([
        'id'=>'form-spesifikasi',
        'options' => [
             'class' => 'smart-form',

            ],
    ]) ?>

    <fieldset>
        <div class="row">
            <section class="col col-6">
                <?= $form->field($model,'name')?>
            </section>
            <section class="col col-6">
                <?= $form->field($model,'rk_no')?>
            </section>
        </div>
        <div class="row">
            <section id="tbl">
                <?= $model->renderTypeParameter($form) ?>
                <a data-action="add_row_table" class="btn btn-primary btn-xs btn-block">
                    <i class="fa fa-plus-circle"></i> Add Parameter
                </a>
            </section>

        </div>
    </fieldset>
    <footer>
        <?= Html::submitButton($model->isNewRecord?'Save':'Update',['class'=>'btn btn-primary']) ?>
    </footer>

    <?php ActiveForm::end() ?>

</div>
<?php
$ajax=<<<JS
    $(function (){
        let form=$('#form-spesifikasi');
        
         let par = function () {
        return {
            width: "100%",
            ajax: {
                url: function (a) {
                    return this.attr('data-url');
                },
                data: function (params) {
                    let el = this;
                    let query = {
                        q: params.term,
                    };
                    return query;
                },
                processResults: function (data) {
                    return {
                        results: data.results
                    };
                }
            }
        }
    };
         
       $('.select2').select2(par()) 
       const select_detail=(t)=>{
            t.find('.select2').select2(par());   
       }
          
        $('#tbl').delegate('[data-action=add_row_table]','click',function (e){
            e.preventDefault();    
            let elm=$(this);
            let table=$('section').find('table');
            let form=elm.closest('form');
                 $.ajax({
                method: 'post',
                data: {'new_row': (table.find('tr').length - 1)},
                error: function (response) {
                    console.log(response)
                    // alert_notif({status: "error", message: 'tidak bisa save'});
                },
                success: function (response) {
                    var t = $(response.data);
                    console.log(t);
                    select_detail(t)
                    // genselectother(t.attr('data-id'));
                    table.find('tbody').append(t);
    
                    $.each(response.jsAdd, function (i, v) {
                        var vali = eval("var f = function(){ return " + v.validate.expression + ";}; f() ;");
                        v.validate = vali;
                        $(form).yiiActiveForm('add', v);
                    });
                }
            });
        })
        $('#tbl').delegate('[data-action=delete_row]', 'click', function () {
        var elm = $(this);
        var row = elm.closest('tr');
        p ='TypeReportParameters[new]';
        p = p.replace('[new]', '[' + row.attr('data-id') + ']');
        row.html('<input type="hidden" name="' + p + '[actionId]" value="del" >');
        // $(form).yiiActiveForm('remove','paramater_id-'+ row.attr('data-id'));
    });
    })
JS;
$this->registerJs($ajax,4,'oke');