<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$db1 = require __DIR__ . '/db1.php';
$db2 = require __DIR__ . '/db2.php';
$db3 = require __DIR__ . '/db3.php';


$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'users'],
    'timeZone' => 'Asia/Singapore',
    'aliases' => [
        '@bower' => '@vendor/bower',
        '@npm'   => '@vendor/npm-asset',
        '@arsipdir' => dirname(__DIR__) . '/smartadmin/resources/assets/data',
    ],
    'components' => [
        'HM' => [
            'class' => 'app\helpers\HelpMe'
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'Gb3zq_dIDTzOuoH1aLtkTUL5BDQlBQ2Q',
            // 'baseUrl'=>''
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'db1' => $db1,
        'db2' => $db2,
        'db3' => $db3,

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'site/index',
                '<action:(login|logout|about|contact|register|profile|change-password|reset-password|request-password-reset)>'
                => 'site/<action>',
                '<controller>/v/<id:\d+>' => '<controller>/view',
                '<controller>/u/<id:\d+>' => '<controller>/update',
                '<controller>/<action>' => '<controller>/<action>',
                '<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>' => '<controller>/<action>',
                '<controller:[\w\-]+>/<action:[\w\-]+>' => '<controller>/<action>',
                '<module>/<controller>/v/<id:\d+>' => '<module>/<controller>/view',
                '<module>/<controller>/u/<id:\d+>' => '<module>/<controller>/update',
                '<module>/<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module>/<controller:[\w\-]+>/<action:[\w\-]+>' => '<module>/<controller>/<action>',
            ],
        ],

        'assetManager' => [
            'linkAssets' => true,
        ],

        'formatter' => [
            'dateFormat' => 'dd-MM-yyyy',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => 'ID',
            'nullDisplay' => '',
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@app/smartadmin/views'
                ],
            ],
        ],

    ],
    'modules' => [
        'users' => [
            'class' => 'app\modules\users\Module',
        ],
        'ajax' => [
            'class' => 'app\modules\ajax\Module',
        ],
        'monitoring' => [
            'class' => 'app\modules\monitoring\Module',
        ],
        'master' => [
            'class' => 'app\modules\master\Module',
        ],
        'reports' => [
            'class' => 'app\modules\reports\Module',
        ],
    ],

    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
