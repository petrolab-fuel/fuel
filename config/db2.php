<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=192.168.2.11;port=3306;dbname=pama',
    'username' => 'db_user',
    'password' => 'dbuser',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
