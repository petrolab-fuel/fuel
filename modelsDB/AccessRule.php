<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "access_rule".
 *
 * @property int $id
 * @property string $role_name
 * @property string $role_description
 * @property string $rule_settings
 * @property string $time_create
 * @property string $time_update
 *
 * @property Users[] $users
 */
class AccessRule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'access_rule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_name', 'time_create', 'time_update'], 'required'],
            [['rule_settings'], 'string'],
            [['time_create', 'time_update'], 'safe'],
            [['role_name'], 'string', 'max' => 50],
            [['role_description'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role_name' => 'Role Name',
            'role_description' => 'Role Description',
            'rule_settings' => 'Rule Settings',
            'time_create' => 'Time Create',
            'time_update' => 'Time Update',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['access_rule_id' => 'id']);
    }
}
