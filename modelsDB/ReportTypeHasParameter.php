<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "report_type_has_parameter".
 *
 * @property int $report_type_id
 * @property int $parameter_id
 * @property string $p_code
 * @property int $p_order
 *
 * @property Parameter $parameter
 * @property ReportType $reportType
 */
class ReportTypeHasParameter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_type_has_parameter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_type_id', 'parameter_id'], 'required'],
            [['report_type_id', 'parameter_id'], 'integer'],
            [['parameter_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['parameter_id' => 'id']],
            [['report_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ReportType::className(), 'targetAttribute' => ['report_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'report_type_id' => 'Report Type ID',
            'parameter_id' => 'Parameter',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameter()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'parameter_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportType()
    {
        return $this->hasOne(ReportType::className(), ['id' => 'report_type_id']);
    }
}
