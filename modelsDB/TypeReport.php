<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "type_report".
 *
 * @property int $id
 * @property string $name
 * @property string $lab_no_temp
 * @property string $template
 * @property string $rk_no
 * @property string $draft_rk_no
 * @property string $header_name
 * @property string $deskription
 * @property int $history
 * @property int $reference
 * @property string $limit_type
 * @property int $mpc
 * @property int $blotter_spot_test
 * @property int $ffp
 * @property int $ptest
 * @property int $chart
 * @property int $active
 * @property int $type_view_id
 *
 * @property DataAnalisa[] $dataAnalisas
 * @property ParameterHasTypeReport[] $parameterHasTypeReports
 * @property Parameter[] $parameters
 * @property Spesifikasi[] $spesifikasis

 */
class TypeReport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['template', 'deskription'], 'string'],
            [['history', 'reference', 'mpc', 'blotter_spot_test', 'ffp', 'ptest', 'chart', 'active'], 'integer'],
            [['name', 'lab_no_temp', 'rk_no', 'draft_rk_no'], 'string', 'max' => 100],
            [['header_name'], 'string', 'max' => 200],
            [['limit_type'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'lab_no_temp' => Yii::t('app', 'Lab No Temp'),
            'template' => Yii::t('app', 'Template'),
            'rk_no' => Yii::t('app', 'Rk No'),
            'draft_rk_no' => Yii::t('app', 'Draft Rk No'),
            'header_name' => Yii::t('app', 'Header Name'),
            'deskription' => Yii::t('app', 'Deskription'),
            'history' => Yii::t('app', 'History'),
            'reference' => Yii::t('app', 'Reference'),
            'limit_type' => Yii::t('app', 'Limit Type'),
            'mpc' => Yii::t('app', 'Mpc'),
            'blotter_spot_test' => Yii::t('app', 'Blotter Spot Test'),
            'ffp' => Yii::t('app', 'Ffp'),
            'ptest' => Yii::t('app', 'Ptest'),
            'chart' => Yii::t('app', 'Chart'),
            'active' => Yii::t('app', 'Active'),
            'type_view_id' => Yii::t('app', 'Type View ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataAnalisas()
    {
        return $this->hasMany(DataAnalisa::className(), ['type_report_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameterHasTypeReports()
    {
        return $this->hasMany(ParameterHasTypeReport::className(), ['type_report_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameters()
    {
        return $this->hasMany(Parameter::className(), ['id' => 'parameter_id'])->viaTable('parameter_has_type_report', ['type_report_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpesifikasis()
    {
        return $this->hasMany(Spesifikasi::className(), ['type_report_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

}
