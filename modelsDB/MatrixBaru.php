<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "matrix".
 *
 * @property int $id
 * @property int $parameter_id
 * @property string $jenis
 * @property string $tahun
 * @property string $min
 * @property string $max
 * @property string $typical
 * @property string $satuan
 * @property string $method
 *
 * @property Parameter $parameter
 */
class MatrixBaru extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'matrix';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jenis','min', 'max', 'typical', 'satuan', 'method','tahun'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parameter_id' => 'Parameter ID',
            'jenis' => 'Jenis',
            'min' => 'Min',
            'max' => 'Max',
            'typical' => 'Typical',
            'satuan' => 'Satuan',
            'method' => 'Method',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getParameters()
//    {
//        return $this->hasOne(Parameter::className(), ['id' => 'parameter_id']);
//    }
}
