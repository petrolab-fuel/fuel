<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "unit".
 *
 * @property int $id
 * @property int $customer_id
 * @property string $location
 * @property string $serial_number
 * @property string $model_unit
 * @property string $type_unit
<<<<<<< HEAD
=======

>>>>>>> 02aab0ab5babd17c51393945f36e75e08fdda9fb
 *
 * @property DataAnalisa[] $dataAnalisas
 * @property Customer $customer
 */
class Unit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id'], 'required'],
            [['customer_id'], 'integer'],
            [['location'], 'string', 'max' => 200],
            [['serial_number', 'model_unit', 'type_unit'], 'string', 'max' => 45],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'customer_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer',
            'location' => 'Location',
            'serial_number' => 'Serial Number',
            'model_unit' => 'Engine Builder',
            'type_unit' => 'Engine Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataAnalisas()
    {
        return $this->hasMany(DataAnalisa::className(), ['unit_id' => 'id']);
    }

    public function getSamplingPoint()
    {
        return $this->hasMany(SamplingPoint::className(), ['unit_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['customer_id' => 'customer_id']);
    }
}
