<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "report_type".
 *
 * @property int $id
 * @property string $name
 * @property string $tarmplate
 *
 * @property DataAnalisa[] $dataAnalisas
 * @property ReportTypeHasParameter[] $reportTypeHasParameters
 * @property Parameter[] $parameters
 */
class ReportType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tarmplate'], 'string'],
            [['name'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'tarmplate' => 'Tarmplate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getDataAnalisas()
//    {
//        return $this->hasMany(DataAnalisa::className(), ['report_type_id' => 'id']);
//    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportTypeHasParameters()
    {
        return $this->hasMany(ReportTypeHasParameter::className(), ['report_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameters()
    {
        return $this->hasMany(Parameter::className(), ['id' => 'parameter_id'])->viaTable('report_type_has_parameter', ['report_type_id' => 'id']);
    }
}
