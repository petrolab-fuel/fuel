<?php

namespace app\modelsDB;

use app\component\ArrayHelper;
use Yii;

/**
 * This is the model class for table "matrix".
 *
 * @property int $id
 * @property int $parameter_id
 * @property string $condition1
 * @property string $condition2
 * @property string $condition3
 * @property string $condition4
 * @property string $condition5
 * @property string $recom1
 * @property string $rekom2
 * @property string $rekom3
 * @property string $rekom4
 * @property string $jenis
 * @property string $tahun
 * @property string $satuan
 * @property string $method
 * @property array $jenisFuel
 * @property array $tahunArray
 * @property Parameter $parameter
 */
class Matrix extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'matrix';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parameter_id'], 'required'],
            [['parameter_id'], 'integer'],
            [['recom1', 'rekom2', 'rekom3', 'rekom4'], 'string'],
            [['tahun','jenis','satuan','method','condition1', 'condition2', 'condition3', 'condition4', 'condition5'], 'string', 'max' => 45],
            [['parameter_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['parameter_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parameter_id' => 'Parameter ID',
            'condition1' => 'Min',
            'condition2' => 'Max',
            'condition3' => 'Batasan di Report',
            'condition4' => 'Condition4',
            'condition5' => 'Condition5',
            'recom1' => 'Recom1',
            'rekom2' => 'Rekom2',
            'rekom3' => 'Rekom3',
            'rekom4' => 'Rekom4',
            'tahun'=>'Tahun',
            'jenis'=>'Jenis',
            'satuan'=>'Unit',
            'method'=>'Method',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameter()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'parameter_id']);
    }

    public function getSpesifikasi(){
        return $this->hasOne(Spesifikasi::className(),['name'=>'jenis']);
    }

    public function getListSpesifikasi(){
        return ArrayHelper::map(Spesifikasi::find()->all(),'name','name');
    }
    // public function getJenisFuel(){
    //     return [
    //         'FUEL'=>'FUEL',
    //         'B20(CN 48)'=>'B20(CN 48)',
    //         'B30(CN 48)'=>'B30(CN 48)',
    //         'B30(CN 51)'=>'B30(CN 51)',
    //         'MFO'=>'MFO',
    //         'MFO 1'=>'MFO 1',
    //         'MFO 2'=>'MFO 2',
    //         'HSD'=>'HSD',
    //         'MDO'=>'MDO',
    //         'FAME/B100'=>'FAME/B100',
    //     ];
    // }


    public function getTahunArray(){
        $thn=date("Y");
        $thn=$thn-10;
//        $ArrTahun=[];
        for($i=1;$i<=10;$i++){
            $thn++;
            $ArrTahun[$thn]=$thn;
        }
        return $ArrTahun;
    }
}
