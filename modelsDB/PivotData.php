<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "pivot_data".
 *
 * @property int $id
 * @property string $received_date
 * @property string $start_analisa
 * @property string $end_analisa
 * @property string $report_date
 * @property string $sampling_date
 * @property string $lab_number
 * @property int $customer_id
 * @property int $unit_id
 * @property int $report_type_id
 * @property string $appearance
 * @property string $appearance_code
 * @property string $colour
 * @property string $colour_code
 * @property string $breakdown_voltage
 * @property string $breakdown_voltage_code
 * @property string $water_content
 * @property string $water_content_code
 * @property string $acidity
 * @property string $acidity_code
 * @property string $dielectric_dissipation_factor
 * @property string $dielectric_dissipation_factor_code
 * @property string $resistivity
 * @property string $resistivity_code
 * @property string $inhibitor_content
 * @property string $inhibitor_content_code
 * @property string $passivator_content
 * @property string $passivator_content_code
 * @property string $sediment_and_sludge
 * @property string $sediment_and_sludge_code
 * @property string $interfacial_tension
 * @property string $interfacial_tension_code
 * @property string $corrosive_sulfur
 * @property string $corrosive_sulfur_code
 * @property string $sludge_content
 * @property string $sludge_content_code
 * @property string $kinematic_viscosity
 * @property string $kinematic_viscosity_code
 * @property string $particles_4
 * @property string $particles_6
 * @property string $particles_14
 * @property string $particles_4_code
 * @property string $particles_6_code
 * @property string $particles_14_code
 * @property string $flash_point
 * @property string $flash_point_code
 * @property string $pcb
 * @property string $pcb_code
 * @property string $oil_quality_index
 * @property string $oil_quality_index_code
 * @property string $density
 * @property string $density_code
 * @property string $specific_gravity
 * @property string $specific_gravity_code
 * @property string $metal_fe
 * @property string $metal_cu
 * @property string $metal_al
 * @property string $metal_fe_code
 * @property string $metal_cu_code
 * @property string $metal_al_code
 * @property string $note_oil_quality
 * @property string $recommended1
 * @property string $recommended2
 * @property string $recommended3
 * @property string $hydrogen
 * @property string $hydrogen_code
 * @property string $methane
 * @property string $methane_code
 * @property string $ethane
 * @property string $ethane_code
 * @property string $ethylene
 * @property string $ethylene_code
 * @property string $acetylene
 * @property string $acetylene_code
 * @property string $carbon_monoxide
 * @property string $carbon_monoxide_code
 * @property string $carbon_dioxide
 * @property string $carbon_dioxide_code
 * @property string $oxygen
 * @property string $oxygen_code
 * @property string $nitrogen
 * @property string $nitrogen_code
 * @property string $persen_gas_by_volume
 * @property string $persen_gas_by_volume_code
 * @property string $tdcg
 * @property string $tdcg_code
 * @property string $co2_co
 * @property string $co2_co_code
 * @property string $equipment_condition
 * @property string $equipment_condition_code
 * @property string $furan_5h2f
 * @property string $furan_5h2f_code
 * @property string $furan_2fol
 * @property string $furan_2fol_code
 * @property string $furan_2fal
 * @property string $furan_2fal_code
 * @property string $furan_2acf
 * @property string $furan_2acf_code
 * @property string $furan_5m2f
 * @property string $furan_5m2f_code
 * @property string $furan_total
 */
class PivotData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pivot_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['received_date', 'start_analisa', 'end_analisa', 'report_date', 'sampling_date'], 'safe'],
            [['customer_id', 'unit_id'], 'required'],
            [['customer_id', 'unit_id', 'report_type_id'], 'integer'],
            [['note_oil_quality', 'recommended1'], 'string'],
            [['lab_number', 'appearance', 'appearance_code', 'colour', 'colour_code', 'breakdown_voltage', 'breakdown_voltage_code', 'water_content', 'water_content_code', 'acidity', 'acidity_code', 'dielectric_dissipation_factor', 'dielectric_dissipation_factor_code', 'resistivity', 'resistivity_code', 'inhibitor_content', 'inhibitor_content_code', 'passivator_content', 'passivator_content_code', 'sediment_and_sludge', 'sediment_and_sludge_code', 'interfacial_tension', 'interfacial_tension_code', 'corrosive_sulfur', 'corrosive_sulfur_code', 'sludge_content', 'sludge_content_code', 'kinematic_viscosity', 'kinematic_viscosity_code', 'particles_4', 'particles_6', 'particles_14', 'particles_4_code', 'particles_6_code', 'particles_14_code', 'flash_point', 'flash_point_code', 'pcb', 'pcb_code', 'oil_quality_index', 'oil_quality_index_code', 'density', 'density_code', 'specific_gravity', 'specific_gravity_code', 'metal_fe', 'metal_cu', 'metal_al', 'metal_fe_code', 'metal_cu_code', 'metal_al_code', 'recommended2', 'recommended3', 'hydrogen', 'hydrogen_code', 'methane', 'methane_code', 'ethane', 'ethane_code', 'ethylene', 'ethylene_code', 'acetylene', 'acetylene_code', 'carbon_monoxide', 'carbon_monoxide_code', 'carbon_dioxide', 'carbon_dioxide_code', 'oxygen', 'oxygen_code', 'nitrogen', 'nitrogen_code', 'persen_gas_by_volume', 'persen_gas_by_volume_code', 'tdcg', 'tdcg_code', 'co2_co', 'co2_co_code', 'equipment_condition', 'equipment_condition_code', 'furan_5h2f', 'furan_5h2f_code', 'furan_2fol', 'furan_2fol_code', 'furan_2fal', 'furan_2fal_code', 'furan_2acf', 'furan_2acf_code', 'furan_5m2f', 'furan_5m2f_code', 'furan_total'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'received_date' => 'Received Date',
            'start_analisa' => 'Start Analisa',
            'end_analisa' => 'End Analisa',
            'report_date' => 'Report Date',
            'sampling_date' => 'Sampling Date',
            'lab_number' => 'Lab Number',
            'customer_id' => 'Customer ID',
            'unit_id' => 'Unit ID',
            'report_type_id' => 'Report Type ID',
            'appearance' => 'Appearance',
            'appearance_code' => 'Appearance Code',
            'colour' => 'Colour',
            'colour_code' => 'Colour Code',
            'breakdown_voltage' => 'Breakdown Voltage',
            'breakdown_voltage_code' => 'Breakdown Voltage Code',
            'water_content' => 'Water Content',
            'water_content_code' => 'Water Content Code',
            'acidity' => 'Acidity',
            'acidity_code' => 'Acidity Code',
            'dielectric_dissipation_factor' => 'Dielectric Dissipation Factor',
            'dielectric_dissipation_factor_code' => 'Dielectric Dissipation Factor Code',
            'resistivity' => 'Resistivity',
            'resistivity_code' => 'Resistivity Code',
            'inhibitor_content' => 'Inhibitor Content',
            'inhibitor_content_code' => 'Inhibitor Content Code',
            'passivator_content' => 'Passivator Content',
            'passivator_content_code' => 'Passivator Content Code',
            'sediment_and_sludge' => 'Sediment And Sludge',
            'sediment_and_sludge_code' => 'Sediment And Sludge Code',
            'interfacial_tension' => 'Interfacial Tension',
            'interfacial_tension_code' => 'Interfacial Tension Code',
            'corrosive_sulfur' => 'Corrosive Sulfur',
            'corrosive_sulfur_code' => 'Corrosive Sulfur Code',
            'sludge_content' => 'Sludge Content',
            'sludge_content_code' => 'Sludge Content Code',
            'kinematic_viscosity' => 'Kinematic Viscosity',
            'kinematic_viscosity_code' => 'Kinematic Viscosity Code',
            'particles_4' => 'Particles 4',
            'particles_6' => 'Particles 6',
            'particles_14' => 'Particles 14',
            'particles_4_code' => 'Particles 4 Code',
            'particles_6_code' => 'Particles 6 Code',
            'particles_14_code' => 'Particles 14 Code',
            'flash_point' => 'Flash Point',
            'flash_point_code' => 'Flash Point Code',
            'pcb' => 'Pcb',
            'pcb_code' => 'Pcb Code',
            'oil_quality_index' => 'Oil Quality Index',
            'oil_quality_index_code' => 'Oil Quality Index Code',
            'density' => 'Density',
            'density_code' => 'Density Code',
            'specific_gravity' => 'Specific Gravity',
            'specific_gravity_code' => 'Specific Gravity Code',
            'metal_fe' => 'Metal Fe',
            'metal_cu' => 'Metal Cu',
            'metal_al' => 'Metal Al',
            'metal_fe_code' => 'Metal Fe Code',
            'metal_cu_code' => 'Metal Cu Code',
            'metal_al_code' => 'Metal Al Code',
            'note_oil_quality' => 'Note Oil Quality',
            'recommended1' => 'Recommended1',
            'recommended2' => 'Recommended2',
            'recommended3' => 'Recommended3',
            'hydrogen' => 'Hydrogen',
            'hydrogen_code' => 'Hydrogen Code',
            'methane' => 'Methane',
            'methane_code' => 'Methane Code',
            'ethane' => 'Ethane',
            'ethane_code' => 'Ethane Code',
            'ethylene' => 'Ethylene',
            'ethylene_code' => 'Ethylene Code',
            'acetylene' => 'Acetylene',
            'acetylene_code' => 'Acetylene Code',
            'carbon_monoxide' => 'Carbon Monoxide',
            'carbon_monoxide_code' => 'Carbon Monoxide Code',
            'carbon_dioxide' => 'Carbon Dioxide',
            'carbon_dioxide_code' => 'Carbon Dioxide Code',
            'oxygen' => 'Oxygen',
            'oxygen_code' => 'Oxygen Code',
            'nitrogen' => 'Nitrogen',
            'nitrogen_code' => 'Nitrogen Code',
            'persen_gas_by_volume' => 'Persen Gas By Volume',
            'persen_gas_by_volume_code' => 'Persen Gas By Volume Code',
            'tdcg' => 'Tdcg',
            'tdcg_code' => 'Tdcg Code',
            'co2_co' => 'Co2 Co',
            'co2_co_code' => 'Co2 Co Code',
            'equipment_condition' => 'Equipment Condition',
            'equipment_condition_code' => 'Equipment Condition Code',
            'furan_5h2f' => 'Furan 5h2f',
            'furan_5h2f_code' => 'Furan 5h2f Code',
            'furan_2fol' => 'Furan 2fol',
            'furan_2fol_code' => 'Furan 2fol Code',
            'furan_2fal' => 'Furan 2fal',
            'furan_2fal_code' => 'Furan 2fal Code',
            'furan_2acf' => 'Furan 2acf',
            'furan_2acf_code' => 'Furan 2acf Code',
            'furan_5m2f' => 'Furan 5m2f',
            'furan_5m2f_code' => 'Furan 5m2f Code',
            'furan_total' => 'Furan Total',
        ];
    }
}
