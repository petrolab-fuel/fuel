<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_customers".
 *
 * @property int $CustomerID
 * @property string $Name
 * @property string $Address
 * @property string $Branch
 * @property string $login
 * @property string $password
 * @property string $entrydate
 * @property string $updatedate
 * @property string $loginpusat
 * @property string $passwordpusat
 * @property string $owner
 * @property int $reports_to
 * @property int $userlevel
 */
class TblCustomersOTHERS extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_customers';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db3');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Address'], 'string'],
            [['entrydate', 'updatedate'], 'safe'],
            [['reports_to', 'userlevel'], 'integer'],
            [['Name', 'Branch'], 'string', 'max' => 50],
            [['login', 'password', 'loginpusat', 'passwordpusat', 'owner'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CustomerID' => 'Customer ID',
            'Name' => 'Name',
            'Address' => 'Address',
            'Branch' => 'Branch',
            'login' => 'Login',
            'password' => 'Password',
            'entrydate' => 'Entrydate',
            'updatedate' => 'Updatedate',
            'loginpusat' => 'Loginpusat',
            'passwordpusat' => 'Passwordpusat',
            'owner' => 'Owner',
            'reports_to' => 'Reports To',
            'userlevel' => 'Userlevel',
        ];
    }
}
