<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "parameter_has_data_analisa".
 *
 * @property int $parameter_id
 * @property int $data_analisa_id
 * @property string $value
 * @property string $eval
 *
 * @property DataAnalisa $dataAnalisa
 * @property Parameter $parameter
 */
class ParameterHasDataAnalisa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parameter_has_data_analisa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parameter_id', 'data_analisa_id'], 'required'],
            [['parameter_id', 'data_analisa_id'], 'integer'],
            [['value'], 'string', 'max' => 45],
            [['eval'], 'string', 'max' => 10],
            [['data_analisa_id'], 'exist', 'skipOnError' => true, 'targetClass' => DataAnalisa::className(), 'targetAttribute' => ['data_analisa_id' => 'id']],
            [['parameter_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['parameter_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parameter_id' => 'Parameter ID',
            'data_analisa_id' => 'Data Analisa ID',
            'value' => 'Value',
            'eval' => 'Eval',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataAnalisa()
    {
        return $this->hasOne(DataAnalisa::className(), ['id' => 'data_analisa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameter()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'parameter_id']);
    }
}
