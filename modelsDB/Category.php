<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $recom
 *
 * @property PenentuanCategory[] $penentuanCategories
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'recom'], 'string'],
            [['name'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'recom' => 'Recom',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenentuanCategories()
    {
        return $this->hasMany(PenentuanCategory::className(), ['category_id' => 'id']);
    }
}
