<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "parameter".
 *
 * @property int $id
 * @property string $nama
 * @property string $column_analisis_name
 * @property int $order
 *
 * @property Matrix[] $matrices
 * @property ParameterHasDataAnalisa[] $parameterHasDataAnalisas
 * @property DataAnalisa[] $dataAnalisas
 * @property ReportTypeHasParameter[] $reportTypeHasParameters
 * @property ReportType[] $reportTypes
 */
class ParameterBaru extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'parameter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order','parent'], 'integer'],
            [['nama','unit','method'], 'string', 'max' => 150],
            [['column_analisis_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'column_analisis_name' => 'Column Analisis Name',
            'order' => 'Order',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatrices()
    {
        return $this->hasMany(Matrix::className(), ['parameter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameterHasDataAnalisas()
    {
        return $this->hasMany(ParameterHasDataAnalisa::className(), ['parameter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataAnalisas()
    {
        return $this->hasMany(DataAnalisa::className(), ['id' => 'data_analisa_id'])->viaTable('parameter_has_data_analisa', ['parameter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportTypeHasParameters()
    {
        return $this->hasMany(ReportTypeHasParameter::className(), ['parameter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportTypes()
    {
        return $this->hasMany(ReportType::className(), ['id' => 'report_type_id'])->viaTable('report_type_has_parameter', ['parameter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function  getSpecParameter(){
        return $this->hasMany(SpesifikasiParameters::className(),['parameter_id'=>'id']);
    }

    public function getChilds(){
        return $this->hasMany(self::className(),['id'=>'parent']);
    }
}
