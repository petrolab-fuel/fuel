<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "data_analisa".
 *
 * @property int $id
 * @property string $received_date
 * @property string $start_analisa
 * @property string $end_analisa
 * @property string $report_date
 * @property string $sampling_date
 * @property string $lab_number
 * @property string $compartment
 * @property string $eval_code
 * @property string $eval_code_oil_quality
 * @property int $customer_id
 * @property int $unit_id
 * @property int $attention_id
 * @property int $report_type_id
 * @property string $appearance
 * @property string $appearance_code
 * @property string $density
 * @property string $density_code
 * @property string $total_acid_number
 * @property string $total_acid_number_code
 * @property string $flash_point
 * @property string $flash_point_code
 * @property string $kinematic_viscosity
 * @property string $kinematic_viscosity_code
 * @property string $water_content
 * @property string $water_content_code
 * @property string $ash_content
 * @property string $ash_content_code
 * @property string $pour_point
 * @property string $pour_point_code
 * @property string $cetane_index
 * @property string $cetane_index_code
 * @property string $conradson_carbon
 * @property string $conradson_carbon_code
 * @property string $distillation_recov
 * @property string $distillation_ibp
 * @property string $distillation_ibp_code
 * @property string $distillation_5
 * @property string $distillation_5_code
 * @property string $distillation_10
 * @property string $distillation_10_code
 * @property string $distillation_20
 * @property string $distillation_20_code
 * @property string $distillation_30
 * @property string $distillation_30_code
 * @property string $distillation_40
 * @property string $distillation_40_code
 * @property string $distillation_50
 * @property string $distillation_50_code
 * @property string $distillation_60
 * @property string $distillation_60_code
 * @property string $distillation_70
 * @property string $distillation_70_code
 * @property string $distillation_80
 * @property string $distillation_80_code
 * @property string $distillation_90
 * @property string $distillation_90_code
 * @property string $distillation_95
 * @property string $distillation_95_code
 * @property string $distillation_ep
 * @property string $distillation_ep_code
 * @property string $distillation_recovery
 * @property string $distillation_recovery_code
 * @property string $distillation_residue
 * @property string $destillation_residue_code
 * @property string $destillation_loss
 * @property string $destillation_loss_code
 * @property string $destillation_recovery_300
 * @property string $destillation_recovery_300_code
 * @property string $sulphur_content
 * @property string $sulphur_content_code
 * @property string $sediment
 * @property string $sediment_code
 * @property string $colour
 * @property string $colour_code
 * @property string $copper_corrosion
 * @property string $copper_corrosion_code
 * @property string $sludge_content
 * @property string $particles_4
 * @property string $particles_4_code
 * @property string $particles_6
 * @property string $particles_6_code
 * @property string $particles_14
 * @property string $particles_14_code
 * @property string $iso_4406
 * @property string $iso_4406_code
 * @property string $specific_gravity
 * @property string $specific_gravity_code
 * @property string $api_gravity
 * @property string $api_gravity_code
 * @property string $cloudn_point
 * @property string $cloudn_point_code
 * @property string $rancimat
 * @property string $rancimat_point
 * @property string $lubricity_of_diesel
 * @property string $lubricity_of_diesel_code
 * @property string $fame_content
 * @property string $fame_content_code
 * @property string $calorific_value
 * @property string $calorific_value_code
 * @property string $calorific_value1
 * @property string $calorific_value1_code
 * @property string $strong_acid
 * @property string $strong_acid_code
 * @property string $cerosine_content
 * @property string $cerosine_content_code
 * @property string $recommended1
 * @property string $recommended2
 * @property string $recommended3
 * @property string $equipment_condition
 * @property string $equipment_condition_code
 *
 * @property Attention $attention
 * @property Customer $customer
 * @property ReportType $reportType
 * @property Unit $unit
 * @property ParameterHasDataAnalisa[] $parameterHasDataAnalisas
 * @property Parameter[] $parameters
 */

class DataAnalisa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_analisa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['received_date', 'start_analisa', 'end_analisa', 'report_date', 'sampling_date'], 'safe'],
            [['customer_id'], 'required'],
            [['customer_id', 'unit_id', 'attention_id','harga'], 'integer'],
            [['recommended1', 'recommended2', 'recommended3','eng_builder','eng_type','eng_sn'], 'string'],
            [['lab_number'], 'string', 'max' => 200],
            [['compartment'], 'string', 'max' => 100],
            [['eng_location'], 'string', 'max' => 150],
            [['spb'], 'string', 'max' => 200],
            
            [['eval_code', 'eval_code_oil_quality', 'appearance', 'appearance_code', 'density', 'density_code', 'total_acid_number', 'total_acid_number_code', 'flash_point', 'flash_point_code', 'kinematic_viscosity', 'kinematic_viscosity_code', 'water_content', 'water_content_code', 'ash_content', 'ash_content_code', 'pour_point', 'pour_point_code', 'cetane_index', 'cetane_index_code', 'conradson_carbon', 'conradson_carbon_code', 'distillation_recov', 'distillation_ibp', 'distillation_ibp_code', 'distillation_5', 'distillation_5_code', 'distillation_10', 'distillation_10_code', 'distillation_20', 'distillation_20_code', 'distillation_30', 'distillation_30_code', 'distillation_40', 'distillation_40_code', 'distillation_50', 'distillation_50_code', 'distillation_60', 'distillation_60_code', 'distillation_70', 'distillation_70_code', 'distillation_80', 'distillation_80_code', 'distillation_90', 'distillation_90_code', 'distillation_95', 'distillation_95_code', 'distillation_ep', 'distillation_ep_code', 'distillation_recovery', 'distillation_recovery_code', 'distillation_residue', 'distillation_residue_code', 'distillation_loss', 'distillation_loss_code', 'distillation_recovery_300', 'distillation_recovery_300_code', 'sulphur_content', 'sulphur_content_code', 'sediment', 'sediment_code', 'colour', 'colour_code', 'copper_corrosion', 'copper_corrosion_code', 'sludge_content', 'particles_4', 'particles_4_code', 'particles_6', 'particles_6_code', 'particles_14', 'particles_14_code', 'iso_4406', 'iso_4406_code', 'specific_gravity', 'specific_gravity_code', 'api_gravity', 'api_gravity_code', 'cloudn_point', 'cloudn_point_code', 'rancimat', 'rancimat_code', 'lubricity_of_diesel', 'lubricity_of_diesel_code', 'fame_content', 'fame_content_code', 'calorific_value', 'calorific_value_code','calorific_value1','calorific_value1_code', 'strong_acid', 'strong_acid_code', 'cerosine_content', 'cerosine_content_code', 'equipment_condition', 'equipment_condition_code','silicon','silicon_code','alumunium','alumunium_code','po','monoglyceride','monoglyceride_code',
            'free_glyceride','free_glyceride_code','total_glyceride','total_glyceride_code','particulat','particulat_code'], 'string', 'max' => 45],
            [['lab_number'], 'unique'],
            [['attention_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attention::className(), 'targetAttribute' => ['attention_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'customer_id']],
            [['unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => Unit::className(), 'targetAttribute' => ['unit_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'received_date' => 'Received Date',
            'start_analisa' => 'Start Analisa',
            'end_analisa' => 'End Analisa',
            'report_date' => 'Report Date',
            'sampling_date' => 'Sampling Date',
            'lab_number' => 'Lab Number',
            'eval_code' => 'Eval Code',
            'eval_code_oil_quality' => 'Eval Code Oil Quality',
            'customer_id' => 'Customer',
            'unit_id' => 'Unit ID',
            'eng_builder'=>'Eng Builder',
            'eng_type'=>'Eng Type/Model',
            'eng_sn'=>'Serial Number',
            'eng_location'=>'Eng Location',
            'attention_id' => 'Attention',
            'appearance' => 'Appearance',
            'appearance_code' => 'Appearance Code',
            'density' => 'Density',
            'density_code' => 'Density Code',
            'total_acid_number' => 'Total Acid Number',
            'total_acid_number_code' => 'Total Acid Number Code',
            'flash_point' => 'Flash Point',
            'flash_point_code' => 'Flash Point Code',
            'kinematic_viscosity' => 'Kinematic Viscosity',
            'kinematic_viscosity_code' => 'Kinematic Viscosity Code',
            'water_content' => 'Water Content',
            'water_content_code' => 'Water Content Code',
            'ash_content' => 'Ash Content',
            'ash_content_code' => 'Ash Content Code',
            'pour_point' => 'Pour Point',
            'pour_point_code' => 'Pour Point Code',
            'cetane_index' => 'Cetane Index',
            'cetane_index_code' => 'Cetane Index Code',
            'conradson_carbon' => 'Conradson Carbon',
            'conradson_carbon_code' => 'Conradson Carbon Code',
            'distillation_recov' => 'Distillation Recov',
            'distillation_ibp' => 'Distillation Ibp',
            'distillation_ibp_code' => 'Distillation Ibp Code',
            'distillation_5' => 'Distillation 5',
            'distillation_5_code' => 'Distillation 5 Code',
            'distillation_10' => 'Distillation 10',
            'distillation_10_code' => 'Distillation 10 Code',
            'distillation_20' => 'Distillation 20',
            'distillation_20_code' => 'Distillation 20 Code',
            'distillation_30' => 'Distillation 30',
            'distillation_30_code' => 'Distillation 30 Code',
            'distillation_40' => 'Distillation 40',
            'distillation_40_code' => 'Distillation 40 Code',
            'distillation_50' => 'Distillation 50',
            'distillation_50_code' => 'Distillation 50 Code',
            'distillation_60' => 'Distillation 60',
            'distillation_60_code' => 'Distillation 60 Code',
            'distillation_70' => 'Distillation 70',
            'distillation_70_code' => 'Distillation 70 Code',
            'distillation_80' => 'Distillation 80',
            'distillation_80_code' => 'Distillation 80 Code',
            'distillation_90' => 'Distillation 90',
            'distillation_90_code' => 'Distillation 90 Code',
            'distillation_95' => 'Distillation 95',
            'distillation_95_code' => 'Distillation 95 Code',
            'distillation_ep' => 'Distillation Ep',
            'distillation_ep_code' => 'Distillation Ep Code',
            'distillation_recovery' => 'Distillation Recovery',
            'distillation_recovery_code' => 'Distillation Recovery Code',
            'distillation_residue' => 'Distillation Residue',
            'distillation_residue_code' => 'Destillation Residue Code',
            'distillation_loss' => 'Distillation Loss',
            'distillation_loss_code' => 'Distillation Loss Code',
            'distillation_recovery_300' => 'Distillation Recovery 300',
            'distillation_recovery_300_code' => 'Distillation Recovery 300 Code',
            'sulphur_content' => 'Sulphur Content',
            'sulphur_content_code' => 'Sulphur Content Code',
            'sediment' => 'Sediment',
            'sediment_code' => 'Sediment Code',
            'colour' => 'Colour',
            'colour_code' => 'Colour Code',
            'copper_corrosion' => 'Copper Corrosion',
            'copper_corrosion_code' => 'Copper Corrosion Code',
            'sludge_content' => 'Sampling Point',
            'particles_4' => 'Particles 4',
            'particles_4_code' => 'Particles 4 Code',
            'particles_6' => 'Particles 6',
            'particles_6_code' => 'Particles 6 Code',
            'particles_14' => 'Particles 14',
            'particles_14_code' => 'Particles 14 Code',
            'iso_4406' => 'Iso 4406',
            'iso_4406_code' => 'Iso 4406 Code',
            'specific_gravity' => 'Specific Gravity',
            'specific_gravity_code' => 'Specific Gravity Code',
            'api_gravity' => 'Api Gravity',
            'api_gravity_code' => 'Api Gravity Code',
            'cloudn_point' => 'Cloudn Point',
            'cloudn_point_code' => 'Cloudn Point Code',
            'rancimat' => 'Rancimat',
            'rancimat_code' => 'Rancimat Code',
            'lubricity_of_diesel' => 'Lubricity Of Diesel',
            'lubricity_of_diesel_code' => 'Lubricity Of Diesel Code',
            'fame_content' => 'Fame Content',
            'fame_content_code' => 'Fame Content Code',
            'calorific_value' => 'Calorific Value',
            'calorific_value_code' => 'Calorific Value Code',
            'calorific_value1' => 'Calorific Value',
            'calorific_value1_code' => 'Calorific Value Code',
            'strong_acid' => 'Strong Acid',
            'strong_acid_code' => 'Strong Acid Code',
            'cerosine_content' => 'Cerosine Content',
            'cerosine_content_code' => 'Cerosine Content Code',
            'silicon'=>'silicon',
            'silicon_code'=>'silicon_code',
            'alumunium'=>'alumunium',
            'alumunium_code'=>'alumunium_code',
            'recommended1' => 'Recommended1',
            'recommended2' => 'Recommended2',
            'recommended3' => 'Recommended3',
            'equipment_condition' => 'Equipment Condition',
            'equipment_condition_code' => 'Equipment Condition Code',
            'monoglyceride'=>'Monoglyceride',
            'total_glyceride'=>'Total Glycerine',
            'free_glyceride'=>'Free Glycerine',
            'particulat'=>'Particulat',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttention()
    {
        return $this->hasOne(Attention::className(), ['id' => 'attention_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['customer_id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportType()
    {
        return $this->hasOne(ReportType::className(), ['id' => 'report_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(Unit::className(), ['id' => 'unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameterHasDataAnalisas()
    {
        return $this->hasMany(ParameterHasDataAnalisa::className(), ['data_analisa_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameters()
    {
        return $this->hasMany(Parameter::className(), ['id' => 'parameter_id'])->viaTable('parameter_has_data_analisa', ['data_analisa_id' => 'id']);
    }
}
