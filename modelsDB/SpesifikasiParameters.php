<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "spesifikasi_parameters".
 *
 * @property int $id
 * @property int $spesifikasi_id
 * @property int $parameter_id
 */
class SpesifikasiParameters extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'spesifikasi_parameters';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['spesifikasi_id', 'parameter_id'], 'integer'],
            [['parameter_id'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'spesifikasi_id' => 'Spesifikasi ID',
            'parameter_id' => 'Parameter ID',
        ];
    }
}
