<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "typical".
 *
 * @property int $id
 * @property int $spesifikasi_id
 * @property int $new_oil_parameter_id
 * @property string $value
 * @property string $condition1
 * @property string $condition2
 * @property string $condition3
 * @property string $condition4
 * @property string $condition5
 *
 * @property Parameter $newOilParameter
 * @property Spesifikasi $spesifikasi
 */
class Typical extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'typical';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['spesifikasi_id', 'new_oil_parameter_id'], 'required'],
            [['spesifikasi_id', 'new_oil_parameter_id'], 'integer'],
            [['value'], 'string', 'max' => 200],
            [['condition1', 'condition2', 'condition3', 'condition4', 'condition5'], 'string', 'max' => 45],
            [['new_oil_parameter_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['new_oil_parameter_id' => 'id']],
            [['spesifikasi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Spesifikasi::className(), 'targetAttribute' => ['spesifikasi_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'spesifikasi_id' => Yii::t('app', 'Spesifikasi ID'),
            'new_oil_parameter_id' => Yii::t('app', 'New Oil Parameter ID'),
            'value' => Yii::t('app', 'Value'),
            'condition1' => Yii::t('app', 'Condition1'),
            'condition2' => Yii::t('app', 'Condition2'),
            'condition3' => Yii::t('app', 'Condition3'),
            'condition4' => Yii::t('app', 'Condition4'),
            'condition5' => Yii::t('app', 'Condition5'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewOilParameter()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'new_oil_parameter_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpesifikasi()
    {
        return $this->hasOne(Spesifikasi::className(), ['id' => 'spesifikasi_id']);
    }
}
