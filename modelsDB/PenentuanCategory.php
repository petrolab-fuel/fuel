<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "penentuan_category".
 *
 * @property int $id
 * @property string $voltage
 * @property string $type_of_equipment
 * @property int $category_id
 *
 * @property Category $category
 */
class PenentuanCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'penentuan_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id'], 'required'],
            [['category_id'], 'integer'],
            [['voltage'], 'string', 'max' => 45],
            [['type_of_equipment'], 'string', 'max' => 200],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'voltage' => 'Voltage',
            'type_of_equipment' => 'Type Of Equipment',
            'category_id' => 'Category ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}
