<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property int $customer_id
 * @property string $name
 * @property string $address
 * @property string $type jenis perusahaan contoh PT, CV
 * @property string $time_create
 * @property string $time_update
 * @property int $users_index
 *
 * @property Attention[] $attentions
 * @property Users $usersIndex
 * @property DataAnalisa[] $dataAnalisas
 * @property Unit[] $units
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address','phone'], 'string'],
            [['name'],'required'],
            [['time_create', 'time_update'], 'safe'],
            [['users_index'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 4],
            [['users_index'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_index' => 'index']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'customer_id' => 'Customer ID',
            'name' => 'Name',
            'address' => 'Address',
            'phone'=>'Phone',
            'type' => 'Type',
            'time_create' => 'Time Create',
            'time_update' => 'Time Update',
            'users_index' => 'Users Index',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttentions()
    {
        return $this->hasMany(Attention::className(), ['customer_id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersIndex()
    {
        return $this->hasOne(Users::className(), ['index' => 'users_index']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataAnalisas()
    {
        return $this->hasMany(DataAnalisa::className(), ['customer_id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnits()
    {
        return $this->hasMany(Unit::className(), ['customer_id' => 'customer_id']);
    }
}
