<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $index
 * @property string $username
 * @property string $password_hash
 * @property string $auth_key
 * @property string $access_token
 * @property int $status
 * @property string $password_reset_token
 * @property string $email
 * @property string $time_create
 * @property int $access_rule_id
 *
 * @property Customer[] $customers
 * @property UserLog[] $userLogs
 * @property AccessRule $accessRule
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'time_create'], 'required'],
            [['status', 'access_rule_id'], 'integer'],
            [['time_create'], 'safe'],
            [['username', 'email'], 'string', 'max' => 100],
            [['password_hash', 'auth_key', 'access_token', 'password_reset_token'], 'string', 'max' => 200],
            [['username'], 'unique'],
            [['access_rule_id'], 'exist', 'skipOnError' => true, 'targetClass' => AccessRule::className(), 'targetAttribute' => ['access_rule_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'index' => 'Index',
            'username' => 'Username',
            'password_hash' => 'Password Hash',
            'auth_key' => 'Auth Key',
            'access_token' => 'Access Token',
            'status' => 'Status',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'time_create' => 'Time Create',
            'access_rule_id' => 'Access Rule ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customer::className(), ['users_index' => 'index']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLogs()
    {
        return $this->hasMany(UserLog::className(), ['users_index' => 'index']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccessRule()
    {
        return $this->hasOne(AccessRule::className(), ['id' => 'access_rule_id']);
    }
}
