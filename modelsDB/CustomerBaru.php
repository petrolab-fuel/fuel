<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property int $customer_id
 * @property int $id
 * @property int $type jenis perusahaan contoh PT, CV
 * @property string $name
 * @property string $address
 * @property string $phone
 * @property string $time_create
 * @property string $time_update
 *
 * @property Unit[] $units
 */
class CustomerBaru extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'type'], 'integer'],
            [['address', 'phone'], 'string'],
            [['time_create', 'time_update'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'customer_id' => 'Customer ID',
            'id' => 'ID',
            'type' => 'Type',
            'name' => 'Name',
            'address' => 'Address',
            'phone' => 'Phone',
            'time_create' => 'Time Create',
            'time_update' => 'Time Update',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnits()
    {
        return $this->hasMany(Unit::className(), ['customer_id' => 'customer_id']);
    }
}
