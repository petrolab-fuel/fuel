<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "user_log".
 *
 * @property int $id
 * @property string $date_login
 * @property string $date_logout
 * @property string $ip_login
 * @property string $log
 * @property int $users_index
 *
 * @property Users $usersIndex
 */
class UserLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_login', 'users_index'], 'required'],
            [['date_login', 'date_logout'], 'safe'],
            [['log'], 'string'],
            [['users_index'], 'integer'],
            [['ip_login'], 'string', 'max' => 45],
            [['users_index'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_index' => 'index']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_login' => 'Date Login',
            'date_logout' => 'Date Logout',
            'ip_login' => 'Ip Login',
            'log' => 'Log',
            'users_index' => 'Users Index',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersIndex()
    {
        return $this->hasOne(Users::className(), ['index' => 'users_index']);
    }
}
